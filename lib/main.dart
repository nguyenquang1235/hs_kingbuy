import 'package:King_Buy/src/utils/utils.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';

import 'src/configs/configs.dart';
import 'src/presentation/presentation.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await Firebase.initializeApp();
  FirebaseCloudMessaging.initFirebaseMessaging();
  LocalNotification.setup();
  runApp(RestartWidget(child: App()));
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ThemeSwitcherWidget(
        initialThemeData: normalTheme(context), child: MyApp());
  }
}

class MyApp extends StatefulWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  void didChangeDependencies() async {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    await preloadAssets();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    AppDeviceInfo.init();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("ChangeAppLifecycleState: $state");
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: AppValues.APP_NAME,
      theme: ThemeSwitcher.of(context).themeData,
      supportedLocales: AppLanguage.getSupportLanguage().map((e) => e.locale),
      navigatorObservers: <NavigatorObserver>[MyApp.observer],
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        DefaultCupertinoLocalizations.delegate
      ],
      localeResolutionCallback: (locale, supportedLocales) {
        if (locale == null) {
          debugPrint("*language locale is null!!!");
          return supportedLocales.first;
        }
        for (var supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode &&
              supportedLocale.countryCode == locale.countryCode) {
            return supportedLocale;
          }
        }
        return supportedLocales.first;
      },
      locale: Locale('vi', 'VN'),
      home: SplashScreen(),
      onGenerateRoute: Routers.generateRoute,
      builder: EasyLoading.init(),
    );
  }

  preloadAssets() async {
    await precacheImage(AssetImage(AppImages.bgSplashScreen), context);
    await precacheImage(AssetImage(AppImages.icLogo), context);
    await precacheImage(AssetImage(AppImages.bgDrawerItem), context);
    await precacheImage(AssetImage(AppImages.bgDrawerItemSelected), context);
    // precacheImage(AssetImage(AppImages.imgBgVSYellow), context);
    // precacheImage(AssetImage(AppImages.imgGameMemory), context);
    // precacheImage(AssetImage(AppImages.imgGamePicture), context);
    // precacheImage(AssetImage(AppImages.imgGameGeneral), context);
    // precacheImage(AssetImage(AppImages.imgLeaderBoard), context);
    // precacheImage(AssetImage(AppImages.imgChallengeRandom), context);
    // precacheImage(AssetImage(AppImages.imgChallengeFriend), context);
    // precacheImage(AssetImage(AppImages.imgBgLogin), context);
  }
}
