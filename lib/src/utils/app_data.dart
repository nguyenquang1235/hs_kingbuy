import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/resource/repo/repo.dart';

class AppData {
  ///
  /// Có nên lưu lại thông tin user ở đây hay là lưu trên appShared ?
  /// Thay đổi logic lưu lại thông tin user.
  ///

  static AppData _appData;

  List<CategoryModel> _listCategories;
  List<PromotionModel> _listPromotions;
  List<PromotionModel> _listMyPromotions;
  List<BrandModel> _listBrands;

  Future<List<CategoryModel>> get listCategories async {
    return await _getData<CategoryModel>(
        list: _listCategories, request: getCategories());
  }

  Future<List<PromotionModel>> get listPromotions async {
    return await _getData<PromotionModel>(
        list: _listPromotions, request: getPromotions());
  }

  Future<List<PromotionModel>> get listMyPromotions async {
    return await _getData<PromotionModel>(
        list: _listMyPromotions, request: getMyPromotions());
  }

  Future<List<BrandModel>> get listBrands async {
    return await _getData<BrandModel>(list: _listBrands, request: getBrands());
  }

  PromotionRepository _promotionRepository = PromotionRepository();
  CategoryRepository _categoryRepository = CategoryRepository();
  SearchRepository _searchRepository = SearchRepository();

  static Future<AppData> instance() async {
    if (_appData == null) {
      _appData = AppData();
      await _appData.init();
    }
    return _appData;
  }

  init() async {
    _listPromotions = await getPromotions();
    _listMyPromotions = await getMyPromotions();
    _listCategories = await getCategories();
    _listBrands = await getBrands();
  }

  Future<List<PromotionModel>> getPromotions() async {
    try {
      NetworkState<List<PromotionModel>> networkState =
          await _promotionRepository.getPromotions();
      return networkState.data;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<PromotionModel>> getMyPromotions() async {
    try {
      NetworkState<List<PromotionModel>> networkState =
          await _promotionRepository.getMyPromotion();
      return networkState.data;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<CategoryModel>> getCategories() async {
    try {
      NetworkState<List<CategoryModel>> networkState =
          await _categoryRepository.getAllCategory(limit: 20);
      return networkState.data;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<BrandModel>> getBrands() async {
    try {
      NetworkState<List<BrandModel>> networkState =
          await _searchRepository.getBrands();
      return networkState.data;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<T>> _getData<T>({Future<List<T>> request, List<T> list}) async {
    if (list.length > 0) {
      return list;
    } else {
      list = await request;
      return list;
    }
  }
}
