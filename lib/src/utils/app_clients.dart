import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import '../configs/configs.dart';
import 'utils.dart';
import 'dart:developer';

class AppClients extends DioForNative {

  AppClients({String baseUrl = AppEndpoint.BASE_URL, BaseOptions options})
      : super(options) {
    this.interceptors.add(InterceptorsWrapper(
          onRequest: _requestInterceptor,
          onResponse: _responseInterceptor,
          onError: _errorInterceptor,
        ));
    this.options.baseUrl = baseUrl;
  }


  _requestInterceptor(RequestOptions options) async {

    String accessToken = await AppShared.getAccessToken();
    // print(accessToken);
    // String accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMsImlzcyI6Imh0dHBzOi8va2luZ2J1eS52bi9hcGkvbG9naW5BcHAiLCJpYXQiOjE2MTY3MjEyNzcsImV4cCI6MTYxOTMxMzI3NywibmJmIjoxNjE2NzIxMjc3LCJqdGkiOiIzMHRnT0tJNzN2RmN1VmlPIn0.GArbCsiKslngJx3UyEJjVOue46gPNkkzMrWZJgHA75M";
    if (options.headers == null) {
      options.headers = {};
    }
    if (options.headers.containsKey(AppEndpoint.keyAuthorization)) {
      options.headers.remove(AppEndpoint.keyAuthorization);
    }
    if (accessToken != null && accessToken.length > 0) {
      options.headers[AppEndpoint.keyAuthorization] = "Bearer ${accessToken}";
    }
    options.connectTimeout = AppEndpoint.connectionTimeout;
    options.receiveTimeout = AppEndpoint.receiveTimeout;
    return options;
  }

  _responseInterceptor(Response response) {
    // log("Response ${response.request.uri}: ${response.statusCode}\nData: ${response.data}");
  }

  _errorInterceptor(DioError dioError) {}
}
