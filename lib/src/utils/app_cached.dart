import 'package:King_Buy/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class AppCached {
  AppCached._();
  static preLoadImage(String url, BuildContext context) async {
    await precacheImage(
        CachedNetworkImageProvider(AppUtils.pathMediaToUrl(url)), context);
  }
}
