import 'dart:async';
import 'dart:convert';

import 'package:King_Buy/src/resource/resource.dart';
import 'package:rx_shared_preferences/rx_shared_preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppShared {
  AppShared._();

  static final _prefs = RxSharedPreferences(SharedPreferences.getInstance());

  static const String _keyAccessToken = "_keyAccessToken";
  static const String _keyUser = "_keyUser";
  static const String _keyIsLogged = "_keyIsLogged";
  static const String _keyListSearch = "_keyListSearch";
  static const String _keyListCategories = "_keyListCategories";
  static const String _keyListBrands = "_keyListBrands";
  static const String _keyDeliveryAddress = "_keyDeliveryAddress";

  static Future<bool> setAccessToken(String token) =>
      _prefs.setString(_keyAccessToken, token);

  static Future<String> getAccessToken() => _prefs.getString(_keyAccessToken);

  static Future<bool> setUser(UserModel data) async {
    String json = data != null ? jsonEncode(data) : "";
    return _prefs.setString(_keyUser, json);
  }

  static Future<UserModel> getUser() async {
    String string = await _prefs.getString(_keyUser);
    if (string != null && string.length != 0)
      return UserModel.fromJson(jsonDecode(string));
    else
      return null;
  }

  static setDeliveryAddress(DeliveryAddressModel model) async {
    String json = model != null ? jsonEncode(model) : "";
    return _prefs.setString(_keyDeliveryAddress, json);
  }

  static Future<DeliveryAddressModel> getDeliveryAddress() async {
    String string = await _prefs.getString(_keyDeliveryAddress);
    if (string != null && string.length != 0)
      return DeliveryAddressModel.fromJson(jsonDecode(string));
    else
      return null;
  }

  static Future<bool> setLogged(bool value) =>
      _prefs.setBool(_keyIsLogged, value);

  static Future<bool> getLogged() => _prefs.getBool(_keyIsLogged);

  static Future<bool> setListSearch(List<String> value) async {
    return _prefs.setStringList(_keyListSearch, value);
  }

  static Future<List<String>> getListSearch() =>
      _prefs.getStringList(_keyListSearch);

  static Future<bool> setListCategories(List<CategoryModel> value) async {
    String val = value != null ? json.encode(value) : "";
    return _prefs.setString(
      _keyListCategories,
      val,
    );
  }

  static Future<List<CategoryModel>> getListCategories() async {
    String val = await _prefs.getString(_keyListCategories);
    List<CategoryModel> value = [];
    if (val != null) {
      List<dynamic> data = json.decode(val);
      for (var item in data) {
        value.add(CategoryModel.fromJson(item));
      }
    }
    return value;
  }

  static Future<bool> setListBrands(List<BrandModel> value) async {
    String val = value != null ? json.encode(value) : "";
    return _prefs.setString(
      _keyListBrands,
      val,
    );
  }

  static Future<List<BrandModel>> getListBrands() async {
    String val = await _prefs.getString(_keyListBrands);
    List<BrandModel> value = [];
    if (val != null) {
      List<dynamic> data = json.decode(val);
      for (var item in data) {
        value.add(BrandModel.fromJson(item));
      }
    }
    return value;
  }

  /// DEMO: Use stream listen change
  // static Future<bool> setWeather(WeatherModel weather) async {
  //   String json = weather != null ? jsonEncode(weather) : "";
  //   return prefs.setString(keyWeather, json);
  // }
  //
  // static Future<WeatherModel> getWeather() async {
  //   String string = await prefs.getString(keyWeather);
  //   if (string != null && string.length != 0)
  //     return WeatherModel.fromJson(jsonDecode(string));
  //   else
  //     return null;
  // }
  //
  // static Stream<WeatherModel> watchWeather() {
  //   return prefs.getStringStream(keyWeather).transform(
  //       StreamTransformer.fromHandlers(
  //           handleData: (data, sink) => (data == null || data.length == 0)
  //               ? sink.add(null)
  //               : sink.add(WeatherModel.fromJson(jsonDecode(data)))));
  // }
}
