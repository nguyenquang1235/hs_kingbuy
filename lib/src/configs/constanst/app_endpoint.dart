class AppEndpoint {
  AppEndpoint._();

  static const String BASE_URL = "https://kingbuy.vn";
  static const String Api = "/api";
  static const String tokenTest =
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMsImlzcyI6Imh0dHBzOi8va2luZ2J1eS52bi9hcGkvbG9naW5BcHAiLCJpYXQiOjE2MTY3MjEyNzcsImV4cCI6MTYxOTMxMzI3NywibmJmIjoxNjE2NzIxMjc3LCJqdGkiOiIzMHRnT0tJNzN2RmN1VmlPIn0.GArbCsiKslngJx3UyEJjVOue46gPNkkzMrWZJgHA75M";

  static const int connectionTimeout = 10000;
  static const int receiveTimeout = 10000;
  static const String keyAuthorization = "Authorization";

  static const int SUCCESS = 200;
  static const int ERROR_TOKEN = 401;
  static const int ERROR_VALIDATE = 422;
  static const int ERROR_SERVER = 500;
  static const int ERROR_DISCONNECT = -1;

  //Profile
  static const String REGISTER = '$Api/registerApp';
  static const String LOGIN_IDENTITY = '$Api/loginApp';
  static const String LOGIN_FACEBOOK = '$Api/loginAppSocial';
  static const String LOGIN_GOOGLE = '$Api/loginAppGoogle';
  static const String LOGIN_APPLE = '$Api/loginWithApple';
  static const String GET_USER_PROFILE = '$Api/getProfileOfUser';
  static const String UPDATE_USER_PROFILE = '$Api/updateInfoOfUser';
  static const String ACTIVE_ACCOUNT = '$Api/activeAccount';
  static const String LOG_OUT = '$Api/logoutApp';

  //Forgot pass
  static const String SEND_OTP_FORGOT_PASS = "$Api/sendOtpWhenUserLogin";
  static const String CHECK_OTP_FORGOT_PASS = "$Api/checkOtpSmsExpire";
  static const String RESET_PASS = "$Api/resetPasswordForUser";
  static const String UPDATE_PASS = "$Api/updatePassword";

  //Contact
  static const String APP_CONFIGS = "$Api/apiGetAppConfig";

  //Promotion
  static const String GET_ALL_PROMOTION = "$Api/getAllPromotion";
  static const String GET_ALL_MY_PROMOTION = "$Api/getAllMyPromotion";

  //Notice
  static const String COUNT_NOTIFICATIONS = "$Api/countNotification";
  static const String GET_NOTIFICATIONS = "$Api/listNotification";

  //Commitments
  static const String GET_COMMITMENTS = "$Api/getCommitments";

  //Invoice
  static const String CREATE_INVOICE = "$Api/createInvoice";
  static const String GET_INVOICE_HISTORY = "$Api/listBuyHistoryOfUser";

  //Delivery
  static const String GET_DELIVERY_ADDRESS = "$Api/getDeliveryAddress";
  static const String CREATE_DELIVERY_ADDRESS = "$Api/createDeliveryAddress";
  static const String UPDATE_DELIVERY_ADDRESS = "$Api/updateDeliveryAddress";
  static const String DELETE_DELIVERY_ADDRESS = "$Api/deleteDeliveryAddress";

  //Province
  static const String GET_All_PROVINCE = "$Api/getAllProvince";
  static const String GET_DISTRICT_BY_PROVINCE_CODE =
      "$Api/getDistrictByProvinceCode";
  static const String GET_WARD_BY_DISTRICT_CODE = "$Api/getWardByDistrictCode";

  //Contact
  static const String GET_CONTACT_US = "$Api/contactUs";
  static const String CHECK_FEEDBACK_OF_USER = "$Api/checkFeedbackOfUser";
  static const String SEND_CONTACT_FORM = "$Api/userSendContactForm";
  static const String GET_FEED_BACK = "$Api/getAllFeedback";
  static const String USER_REPLY = "$Api/userReplyFeedback";

  //Coupon
  static const String GET_MY_COUPONS = "$Api/getMyCoupons";
  static const String GET_COUPON_DETAILS = "$Api/getDetailCoupon";

  //Warranty
  static const String GET_WARRANTY = "$Api/listDataWarranty";
  // static const String GET_COUPON_DETAILS = "$Api/getDetailCoupon";

  //Product
  static const String GET_VIEWED_PRODUCTS = "$Api/getViewedProducts";
  static const String GET_PRODUCT_BY_ID = "$Api/getProductsByIds";
  static const String GET_NEW_PRODUCTS = "$Api/getAllProductNew";
  static const String GET_SELLING_PRODUCTS = "$Api/getAllProductSelling";
  static const String GET_PRODUCTS_BY_CATEGORY = "$Api/getProductsByCategory";
  static const String GET_SINGLE_PRODUCT = "$Api/getSingleProduct";
  static const String GET_RELATED_PRODUCT = "$Api/relatedProduct";

  //Category
  static const String GET_ALL_CATEGORIES = "$Api/getAllCategories";

  //Popups
  static const String GET_POPUPS = "$Api/getPopup";

  //Search
  static const String SEARCH_PRODUCTS = "$Api/searchProduct";
  static const String GET_BRANDS = "$Api/getBrands";

  //Address
  static const String GET_ALL_ADDRESS = "$Api/getAllAddress";
  static const String GET_DETAILS_ADDRESS = "$Api/getDetailAddress";

  //Rating
  static const String GET_RATING_INFO = "$Api/ratingInfoByProduct";
  static const String GET_REVIEW = "$Api/getReviewByProduct";
  static const String USER_RATING = "$Api/userRatingProduct";
}
