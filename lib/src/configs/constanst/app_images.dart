class AppImages {
  AppImages._();

  // icons
  static final String icUser = 'assets/icons/user.png';
  static final String icLogo = 'assets/icons/logo.png';
  static final String icFacebook = 'assets/icons/facebook.png';
  static final String icZalo = 'assets/icons/zalo.png';
  static final String icAboutUs = 'assets/icons/about_us.png';
  static final String icAlarm = 'assets/icons/alarm.png';
  static final String icCart = 'assets/icons/cart.png';
  static final String icCoupons = 'assets/icons/coupons.png';
  static final String icDelivery = 'assets/icons/devivery.png';
  static final String icEye = 'assets/icons/eye.png';
  static final String icGift = 'assets/icons/gift.png';
  static final String icGiftColor = 'assets/icons/gift_color.png';
  static final String icHome = 'assets/icons/home.png';
  static final String icLock = 'assets/icons/lock.png';
  static final String icMenuGrey = 'assets/icons/menu_grey.png';
  static final String icMenuRed = 'assets/icons/menu_red.png';
  static final String icPage = 'assets/icons/page.png';
  static final String icPinGrey = 'assets/icons/pin_grey.png';
  static final String icPinRed = 'assets/icons/pin_red.png';
  static final String icPhoneBlue = 'assets/icons/phone_blue.png';
  static final String icPhoneGrey = 'assets/icons/phone_grey.png';
  static final String icRedCircle = 'assets/icons/red_cicle.png';
  static final String icRedPoint = 'assets/icons/red_point.png';
  static final String icReload = 'assets/icons/reload.png';
  static final String icStars = 'assets/icons/stars.png';
  static final String icSuccess = 'assets/icons/success.png';
  static final String icSurface = 'assets/icons/surface.png';
  static final String icTag = 'assets/icons/tag.png';
  static final String icUserCircleRed = 'assets/icons/user_cicle_red.png';
  static final String icUserRed = 'assets/icons/user_red.png';
  static final String icGoogle  = 'assets/icons/google.png';
  static final String icMessenger = 'assets/icons/messeger.png';
  static final String icPoint = 'assets/icons/ic_point.png';
  static final String icProfile = 'assets/icons/ic_profile.png';
  static final String icRefreshActive = 'assets/icons/ic_refresh_active.png';
  static final String icRefreshDisable = 'assets/icons/ic_refresh_disable.png';
  static final String icAllCategories = 'assets/icons/all_categories.jpg';
  static final String icCoin = 'assets/icons/ic_coin.png';

  //background
  static final String bgSplashScreen = 'assets/backgrounds/flashscreen.png';
  static final String bgDrawerItem = 'assets/backgrounds/bg_drawer_item.png';
  static final String bgDrawerItemSelected = 'assets/backgrounds/bg_drawer_item_selected.png';

  //image
  static final String noImage = 'assets/images/no_image.jpg';

}
