class AppValues {
  AppValues._();

  static const String APP_NAME = "FLUTTER APPLICATION";
  static const double INPUT_FORM_HEIGHT = 55;
  static const double HEIGHT_APP_BAR = 70;
}
