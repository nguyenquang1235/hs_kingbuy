import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../resource.dart';

final String tableCart = 'tb_cart';
final String tableCartItem = 'tb_cart_item';

class CartDataBase {
  static CartDataBase _cartDataBase;
  Database _database;

  // String cartColumns =
  //     'id, total_quantity, delivery_address_id, payment_type, note, is_export_invoice, coupon_id, total_unread, delivery_status, tax_code, company_name, company_address, company_email';
  //
  // String cartItemColumns = 'id, qty, product_id, invoice_id, has_read';

  static final String _dbName = "king_buy.db";

  static CartDataBase instance() {
    if (_cartDataBase == null) {
      _cartDataBase = CartDataBase();
    }
    return _cartDataBase;
  }

  //
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    print("Khoi tao data base");
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _dbName);
//    File tempLocalFile = File(path);
//    if (tempLocalFile.existsSync()) {
//      await tempLocalFile.delete(recursive: true,);
//    }
    // Open the database. Can also add an onUpdate callback parameter.
    return open(path);
  }

  Future open(String path) async {
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          create table $tableCart(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            total INTEGER,
            discount INTEGER,
            payment_type INTEGER,
            note TEXT,
            is_export_invoice INTEGER,
            total_unread INTEGER,
            delivery_status INTEGER,
            tax_code TEXT,
            company_name TEXT,
            company_address TEXT,
            company_email TEXT
            )
    ''');

    await db.execute('''
          create table $tableCartItem (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            product_id INTEGER NOT NULL,
            qty INTEGER NOT NULL,
            product_name TEXT,
            image_source TEXT,
            has_read INTEGER,
            color_id INTEGER
            )
          ''');
  }

  Future<CartModel> getCart() async {
    Database db = await database;
    List<Map> maps = await db.query(
      tableCart,
      columns: [
        "id",
        "total",
        "discount",
        "payment_type",
        "note",
        "is_export_invoice",
        "total_unread",
        "delivery_status",
        "discount",
        "tax_code",
        "company_name",
        "company_address",
        "company_email",
      ].toList(),
    );
    if (maps.length > 0) {
      return CartModel.fromJson(maps.first);
    }
    return null;
  }

  Future<List<CartItem>> getCartItems() async {
    Database db = await database;
    var maps = await db.query(
      tableCartItem,
      columns: [
        "id",
        "product_id",
        "qty",
      ].toList(),
    );
    if (maps.length > 0) {
      return CartItem.listFromJson(maps);
    }
    return [];
  }

  Future<CartItem> getCartItem(String field, dynamic val) async {
    Database db = await database;
    var maps = await db.query(
      tableCartItem,
      columns: [
        "id",
        "product_id",
        "qty",
      ].toList(),
      where: '$field = ?',
      whereArgs: [val],
    );
    if (maps.length > 0) {
      return CartItem.fromJson(maps.first);
    }
    return null;
  }

  Future<List<CartItem>> getItemHasRead() async {
    Database db = await database;
    var maps = await db.query(tableCartItem,
        columns: [
          "id",
          "product_id",
          "qty",
          "has_read",
        ].toList(),
        where: "has_read = 1");
    if (maps.length > 0) {
      return CartItem.listFromJson(maps);
    }
    return null;
  }

  Future insertCart(CartModel model) async {
    Database db = await database;
    return await db.insert(tableCart, model.toJson());
  }

  Future insertCartItem(CartItem item) async {
    Database db = await database;
    int id = await db.insert(tableCartItem, item.toJson());
    return id;
  }

  Future<int> updateCart(CartModel model) async {
    Database db = await database;
    return await db.update(tableCart, model.toJson(), where: 'id = 1');
  }

  Future<int> updateCartItem(CartItem item) async {
    Database db = await database;
    return await db.update(tableCartItem, item.toJson(),
        where: 'id = ?', whereArgs: [item.id]);
  }

  Future<int> deleteCartItem(int id) async {
    Database db = await database;
    return await db.delete(tableCartItem, where: 'id = ?', whereArgs: [id]);
  }

  Future<int> deleteAllCartItem() async {
    Database db = await database;
    return await db.delete(tableCartItem);
  }

  Future deleteDataBase() async {
    Database db = await database;
    // await db.delete(tableCartItem);
    // await db.delete(tableCart);
    await db.execute("DROP TABLE IF EXISTS $tableCart");
    await db.execute("DROP TABLE IF EXISTS $tableCartItem");
  }
}
