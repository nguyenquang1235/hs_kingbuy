/// status : 1
/// data : {"non_use":{"count":0,"rows":[]},"used":{"count":2,"rows":[{"id":3,"name":"TEST 2","description":"mô tả","apply_target":1,"type":1,"invoice_status":1,"invoice_total":1000000,"percent_off":15,"max_sale":300000,"sale_price":null,"status":1,"expires_at":"2022-01-29","image_source":"/photos/1/coupon/56d4c61d-cbdd-47d8-bb22-0717c22600bc.png","created_at":"2021-01-29 09:11:12","updated_at":"2021-01-29 09:11:12"},{"id":2,"name":"TEST","description":"1111","apply_target":1,"type":1,"invoice_status":1,"invoice_total":1000000,"percent_off":20,"max_sale":1000000,"sale_price":null,"status":1,"expires_at":"2021-03-20","image_source":"/photos/1/29dd515c-715b-4ffe-bd4f-d179646b55c3.png","created_at":"2020-08-28 10:25:55","updated_at":"2021-01-29 08:55:37"}]},"expired":{"count":1,"rows":[{"id":1,"name":"Giảm 15% giá trị với đơn đặt hàng tại app Kingbuy tối đa 200k","description":"Áp dụng cho đơn hàng từ 5.000.000đ\r\nLoại coupon: Giảm 15% tối đa 200.000\r\nMô tả:\r\n- Kingbuy TẶNG RIÊNG Khách hàng sử dụng app: Voucher giảm giá 20% cho hóa đơn mua hàng từ 1.000.000đ\r\n- 01 hóa đơn áp dụng tối đa 01 Voucher\r\n- Không áp dụng với các chương trình khuyến mãi khác\r\n Hạn sử dụng đến 06/03/2019","apply_target":1,"type":1,"invoice_status":1,"invoice_total":5000000,"percent_off":15,"max_sale":200000,"sale_price":null,"status":1,"expires_at":"2021-03-06","image_source":"/photos/1/coupon/56d4c61d-cbdd-47d8-bb22-0717c22600bc.png","created_at":"2020-03-18 09:22:07","updated_at":"2021-01-29 08:54:58"}]}}
class CouponApplication {
  List<CouponModel> nonUse;
  List<CouponModel> used;
  List<CouponModel> expired;

  CouponApplication({this.nonUse, this.used, this.expired});

  CouponApplication.fromJson(dynamic json) {
    nonUse = json["non_use"] != null
        ? CouponModel.listFromJson(json["non_use"]["rows"])
        : null;
    used = json["used"] != null
        ? CouponModel.listFromJson(json["used"]["rows"])
        : null;
    expired = json["expired"] != null
        ? CouponModel.listFromJson(json["expired"]["rows"])
        : null;
  }
}

/// count : 1
/// rows : [{"id":1,"name":"Giảm 15% giá trị với đơn đặt hàng tại app Kingbuy tối đa 200k","description":"Áp dụng cho đơn hàng từ 5.000.000đ\r\nLoại coupon: Giảm 15% tối đa 200.000\r\nMô tả:\r\n- Kingbuy TẶNG RIÊNG Khách hàng sử dụng app: Voucher giảm giá 20% cho hóa đơn mua hàng từ 1.000.000đ\r\n- 01 hóa đơn áp dụng tối đa 01 Voucher\r\n- Không áp dụng với các chương trình khuyến mãi khác\r\n Hạn sử dụng đến 06/03/2019","apply_target":1,"type":1,"invoice_status":1,"invoice_total":5000000,"percent_off":15,"max_sale":200000,"sale_price":null,"status":1,"expires_at":"2021-03-06","image_source":"/photos/1/coupon/56d4c61d-cbdd-47d8-bb22-0717c22600bc.png","created_at":"2020-03-18 09:22:07","updated_at":"2021-01-29 08:54:58"}]

/// id : 3
/// name : "TEST 2"
/// description : "mô tả"
/// apply_target : 1
/// type : 1
/// invoice_status : 1
/// invoice_total : 1000000
/// percent_off : 15
/// max_sale : 300000
/// sale_price : null
/// status : 1
/// expires_at : "2022-01-29"
/// image_source : "/photos/1/coupon/56d4c61d-cbdd-47d8-bb22-0717c22600bc.png"
/// created_at : "2021-01-29 09:11:12"
/// updated_at : "2021-01-29 09:11:12"

class CouponModel {
  int id;
  String name;
  String description;
  int applyTarget;
  int type;
  int invoiceStatus;
  int invoiceTotal;
  int percentOff;
  int maxSale;
  int salePrice;
  int status;
  String expiresAt;
  String imageSource;
  String createdAt;
  String updatedAt;

  CouponModel(
      {this.id,
      this.name,
      this.description,
      this.applyTarget,
      this.type,
      this.invoiceStatus,
      this.invoiceTotal,
      this.percentOff,
      this.maxSale,
      this.salePrice,
      this.status,
      this.expiresAt,
      this.imageSource,
      this.createdAt,
      this.updatedAt});

  CouponModel.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    description = json["description"];
    applyTarget = json["apply_target"];
    type = json["type"];
    invoiceStatus = json["invoice_status"];
    invoiceTotal = json["invoice_total"];
    percentOff = json["percent_off"];
    maxSale = json["max_sale"];
    salePrice = json["sale_price"];
    status = json["status"];
    expiresAt = json["expires_at"];
    imageSource = json["image_source"];
    createdAt = json["created_at"];
    updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["description"] = description;
    map["apply_target"] = applyTarget;
    map["type"] = type;
    map["invoice_status"] = invoiceStatus;
    map["invoice_total"] = invoiceTotal;
    map["percent_off"] = percentOff;
    map["max_sale"] = maxSale;
    map["sale_price"] = salePrice;
    map["status"] = status;
    map["expires_at"] = expiresAt;
    map["image_source"] = imageSource;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    return map;
  }

  static List<CouponModel> listFromJson(dynamic json) {
    List<CouponModel> coupons = [];
    if (json != null) {
      json.forEach((v) {
        coupons.add(CouponModel.fromJson(v));
      });
    }
    return coupons;
  }
}
