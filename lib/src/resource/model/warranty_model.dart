

/// status_of_receipt : "máy bình"
/// date_purchase : "2021-01-22T16:48:31"
/// date_received : "2021-01-27T16:48:31"
/// comment : "sửa gấp"
/// wr_type_name : "Bảo hành"
/// total_amount_fc2 : "Miễn phí"
/// product_code : "97448333"
/// product_name : "Phụ kiện máy rửa xe Karcher - Đầu phun ngắn VP-160S"
/// status_id : "20A387047"
/// status : "1"
/// status_name : "Chưa xử lý"

class WarrantyModel {
  String statusOfReceipt;
  String datePurchase;
  String dateReceived;
  String comment;
  String wrTypeName;
  String totalAmountFc2;
  String productCode;
  String productName;
  String statusId;
  int status;
  String statusName;

  WarrantyModel(
      {this.statusOfReceipt,
      this.datePurchase,
      this.dateReceived,
      this.comment,
      this.wrTypeName,
      this.totalAmountFc2,
      this.productCode,
      this.productName,
      this.statusId,
      this.status,
      this.statusName});

  WarrantyModel.fromJson(dynamic json) {
    statusOfReceipt = json["status_of_receipt"];
    datePurchase = json["date_purchase"];
    dateReceived = json["date_received"];
    comment = json["comment"];
    wrTypeName = json["wr_type_name"];
    totalAmountFc2 = json["total_amount_fc2"];
    productCode = json["product_code"];
    productName = json["product_name"];
    statusId = json["status_id"];
    status = int.parse(json["status"]??"0");
    statusName = json["status_name"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status_of_receipt"] = statusOfReceipt;
    map["date_purchase"] = datePurchase;
    map["date_received"] = dateReceived;
    map["comment"] = comment;
    map["wr_type_name"] = wrTypeName;
    map["total_amount_fc2"] = totalAmountFc2;
    map["product_code"] = productCode;
    map["product_name"] = productName;
    map["status_id"] = statusId;
    map["status"] = status;
    map["status_name"] = statusName;
    return map;
  }

  static List<WarrantyModel> listFromJson(dynamic json) {
    List<WarrantyModel> listWarrantyModel = [];
    if (json != null) {
      json.forEach((v) {
        listWarrantyModel.add(WarrantyModel.fromJson(v));
      });
    }
    return listWarrantyModel;
  }
}
