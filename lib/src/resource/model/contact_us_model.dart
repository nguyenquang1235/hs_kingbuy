/// message : "@Demo"
/// hotLine : "19009434"
/// email : "hello@demo.com"

class ContactUsModel {
  String message;
  String hotLine;
  String email;

  ContactUsModel({this.message, this.hotLine, this.email});

  ContactUsModel.fromJson(dynamic json) {
    message = json["message"];
    hotLine = json["hotLine"];
    email = json["email"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["message"] = message;
    map["hotLine"] = hotLine;
    map["email"] = email;
    return map;
  }

  static List<ContactUsModel> listFromJson(dynamic json) {
    List<ContactUsModel> listContactUs = [];
    if (json != null) {
      json.forEach((v) {
        listContactUs.add(ContactUsModel.fromJson(v));
      });
    }
    return listContactUs;
  }
}
