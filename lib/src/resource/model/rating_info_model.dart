/// one_star_count : 0
/// two_star_count : 1
/// three_star_count : 0
/// four_star_count : 0
/// five_star_count : 0
/// avg_rating : 2
/// rating_count : 1

class RatingInfoModel {
  int oneStarCount;
  int twoStarCount;
  int threeStarCount;
  int fourStarCount;
  int fiveStarCount;
  double avgRating;
  int ratingCount;

  RatingInfoModel(
      {this.oneStarCount,
      this.twoStarCount,
      this.threeStarCount,
      this.fourStarCount,
      this.fiveStarCount,
      this.avgRating,
      this.ratingCount});

  RatingInfoModel.fromJson(dynamic json) {
    oneStarCount = json["one_star_count"];
    twoStarCount = json["two_star_count"];
    threeStarCount = json["three_star_count"];
    fourStarCount = json["four_star_count"];
    fiveStarCount = json["five_star_count"];
    avgRating = double.parse(json["avg_rating"].toString());
    ratingCount = json["rating_count"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["one_star_count"] = oneStarCount;
    map["two_star_count"] = twoStarCount;
    map["three_star_count"] = threeStarCount;
    map["four_star_count"] = fourStarCount;
    map["five_star_count"] = fiveStarCount;
    map["avg_rating"] = avgRating;
    map["rating_count"] = ratingCount;
    return map;
  }
}
