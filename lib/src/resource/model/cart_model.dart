class CartModel {
  int id;
  String note;
  int totalUnread;
  int discount;
  int total;
  int deliveryStatus;
  int paymentType;
  int isExportInvoice;
  String taxCode;
  String companyName;
  String companyAddress;
  String companyEmail;

  CartModel(
      {this.id,
      this.note,
      this.totalUnread,
      this.discount,
      this.total,
      this.deliveryStatus,
      this.paymentType,
      this.isExportInvoice,
      this.taxCode,
      this.companyName,
      this.companyAddress,
      this.companyEmail});

  CartModel.fromJson(dynamic json) {
    id = json["id"];
    paymentType = json["payment_type"];
    note = json["note"];
    discount = json["discount"];
    total = json["total"];
    deliveryStatus = json["delivery_status"];
    isExportInvoice = json["is_export_invoice"];
    taxCode = json["tax_code"];
    companyName = json["company_name"];
    companyAddress = json["company_address"];
    companyEmail = json["company_email"];
    totalUnread = json["total_unread"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["payment_type"] = paymentType;
    map["note"] = note;
    map["discount"] = discount;
    map["total"] = total;
    map["delivery_status"] = deliveryStatus;
    map["is_export_invoice"] = isExportInvoice;
    map["tax_code"] = taxCode;
    map["company_name"] = companyName;
    map["company_address"] = companyAddress;
    map["company_email"] = companyEmail;
    map["total_unread"] = totalUnread;
    return map;
  }
}

class CartItem {
  int id;
  int productId;
  int qty;
  int hasRead;
  int colorId;

  CartItem({this.id, this.productId, this.qty, this.hasRead, this.colorId});

  CartItem.fromJson(dynamic json) {
    id = json["id"];
    productId = json["product_id"];
    qty = json["qty"];
    hasRead = json["has_read"];
    colorId = json["color_id"];
  }

  toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["product_id"] = productId;
    map["qty"] = qty;
    map["has_read"] = hasRead;
    map["color_id"] = colorId;
    return map;
  }

  CartItem copyWith({
    int qty,
    int hasRead,
  }) {
    return CartItem(
        id: this.id,
        productId: this.productId,
        colorId: this.colorId,
        hasRead: hasRead ?? this.hasRead,
        qty: qty ?? this.qty);
  }

  static List<CartItem> listFromJson(dynamic json) {
    List<CartItem> invoiceItems = [];
    if (json != null) {
      json.forEach((v) {
        invoiceItems.add(CartItem.fromJson(v));
      });
    }
    return invoiceItems;
  }
}
