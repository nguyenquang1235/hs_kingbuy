/// id : 2
/// name : "thương hiệu 2"
/// image_source : "/photos/1/IMG_3184 (1).jpg"
/// content : "<p>thương hiệu 1</p>"
/// slug : "thuong-hieu-2"
/// display_order : 0
/// created_at : "2020-02-14 11:13:15"
/// updated_at : "2020-03-05 11:05:45"

class BrandModel {
  int id;
  String name;
  String imageSource;
  String content;
  String slug;
  int displayOrder;
  String createdAt;
  String updatedAt;

  BrandModel(
      {this.id,
      this.name,
      this.imageSource,
      this.content,
      this.slug,
      this.displayOrder,
      this.createdAt,
      this.updatedAt});

  BrandModel.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    imageSource = json["image_source"];
    content = json["content"];
    slug = json["slug"];
    displayOrder = json["display_order"];
    createdAt = json["created_at"];
    updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["image_source"] = imageSource;
    map["content"] = content;
    map["slug"] = slug;
    map["display_order"] = displayOrder;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    return map;
  }

  static List<BrandModel> listFromJson(dynamic json) {
    List<BrandModel> listBrandModels = [];
    if (json != null) {
      json.forEach((v) {
        listBrandModels.add(BrandModel.fromJson(v));
      });
    }
    return listBrandModels;
  }

  @override
  bool operator ==(Object other) {
    // TODO: implement ==
    return (other is BrandModel) &&
        other.name == name &&
        other.id == id;
  }
}
