/// id : 1
/// address : "275 Lạch Tray, Ngô Quyền, Hải Phòng"
/// hot_line : "023123232"
/// opening_hours : "16:00 - 20:00"
/// image_source : "/photos/1/1001_4trungbaybomachchutaiphilong.jpg"
/// latitude : "20.834142"
/// longitude : "106.697442"

class AddressModel {
  int id;
  String address;
  String hotLine;
  String openingHours;
  String imageSource;
  String latitude;
  String longitude;

  AddressModel(
      {this.id,
      this.address,
      this.hotLine,
      this.openingHours,
      this.imageSource,
      this.latitude,
      this.longitude});

  AddressModel.fromJson(dynamic json) {
    id = json["id"];
    address = json["address"];
    hotLine = json["hot_line"];
    openingHours = json["opening_hours"];
    imageSource = json["image_source"];
    latitude = json["latitude"];
    longitude = json["longitude"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["address"] = address;
    map["hot_line"] = hotLine;
    map["opening_hours"] = openingHours;
    map["image_source"] = imageSource;
    map["latitude"] = latitude;
    map["longitude"] = longitude;
    return map;
  }

  static List<AddressModel> listFromJson(dynamic json) {
    List<AddressModel> list = [];
    if (json != null) {
      json.forEach((v) {
        list.add(AddressModel.fromJson(v));
      });
    }
    return list;
  }
}
