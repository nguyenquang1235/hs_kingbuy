
/// des_seo : "KingBuy - Ông Hoàng Mua Sắm - Nhà phân phối hàng gia dụng, sức khoẻ hàng đầu Việt Nam với các mặt hàng như máy chạy bộ, ghế massage toàn thân, xe đạp tập thể dục, máy rung bụng"
/// fanpage : "https://www.facebook.com/kingbuy.vn/"
/// fee_ship : "50000"
/// footer_about_us : "<ul>\r\n\t<li><a href=\"#\"><img alt=\"vcb\" src=\"http://kingbuy.vn/design/img/vcb.png\" /> </a></li>\r\n\t<li><a href=\"#\"><img alt=\"acb\" src=\"http://kingbuy.vn/design/img/acb.png\" /> </a></li>\r\n\t<li><a href=\"#\"><img alt=\"agri\" src=\"http://kingbuy.vn/design/img/agri.png\" /> </a></li>\r\n\t<li><a href=\"#\"><img alt=\"viettin\" src=\"http://kingbuy.vn/design/img/viettin.png\" /> </a></li>\r\n</ul>\r\n\r\n<p>Qu&yacute; kh&aacute;ch c&oacute; thể lựa chọn thanh to&aacute;n chuyển khoản qua t&agrave;i khoản ng&acirc;n h&agrave;ng của Kingbuy tại 1 trong 4 ng&acirc;n h&agrave;ng tr&ecirc;n.</p>"
/// footer_company : "<p>Giấy CNĐKDN: 0313005743<br />\r\nNg&agrave;y cấp: 07/11/2014.<br />\r\nCấp bởi: Sở Kế hoạch v&agrave; Đầu tư TP. HCM.</p>\r\n\r\n<p>CN T&acirc;n B&igrave;nh:&nbsp;658 CỘNG H&Ograve;A, P.13, Q.T&Acirc;N B&Igrave;NH, TP.HCM<br />\r\n(C&oacute; chỗ đậu &Ocirc;t&ocirc;)</p>\r\n\r\n<p>CN G&Ograve; VẤP:&nbsp;71 TRẦN THỊ NGHỈ, P.7, Q.G&Ograve; VẤP, TP.HCM<br />\r\n(C&oacute; chỗ đậu &Ocirc;t&ocirc;)</p>\r\n\r\n<p><img alt=\"s\" src=\"http://kingbuy.vn/design/img/0ddac77f-1d85-439b-8743-24f15b5ae2be.png\" style=\"width:100%\" /></p>"
/// footer_installment : "<ul>\r\n\t<li><a href=\"#\"><img alt=\"hdsg\" src=\"http://kingbuy.vn/design/img/hdsg.png\" /> </a></li>\r\n\t<li><a href=\"$\"><img alt=\"aeon\" src=\"http://kingbuy.vn/design/img/aeon.png\" /> </a></li>\r\n</ul>\r\n\r\n<p>Nhằm mang đến cho qu&yacute; kh&aacute;ch h&agrave;ng sự tiện lợi v&agrave; h&agrave;i l&ograve;ng tuyệt đối, Kingcare cung cấp cho kh&aacute;ch h&agrave;ng dịch vụ trả g&oacute;p với l&atilde;i suất 0% từ HDSaiSon v&agrave; ACS.</p>"
/// footer_quick_support : "<p>Mail hỗ trợ nhanh:<br />\r\ncontact@kingbuy.vn<br />\r\nTổng đ&agrave;i miễn ph&iacute;:</p>\r\n\r\n<p><img alt=\"help\" src=\"http://kingbuy.vn/design/img/help.svg\" /> <span style=\"color:#1aa7fd\"><strong>18006810</strong></span></p>"
/// footer_top_about_us : "<ul>\r\n\t<li><a href=\"\"> Giới thiệu chung về Kingcare </a></li>\r\n\t<li><a href=\"\"> Tầm nh&igrave;n v&agrave; sứ mệnh </a></li>\r\n\t<li><a href=\"\"> Định hướng ph&aacute;t triển </a></li>\r\n\t<li><a href=\"\"> Ch&iacute;nh s&aacute;ch bảo mật </a></li>\r\n\t<li><a href=\"\"> Cam kết về th&ocirc;ng tin </a></li>\r\n\t<li><a href=\"\"> Trả g&oacute;p qua thẻ t&iacute;n dụng </a></li>\r\n</ul>"
/// footer_top_facebook : "<iframe frameborder=\"0\" height=\"180\" scrolling=\"no\" src=\"https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fanmedaspa%2F&amp;tabs=timeline&amp;width=340&amp;height=180&amp;small_header=false&amp;adapt_container_width=false&amp;hide_cover=false&amp;show_facepile=true&amp;appId=222486395281473\" width=\"250\"></iframe>"
/// footer_top_guide : "<ul>\r\n\t<li><a href=\"\"> Ti&ecirc;u ch&iacute; b&aacute;n h&agrave;ng </a></li>\r\n\t<li><a href=\"\"> Ch&iacute;nh s&aacute;ch thanh to&aacute;n </a></li>\r\n\t<li><a href=\"\"> Ch&iacute;nh s&aacute;ch đổi trả </a></li>\r\n\t<li><a href=\"\"> Ch&iacute;nh s&aacute;ch vận chuyển </a></li>\r\n\t<li><a href=\"\"> Ch&iacute;nh s&aacute;ch bảo h&agrave;nh </a></li>\r\n</ul>"
/// footer_top_view_more : "<ul>\r\n\t<li><a href=\"\"> Ti&ecirc;u ch&iacute; b&aacute;n h&agrave;ng </a></li>\r\n\t<li><a href=\"\"> Ch&iacute;nh s&aacute;ch thanh to&aacute;n </a></li>\r\n\t<li><a href=\"\"> Ch&iacute;nh s&aacute;ch đổi trả </a></li>\r\n\t<li><a href=\"\"> Ch&iacute;nh s&aacute;ch vận chuyển </a></li>\r\n\t<li><a href=\"\"> Ch&iacute;nh s&aacute;ch bảo h&agrave;nh </a></li>\r\n</ul>"
/// free_ship_range_in : "5"
/// h1_tag : "KingBuy - Ông Hoàng Mua Sắm - Nhà phân phối hàng gia dụng, sức khoẻ hàng đầu Việt Nam"
/// hot_line : "1900.6810"
/// hot_line_2 : "0972.413.233"
/// logo_website : ""
/// main_menu : ""
/// privacy_and_terms : "<p>@Demo,19009434,hello@demo.com</p>"
/// range_greater_than_or_equal_to : "9"
/// resize_banner_height : ""
/// resize_banner_width : ""
/// resize_coupon_height : ""
/// resize_coupon_width : ""
/// resize_new_height : ""
/// resize_new_width : ""
/// resize_product_height : ""
/// resize_product_width : ""
/// resize_promotion_height : ""
/// resize_promotion_width : ""
/// title_seo : "KingBuy - Ông Hoàng Mua Sắm - Nhà phân phối hàng gia dụng, sức khoẻ hàng đầu Việt Nam"
/// top_menu : "<ul>\r\n\t<li><a href=\"/bao-hanh\">Kiểm tra bảo h&agrave;nh </a></li>\r\n\t<li><a href=\"/tin-tuc\">Tin tức </a></li>\r\n\t<li><a href=\"/khuyen-mai\">Khuyến m&atilde;i </a></li>\r\n</ul>"
/// use_of_terms : ""
/// ver_app : "2020 Kingbuy.Ver 1.07"
/// zalo : "http://zaloapp.com/qr/p/3v7doxdwrln3"
/// commitments : [{"id":3,"short_title":"Miễn phí","title":"Giao hàng, lắp đặt, trải nghiệm miễn phí","description":"Hỗ trợ giao hàng, lắp đặt miễn phí tại nhà trên toàn quốc. Trải nghiệm miễn phí tại showroom trên toàn quốc.","display_order":0,"created_at":"2020-03-20 09:27:09","updated_at":"2020-03-20 09:27:09"},{"id":4,"short_title":"7 ngày","title":"Đổi trả trong vòng 7 ngày","description":"Nếu phụ kiện bị lỗi do nhà sản xuất, Kingbuy sẽ hỗ trợ Quý khách trả lại trong vòng 7 ngày mà không phát sinh thêm chi phí.","display_order":0,"created_at":"2020-03-20 09:27:52","updated_at":"2020-03-20 09:27:52"},{"id":2,"short_title":"Miễn phí","title":"Giao hàng, lắp đặt, trải nghiệm miễn phí","description":"Hỗ trợ giao hàng, lắp đặt miễn phí tại nhà trên toàn quốc. Trải nghiệm miễn phí tại showroom trên toàn quốc.","display_order":1,"created_at":"2020-03-20 09:26:42","updated_at":"2020-03-20 09:29:10"}]
/// delivery_status : [{"id":1,"name":"Giao hàng trong 3h"},{"id":2,"name":"Giao hàng trong 24h"},{"id":3,"name":"Giao hàng 2-3 ngày"},{"id":4,"name":"Giao hàng 3-5 ngày"},{"id":5,"name":"Nhân viên gọi xác nhận"}]
/// invoice_status : [{"id":0,"name":"Đơn hàng chờ xác nhận"},{"id":1,"name":"Đơn hàng chờ vận chuyển"},{"id":2,"name":"Đơn hàng thành công"},{"id":3,"name":"Đơn hàng đã hủy"},{"id":4,"name":"Đơn hàng chờ thanh toán"}]

class ConfigModel {
  String desSeo;
  String fanPage;
  String feeShip;
  String footerAboutUs;
  String footerCompany;
  String footerInstallment;
  String footerQuickSupport;
  String footerTopAboutUs;
  String footerTopFacebook;
  String footerTopGuide;
  String footerTopViewMore;
  String freeShipRangeIn;
  String h1Tag;
  String hotLine;
  String hotLine2;
  String logoWebsite;
  String mainMenu;
  String privacyAndTerms;
  String rangeGreaterThanOrEqualTo;
  String resizeBannerHeight;
  String resizeBannerWidth;
  String resizeCouponHeight;
  String resizeCouponWidth;
  String resizeNewHeight;
  String resizeNewWidth;
  String resizeProductHeight;
  String resizeProductWidth;
  String resizePromotionHeight;
  String resizePromotionWidth;
  String titleSeo;
  String topMenu;
  String useOfTerms;
  String verApp;
  String zalo;
  List<Commitments> commitments;
  List<DeliveryStatus> deliveryStatus;
  List<InvoiceStatus> invoiceStatus;

  ConfigModel({
      this.desSeo, 
      this.fanPage,
      this.feeShip, 
      this.footerAboutUs, 
      this.footerCompany, 
      this.footerInstallment, 
      this.footerQuickSupport, 
      this.footerTopAboutUs, 
      this.footerTopFacebook, 
      this.footerTopGuide, 
      this.footerTopViewMore, 
      this.freeShipRangeIn, 
      this.h1Tag, 
      this.hotLine, 
      this.hotLine2, 
      this.logoWebsite, 
      this.mainMenu, 
      this.privacyAndTerms, 
      this.rangeGreaterThanOrEqualTo, 
      this.resizeBannerHeight, 
      this.resizeBannerWidth, 
      this.resizeCouponHeight, 
      this.resizeCouponWidth, 
      this.resizeNewHeight, 
      this.resizeNewWidth, 
      this.resizeProductHeight, 
      this.resizeProductWidth, 
      this.resizePromotionHeight, 
      this.resizePromotionWidth, 
      this.titleSeo, 
      this.topMenu, 
      this.useOfTerms, 
      this.verApp, 
      this.zalo, 
      this.commitments, 
      this.deliveryStatus, 
      this.invoiceStatus});

  ConfigModel.fromJson(dynamic json) {
    desSeo = json["des_seo"];
    fanPage = json["fanpage"];
    feeShip = json["fee_ship"];
    footerAboutUs = json["footer_about_us"];
    footerCompany = json["footer_company"];
    footerInstallment = json["footer_installment"];
    footerQuickSupport = json["footer_quick_support"];
    footerTopAboutUs = json["footer_top_about_us"];
    footerTopFacebook = json["footer_top_facebook"];
    footerTopGuide = json["footer_top_guide"];
    footerTopViewMore = json["footer_top_view_more"];
    freeShipRangeIn = json["free_ship_range_in"];
    h1Tag = json["h1_tag"];
    hotLine = json["hot_line"];
    hotLine2 = json["hot_line_2"];
    logoWebsite = json["logo_website"];
    mainMenu = json["main_menu"];
    privacyAndTerms = json["privacy_and_terms"];
    rangeGreaterThanOrEqualTo = json["range_greater_than_or_equal_to"];
    resizeBannerHeight = json["resize_banner_height"];
    resizeBannerWidth = json["resize_banner_width"];
    resizeCouponHeight = json["resize_coupon_height"];
    resizeCouponWidth = json["resize_coupon_width"];
    resizeNewHeight = json["resize_new_height"];
    resizeNewWidth = json["resize_new_width"];
    resizeProductHeight = json["resize_product_height"];
    resizeProductWidth = json["resize_product_width"];
    resizePromotionHeight = json["resize_promotion_height"];
    resizePromotionWidth = json["resize_promotion_width"];
    titleSeo = json["title_seo"];
    topMenu = json["top_menu"];
    useOfTerms = json["use_of_terms"];
    verApp = json["ver_app"];
    zalo = json["zalo"];
    if (json["commitments"] != null) {
      commitments = [];
      json["commitments"].forEach((v) {
        commitments.add(Commitments.fromJson(v));
      });
    }
    if (json["delivery_status"] != null) {
      deliveryStatus = [];
      json["delivery_status"].forEach((v) {
        deliveryStatus.add(DeliveryStatus.fromJson(v));
      });
    }
    if (json["invoice_status"] != null) {
      invoiceStatus = [];
      json["invoice_status"].forEach((v) {
        invoiceStatus.add(InvoiceStatus.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["des_seo"] = desSeo;
    map["fanpage"] = fanPage;
    map["fee_ship"] = feeShip;
    map["footer_about_us"] = footerAboutUs;
    map["footer_company"] = footerCompany;
    map["footer_installment"] = footerInstallment;
    map["footer_quick_support"] = footerQuickSupport;
    map["footer_top_about_us"] = footerTopAboutUs;
    map["footer_top_facebook"] = footerTopFacebook;
    map["footer_top_guide"] = footerTopGuide;
    map["footer_top_view_more"] = footerTopViewMore;
    map["free_ship_range_in"] = freeShipRangeIn;
    map["h1_tag"] = h1Tag;
    map["hot_line"] = hotLine;
    map["hot_line_2"] = hotLine2;
    map["logo_website"] = logoWebsite;
    map["main_menu"] = mainMenu;
    map["privacy_and_terms"] = privacyAndTerms;
    map["range_greater_than_or_equal_to"] = rangeGreaterThanOrEqualTo;
    map["resize_banner_height"] = resizeBannerHeight;
    map["resize_banner_width"] = resizeBannerWidth;
    map["resize_coupon_height"] = resizeCouponHeight;
    map["resize_coupon_width"] = resizeCouponWidth;
    map["resize_new_height"] = resizeNewHeight;
    map["resize_new_width"] = resizeNewWidth;
    map["resize_product_height"] = resizeProductHeight;
    map["resize_product_width"] = resizeProductWidth;
    map["resize_promotion_height"] = resizePromotionHeight;
    map["resize_promotion_width"] = resizePromotionWidth;
    map["title_seo"] = titleSeo;
    map["top_menu"] = topMenu;
    map["use_of_terms"] = useOfTerms;
    map["ver_app"] = verApp;
    map["zalo"] = zalo;
    if (commitments != null) {
      map["commitments"] = commitments.map((v) => v.toJson()).toList();
    }
    if (deliveryStatus != null) {
      map["delivery_status"] = deliveryStatus.map((v) => v.toJson()).toList();
    }
    if (invoiceStatus != null) {
      map["invoice_status"] = invoiceStatus.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 0
/// name : "Đơn hàng chờ xác nhận"

class InvoiceStatus {
  int id;
  String name;

  InvoiceStatus({
      this.id, 
      this.name});

  InvoiceStatus.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    return map;
  }

}

/// id : 1
/// name : "Giao hàng trong 3h"

class DeliveryStatus {
  int id;
  String name;

  DeliveryStatus({
      this.id, 
      this.name});

  DeliveryStatus.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    return map;
  }

}

/// id : 3
/// short_title : "Miễn phí"
/// title : "Giao hàng, lắp đặt, trải nghiệm miễn phí"
/// description : "Hỗ trợ giao hàng, lắp đặt miễn phí tại nhà trên toàn quốc. Trải nghiệm miễn phí tại showroom trên toàn quốc."
/// display_order : 0
/// created_at : "2020-03-20 09:27:09"
/// updated_at : "2020-03-20 09:27:09"

class Commitments {
  int id;
  String shortTitle;
  String title;
  String description;
  int displayOrder;
  String createdAt;
  String updatedAt;

  Commitments({
      this.id, 
      this.shortTitle, 
      this.title, 
      this.description, 
      this.displayOrder, 
      this.createdAt, 
      this.updatedAt});

  Commitments.fromJson(dynamic json) {
    id = json["id"];
    shortTitle = json["short_title"];
    title = json["title"];
    description = json["description"];
    displayOrder = json["display_order"];
    createdAt = json["created_at"];
    updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["short_title"] = shortTitle;
    map["title"] = title;
    map["description"] = description;
    map["display_order"] = displayOrder;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    return map;
  }

  static List<Commitments> listFromJson(dynamic json){
    List<Commitments> notifications = [];
    if (json!= null) {
      json.forEach((v) {
        notifications.add(Commitments.fromJson(v));
      });
    }
    return notifications;
  }

}