class NetworkResponse<T> {
  int status;
  String message;
  T data;

  bool get isSuccess => status == 1;

  NetworkResponse({this.status, this.message, this.data});

  NetworkResponse.fromJson(dynamic json, {converter}) {
    status = json["status"];
    message = json["message"];
    data = converter != null && json["data"] != null
        ? converter(json["data"])
        : json["data"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["message"] = message;
    map["data"] = data;
    return map;
  }
}