/// id : 11
/// name : "sp2"
/// price : 1000000
/// sale_price : 500000
/// product_category_id : 2
/// brand_id : 1
/// gift_ids : [2]
/// image_source : "/photos/1/10bb11aa-4837-47a5-a5dc-ad2402323271.png"
/// description : "mô tả"
/// content : "<p>nội dung</p>"
/// specifications : "<p>kỹ thuật</p>"
/// video_link : null
/// slug : "sp2.html"
/// status : 1
/// goods_status : 1
/// is_bulky : 1
/// is_installment : 1
/// barcode : "sp2"
/// created_at : "16:16:33 2020-06-11"
/// updated_at : "16:16:33 2020-06-11"
/// product_category_name : "Danh mục 2"
/// brand_name : "thương hiệu 1"
/// brand_info : "<p>thương hiệu 1</p>"
/// sale_off : 50
/// gifts : [{"id":2,"name":"asss","price":0,"image_source":"/photos/1/IMG_3184 (1).jpg","description":"sss","created_at":"2020-02-14 15:04:05","updated_at":"2020-02-14 15:04:05"}]
/// star : 0
/// colors : []

class ProductModel {
  int id;
  int qty;
  String name;
  int price;
  int salePrice;
  int productCategoryId;
  int brandId;
  List<int> giftIds;
  String imageSource;
  String description;
  String content;
  String specifications;
  String videoLink;
  String slug;
  int status;
  int goodsStatus;
  int isBulky;
  int isInstallment;
  String barcode;
  String createdAt;
  String updatedAt;
  String productCategoryName;
  String brandName;
  String brandInfo;
  int saleOff;
  List<Gifts> gifts;
  double star;
  List<String> colors;

  ProductModel(
      {this.id,
      this.name,
        this.qty,
      this.price,
      this.salePrice,
      this.productCategoryId,
      this.brandId,
      this.giftIds,
      this.imageSource,
      this.description,
      this.content,
      this.specifications,
      this.videoLink,
      this.slug,
      this.status,
      this.goodsStatus,
      this.isBulky,
      this.isInstallment,
      this.barcode,
      this.createdAt,
      this.updatedAt,
      this.productCategoryName,
      this.brandName,
      this.brandInfo,
      this.saleOff,
      this.gifts,
      this.star,
      this.colors});

  ProductModel.fromJson(dynamic json) {
    id = json["id"];
    qty = json["qty"] ?? null;
    name = json["name"];
    price = json["price"];
    salePrice = json["sale_price"];
    productCategoryId = json["product_category_id"];
    brandId = json["brand_id"];
    giftIds = json["gift_ids"] != null ? json["gift_ids"].cast<int>() : [];
    imageSource = json["image_source"];
    description = json["description"];
    content = json["content"];
    specifications = json["specifications"];
    videoLink = json["video_link"];
    slug = json["slug"];
    status = json["status"];
    goodsStatus = json["goods_status"];
    isBulky = json["is_bulky"];
    isInstallment = json["is_installment"];
    barcode = json["barcode"];
    createdAt = json["created_at"];
    updatedAt = json["updated_at"];
    productCategoryName = json["product_category_name"];
    brandName = json["brand_name"];
    brandInfo = json["brand_info"];
    saleOff = json["sale_off"];
    if (json["gifts"] != null) {
      gifts = [];
      json["gifts"].forEach((v) {
        gifts.add(Gifts.fromJson(v));
      });
    }
    star = double.parse(json["star"]?.toString() ?? "0");
    if (json["colors"] != null) {
      colors = [];
      json["colors"].forEach((v) {
        colors.add(v.toString());
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["qty"] = qty;
    map["price"] = price;
    map["sale_price"] = salePrice;
    map["product_category_id"] = productCategoryId;
    map["brand_id"] = brandId;
    map["gift_ids"] = giftIds;
    map["image_source"] = imageSource;
    map["description"] = description;
    map["content"] = content;
    map["specifications"] = specifications;
    map["video_link"] = videoLink;
    map["slug"] = slug;
    map["status"] = status;
    map["goods_status"] = goodsStatus;
    map["is_bulky"] = isBulky;
    map["is_installment"] = isInstallment;
    map["barcode"] = barcode;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    map["product_category_name"] = productCategoryName;
    map["brand_name"] = brandName;
    map["brand_info"] = brandInfo;
    map["sale_off"] = saleOff;
    if (gifts != null) {
      map["gifts"] = gifts.map((v) => v.toJson()).toList();
    }
    map["star"] = star;
    if (colors != null) {
      map["colors"] = colors.map((v) => v).toList();
    }
    return map;
  }

  static List<ProductModel> listFromJson(dynamic json) {
    List<ProductModel> listIProductModel = [];
    if (json != null) {
      json.forEach((v) {
        listIProductModel.add(ProductModel.fromJson(v));
      });
    }
    return listIProductModel;
  }
}

/// id : 2
/// name : "asss"
/// price : 0
/// image_source : "/photos/1/IMG_3184 (1).jpg"
/// description : "sss"
/// created_at : "2020-02-14 15:04:05"
/// updated_at : "2020-02-14 15:04:05"

class Gifts {
  int id;
  String name;
  int price;
  String imageSource;
  String description;
  String createdAt;
  String updatedAt;

  Gifts(
      {this.id,
      this.name,
      this.price,
      this.imageSource,
      this.description,
      this.createdAt,
      this.updatedAt});

  Gifts.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    price = json["price"];
    imageSource = json["image_source"];
    description = json["description"];
    createdAt = json["created_at"];
    updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["price"] = price;
    map["image_source"] = imageSource;
    map["description"] = description;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    return map;
  }
}
