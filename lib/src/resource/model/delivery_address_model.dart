/// id : 53
/// user_id : 3
/// address : "jdkkdkdkkdkdk, Xã Sơn Giang, Huyện Sơn Hà, Tỉnh Quảng Ngãi"
/// province_code : "51"
/// province_name : "Quảng Ngãi"
/// district_name : "Huyện Sơn Hà"
/// ward_name : "Xã Sơn Giang"
/// district_code : "529"
/// delivery_status : [5]
/// ward_code : "21307"
/// is_default : 0
/// is_export_invoice : 0
/// tax_code : null
/// company_name : null
/// company_address : null
/// company_email : null
/// first_phone : "0978648597"
/// second_phone : "0978648597"
/// fullname : "tuan"
/// ship_fee_bulky : 0
/// ship_fee_not_bulky : 0
/// full_address : "jdkkdkdkkdkdk, Xã Sơn Giang, Huyện Sơn Hà, Tỉnh Quảng Ngãi, Xã Sơn Giang, Huyện Sơn Hà, Tỉnh Quảng Ngãi"

// import 'package:copy_with_extension/copy_with_extension.dart';
//
// part 'delivery_address_model.g.dart';
//
// @CopyWith()

class DeliveryAddressModel {
  int id;
  int userId;
  String address;
  String provinceCode;
  String provinceName;
  String districtName;
  String wardName;
  String districtCode;
  List<int> deliveryStatus;
  String wardCode;
  int isDefault;
  int isExportInvoice;
  String taxCode;
  String companyName;
  String companyAddress;
  String companyEmail;
  String firstPhone;
  String secondPhone;
  String fullName;
  int shipFeeBulky;
  int shipFeeNotBulky;
  String fullAddress;

  DeliveryAddressModel(
      {this.id,
      this.userId,
      this.address,
      this.provinceCode,
      this.provinceName,
      this.districtName,
      this.wardName,
      this.districtCode,
      this.deliveryStatus,
      this.wardCode,
      this.isDefault,
      this.isExportInvoice,
      this.taxCode,
      this.companyName,
      this.companyAddress,
      this.companyEmail,
      this.firstPhone,
      this.secondPhone,
      this.fullName,
      this.shipFeeBulky,
      this.shipFeeNotBulky,
      this.fullAddress});

  DeliveryAddressModel.fromJson(dynamic json) {
    id = json["id"];
    userId = json["user_id"];
    address = json["address"];
    provinceCode = json["province_code"];
    provinceName = json["province_name"];
    districtName = json["district_name"];
    wardName = json["ward_name"];
    districtCode = json["district_code"];
    deliveryStatus = json["delivery_status"] != null
        ? json["delivery_status"].cast<int>()
        : [];
    wardCode = json["ward_code"];
    isDefault = json["is_default"];
    isExportInvoice = json["is_export_invoice"];
    taxCode = json["tax_code"];
    companyName = json["company_name"];
    companyAddress = json["company_address"];
    companyEmail = json["company_email"];
    firstPhone = json["first_phone"];
    secondPhone = json["second_phone"];
    fullName = json["fullname"];
    shipFeeBulky = json["ship_fee_bulky"];
    shipFeeNotBulky = json["ship_fee_not_bulky"];
    fullAddress = json["full_address"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["user_id"] = userId;
    map["address"] = address;
    map["province_code"] = provinceCode;
    map["province_name"] = provinceName;
    map["district_name"] = districtName;
    map["ward_name"] = wardName;
    map["district_code"] = districtCode;
    map["delivery_status"] = deliveryStatus;
    map["ward_code"] = wardCode;
    map["is_default"] = isDefault;
    map["is_export_invoice"] = isExportInvoice;
    map["tax_code"] = taxCode;
    map["company_name"] = companyName;
    map["company_address"] = companyAddress;
    map["company_email"] = companyEmail;
    map["first_phone"] = firstPhone;
    map["second_phone"] = secondPhone;
    map["fullname"] = fullName;
    map["ship_fee_bulky"] = shipFeeBulky;
    map["ship_fee_not_bulky"] = shipFeeNotBulky;
    map["full_address"] = fullAddress;
    return map;
  }

  DeliveryAddressModel copyWith({
    String address,
    String companyAddress,
    String companyEmail,
    String companyName,
    List<int> deliveryStatus,
    String districtCode,
    String districtName,
    String firstPhone,
    String fullAddress,
    String fullName,
    int id,
    int isDefault,
    int isExportInvoice,
    String provinceCode,
    String provinceName,
    String secondPhone,
    int shipFeeBulky,
    int shipFeeNotBulky,
    String taxCode,
    int userId,
    String wardCode,
    String wardName,
  }) {
    return DeliveryAddressModel(
      address: address ?? this.address,
      companyAddress: companyAddress ?? this.companyAddress,
      companyEmail: companyEmail ?? this.companyEmail,
      companyName: companyName ?? this.companyName,
      deliveryStatus: deliveryStatus ?? this.deliveryStatus,
      districtCode: districtCode ?? this.districtCode,
      districtName: districtName ?? this.districtName,
      firstPhone: firstPhone ?? this.firstPhone,
      fullAddress: fullAddress ?? this.fullAddress,
      fullName: fullName ?? this.fullName,
      id: id ?? this.id,
      isDefault: isDefault ?? this.isDefault,
      isExportInvoice: isExportInvoice ?? this.isExportInvoice,
      provinceCode: provinceCode ?? this.provinceCode,
      provinceName: provinceName ?? this.provinceName,
      secondPhone: secondPhone ?? this.secondPhone,
      shipFeeBulky: shipFeeBulky ?? this.shipFeeBulky,
      shipFeeNotBulky: shipFeeNotBulky ?? this.shipFeeNotBulky,
      taxCode: taxCode ?? this.taxCode,
      userId: userId ?? this.userId,
      wardCode: wardCode ?? this.wardCode,
      wardName: wardName ?? this.wardName,
    );
  }

  static List<DeliveryAddressModel> listFromJson(dynamic json) {
    List<DeliveryAddressModel> listDeliveryAddressModel = [];
    if (json != null) {
      json.forEach((v) {
        listDeliveryAddressModel.add(DeliveryAddressModel.fromJson(v));
      });
    }
    return listDeliveryAddressModel;
  }

  @override
  bool operator ==(Object other) {
    return (other is DeliveryAddressModel) &&
        other.fullName == fullName &&
        other.provinceCode == provinceCode &&
        other.provinceName == provinceName &&
        other.districtCode == districtCode &&
        other.districtName == districtName &&
        other.wardCode == wardCode &&
        other.wardName == wardName &&
        other.firstPhone == firstPhone &&
        other.secondPhone == secondPhone &&
        other.address == address &&
        other.fullAddress == fullAddress &&
        other.isDefault == isDefault;
  }
}
