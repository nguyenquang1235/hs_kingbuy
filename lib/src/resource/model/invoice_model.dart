/// id : 136
/// invoice_code : "QU13616352"
/// user_id : 3
/// payment_type : 4
/// order_address : "vzhsjsjksk, Phường Hàng Bột, Quận Đống Đa, Hà Nội"
/// order_phone : "0978659847"
/// order_phone_2 : null
/// order_name : "jjskkskskksk"
/// note : ""
/// delivery_charge : 0
/// discount : 0
/// total : 18000000
/// reward_points : 1800
/// is_use_point : 0
/// used_points : 0
/// status : 0
/// delivery_status : null
/// payment : 0
/// is_export_invoice : 0
/// tax_code : null
/// company_name : null
/// company_address : null
/// company_email : null
/// created_at : "16:54:26 2021-01-29"
/// delivery_date : ""
/// point_to_money_discount : 0
/// coupon_id : null
/// items : [{"id":152,"invoice_id":136,"product_id":109,"image_source":"/photos/1/san-pham/GHẾ MASSAGE/Swarovski PE-09Black/ghe-massage-toan-than-oto-pe-09-kim-cuong-xam-28.jpg","product_name":"Ghế massage TSSRT","color_name":null,"gifts":[{"name":"Cân sức khỏe điện tử PERSONAL SCALE - EV33","price":170000,"image_source":"/photos/1/qua-tang/can-suc-khoe-dien-tu_2012910114840468.jpg"}],"price":18000000,"qty":1,"total":18000000,"created_at":"2021-01-29 16:54:26","updated_at":"2021-01-29 16:54:26"}]
/// months_installment : 3
/// prepay_installment_amount : 0
/// prepay_installment : 0
/// pay_monthly_installment : 6000000

class InvoiceModel {
  int id;
  String invoiceCode;
  int userId;
  int paymentType;
  String orderAddress;
  String orderPhone;
  String orderPhone2;
  String orderName;
  String note;
  int deliveryCharge;
  int discount;
  int total;
  int rewardPoints;
  int isUsePoint;
  int usedPoints;
  int status;
  int deliveryStatus;
  int payment;
  int isExportInvoice;
  String taxCode;
  String companyName;
  String companyAddress;
  String companyEmail;
  String createdAt;
  String deliveryDate;
  int pointToMoneyDiscount;
  int couponId;
  List<InvoiceItem> items;
  int monthsInstallment;
  int prepayInstallmentAmount;
  int prepayInstallment;
  int payMonthlyInstallment;

  InvoiceModel(
      {this.id,
      this.invoiceCode,
      this.userId,
      this.paymentType,
      this.orderAddress,
      this.orderPhone,
      this.orderPhone2,
      this.orderName,
      this.note,
      this.deliveryCharge,
      this.discount,
      this.total,
      this.rewardPoints,
      this.isUsePoint,
      this.usedPoints,
      this.status,
      this.deliveryStatus,
      this.payment,
      this.isExportInvoice,
      this.taxCode,
      this.companyName,
      this.companyAddress,
      this.companyEmail,
      this.createdAt,
      this.deliveryDate,
      this.pointToMoneyDiscount,
      this.couponId,
      this.items,
      this.monthsInstallment,
      this.prepayInstallmentAmount,
      this.prepayInstallment,
      this.payMonthlyInstallment});

  InvoiceModel.fromJson(dynamic json) {
    id = json["id"];
    invoiceCode = json["invoice_code"];
    userId = json["user_id"];
    paymentType = json["payment_type"];
    orderAddress = json["order_address"];
    orderPhone = json["order_phone"];
    orderPhone2 = json["order_phone_2"];
    orderName = json["order_name"];
    note = json["note"];
    deliveryCharge = json["delivery_charge"];
    discount = json["discount"];
    total = json["total"];
    rewardPoints = json["reward_points"];
    isUsePoint = json["is_use_point"];
    usedPoints = json["used_points"];
    status = json["status"];
    deliveryStatus = json["delivery_status"];
    payment = json["payment"];
    isExportInvoice = json["is_export_invoice"];
    taxCode = json["tax_code"];
    companyName = json["company_name"];
    companyAddress = json["company_address"];
    companyEmail = json["company_email"];
    createdAt = json["created_at"];
    deliveryDate = json["delivery_date"];
    pointToMoneyDiscount = json["point_to_money_discount"];
    couponId = json["coupon_id"];
    if (json["items"] != null) {
      items = [];
      json["items"].forEach((v) {
        items.add(InvoiceItem.fromJson(v));
      });
    }
    monthsInstallment = json["months_installment"];
    prepayInstallmentAmount = json["prepay_installment_amount"];
    prepayInstallment = json["prepay_installment"];
    payMonthlyInstallment = json["pay_monthly_installment"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["invoice_code"] = invoiceCode;
    map["user_id"] = userId;
    map["payment_type"] = paymentType;
    map["order_address"] = orderAddress;
    map["order_phone"] = orderPhone;
    map["order_phone_2"] = orderPhone2;
    map["order_name"] = orderName;
    map["note"] = note;
    map["delivery_charge"] = deliveryCharge;
    map["discount"] = discount;
    map["total"] = total;
    map["reward_points"] = rewardPoints;
    map["is_use_point"] = isUsePoint;
    map["used_points"] = usedPoints;
    map["status"] = status;
    map["delivery_status"] = deliveryStatus;
    map["payment"] = payment;
    map["is_export_invoice"] = isExportInvoice;
    map["tax_code"] = taxCode;
    map["company_name"] = companyName;
    map["company_address"] = companyAddress;
    map["company_email"] = companyEmail;
    map["created_at"] = createdAt;
    map["delivery_date"] = deliveryDate;
    map["point_to_money_discount"] = pointToMoneyDiscount;
    map["coupon_id"] = couponId;
    if (items != null) {
      map["items"] = items.map((v) => v.toJson()).toList();
    }
    map["months_installment"] = monthsInstallment;
    map["prepay_installment_amount"] = prepayInstallmentAmount;
    map["prepay_installment"] = prepayInstallment;
    map["pay_monthly_installment"] = payMonthlyInstallment;
    return map;
  }

  static List<InvoiceModel> listFromJson(dynamic json) {
    List<InvoiceModel> listInvoiceModel = [];
    if (json != null) {
      json.forEach((v) {
        listInvoiceModel.add(InvoiceModel.fromJson(v));
      });
    }
    return listInvoiceModel;
  }
}

/// id : 152
/// invoice_id : 136
/// product_id : 109
/// image_source : "/photos/1/san-pham/GHẾ MASSAGE/Swarovski PE-09Black/ghe-massage-toan-than-oto-pe-09-kim-cuong-xam-28.jpg"
/// product_name : "Ghế massage TSSRT"
/// color_name : null
/// gifts : [{"name":"Cân sức khỏe điện tử PERSONAL SCALE - EV33","price":170000,"image_source":"/photos/1/qua-tang/can-suc-khoe-dien-tu_2012910114840468.jpg"}]
/// price : 18000000
/// qty : 1
/// total : 18000000
/// created_at : "2021-01-29 16:54:26"
/// updated_at : "2021-01-29 16:54:26"

class InvoiceItem {
  int id;
  int invoiceId;
  int productId;
  String imageSource;
  String productName;
  dynamic colorName;
  List<Gift> gifts;
  int price;
  int qty;
  int total;
  String createdAt;
  String updatedAt;

  InvoiceItem(
      {this.id,
      this.invoiceId,
      this.productId,
      this.imageSource,
      this.productName,
      this.colorName,
      this.gifts,
      this.price,
      this.qty,
      this.total,
      this.createdAt,
      this.updatedAt});

  InvoiceItem.fromJson(dynamic json) {
    id = json["id"];
    invoiceId = json["invoice_id"];
    productId = json["product_id"];
    imageSource = json["image_source"];
    productName = json["product_name"];
    colorName = json["color_name"];
    if (json["gifts"] != null) {
      gifts = [];
      json["gifts"].forEach((v) {
        gifts.add(Gift.fromJson(v));
      });
    }
    price = json["price"];
    qty = json["qty"];
    total = json["total"];
    createdAt = json["created_at"];
    updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["invoice_id"] = invoiceId;
    map["product_id"] = productId;
    map["image_source"] = imageSource;
    map["product_name"] = productName;
    map["color_name"] = colorName;
    if (gifts != null) {
      map["gifts"] = gifts.map((v) => v.toJson()).toList();
    }
    map["price"] = price;
    map["qty"] = qty;
    map["total"] = total;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    return map;
  }

  static List<InvoiceItem> listFromJson(dynamic json) {
    List<InvoiceItem> invoiceItems = [];
    if (json != null) {
      json.forEach((v) {
        invoiceItems.add(InvoiceItem.fromJson(v));
      });
    }
    return invoiceItems;
  }
}

/// name : "Cân sức khỏe điện tử PERSONAL SCALE - EV33"
/// price : 170000
/// image_source : "/photos/1/qua-tang/can-suc-khoe-dien-tu_2012910114840468.jpg"

class Gift {
  String name;
  int price;
  String imageSource;

  Gift({this.name, this.price, this.imageSource});

  Gift.fromJson(dynamic json) {
    name = json["name"];
    price = json["price"];
    imageSource = json["image_source"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = name;
    map["price"] = price;
    map["image_source"] = imageSource;
    return map;
  }

  static List<Gift> listFromJson(dynamic json) {
    List<Gift> gifts = [];
    if (json != null) {
      json.forEach((v) {
        gifts.add(Gift.fromJson(v));
      });
    }
    return gifts;
  }
}
