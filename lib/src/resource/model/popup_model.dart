/// popup_image : "/photos/1/san-pham/48a61b6c-f5ce-4445-a863-c2eabe95d405.png"
/// popup_product_id : 91
/// popup_type : 1
/// popup_link : ""

class PopupModel {
  String popupImage;
  int popupProductId;
  int popupType;
  String popupLink;

  PopupModel(
      {this.popupImage, this.popupProductId, this.popupType, this.popupLink});

  PopupModel.fromJson(dynamic json) {
    popupImage = json["popup_image"];
    popupProductId = json["popup_product_id"];
    popupType = json["popup_type"];
    popupLink = json["popup_link"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["popup_image"] = popupImage;
    map["popup_product_id"] = popupProductId;
    map["popup_type"] = popupType;
    map["popup_link"] = popupLink;
    return map;
  }
}
