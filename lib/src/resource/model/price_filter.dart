import 'package:intl/intl.dart';

class PriceFilter {
  final int id;
  final String name;
  final int priceFrom;
  final int priceTo;

  PriceFilter(this.id, {this.name = "", this.priceFrom = 0, this.priceTo});

  static List<PriceFilter> raw = [
    PriceFilter(1, name: "8-10 triệu", priceFrom: 8000000, priceTo: 10000000),
    PriceFilter(2, name: "10-15 triệu", priceFrom: 10000000, priceTo: 15000000),
    PriceFilter(3, name: "15-20 triệu", priceFrom: 15000000, priceTo: 20000000),
    PriceFilter(4, name: "trên 20 triệu", priceFrom: 20000000, priceTo: null),
  ];

  factory PriceFilter.fromJson(dynamic json) {
    return PriceFilter(json["id"],
        priceTo: json["maxPrice"],
        priceFrom: json["minPrice"],
        name: json["name"]);
  }
  @override
  bool operator ==(Object other) {
    // TODO: implement ==
    return (other is PriceFilter) &&
        other.id == id &&
        other.priceFrom == priceFrom &&
        other.priceTo == priceTo &&
        other.name == name;
  }

  @override
  String toString() {
    if (priceFrom == null) {
      String to = NumberFormat(",###", "vi").format(priceTo);
      return "dưới $to đ";
    } else if (priceTo == null) {
      String to = NumberFormat(",###", "vi").format(priceFrom);
      return "trên $to đ";
    } else {
      String from = NumberFormat(",###", "vi").format(priceFrom);
      String to = NumberFormat(",###", "vi").format(priceTo);
      // TODO: implement toString
      return "$from - $to đ";
    }
  }
}
