/// title : "Chào đón cửa hàng mới, ưu đãi giảm giá lên đến 50%, mua ngay thôi"
/// description : "Chào đón cửa hàng mới, ưu đãi giảm giá lên đến 50%, mua ngay thôi"
/// content : "<p>Ch&agrave;o đ&oacute;n cửa h&agrave;ng mới, ưu đ&atilde;i giảm gi&aacute; l&ecirc;n đến 50%, mua ngay th&ocirc;i</p>"
/// slug : "chao-don-cua-hang-moi-uu-dai-giam-gia-len-den-50-mua-ngay-thoi"
/// status : 1
/// created_at : "2020-03-18 09:59:43"
/// updated_at : "2020-03-18 09:59:43"

class NotificationModel {
  String title;
  String description;
  String content;
  String slug;
  int status;
  String createdAt;
  String updatedAt;

  NotificationModel({
      this.title, 
      this.description, 
      this.content, 
      this.slug, 
      this.status, 
      this.createdAt, 
      this.updatedAt});

  NotificationModel.fromJson(dynamic json) {
    title = json["title"];
    description = json["description"];
    content = json["content"];
    slug = json["slug"];
    status = json["status"];
    createdAt = json["created_at"];
    updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["title"] = title;
    map["description"] = description;
    map["content"] = content;
    map["slug"] = slug;
    map["status"] = status;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    return map;
  }

  static List<NotificationModel> listFromJson(dynamic json){
    List<NotificationModel> notifications = [];
    if (json!= null) {
      json.forEach((v) {
        notifications.add(NotificationModel.fromJson(v));
      });
    }
    return notifications;
  }

}