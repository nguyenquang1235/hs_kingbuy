class UserModel {
  UserModel({
    this.id,
    this.name,
    this.email,
    this.phoneNumber,
    this.avatarSource,
    this.dateOfBirth,
    this.gender,
    this.status,
    this.deviceToken,
    this.createdAt,
    this.updatedAt,
    this.memberCardNumber,
    this.rewardPoints,
  });

  int id;
  String name;
  String email;
  String phoneNumber;
  String avatarSource;
  String dateOfBirth;
  int gender;
  int status;
  String deviceToken;
  String createdAt;
  String updatedAt;
  String memberCardNumber;
  int rewardPoints;

  copyWith(
      {int id,
      String name,
      String email,
      String phoneNumber,
      String avatarSource,
      String dateOfBirth,
        int gender,
      String status,
      String deviceToken,
      String createdAt,
      String updatedAt,
      String memberCardNumber,
      int rewardPoints}) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      email: email ?? this.email,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      avatarSource: avatarSource ?? this.avatarSource,
      dateOfBirth: dateOfBirth ?? this.dateOfBirth,
      gender: gender ?? this.gender,
      status: status ?? this.status,
      deviceToken: deviceToken ?? this.deviceToken,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      rewardPoints: rewardPoints ?? this.rewardPoints,
      memberCardNumber: memberCardNumber ?? this.memberCardNumber,
    );
  }

  factory UserModel.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return UserModel(
      id: json["id"],
      name: json["name"],
      email: json["email"],
      phoneNumber: json["phone_number"],
      avatarSource: json["avatar_source"],
      dateOfBirth: json["date_of_birth"],
      gender: json["gender"],
      status: json["status"],
      deviceToken: json["device_token"],
      createdAt: json["created_at"],
      updatedAt: json["updated_at"],
      rewardPoints: json["reward_points"] ?? 0,
      memberCardNumber: json["member_card_number"] ?? "",
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "phone_number": phoneNumber,
        "avatar_source": avatarSource,
        "date_of_birth": dateOfBirth,
        "gender": gender,
        "status": status,
        "device_token": deviceToken,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "reward_points": rewardPoints,
        "member_card_number": memberCardNumber
      };

  @override
  bool operator == (Object other) {
    return (other is UserModel) &&
        other.name == name &&
        other.phoneNumber == phoneNumber &&
        other.email == email &&
        other.dateOfBirth == dateOfBirth &&
        other.gender == gender
    ;
  }
}
