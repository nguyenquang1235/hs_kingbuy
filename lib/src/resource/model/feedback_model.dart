/// status : 1
/// data : {"feedback":[{"id":31,"title":"Lam quen nhan vien cua hang","content":"helo","type":2},{"id":32,"title":"Lam quen nhan vien cua hang","content":"helo","type":2},{"id":33,"title":"Lam quen nhan vien cua hang","content":"xin chao","type":2},{"id":34,"title":"Lam quen nhan vien cua hang","content":"helo","type":2},{"id":35,"title":"Lam quen nhan vien cua hang","content":"xin chao my name is Cuong","type":2},{"id":38,"title":"Lam quen nhan vien cua hang","content":"hi","type":2},{"id":41,"title":"Lam quen nhan vien cua hang","content":"wwhat","type":2},{"id":42,"title":"Lam quen nhan vien cua hang","content":"wtf","type":2},{"id":43,"title":"Lam quen nhan vien cua hang","content":"wtf","type":2},{"id":44,"title":"Lam quen nhan vien cua hang","content":"he","type":2},{"id":45,"title":"Lam quen nhan vien cua hang","content":"hang chat luong cao","type":2},{"id":46,"title":"Lam quen nhan vien cua hang","content":"hjhhhhhhhhhhh","type":2},{"id":47,"title":"Lam quen nhan vien cua hang","content":"hiiuuu","type":2},{"id":48,"title":"Lam quen nhan vien cua hang","content":"hehe","type":2},{"id":49,"title":"Lam quen nhan vien cua hang","content":"gg","type":2}],"titleFirst":"Lam quen nhan vien cua hang","contentFirst":"Chao chi"}

// class FeedbackModel {
//   int status;
//   Data data;
//
//   FeedbackModel({
//       this.status,
//       this.data});
//
//   FeedbackModel.fromJson(dynamic json) {
//     status = json["status"];
//     data = json["data"] != null ? Data.fromJson(json["data"]) : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map["status"] = status;
//     if (data != null) {
//       map["data"] = data.toJson();
//     }
//     return map;
//   }
//
// }

/// feedback : [{"id":31,"title":"Lam quen nhan vien cua hang","content":"helo","type":2},{"id":32,"title":"Lam quen nhan vien cua hang","content":"helo","type":2},{"id":33,"title":"Lam quen nhan vien cua hang","content":"xin chao","type":2},{"id":34,"title":"Lam quen nhan vien cua hang","content":"helo","type":2},{"id":35,"title":"Lam quen nhan vien cua hang","content":"xin chao my name is Cuong","type":2},{"id":38,"title":"Lam quen nhan vien cua hang","content":"hi","type":2},{"id":41,"title":"Lam quen nhan vien cua hang","content":"wwhat","type":2},{"id":42,"title":"Lam quen nhan vien cua hang","content":"wtf","type":2},{"id":43,"title":"Lam quen nhan vien cua hang","content":"wtf","type":2},{"id":44,"title":"Lam quen nhan vien cua hang","content":"he","type":2},{"id":45,"title":"Lam quen nhan vien cua hang","content":"hang chat luong cao","type":2},{"id":46,"title":"Lam quen nhan vien cua hang","content":"hjhhhhhhhhhhh","type":2},{"id":47,"title":"Lam quen nhan vien cua hang","content":"hiiuuu","type":2},{"id":48,"title":"Lam quen nhan vien cua hang","content":"hehe","type":2},{"id":49,"title":"Lam quen nhan vien cua hang","content":"gg","type":2}]
/// titleFirst : "Lam quen nhan vien cua hang"
/// contentFirst : "Chao chi"

class FeedbackModel {
  List<FeedbackItem> feedback;
  String titleFirst;
  String contentFirst;

  FeedbackModel({
      this.feedback, 
      this.titleFirst, 
      this.contentFirst});

  FeedbackModel.fromJson(dynamic json) {
    if (json["feedback"] != null) {
      feedback = [];
      json["feedback"].forEach((v) {
        feedback.add(FeedbackItem.fromJson(v));
      });
    }
    titleFirst = json["titleFirst"];
    contentFirst = json["contentFirst"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (feedback != null) {
      map["feedback"] = feedback.map((v) => v.toJson()).toList();
    }
    map["titleFirst"] = titleFirst;
    map["contentFirst"] = contentFirst;
    return map;
  }

}

/// id : 31
/// title : "Lam quen nhan vien cua hang"
/// content : "helo"
/// type : 2

class FeedbackItem {
  int id;
  String title;
  String content;
  int type;

  FeedbackItem({
      this.id, 
      this.title, 
      this.content, 
      this.type});

  FeedbackItem.fromJson(dynamic json) {
    id = json["id"];
    title = json["title"];
    content = json["content"];
    type = json["type"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["title"] = title;
    map["content"] = content;
    map["type"] = type;
    return map;
  }

}