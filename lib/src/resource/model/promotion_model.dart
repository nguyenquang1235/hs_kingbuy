/// id : 2
/// title : "Chào mừng đến với Kingbuy"
/// image_source : "/photos/1/768-375-banner-slide-ghe-massage..jpg"
/// description : "Dù ai ở đâu cũng, đang làm gì cũng mong muốn gia đình mình ai cũng khoẻ mạnh, vui vẻ. Vì thế, chăm sóc sức khoẻ mỗi ngày cho gia đình là điều vô cùng quan trọng. Hiểu được điều đó"
/// content : "<p>D&ugrave; ai ở đ&acirc;u cũng, đang l&agrave;m g&igrave; cũng mong muốn gia đ&igrave;nh m&igrave;nh ai cũng khoẻ mạnh, vui vẻ. V&igrave; thế, chăm s&oacute;c sức khoẻ mỗi ng&agrave;y cho gia đ&igrave;nh l&agrave; điều v&ocirc; c&ugrave;ng quan trọng. Hiểu được điều đ&oacute;</p>"
/// slug : "chao-mung-den-voi-kingbuy.html"
/// is_auto_slug : 1
/// seo_title : "Chào mừng đến với Kingbuy"
/// seo_description : null
/// check_promotion : 0
/// status : 1
/// is_featured : 1
/// order : 1111
/// created_at : "2020-03-18 09:40:56"
/// updated_at : "2020-09-01 10:30:02"

class PromotionModel {
  int id;
  String title;
  String imageSource;
  String description;
  String content;
  String slug;
  int isAutoSlug;
  String seoTitle;
  dynamic seoDescription;
  int checkPromotion;
  int status;
  int isFeatured;
  int order;
  String createdAt;
  String updatedAt;

  PromotionModel(
      {this.id,
      this.title,
      this.imageSource,
      this.description,
      this.content,
      this.slug,
      this.isAutoSlug,
      this.seoTitle,
      this.seoDescription,
      this.checkPromotion,
      this.status,
      this.isFeatured,
      this.order,
      this.createdAt,
      this.updatedAt});

  PromotionModel.fromJson(dynamic json) {
    id = json["id"];
    title = json["title"];
    imageSource = json["image_source"];
    description = json["description"];
    content = json["content"];
    slug = json["slug"];
    isAutoSlug = json["is_auto_slug"];
    seoTitle = json["seo_title"];
    seoDescription = json["seo_description"];
    checkPromotion = json["check_promotion"];
    status = json["status"];
    isFeatured = json["is_featured"];
    order = json["order"];
    createdAt = json["created_at"];
    updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["title"] = title;
    map["image_source"] = imageSource;
    map["description"] = description;
    map["content"] = content;
    map["slug"] = slug;
    map["is_auto_slug"] = isAutoSlug;
    map["seo_title"] = seoTitle;
    map["seo_description"] = seoDescription;
    map["check_promotion"] = checkPromotion;
    map["status"] = status;
    map["is_featured"] = isFeatured;
    map["order"] = order;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    return map;
  }

  static List<PromotionModel> listFromJson(dynamic json) {
    List<PromotionModel> news = [];
    if (json != null) {
      news = [];
      json.forEach((v) {
        news.add(PromotionModel.fromJson(v));
      });
    }
    return news;
  }

}
