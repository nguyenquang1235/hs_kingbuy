/// id : 1
/// name : "Ghế massage"
/// image_source : "/photos/1/san-pham/GHẾ MASSAGE/ES-05A ĐỎ/ghe-massage-toan-than-OTO-Essentia-ES-05-A-1.jpg"
/// icon_source : "/photos/1/danh-muc/Group 2932.svg"
/// background_image : "/photos/1/danh-muc/768-375-danh-muc-ghe-massage.jpg"
/// parent_id : null
/// parent : null
/// children : [{"id":15,"name":"Ghế massage thương gia","image_source":"/photos/1/san-pham/GHẾ MASSAGE/Swarovski PE-09Black/ghe-massage-toan-than-oto-pe-09-kim-cuong-xam-1.jpg","icon_source":null,"background_image":null,"parent_id":1,"parent":{"id":1,"name":"Ghế massage","slug":"ghe-massage","video_link":"https://www.youtube.com/watch?v=pF2yjiSR2Ig","parent":null},"children":[],"description":null,"slug":"ghe-massage-thuong-gia","video_link":null},{"id":16,"name":"Ghế massage phổ thông","image_source":"/photos/1/san-pham/GHẾ MASSAGE/OTO EQ-09S/ghe-massage-OTO-Essentia-EQ-09.jpg","icon_source":null,"background_image":null,"parent_id":1,"parent":{"id":1,"name":"Ghế massage","slug":"ghe-massage","video_link":"https://www.youtube.com/watch?v=pF2yjiSR2Ig","parent":null},"children":[],"description":null,"slug":"ghe-massage-pho-thong","video_link":null}]
/// description : "<p style=\"text-align:center\">Kh&ocirc;ng cần phải đến ph&ograve;ng tập thể h&igrave;nh mới sử dụng được m&aacute;y rung to&agrave;n th&acirc;n, sản phẩm với gi&aacute; th&agrave;nh ph&ugrave; hợp n&ecirc;n bạn ho&agrave;n to&agrave;n c&oacute; thể sở hữu, thoải m&aacute;i d&ugrave;ng tại nh&agrave;. V&agrave; <strong>M&aacute;y rung giảm c&acirc;n Super Califit Massage CF900 </strong>l&agrave; một trong những &ldquo;si&ecirc;u phẩm&rdquo; được nhiều gia đ&igrave;nh Việt quan t&acirc;m hiện nay. Kh&ocirc;ng chỉ nhỏ gọn, sang trọng m&agrave; sản phẩm mang lại hiệu quả rất cao cho người tập. Kh&ocirc;ng mất nhiều c&ocirc;ng sức, thời gian, chi ph&iacute;, kh&ocirc;ng sợ chấn thương m&agrave; bạn vẫn c&oacute; được v&oacute;c d&aacute;ng thon gọn như &yacute;. Sản phẩm ph&ugrave; hợp mọi đối tượng sử dụng, ngay cả nam v&agrave; nữ giới.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"http://kingbuy.sapp.asia/photos/1/san-pham/RUNG TOÀN THÂN/CF900/may-rung-toan-than-super-califit-massage-cf-900-11.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\"><strong>ĐẶC ĐIỂM NỔI BẬT</strong></p>\r\n\r\n<p style=\"text-align:center\"><strong>Thiết kế b&agrave;n đứng rộng</strong></p>\r\n\r\n<p style=\"text-align:center\">M&aacute;y c&oacute; cấu tạo khung sườn bằng th&eacute;p chống gỉ s&eacute;t, chắc chắn, chịu tải trọng nặng đến 150kg. B&agrave;n đứng rộng r&atilde;i, thoải m&aacute;i th&iacute;ch hợp cho nhiều người với v&oacute;c d&aacute;ng cao &ndash; thấp, mập - ốm đều d&ugrave;ng được.</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"http://kingbuy.sapp.asia/photos/1/san-pham/RUNG TOÀN THÂN/CF900/may-rung-toan-than-super-califit-massage-cf-900-13.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"http://kingbuy.sapp.asia/photos/1/san-pham/RUNG TOÀN THÂN/CF900/may-rung-toan-than-super-califit-massage-cf-900-15.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\"><strong>Động cơ khỏe v&agrave; &ecirc;m</strong></p>\r\n\r\n<p style=\"text-align:center\">Motor với c&ocirc;ng suất 1000W cho lực rung mạnh, t&aacute;c động s&acirc;u v&agrave;o b&ecirc;n trong cơ thể đem đến hiệu quả cao gấp 3 lần c&aacute;c phương ph&aacute;p tập luyện kh&aacute;c. Đồng thời, m&aacute;y tiết kiệm điện nặng đến 40% hơn c&aacute;c loại m&aacute;y rung kh&aacute;c tr&ecirc;n thị trường. Vận h&agrave;nh rất &ecirc;m, kh&ocirc;ng tạo ra tiếng ồn l&agrave;m ảnh hưởng người xung quanh.</p>\r\n\r\n<p style=\"text-align:center\">Sau khi v&ograve;ng đai qua bụng hoặc m&ocirc;ng, đ&ugrave;i ở tư thế đứng thẳng, bạn chỉ cần k&eacute;o căng chiếc đai về ph&iacute;a m&igrave;nh v&agrave; khởi động m&aacute;y. Motor sẽ tạo ra lực rung t&aacute;c động trực tiếp đến c&aacute;c v&ugrave;ng mỡ thừa gi&uacute;p bạn cải thiện một c&aacute;ch đ&aacute;ng kể v&ograve;ng eo qu&aacute; khổ.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"http://kingbuy.sapp.asia/photos/1/san-pham/RUNG TOÀN THÂN/CF900/may-rung-toan-than-super-califit-massage-cf-900-17.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\"><strong>Hiển thị th&ocirc;ng minh</strong></p>\r\n\r\n<p style=\"text-align:center\">M&agrave;n h&igrave;nh hiển thị c&aacute;c th&ocirc;ng số: chương tr&igrave;nh tập, thời gian, tốc độ gi&uacute;p người tập dễ d&agrave;ng theo d&otilde;i qu&aacute; tr&igrave;nh sử dụng v&agrave; thay đổi cho ph&ugrave; hợp mục đ&iacute;ch.</p>\r\n\r\n<p style=\"text-align:center\"><strong>Lập tr&igrave;nh c&aacute;c b&agrave;i tập đa dạng</strong></p>\r\n\r\n<p style=\"text-align:center\">Với nhiều cường độ rung từ nhẹ đến mạnh, c&ugrave;ng c&aacute;c chương tr&igrave;nh tập đa dạng sẽ mang đến cho người tập sự thoải m&aacute;i trong l&uacute;c sử dụng. C&aacute;c b&agrave;i tập đ&atilde; được nh&agrave; nghi&ecirc;n cứu đưa ra một c&aacute;ch khoa học, an to&agrave;n v&agrave; ph&ugrave; hợp thể trạng con người.</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"http://kingbuy.sapp.asia/photos/1/san-pham/RUNG TOÀN THÂN/CF900/may-rung-toan-than-super-califit-massage-cf-900-14.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"http://kingbuy.sapp.asia/photos/1/san-pham/RUNG TOÀN THÂN/CF900/may-rung-toan-than-super-califit-massage-cf-900-12.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><strong>LỢI &Iacute;CH RUNG MASSAGE</strong></p>\r\n\r\n<ul>\r\n\t<li style=\"text-align:center\">Săn chắc c&aacute;c cơ, đ&aacute;nh tan mỡ thừa khắp c&aacute;c bộ phận từ c&aacute;nh tay, bụng, đ&ugrave;i đến m&ocirc;ng, ch&acirc;n.</li>\r\n\t<li style=\"text-align:center\">K&iacute;ch hoạt c&aacute;c khớp ch&acirc;n, tay, cơ, g&acirc;n cốt để l&agrave;m dịu chứng vi&ecirc;m, tho&aacute;i h&oacute;a.</li>\r\n\t<li style=\"text-align:center\">M&aacute;u lưu th&ocirc;ng đều hơn, ngăn ngừa bệnh l&yacute; tim mạch, thấp khấp, tho&aacute;i h&oacute;a cột sống.</li>\r\n\t<li style=\"text-align:center\">Giảm căng thẳng, mệt mỏi, tinh thần uể oải.</li>\r\n</ul>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"http://kingbuy.sapp.asia/photos/1/san-pham/RUNG TOÀN THÂN/CF900/may-rung-toan-than-super-califit-massage-cf-900-18.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"http://kingbuy.sapp.asia/photos/1/san-pham/RUNG TOÀN THÂN/CF900/may-rung-toan-than-super-califit-massage-cf-900-16.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\">M&aacute;y rung giảm c&acirc;n Super Califit Massage CF900 kh&ocirc;ng g&acirc;y t&aacute;c dụng phụ như c&aacute;c sản phẩm giảm b&eacute;o phổ biến kh&aacute;c tr&ecirc;n thị trường. Rất tiện lợi, an to&agrave;n, hiệu quả nhanh v&agrave; l&acirc;u d&agrave;i. Xứng đ&aacute;ng với sự chọn lựa của mọi gia đ&igrave;nh.</p>"
/// slug : "ghe-massage"
/// video_link : "https://www.youtube.com/watch?v=pF2yjiSR2Ig"

class CategoryModel {
  int id;
  String name;
  String imageSource;
  String iconSource;
  String backgroundImage;
  int parentId;
  CategoryModel parent;
  List<CategoryModel> children;
  String description;
  String slug;
  String videoLink;

  CategoryModel(
      {this.id,
      this.name,
      this.imageSource,
      this.iconSource,
      this.backgroundImage,
      this.parentId,
      this.parent,
      this.children,
      this.description,
      this.slug,
      this.videoLink});

  CategoryModel.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    imageSource = json["image_source"];
    iconSource = json["icon_source"];
    backgroundImage = json["background_image"];
    parentId = json["parent_id"] ?? null;
    parent =
        json["parent"] != null ? CategoryModel.fromJson(json["parent"]) : null;
    if (json["children"] != null) {
      children = [];
      json["children"].forEach((v) {
        children.add(CategoryModel.fromJson(v));
      });
    }
    description = json["description"];
    slug = json["slug"];
    videoLink = json["video_link"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["image_source"] = imageSource;
    map["icon_source"] = iconSource;
    map["background_image"] = backgroundImage;
    map["parent_id"] = parentId;
    map["parent"] = parent;
    if (children != null) {
      map["children"] = children.map((v) => v.toJson()).toList();
    }
    map["description"] = description;
    map["slug"] = slug;
    map["video_link"] = videoLink;
    return map;
  }

  static List<CategoryModel> listFromJson(dynamic json) {
    List<CategoryModel> listCategoryModel = [];
    if (json != null) {
      json.forEach((v) {
        listCategoryModel.add(CategoryModel.fromJson(v));
      });
    }
    return listCategoryModel;
  }

  @override
  bool operator ==(Object other) {
    // TODO: implement ==
    return (other is CategoryModel) &&
        other.name == name &&
        other.id == id;
  }
}
