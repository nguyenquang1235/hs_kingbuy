/// status : 1
// /// data : [{"code":"481","name_with_type":"Huyện A Lưới","delivery_status":[5],"ship_fee_bulky":0,"ship_fee_not_bulky":0},{"code":"483","name_with_type":"Huyện Nam Đông","delivery_status":[5],"ship_fee_bulky":0,"ship_fee_not_bulky":0},{"code":"476","name_with_type":"Huyện Phong Điền","delivery_status":[5],"ship_fee_bulky":0,"ship_fee_not_bulky":0},{"code":"482","name_with_type":"Huyện Phú Lộc","delivery_status":[5],"ship_fee_bulky":0,"ship_fee_not_bulky":0},{"code":"478","name_with_type":"Huyện Phú Vang","delivery_status":[5],"ship_fee_bulky":0,"ship_fee_not_bulky":0},{"code":"477","name_with_type":"Huyện Quảng Điền","delivery_status":[5],"ship_fee_bulky":0,"ship_fee_not_bulky":0},{"code":"474","name_with_type":"Thành phố Huế","delivery_status":[5],"ship_fee_bulky":0,"ship_fee_not_bulky":0},{"code":"479","name_with_type":"Thị xã Hương Thủy","delivery_status":[5],"ship_fee_bulky":0,"ship_fee_not_bulky":0},{"code":"480","name_with_type":"Thị xã Hương Trà","delivery_status":[5],"ship_fee_bulky":0,"ship_fee_not_bulky":0}]
//
// class ProvinceModel {
//   int status;
//   List<Data> data;
//
//   ProvinceModel({
//       this.status,
//       this.data});
//
//   ProvinceModel.fromJson(dynamic json) {
//     status = json["status"];
//     if (json["data"] != null) {
//       data = [];
//       json["data"].forEach((v) {
//         data.add(Data.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{};
//     map["status"] = status;
//     if (data != null) {
//       map["data"] = data.map((v) => v.toJson()).toList();
//     }
//     return map;
//   }
//
// }

/// code : "481"
/// name_with_type : "Huyện A Lưới"
/// delivery_status : [5]
/// ship_fee_bulky : 0
/// ship_fee_not_bulky : 0

class ProvinceModel {
  String code;
  String nameWithType;
  String name;
  List<int> deliveryStatus;
  int shipFeeBulky;
  int shipFeeNotBulky;

  ProvinceModel(
      {this.code,
      this.nameWithType,
        this.name,
      this.deliveryStatus,
      this.shipFeeBulky,
      this.shipFeeNotBulky});

  ProvinceModel.fromJson(dynamic json) {
    code = json["code"];
    nameWithType = json["name_with_type"];
    deliveryStatus = json["delivery_status"] != null
        ? json["delivery_status"].cast<int>()
        : [];
    name = json["name"];
    shipFeeBulky = json["ship_fee_bulky"] ?? 0;
    shipFeeNotBulky = json["ship_fee_not_bulky"] ?? 0;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["code"] = code;
    map["name_with_type"] = nameWithType;
    map["name"] = name;
    map["delivery_status"] = deliveryStatus;
    map["ship_fee_bulky"] = shipFeeBulky;
    map["ship_fee_not_bulky"] = shipFeeNotBulky;
    return map;
  }

  static List<ProvinceModel> listFromJson(dynamic json) {
    List<ProvinceModel> models = [];
    if (json != null) {
      models = [];
      json.forEach((v) {
        models.add(ProvinceModel.fromJson(v));
      });
    }
    return models;
  }
}
