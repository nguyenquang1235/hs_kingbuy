/// id : 7
/// user_id : 3
/// product_id : 15
/// name : "Minh Minh"
/// phone_number : ""
/// comment : "okie"
/// status : 1
/// created_at : "2021-04-04 21:34:00"
/// updated_at : "2021-04-04 21:34:00"
/// star : 2
/// avatar_source : "/upload/avatar/edc231617c186f9aef2cfdeb4f7a98ba.jpg"
/// is_buy : 0

class CommentModel {
  int id;
  int userId;
  int productId;
  String name;
  String phoneNumber;
  String comment;
  int status;
  String createdAt;
  String updatedAt;
  int star;
  String avatarSource;
  int isBuy;

  CommentModel(
      {this.id,
      this.userId,
      this.productId,
      this.name,
      this.phoneNumber,
      this.comment,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.star,
      this.avatarSource,
      this.isBuy});

  CommentModel.fromJson(dynamic json) {
    id = json["id"];
    userId = json["user_id"];
    productId = json["product_id"];
    name = json["name"];
    phoneNumber = json["phone_number"];
    comment = json["comment"];
    status = json["status"];
    createdAt = json["created_at"];
    updatedAt = json["updated_at"];
    star = json["star"];
    avatarSource = json["avatar_source"];
    isBuy = json["is_buy"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["user_id"] = userId;
    map["product_id"] = productId;
    map["name"] = name;
    map["phone_number"] = phoneNumber;
    map["comment"] = comment;
    map["status"] = status;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    map["star"] = star;
    map["avatar_source"] = avatarSource;
    map["is_buy"] = isBuy;
    return map;
  }

  static List<CommentModel> listFromJson(dynamic json) {
    List<CommentModel> models = [];
    if (json != null) {
      models = [];
      json.forEach((v) {
        models.add(CommentModel.fromJson(v));
      });
    }
    return models;
  }
}
