import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class AddressRepository {
  AddressRepository._();

  static AddressRepository _instance;

  factory AddressRepository() {
    if (_instance == null) _instance = AddressRepository._();
    return _instance;
  }

  Future<NetworkState<List<AddressModel>>> getAllAddress() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_ALL_ADDRESS);

      print(response.data);

      return NetworkState<List<AddressModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return AddressModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
