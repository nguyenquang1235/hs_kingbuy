import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/network_respone.dart';
import 'package:King_Buy/src/resource/model/promotion_model.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class PromotionRepository {
  PromotionRepository._();

  static PromotionRepository _instance;

  factory PromotionRepository() {
    if (_instance == null) _instance = PromotionRepository._();
    return _instance;
  }

  Future<NetworkState<List<PromotionModel>>> getPromotions({
    int limit = 10,
    int offset = 0,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {

      Map<String, dynamic> params = {
        "offset": offset,
        "limit": limit,
      };

      Response response = await AppClients().get(AppEndpoint.GET_ALL_PROMOTION, queryParameters: params);
      print(response.data);
      return NetworkState<List<PromotionModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return PromotionModel.listFromJson(data["news"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<PromotionModel>>> getMyPromotion({
    int limit = 10,
    int offset = 0,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {

      Map<String, dynamic> params = {
        "offset": offset,
        "limit": limit,
      };

      Response response = await AppClients().get(AppEndpoint.GET_ALL_MY_PROMOTION, queryParameters: params);
      print(response.data);
      return NetworkState<List<PromotionModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return PromotionModel.listFromJson(data["promotions"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
