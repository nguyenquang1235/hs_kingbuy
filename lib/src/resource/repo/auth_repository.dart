import 'dart:io';

import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/network_respone.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../resource.dart';

class AuthRepository {
  AuthRepository._();

  static AuthRepository _instance;

  factory AuthRepository() {
    if (_instance == null) _instance = AuthRepository._();
    return _instance;
  }

  Future<NetworkState<UserModel>> loginFacebook() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = AppEndpoint.LOGIN_FACEBOOK;

      final facebookLogin = FacebookLogin();
      if (await facebookLogin.isLoggedIn) await facebookLogin.logOut();

      FacebookLoginResult result = await facebookLogin.logIn(['email']);

      String token = result.accessToken.token;

      Map<String, String> param = {"access_token": token};

      Response response = await AppClients().post(api, data: param);

      return _onData<UserModel>(response);
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<UserModel>> loginGoogle() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = AppEndpoint.LOGIN_GOOGLE;

      GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);
      if (await _googleSignIn.isSignedIn()) _googleSignIn.signOut();

      GoogleSignInAccount account = await _googleSignIn.signIn();

      String token = (await account.authentication).accessToken;

      Map<String, String> param = {"access_token": token};

      Response response = await AppClients().post(api, data: param);

      return _onData<UserModel>(response);
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<UserModel>> loginIdentity(
      {String identity, String password}) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = AppEndpoint.LOGIN_IDENTITY;

      Map<String, String> param = {"identity": identity, "password": password};

      Response response = await AppClients().post(api, data: param);

      return _onData<UserModel>(response);
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<UserModel>> getProfile() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_USER_PROFILE);

      return _onData<UserModel>(response);
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<int>> register(
      {String identity, String password, String passwordConfirm}) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, String> param = {
        "identity": identity,
        "password": password,
        "password_confirm": passwordConfirm
      };

      Response response =
          await AppClients().post(AppEndpoint.REGISTER, data: param);

      print(response.data);

      return NetworkState<int>(
          status: AppEndpoint.SUCCESS,
          response: NetworkResponse.fromJson(response.data, converter: (data) {
            return data["countdown"];
          }));
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<String>> activeAccount(
      {String identity, String smsCode}) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = AppEndpoint.ACTIVE_ACCOUNT;

      Map<String, String> param = {"identity": identity, "sms_code": smsCode};

      Response response = await AppClients().post(api, data: param);
      return NetworkState<String>(
          status: AppEndpoint.SUCCESS,
          response: NetworkResponse.fromJson(response.data));
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  //Forgot pass
  Future<NetworkState<String>> sendForgotPassOTP({String identity}) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = AppEndpoint.SEND_OTP_FORGOT_PASS;

      Map<String, String> param = {"identity": identity};

      Response response = await AppClients().post(api, data: param);
      return NetworkState<String>(
          status: AppEndpoint.SUCCESS,
          response: NetworkResponse.fromJson(response.data));
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<String>> checkOtpForgotPass(
      {String identity, String smsCode}) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = AppEndpoint.CHECK_OTP_FORGOT_PASS;

      Map<String, String> param = {"identity": identity, "sms_code": smsCode};

      Response response = await AppClients().get(api, queryParameters: param);

      return NetworkState<String>(
          status: AppEndpoint.SUCCESS,
          response: NetworkResponse.fromJson(response.data));
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<String>> resetPassWhenForgot(
      {String identity,
      String smsCode,
      String password,
      String passwordConfirm}) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = AppEndpoint.CHECK_OTP_FORGOT_PASS;

      Map<String, String> param = {
        "identity": identity,
        "sms_code": smsCode,
        "password": password,
        "password_confirm": passwordConfirm
      };

      Response response = await AppClients().post(api, data: param);
      print(response.data);

      return NetworkState<String>(
          status: AppEndpoint.SUCCESS,
          response: NetworkResponse.fromJson(response.data));
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<String>> changePassword({
    String oldPassword,
    String newPassword,
    String newConfirmPassword,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = AppEndpoint.UPDATE_PASS;

      Map<String, String> param = {
        "old_password": oldPassword,
        "new_password": newPassword,
        "new_confirm_password": newConfirmPassword,
      };

      Response response = await AppClients().post(api, data: param);

      return NetworkState<String>(
          status: AppEndpoint.SUCCESS,
          response: NetworkResponse.fromJson(response.data));
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  // Future<void> logout() async {
  //   bool isDisconnect = await WifiService.isDisconnect();
  //
  //   if (isDisconnect) return NetworkState.withDisconnect();
  //
  //   try {
  //     String api = AppEndpoint.LOG_OUT;
  //
  //     Response response = await AppClients().get(api);
  //
  //     return NetworkState<void>(
  //         status: AppEndpoint.SUCCESS,
  //         response: NetworkResponse.fromJson(response.data));
  //   } on DioError catch (e) {
  //     return NetworkState.withError(e);
  //   }
  // }

  Future<NetworkState<UserModel>> updateUser({
    String name,
    String phoneNumber,
    String dateOfBirth,
    int gender,
    String email,
    String avatar,
    int isChangePassword,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = AppEndpoint.UPDATE_USER_PROFILE;

      Map<String, dynamic> params = {
        "name": name,
        "phone_number": phoneNumber,
        "date_of_birth": dateOfBirth,
        "gender": gender.toString(),
        "email": email,
      };

      Response response = await AppClients().post(api, data: params);

      print(response.data);

      return _onData<UserModel>(response);
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  // Chỉ được gọi khi kết nối thành công đến sever,
  // nếu kết nối bị dán đoạn, thì đã có phần try catch ở trên và bắn ra lỗi
  _onData<T>(Response response) {
    return NetworkState<T>(
      status: AppEndpoint.SUCCESS,
      response: NetworkResponse.fromJson(
        response.data,
        converter: (data) {
          if (data["token"] != null && data["token"] != "") {
            AppShared.setAccessToken(data["token"]);
          }
          UserModel userModel = UserModel.fromJson(data["profile"]);
          return userModel.copyWith(
              memberCardNumber: data["member_card_number"],
              rewardPoints: data["reward_points"]);
          // return userModel;
        },
      ),
    );
  }
}
