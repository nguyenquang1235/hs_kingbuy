import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class ContactRepository {
  ContactRepository._();

  static ContactRepository _instance;

  factory ContactRepository() {
    if (_instance == null) _instance = ContactRepository._();
    return _instance;
  }

  Future<NetworkState<List<ContactUsModel>>> getContactUs() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_CONTACT_US);

      return NetworkState<List<ContactUsModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          // print(data);
          return ContactUsModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<bool>> checkUserFeedback() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response =
          await AppClients().get(AppEndpoint.CHECK_FEEDBACK_OF_USER);

      print(response.data);

      return NetworkState<bool>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          // print(data);
          return data;
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState> sendContractForm(
    String fullName,
    String email,
    String phoneNumber,
    String subject,
    String body,
  ) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        "fullname": fullName,
        "email": email,
        "phone": phoneNumber,
        "subject": subject,
        "body": body
      };

      Response response =
          await AppClients().post(AppEndpoint.SEND_CONTACT_FORM, data: params);

      return NetworkState(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: null),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<FeedbackModel>> getFeedback() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_FEED_BACK);

      return NetworkState<FeedbackModel>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data,
            converter: (data) => FeedbackModel.fromJson(data)),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState> userReply(String contentRep) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {"contentRep": contentRep};

      Response response = await AppClients().post(AppEndpoint.USER_REPLY, data: params);

      print(response.data);

      return NetworkState(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: null),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
