import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/network_respone.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class SearchRepository {
  SearchRepository._();

  static SearchRepository _instance;

  factory SearchRepository() {
    if (_instance == null) _instance = SearchRepository._();
    return _instance;
  }

  Future<NetworkState<List<ProductModel>>> searchProduct({
    String keyWord,
    int limit = 10,
    int offset,
    int productCategoryId,
    int brandId,
    Map<String, dynamic> price,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        "searchWord": keyWord,
        "limit": limit,
        "offset": offset,
        "product_category_id": productCategoryId,
        "brand_id": brandId,
        "price": price
      };

      print(params);

      params.removeWhere((key, value) => (value == null));

      Response response = await AppClients().get(
        AppEndpoint.SEARCH_PRODUCTS,
        queryParameters: params,
      );

      print(response.data);

      return NetworkState<List<ProductModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ProductModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<BrandModel>>> getBrands() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(
        AppEndpoint.GET_BRANDS,
      );

      print(response.data);

      return NetworkState<List<BrandModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return BrandModel.listFromJson(data["brands"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
