import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/network_respone.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class CategoryRepository {
  CategoryRepository._();

  static CategoryRepository _instance;

  factory CategoryRepository() {
    if (_instance == null) _instance = CategoryRepository._();
    return _instance;
  }

  Future<NetworkState<List<CategoryModel>>> getAllCategory({
    int limit = 10,
    int offset = 0,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        "offset": offset,
        "limit": limit,
      };

      Response response = await AppClients()
          .get(AppEndpoint.GET_ALL_CATEGORIES, queryParameters: params);
      print(response.data);
      return NetworkState<List<CategoryModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          // return ConfigModel.fromJson(data["news"]);
          return CategoryModel.listFromJson(data["categories"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
