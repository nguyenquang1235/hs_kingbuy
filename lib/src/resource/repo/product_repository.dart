import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class ProductRepository {
  ProductRepository._();

  static ProductRepository _instance;

  factory ProductRepository() {
    if (_instance == null) _instance = ProductRepository._();
    return _instance;
  }

  Future<NetworkState<List<ProductModel>>> getViewedProducts({
    int limit = 10,
    int offset,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {"offset": offset, "limit": limit};

      Response response = await AppClients()
          .get(AppEndpoint.GET_VIEWED_PRODUCTS, queryParameters: params);

      return NetworkState<List<ProductModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ProductModel.listFromJson(data["products"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ProductModel>>> getProductByIds(
      {List<int> listId}) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {"product_ids": listId};

      Response response = await AppClients()
          .get(AppEndpoint.GET_PRODUCT_BY_ID, queryParameters: params);

      print(response.data);

      return NetworkState<List<ProductModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ProductModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ProductModel>>> getNewProducts({
    int limit = 6,
    int offset,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {"offset": offset, "limit": limit};

      Response response = await AppClients()
          .get(AppEndpoint.GET_NEW_PRODUCTS, queryParameters: params);

      return NetworkState<List<ProductModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ProductModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ProductModel>>> getRelatedProduct({
    int productId,
    int limit = 6,
    int offset,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {"offset": offset, "limit": limit,};

      Response response = await AppClients().get(
          AppEndpoint.GET_RELATED_PRODUCT+"/$productId",
          queryParameters: params);

      print(response.request.uri);

      return NetworkState<List<ProductModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ProductModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ProductModel>>> getSellingProducts({
    int limit = 6,
    int offset,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {"offset": offset, "limit": limit};

      Response response = await AppClients()
          .get(AppEndpoint.GET_SELLING_PRODUCTS, queryParameters: params);

      return NetworkState<List<ProductModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ProductModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ProductModel>>> getProductByCategory({
    String keyWord,
    int limit = 6,
    int offset,
    int categoryId,
    int brandId,
    Map<String, dynamic> price,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        "searchWord": keyWord,
        "limit": limit,
        "offset": offset,
        "product_category_id": categoryId,
        "brand_id": brandId,
        "price": price
      };

      params.removeWhere((key, value) => (value == null));

      Response response = await AppClients()
          .get(AppEndpoint.GET_PRODUCTS_BY_CATEGORY, queryParameters: params);

      return NetworkState<List<ProductModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ProductModel.listFromJson(data["products"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<ProductModel>> getSingleProduct({int productId}) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {"product_id": productId};

      Response response = await AppClients()
          .get(AppEndpoint.GET_SINGLE_PRODUCT, queryParameters: params);

      return NetworkState<ProductModel>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ProductModel.fromJson(data["products"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
