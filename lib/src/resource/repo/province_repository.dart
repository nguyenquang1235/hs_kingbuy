import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class ProvinceRepository {
  ProvinceRepository._();

  static ProvinceRepository _instance;

  factory ProvinceRepository() {
    if (_instance == null) _instance = ProvinceRepository._();
    return _instance;
  }

  Future<NetworkState<List<ProvinceModel>>> getAllProvince() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_All_PROVINCE);

      return NetworkState<List<ProvinceModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          // print(data);
          return ProvinceModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ProvinceModel>>> getDistrict(String provinceCode) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = "${AppEndpoint.GET_DISTRICT_BY_PROVINCE_CODE}/$provinceCode";

      Response response = await AppClients().get(api);

      return NetworkState<List<ProvinceModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ProvinceModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ProvinceModel>>> getWard(String districtCode) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      String api = "${AppEndpoint.GET_WARD_BY_DISTRICT_CODE}/$districtCode";

      Response response = await AppClients().get(api);

      return NetworkState<List<ProvinceModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ProvinceModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
