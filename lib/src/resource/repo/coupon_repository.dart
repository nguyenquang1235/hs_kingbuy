import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/network_respone.dart';
import 'package:King_Buy/src/resource/model/promotion_model.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class CouponRepository {
  CouponRepository._();

  static CouponRepository _instance;

  factory CouponRepository() {
    if (_instance == null) _instance = CouponRepository._();
    return _instance;
  }

  Future<NetworkState<CouponApplication>> getMyCoupon() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_MY_COUPONS);
      return NetworkState<CouponApplication>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return CouponApplication.fromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<CouponModel>> getDetails({int couponId}) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {"coupon_id": couponId};

      Response response = await AppClients()
          .get(AppEndpoint.GET_COUPON_DETAILS, queryParameters: params);

      return NetworkState<CouponModel>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return CouponModel.fromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
