import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/invoice_model.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class InvoiceRepository {
  InvoiceRepository._();

  static InvoiceRepository _instance;

  factory InvoiceRepository() {
    if (_instance == null) _instance = InvoiceRepository._();
    return _instance;
  }

  Future<NetworkState<List<InvoiceModel>>> getInvoiceHistory({
    int limit = 10,
    int offset,
    int filter,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {

      Map<String, dynamic> params = {
        "filter": filter,
        "offset":offset,
        "limit":limit
      };

      Response response =
          await AppClients().get(AppEndpoint.GET_INVOICE_HISTORY, queryParameters: params);

      return NetworkState<List<InvoiceModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return InvoiceModel.listFromJson(data["rows"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
