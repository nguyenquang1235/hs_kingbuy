import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class DeliveryRepository {
  DeliveryRepository._();

  static DeliveryRepository _instance;

  factory DeliveryRepository() {
    if (_instance == null) _instance = DeliveryRepository._();
    return _instance;
  }

  Future<NetworkState<List<DeliveryAddressModel>>> getDeliveryAddress() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response =
          await AppClients().get(AppEndpoint.GET_DELIVERY_ADDRESS);

      return NetworkState<List<DeliveryAddressModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return DeliveryAddressModel.listFromJson(data["rows"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<DeliveryAddressModel>>> changeDeliveryAddress({
    int deliveryAddressId,
    String fullName,
    String firstPhone,
    String secondPhone,
    String provinceCode,
    String districtCode,
    String wardCode,
    String address,
    int isDefault,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        "delivery_address_id": deliveryAddressId,
        "fullname": fullName,
        "first_phone": firstPhone,
        "second_phone": secondPhone,
        "province_code": provinceCode,
        "district_code": districtCode,
        "ward_code": wardCode,
        "address": address,
        "is_default": isDefault,
        "is_export_invoice": 0,
      };

      Response response = await AppClients()
          .post(AppEndpoint.UPDATE_DELIVERY_ADDRESS, data: params);

      return NetworkState<List<DeliveryAddressModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse<List<DeliveryAddressModel>>.fromJson(
            response.data,
            converter: (data) =>
                DeliveryAddressModel.listFromJson(data["rows"])),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<DeliveryAddressModel>>> createDeliveryAddress({
    String fullName,
    String firstPhone,
    String secondPhone,
    String provinceCode,
    String districtCode,
    String wardCode,
    String address,
    int isDefault,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      print(isDefault);

      Map<String, dynamic> params = {
        "fullname": fullName,
        "first_phone": firstPhone,
        "second_phone": secondPhone,
        "province_code": provinceCode,
        "district_code": districtCode,
        "ward_code": wardCode,
        "address": address,
        "is_default": isDefault,
        "is_export_invoice": 0,
      };

      Response response = await AppClients()
          .post(AppEndpoint.CREATE_DELIVERY_ADDRESS, data: params);

      return NetworkState<List<DeliveryAddressModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse<List<DeliveryAddressModel>>.fromJson(
            response.data,
            converter: (data) =>
                DeliveryAddressModel.listFromJson(data["rows"])),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
