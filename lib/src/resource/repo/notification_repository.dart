import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/network_respone.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class NotificationRepository {
  NotificationRepository._();

  static NotificationRepository _instance;

  factory NotificationRepository() {
    if (_instance == null) _instance = NotificationRepository._();
    return _instance;
  }

  Future<NetworkState<List<NotificationModel>>> getNotification({
    int limit = 10,
    int offset = 0,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        "offset": offset,
        "limit": limit,
      };

      Response response = await AppClients()
          .get(AppEndpoint.GET_NOTIFICATIONS, queryParameters: params);
      return NetworkState<List<NotificationModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          print(data["notifications"]);
          return NotificationModel.listFromJson(data["notifications"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<int>> getCountNotification() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response =
          await AppClients().get(AppEndpoint.COUNT_NOTIFICATIONS);

      print(response.data);
      return NetworkState<int>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
