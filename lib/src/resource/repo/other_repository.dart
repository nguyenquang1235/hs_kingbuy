import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/config_model.dart';
import 'package:King_Buy/src/resource/model/network_respone.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class OtherRepository {
  OtherRepository._();

  static OtherRepository _instance;

  factory OtherRepository() {
    if (_instance == null) _instance = OtherRepository._();
    return _instance;
  }

  Future<NetworkState<ConfigModel>> getConfigs() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.APP_CONFIGS);

      return NetworkState<ConfigModel>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return ConfigModel.fromJson(data["app_configs"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<Commitments>>> getCommitments() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_COMMITMENTS);

      return NetworkState<List<Commitments>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return Commitments.listFromJson(data["rows"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<PopupModel>> getPopups() async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_POPUPS);
      return NetworkState<PopupModel>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          return PopupModel.fromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
