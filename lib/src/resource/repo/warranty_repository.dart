import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class WarrantyRepository {
  WarrantyRepository._();

  static WarrantyRepository _instance;

  factory WarrantyRepository() {
    if (_instance == null) _instance = WarrantyRepository._();
    return _instance;
  }

  Future<NetworkState<List<WarrantyModel>>> getWarrantyData(
      {String imei}) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> param = {"imei": imei};

      Response response = await AppClients()
          .get(AppEndpoint.GET_WARRANTY, queryParameters: param);

      return NetworkState<List<WarrantyModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          // print(data);
          return WarrantyModel.listFromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
