import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:dio/dio.dart';

import '../resource.dart';

class RatingRepository {
  RatingRepository._();

  static RatingRepository _instance;

  factory RatingRepository() {
    if (_instance == null) _instance = RatingRepository._();
    return _instance;
  }

  Future<NetworkState<RatingInfoModel>> getRatingInfo(int productId) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {"product_id": productId};

      Response response = await AppClients()
          .get(AppEndpoint.GET_RATING_INFO, queryParameters: params);

      print(response.data);

      return NetworkState<RatingInfoModel>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          // print(data);
          return RatingInfoModel.fromJson(data);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<CommentModel>>> getComment(int productId) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {"product_id": productId};

      Response response = await AppClients()
          .get(AppEndpoint.GET_REVIEW, queryParameters: params);

      print("response.data: ${response.data}");

      return NetworkState<List<CommentModel>>(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: (data) {
          // print(data);
          return CommentModel.listFromJson(data["rows"]);
        }),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState> userRating(
  {int productId,
    String name,
    String phoneNumber,
    String comment,
    int rating,}
  ) async {
    bool isDisconnect = await WifiService.isDisconnect();

    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        "product_id": productId,
        "name": name,
        "phone_number": phoneNumber,
        "rating": rating,
        "comment":comment,
      };

      params.removeWhere((key, value) => (value == null));

      Response response = await AppClients()
          .post(AppEndpoint.USER_RATING, data: params);

      print("response.data: ${response.data}");

      return NetworkState(
        status: AppEndpoint.SUCCESS,
        response: NetworkResponse.fromJson(response.data, converter: null),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
