import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:get/get.dart';
import '../presentation.dart';

class WarrantyScreen extends StatefulWidget {
  @override
  _WarrantyScreenState createState() => _WarrantyScreenState();
}

class _WarrantyScreenState extends State<WarrantyScreen> with ResponsiveWidget {
  WarrantyViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<WarrantyViewModel>(
      viewModel: WarrantyViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, appbar) {
        return SafeArea(
          child: Scaffold(
            body: Container(
              child: Column(
                children: [
                  _buildAppbar(),
                  Expanded(child: buildUi(context: context)),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildScreen() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildCheckImageField(),
          Container(
            decoration: BoxDecoration(
                border: Border.all(width: 0.5, color: Colors.grey[200])),
          ),
          Expanded(
              child: StreamBuilder<bool>(
                  stream: _viewModel.loadingSubject,
                  builder: (context, snapshot) {
                    bool isLoading = snapshot.data ?? false;
                    return WidgetLoadingPage(
                      isLoading,
                      page: _buildListWarranty(),
                    );
                  })),
        ],
      ),
    );
  }

  Widget _buildCheckImageField() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        children: [
          _buildInputField(),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: _buildButtonSubmit(),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget _buildInputField() {
    return WidgetInput(
      hint: AppUtils.translate("product_imei", context),
      elevation: 2,
      radiusBorder: 10,
      onChanged: _viewModel.onChangeText,
      inputType: TextInputType.number,
      inputController: _viewModel.checkController,
    );
  }

  Widget _buildButtonSubmit() {
    return StreamBuilder<bool>(
        stream: _viewModel.availableCheck,
        builder: (context, snapshot) {
          bool isConfirm = snapshot.data ?? false;
          return PhysicalModel(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            elevation: 2,
            color: (isConfirm) ? Colors.red : Colors.grey,
            child: Container(
              height: 50,
              child: WidgetButtonGradient(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                colorStart: (isConfirm) ? Colors.redAccent : Colors.grey,
                colorEnd: (isConfirm) ? Colors.red : Colors.grey,
                title: "check",
                action: () {
                  if (isConfirm) {
                    _viewModel.submit();
                  }
                },
              ),
            ),
          );
        });
  }

  Widget _buildListWarranty() {
    if (_viewModel.isShow) {
      if (_viewModel.model.length > 0) {
        return ListView(
          physics: BouncingScrollPhysics(),
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
          children: List.generate(_viewModel.model.length,
              (index) => _buildWarrantyItem(index, _viewModel.model[index])),
        );
      }
      return _buildNoData();
    }
    return Container();
  }

  Widget _buildNoData() {
    return Container(
      height: Get.height,
      width: Get.width,
      child: Center(
        child: Text(AppUtils.translate("no_data", context)),
      ),
    );
  }

  Widget _buildWarrantyItem(index, WarrantyModel data) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: _getColor(data.status),
                        child: Center(
                            child: Icon(
                          FlutterIcons.md_sync_ion,
                          color: Colors.white,
                        )),
                      ),
                    ],
                  ),
                  DottedLine(
                    direction: Axis.vertical,
                    lineLength: Get.width * 0.5 + 10,
                    lineThickness: 1.0,
                    dashLength: 4.0,
                    dashColor: Colors.black,
                    dashRadius: 0.0,
                    dashGapLength: 4.0,
                    dashGapColor: Colors.transparent,
                    dashGapRadius: 0.0,
                  )
                ],
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "${AppUtils.translate("times", context)} ${index + 1}: ${AppUtils.convertString2String(data.dateReceived, outputFormat: "dd/MM/yyyy", inputFormat: "yyyy-MM-ddThh:mm:ss")}",
                      style: AppStyles.DEFAULT_LARGE_BOLD
                          .copyWith(color: Colors.red),
                    ),
                    Card(
                      color: Colors.white,
                      elevation: 2,
                      child: Container(
                        constraints: BoxConstraints(
                          maxHeight: Get.width * 0.5,
                        ),
                        padding: EdgeInsets.all(8),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            _buildItemField("status", data.statusOfReceipt),
                            _buildItemField(
                                "warranty_expenses", data.totalAmountFc2),
                            _buildItemField(
                                "warranty_status",
                                AppUtils.translate(
                                    "warranty_status_${data.status}", context)),
                            _buildItemField(
                                "note",
                                data.comment ??
                                    AppUtils.translate("no", context)),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  ///Tình trạng: Sản phẩm bị móp,...
  Widget _buildItemField(String key, String value, {Color valueColor}) {
    return Row(
      children: [
        Text(
          AppUtils.translate(key, context),
          style: AppStyles.DEFAULT_MEDIUM_BOLD,
        ),
        Expanded(
            child: Text(
          value,
          textAlign: TextAlign.right,
          style: AppStyles.DEFAULT_MEDIUM
              .copyWith(color: valueColor ?? Colors.black),
        ))
      ],
    );
  }

  Color _getColor(int status) {
    switch (status) {
      case 1:
        return Colors.redAccent;
      case 2:
        return Colors.grey;
      case 3:
        return Colors.grey;
      case 4:
        return Colors.green;
      default:
        return Colors.grey;
    }
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "check_warranty",
      colorButton: Colors.white,
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
