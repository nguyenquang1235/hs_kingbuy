import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';

class WarrantyViewModel extends BaseViewModel {
  final TextEditingController checkController = TextEditingController();
  final BehaviorSubject<bool> availableCheck = BehaviorSubject<bool>();
  bool get available => availableCheck.value;

  bool isShow = false;

  List<WarrantyModel> rawData = [
    WarrantyModel(
        statusOfReceipt: "máy bình",
        datePurchase: "2021-01-22T16:48:31",
        dateReceived: "2021-01-27T16:48:31",
        comment: "sửa gấp",
        wrTypeName: "Bảo hành",
        totalAmountFc2: "Miễn phí",
        productCode: "97448333",
        productName: "Phụ kiện máy rửa xe Karcher - Đầu phun ngắn VP-160S",
        statusId: "20A387047",
        status: 1,
        statusName: "Chưa xử lý"),
    WarrantyModel(
        statusOfReceipt: "máy bình thường",
        datePurchase: "2021-01-28T16:48:31",
        dateReceived: "2021-01-29T16:48:31",
        comment: "sửa gấp nhé",
        wrTypeName: "Bảo hành",
        totalAmountFc2: "Miễn phí",
        productCode: "97448333",
        productName: "Phụ kiện máy rửa xe Karcher - Đầu phun ngắn VP-160S",
        statusId: "20A387047",
        status: 2,
        statusName: "Đang thực hiện"),
    WarrantyModel(
        statusOfReceipt: "máy bình",
        datePurchase: "2021-01-10T16:48:31",
        dateReceived: "2021-01-15T16:48:31",
        comment: "sửa gấp",
        wrTypeName: "Bảo hành",
        totalAmountFc2: "Miễn phí",
        productCode: "97448333",
        productName: "Phụ kiện máy rửa xe Karcher - Đầu phun ngắn VP-160S",
        statusId: "20A387047",
        status: 3,
        statusName: "Đã sửa xong"),
    WarrantyModel(
        statusOfReceipt: "máy bình",
        datePurchase: "2021-01-10T16:48:31",
        dateReceived: "2021-01-15T16:48:31",
        comment: "sửa gấp",
        wrTypeName: "Bảo hành",
        totalAmountFc2: "Miễn phí",
        productCode: "97448333",
        productName: "Phụ kiện máy rửa xe Karcher - Đầu phun ngắn VP-160S",
        statusId: "20A387047",
        status: 4,
        statusName: "Đã sửa xong"),
  ];

  List<WarrantyModel> model;

  init() async {}

  getWarranty() async {
    try {
      setLoading(true);
      NetworkState<List<WarrantyModel>> networkState =
          await warrantyRepository.getWarrantyData(imei: checkController.text);
      model = networkState.data;
      setLoading(false);
    } catch (e) {
      print(e);
      setLoading(false);
      return;
    }
  }

  void submit() async {
    unFocus();
    await getWarranty();
    isShow = true;
    notifyListeners();
  }

  void onChangeText(String value) {
    if (value != "") {
      availableCheck.add(true);
    } else {
      availableCheck.add(false);
    }
  }
}
