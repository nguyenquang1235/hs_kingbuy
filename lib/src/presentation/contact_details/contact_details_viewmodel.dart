import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/feedback_model.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_shared.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:rxdart/rxdart.dart';

class ContactDetailsViewModel extends BaseViewModel {

  FeedbackModel model;
  UserModel userModel;

  final ScrollController scrollController = ScrollController();
  final GlobalKey<WidgetLoadMoreWrapVerticalState2> keyListView = GlobalKey();
  final TextEditingController contentController = TextEditingController();
  final BehaviorSubject<bool> availableSend = BehaviorSubject<bool>();
  bool get available => availableSend.value;

  init() async {
    setLoading(true);
    model = await getFeedback();
    userModel = await AppShared.getUser();
    setLoading(false);
    await Future.delayed(const Duration(milliseconds: 50));
    scrollController.animateTo(scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 1000),
        curve: Curves.fastLinearToSlowEaseIn);
  }

  Future<List<FeedbackItem>> getListFeedback() async {
    FeedbackModel model = await getFeedback();
    return model.feedback;
  }

  Future<FeedbackModel> getFeedback() async {
    try {
      NetworkState networkState = await contactRepository.getFeedback();
      return networkState.data;
    } catch (e) {
      print(e);
      return null;
    }
  }

  void userReply()async{
    try {
      NetworkState networkState = await contactRepository.userReply(contentController.text);
      if(networkState.isSuccess){
        if(networkState.response.isSuccess){
          model = await getFeedback();
          notifyListeners();
          await Future.delayed(const Duration(milliseconds: 50));
          scrollController.animateTo(scrollController.position.maxScrollExtent,
              duration: const Duration(milliseconds: 1000),
              curve: Curves.fastLinearToSlowEaseIn);
        }else{
          Toast.show(networkState.response.message, context);
        }
      }
      else{
        Toast.show(AppUtils.translate("can_not_send", context), context);
      }
    } catch (e) {
      print(e);
      Toast.show(AppUtils.translate("system_errors", context), context);
    }
  }

  void onChangeText(String value){
    if(value != ""){
      availableSend.add(true);
    }else{
      availableSend.add(false);
    }
  }

  void onReply(bool isConfirm){
    if (isConfirm) {
      unFocus();
      availableSend.add(false);
      userReply();
      contentController.text = "";
    }
  }

}
