import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/widgets/widget_avatar.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class ContactDetailsScreen extends StatefulWidget {
  final UserContactViewModel viewModel;

  const ContactDetailsScreen({Key key, this.viewModel}) : super(key: key);

  @override
  _ContactDetailsScreenState createState() => _ContactDetailsScreenState();
}

class _ContactDetailsScreenState extends State<ContactDetailsScreen>
    with ResponsiveWidget {
  ContactDetailsViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ContactDetailsViewModel>(
      viewModel: ContactDetailsViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return StreamBuilder<bool>(
            stream: _viewModel.loadingSubject,
            builder: (context, snapshot) {
              bool isLoading = snapshot.data ?? true;
              return WidgetLoadingPage(
                isLoading,
                page: Column(
                  children: [
                    _buildAppbar(),
                    Expanded(child: buildUi(context: context))
                  ],
                ),
              );
            });
      },
    );
  }

  Widget _buildScreen() {
    return Container(
      color: Colors.grey[200],
      child: SingleChildScrollView(
        controller: _viewModel.scrollController,
        physics: BouncingScrollPhysics(),
        padding: const EdgeInsets.only(top: 16, left: 10, right: 10),
        child: _viewModel.userModel != null
            ? Card(
                elevation: 2,
                child: Container(
                  color: Colors.white,
                  child: Column(
                    children: [
                      _buildTitleHeader(),
                      _buildChatBody(),
                    ],
                  ),
                ),
              )
            : Container(
                height: Get.height,
                child: Center(
                  child: Text(AppUtils.translate("no_data", context)),
                ),
              ),
      ),
    );
  }

  Widget _buildTitleHeader() {
    return Card(
      margin: const EdgeInsets.all(0),
      elevation: 2,
      child: Container(
        color: Colors.redAccent,
        padding: const EdgeInsets.all(14),
        child: Row(
          children: [
            Text("-",
                style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.white)),
            SizedBox(
              width: 15,
            ),
            Text(
              _viewModel.model.titleFirst,
              style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.white),
              maxLines: 1,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildChatBody() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 12),
      child: Column(
        children: [
          _buildChatItem(_viewModel.model.contentFirst),
          _buildListChatData(),
          _buildContentField(),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [Expanded(child: SizedBox()), _buildSaveButton()],
          )
        ],
      ),
    );
  }

  Widget _buildListChatData() {
    return Column(
      children: List.generate(_viewModel.model.feedback.length,
          (index) => _buildChatItem(_viewModel.model.feedback[index].content, isUser: _viewModel.model.feedback[index].type == 2)),
    );
  }

  Widget _buildChatItem(String text, {bool isUser = true}) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: [
          isUser
              ? WidgetAvatar(radius: 25, url: _viewModel.userModel.avatarSource)
              : CircleAvatar(
                  radius: 25,
                  backgroundColor: Colors.redAccent,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 22,
                    child: Icon(
                      Icons.admin_panel_settings_outlined,
                      color: Colors.redAccent,
                    ),
                  ),
                ),
          SizedBox(
            width: 10,
          ),
          Expanded(
              child: Text(
            text,
            style: AppStyles.DEFAULT_MEDIUM,
            textAlign: TextAlign.start,
          ))
        ],
      ),
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "contact_details",
      colorButton: Colors.white,
    );
  }

  Widget _buildContentField() {
    return Container(
      width: Get.width,
      decoration: BoxDecoration(
        border: Border.all(width: 0.5, color: AppColors.grey),
        color: Colors.grey[200],
      ),
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("content", context),
            style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.grey),
          ),
          TextFormField(
            keyboardType: TextInputType.text,
            controller: _viewModel.contentController,
            style: AppStyles.DEFAULT_MEDIUM,
            maxLines: 7,
            textAlign: TextAlign.left,
            decoration: InputDecoration.collapsed(),
            onChanged: _viewModel.onChangeText,
            onEditingComplete: () => _viewModel.onReply(_viewModel.available),
          ),
        ],
      ),
    );
  }

  Widget _buildSaveButton() {
    return StreamBuilder<bool>(
        stream: _viewModel.availableSend,
        builder: (context, snapshot) {
          bool isConfirm = snapshot.data ?? false;
          return PhysicalModel(
            borderRadius: BorderRadius.all(Radius.circular(100)),
            elevation: 2,
            color: (isConfirm) ? Colors.red : Colors.grey,
            child: Container(
              height: 40,
              child: WidgetButtonGradient(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                colorStart: (isConfirm) ? Colors.redAccent : Colors.grey,
                colorEnd: (isConfirm) ? Colors.red : Colors.grey,
                title: "reply",
                action: () {
                  _viewModel.onReply(isConfirm);
                },
              ),
            ),
          );
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
