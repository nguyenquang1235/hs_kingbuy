import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:flutter/material.dart';

import 'navigation/navigation_screen.dart';

class Routers {
  static const String navigation = "/navigation";
  static const String login_navigation = "/login_navigation";
  static const String otp = "/otp";
  static const String register_user_profile = "/register_user_profile";
  static const String reset_pass = "/reset_pass";
  static const String promotion = "/promotion";
  static const String user_profile = "/user_profile";
  static const String commitments = "/commitments";
  static const String terms_of_use = "/terms_of_use";
  static const String invoice_history = "/invoice_history";
  static const String invoice_details = "/invoice_details";
  static const String delivery_address = "/delivery_address";
  static const String change_delivery_address = "/change_delivery_address";
  static const String contact = "/contact";
  static const String user_contact = "/user_contact";
  static const String coupons = "/coupons";
  static const String coupon_details = "/coupon_details";
  static const String warranty = "/warranty";
  static const String viewed_products = "/viewed_products";
  static const String search = "/search";
  static const String search_filter = "/search_filter";
  static const String category_details = "/category_details";
  static const String product_details = "/product_details";
  static const String create_rating = "/create_rating";
  static const String cart = "/cart";
  static const String choose_delivery_address = "/choose_delivery_address";
  static const String cart_coupons = "/cart_coupons";
  static const String cart_payment = "/cart_payment";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    var arguments = settings.arguments;
    switch (settings.name) {
      case navigation:
        return animRoute(NavigationScreen(), name: navigation);
        break;
      case login_navigation:
        return animRoute(
          LoginNavigationScreen(
            router: arguments,
          ),
          name: login_navigation,
          beginOffset: _bottom,
        );
        break;
      case otp:
        return animRoute(
            OtpScreen(
              otpScreenArguments: arguments,
            ),
            name: otp,
            beginOffset: _right);
      case register_user_profile:
        return animRoute(RegisterUserProfileScreen(),
            name: register_user_profile, beginOffset: _right);
        break;
      case reset_pass:
        return animRoute(
            ResetPasswordScreen(
              resetPassArgument: arguments,
            ),
            name: reset_pass,
            beginOffset: _right);
        break;
      case promotion:
        return animRoute(PromotionsScreen(),
            name: promotion, beginOffset: _right);
        break;
      case user_profile:
        return animRoute(UserProfileScreen(),
            name: user_profile, beginOffset: _right);
        break;
      case commitments:
        return animRoute(CommitmentsScreen(),
            name: commitments, beginOffset: _right);
        break;
      case terms_of_use:
        return animRoute(TermsOfUseScreen(),
            name: terms_of_use, beginOffset: _right);
        break;
      case invoice_history:
        return animRoute(InvoiceHistoryScreen(),
            name: invoice_history, beginOffset: _right);
        break;
      case invoice_details:
        return animRoute(
            InvoiceDetailsScreen(
              param: arguments,
            ),
            name: invoice_details,
            beginOffset: _right);
        break;
      case delivery_address:
        return animRoute(DeliveryAddressScreen(),
            name: delivery_address, beginOffset: _right);
        break;
      case change_delivery_address:
        return animRoute(
            ChangeDeliveryAddressScreen(
              params: arguments,
            ),
            name: change_delivery_address,
            beginOffset: _right);
        break;
      case contact:
        return animRoute(ContactScreen(), name: contact, beginOffset: _right);
        break;
      case user_contact:
        return animRoute(
            UserContactScreen(
              isSendFeedback: arguments,
            ),
            name: user_contact,
            beginOffset: _right);
        break;
      case coupons:
        return animRoute(CouponsScreen(), name: coupons, beginOffset: _right);
        break;
      case coupon_details:
        return animRoute(
            CouponDetails(
              couponId: arguments,
            ),
            name: coupon_details,
            beginOffset: _right);
        break;
      case warranty:
        return animRoute(WarrantyScreen(), name: warranty, beginOffset: _right);
        break;
      case viewed_products:
        return animRoute(ViewedProductsScreen(),
            name: viewed_products, beginOffset: _right);
        break;
      case search:
        return animRoute(SearchScreen(), name: search, beginOffset: _center);
        break;
      case search_filter:
        return animRoute(
            SearchFilterScreen(
              searchArgument: arguments,
            ),
            name: search_filter,
            beginOffset: _center);
      case category_details:
        return animRoute(
            CategoryDetailsScreen(
              arguments: arguments,
            ),
            name: category_details,
            beginOffset: _bottom);
        break;
      case product_details:
        return animRoute(
            ProductDetailsScreen(
              model: arguments,
            ),
            name: product_details,
            beginOffset: _right);
        break;
      case create_rating:
        return animRoute(
            CreateRatingScreen(
              productId: arguments,
            ),
            name: create_rating,
            beginOffset: _right);
        break;
      case cart:
        return animRoute(CartScreen(), name: cart, beginOffset: _right);
      case choose_delivery_address:
        return animRoute(ChooseDeliveryAddress(),
            name: choose_delivery_address, beginOffset: _center);
        break;
      case cart_coupons:
        return animRoute(
            CartCouponsScreen(
              minPrice: arguments,
            ),
            name: cart_coupons,
            beginOffset: _right);
        break;
      case cart_payment:
        return animRoute(
            CartPaymentScreen(),
            name: cart_payment,
            beginOffset: _right);
        break;
      default:
        return animRoute(Container(
            child:
                Center(child: Text('No route defined for ${settings.name}'))));
    }
  }

  static Route animRoute(Widget page,
      {Offset beginOffset, String name, Object arguments}) {
    return PageRouteBuilder(
      settings: RouteSettings(name: name, arguments: arguments),
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = beginOffset ?? Offset(0.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  static Offset _center = Offset(0.0, 0.0);
  static Offset _top = Offset(0.0, 1.0);
  static Offset _bottom = Offset(0.0, -1.0);
  static Offset _left = Offset(-1.0, 0.0);
  static Offset _right = Offset(1.0, 0.0);
}
