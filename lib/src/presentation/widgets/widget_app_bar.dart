import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WidgetAppBar extends StatelessWidget {
  final Color colorButton;
  final Color colorTitle;
  final double sizeLeading;
  final String keyTitle;
  final Function actionBack;
  final Color bgColor;
  final bool trans;
  final bool isLeading;
  final Widget supWidget;
  final double titleSize;
  final double height;

  const WidgetAppBar(
      {this.colorButton,
        this.height,
      this.titleSize,
      this.bgColor,
      this.colorTitle = Colors.white,
      this.sizeLeading = 30,
      this.keyTitle,
      this.trans = true,
      this.isLeading = true,
      this.actionBack,
      this.supWidget});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: PhysicalModel(
        color: Colors.transparent,
        elevation: 4,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          color: bgColor ?? Colors.blue,
          width: Get.width,
          height: height ?? AppValues.HEIGHT_APP_BAR,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              if (isLeading)
                WidgetButtonBack(
                  action: actionBack,
                  size: sizeLeading,
                  color: colorButton ?? Colors.black,
                  bgColor: bgColor ?? AppColors.primary,
                ),
              SizedBox(
                width: 15,
              ),
              keyTitle == null
                  ? const SizedBox()
                  : Expanded(
                      child: Text(
                        trans
                            ? AppUtils.translate(keyTitle, context)
                            : keyTitle,
                        style: AppStyles.DEFAULT_MEDIUM.copyWith(
                            color: colorTitle ?? Colors.white,
                            fontSize: titleSize ?? 22),
                        maxLines: 2,
                      ),
                    ),
              supWidget ??
                  Opacity(
                    opacity: 0,
                    child: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: colorButton,
                        size: sizeLeading,
                      ),
                    ),
                  )
            ],
          ),
        ),
      ),
    );
  }
}

class WidgetButtonBack extends StatelessWidget {
  final Color bgColor;
  final Color color;
  final double size;
  final action;

  const WidgetButtonBack({this.color, this.size, this.action, this.bgColor});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Icon(
        Icons.arrow_back_rounded,
        color: color ?? Colors.white,
        size: size ?? 30,
      ),
      onTap: action ?? () => Navigator.pop(context),
    );
  }
}
