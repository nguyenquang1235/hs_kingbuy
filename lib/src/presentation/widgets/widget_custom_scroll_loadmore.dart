//
// import 'package:flutter/material.dart';
// import 'package:flutter_app/src/configs/constanst/constants.dart';
// import 'package:flutter_app/src/presentation/widgets/widget_app_bar.dart';
//
// import '../presentation.dart';
//
// class WidgetCustomScrollLoadMore<T> extends StatefulWidget {
//   WidgetCustomScrollLoadMore.build(
//       {Key key,
//         @required this.itemBuilder,
//         @required this.dataRequester,
//         @required this.initRequester,
//         this.styleError,
//         this.loadingColor,
//         this.widgetError,
//         this.actions,
//         this.title,
//         this.isShimmer = true})
//       : assert(itemBuilder != null),
//         assert(dataRequester != null),
//         assert(initRequester != null),
//         super(key: key);
//
//   final List<Widget> actions;
//   final String title;
//   final TextStyle styleError;
//   final ItemBuilder itemBuilder;
//   final DataRequester dataRequester;
//   final InitRequester initRequester;
//   final Color loadingColor;
//   final Widget widgetError;
//   final bool isShimmer;
//
//   @override
//   WidgetCustomScrollLoadMoreState createState() =>
//       WidgetCustomScrollLoadMoreState<T>();
// }
//
// class WidgetCustomScrollLoadMoreState<T>
//     extends State<WidgetCustomScrollLoadMore> {
//   bool isPerformingRequest = false;
//   ScrollController _controller = new ScrollController();
//   List<T> _dataList;
//
//   @override
//   void initState() {
//     super.initState();
//     this.onRefresh(isRefresh: false);
//     _controller.addListener(() {
//       if (_controller.position.pixels == _controller.position.maxScrollExtent) {
//         _loadMore();
//       }
//     });
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//     _controller.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     Color loadingColor = widget.loadingColor ?? AppColors.primary;
//     // return NestedScrollView(
//     //     headerSliverBuilder: (_, innerBoxScrolled) => [
//     //           WidgetCustomAppBar(
//     //               title: widget.title, actions: widget.actions ?? [])
//     //         ],
//     //     body: RefreshIndicator(
//     //         color: loadingColor,
//     //         onRefresh: this.onRefresh,
//     //         child: _dataList == null
//     //             ? (widget.isShimmer
//     //                 ? loadingProgress(loadingColor)
//     //                 : loadingProcessNoneShimmer())
//     //             : _dataList.length > 0
//     //                 ? ListView.builder(
//     //                     itemBuilder: (_, index) => index == _dataList.length
//     //                         ? opacityLoadingProgress(
//     //                             isPerformingRequest, loadingColor)
//     //                         : widget.itemBuilder(_dataList, context, index),
//     //                     itemCount: _dataList.length + 1,
//     //                   )
//     //                 : SingleChildScrollView(
//     //                     child: Container(
//     //                       width: MediaQuery.of(context).size.width,
//     //                       height: MediaQuery.of(context).size.height,
//     //                       padding: const EdgeInsets.only(bottom: 150),
//     //                       child: Center(
//     //                           child: widget.widgetError ??
//     //                               Text(
//     //                                 "Không có dữ liệu",
//     //                               )),
//     //                     ),
//     //                   )));
//     return RefreshIndicator(
//       color: loadingColor,
//       onRefresh: this.onRefresh,
//       child: ListView(
//         controller: _controller,
//         physics: BouncingScrollPhysics(),
//         children: [
//           WidgetAppBar(
//             keyTitle: "news",
//           ),
//           SliverToBoxAdapter(
//             child: this._dataList == null
//                 ? loadingProgress(loadingColor)
//                 : (this._dataList.length > 0
//                 ? Column(
//               children: List.generate(
//                   _dataList.length + 1,
//                       (index) => index == _dataList.length
//                       ? opacityLoadingProgress(
//                       isPerformingRequest, loadingColor)
//                       : widget.itemBuilder(
//                       _dataList, context, index)),
//             )
//                 : SizedBox(
//               width: MediaQuery.of(context).size.width,
//               height: MediaQuery.of(context).size.height - 100,
//               child: Center(
//                   child: widget.widgetError ??
//                       Text(
//                         "Không có dữ liệu",
//                       )),
//             )),
//           )
//         ],
//       ),
//     );
//   }
//
//   Future<Null> onRefresh({bool isRefresh = true}) async {
//     if (_dataList == null && isRefresh) return;
//     if (mounted) this.setState(() => this._dataList = null);
//     List initDataList = await widget.initRequester();
//     if (mounted) this.setState(() => this._dataList = initDataList);
//     return;
//   }
//
//   _loadMore() async {
//     if (mounted) {
//       this.setState(() => isPerformingRequest = true);
//       int currentSize = 0;
//       if (_dataList != null) currentSize = _dataList.length;
//
//       List<T> newDataList = await widget.dataRequester(currentSize);
//       if (newDataList != null) {
//         if (newDataList.length == 0) {
//           double edge = 50.0;
//           double offsetFromBottom = _controller.position.maxScrollExtent -
//               _controller.position.pixels;
//           if (offsetFromBottom < edge) {
//             _controller.animateTo(
//                 _controller.offset - (edge - offsetFromBottom),
//                 duration: new Duration(milliseconds: 500),
//                 curve: Curves.easeOut);
//           }
//         } else {
//           _dataList.addAll(newDataList);
//         }
//       }
//       if (mounted) this.setState(() => isPerformingRequest = false);
//     }
//   }
//
//   Widget loadingProgress(loadingColor) {
//     return WidgetShimmer(
//         child: SingleChildScrollView(
//           padding: EdgeInsets.zero,
//           child: Wrap(
//               children: List.generate(
//                   20,
//                       (index) => widget.itemBuilder(
//                       List.generate(20, (index) => null), context, index))),
//         ));
//   }
//
//   Widget opacityLoadingProgress(isPerformingRequest, loadingColor) {
//     return new Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: new Center(
//         child: new Opacity(
//           opacity: isPerformingRequest ? 1.0 : 0.0,
//           child: new CircularProgressIndicator(
//             strokeWidth: 2.0,
//             valueColor: AlwaysStoppedAnimation<Color>(loadingColor),
//           ),
//         ),
//       ),
//     );
//   }
//
//   Widget loadingProcessNoneShimmer() {
//     return SingleChildScrollView(
//       padding: EdgeInsets.zero,
//       child: Wrap(
//           children: List.generate(
//               20,
//                   (index) => widget.itemBuilder(
//                   List.generate(20, (index) => null), context, index))),
//     );
//   }
// }