
import 'package:flutter/material.dart';

import '../presentation.dart';

class WidgetAvatar extends StatelessWidget {
  final double radius;
  final String url;
  final Image imageStorage;
  final double border;

  const WidgetAvatar({@required this.radius, @required this.url, this.border, this.imageStorage});

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: radius,
      backgroundColor: imageStorage != null ? Colors.redAccent : Colors.white,
      child: CircleAvatar(
          radius: radius - (border ?? 3),
          child: imageStorage == null ? WidgetImageNetwork(
            shape: ImageNetworkShape.circle,
            url: url,
            width: radius * 5,
            height: radius * 5,
            fit: BoxFit.fill,
          ) : _buildImageStorage()),
    );
  }

  _buildImageStorage(){
    return CircleAvatar(
        radius: radius * 2.5, backgroundImage: imageStorage.image,);
  }
}
