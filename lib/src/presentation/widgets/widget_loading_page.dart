import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class WidgetLoadingPage extends StatefulWidget {
  final bool isLoading;
  final Widget page;
  final double opacity;
  WidgetLoadingPage(this.isLoading, {this.page, this.opacity});

  @override
  _WidgetLoadingPageState createState() => _WidgetLoadingPageState();
}

class _WidgetLoadingPageState extends State<WidgetLoadingPage> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        widget.page ?? SizedBox(),
        if (widget.isLoading)
          Container(
            color: Colors.white.withOpacity(widget.opacity ?? 0.95),
            width: Get.width,
            height: Get.height,
            child: WidgetLoading(
              duration: Duration(milliseconds: 1000),
              dotIcon: Icon(Icons.ac_unit),
              dotOneColor: Colors.redAccent,
              dotThreeColor: Colors.redAccent,
              dotTwoColor: Colors.redAccent,
            ),
          )
      ],
    );
  }
}

// class WidgetLoadingPage extends StatelessWidget {
//   final bool isLoading;
//   final Widget page;
//
//   const WidgetLoadingPage( this.isLoading,{this.page});
//
//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       children: [
//         page ?? SizedBox(),
//         if (isLoading)
//           Container(
//             color: Colors.white.withOpacity(0.95),
//             width: Get.width,
//             height: Get.height,
//             child: WidgetLoading(
//               duration: Duration(milliseconds: 1000),
//               dotIcon: Icon(Icons.ac_unit),
//               dotOneColor: Colors.redAccent,
//               dotThreeColor: Colors.redAccent,
//               dotTwoColor: Colors.redAccent,
//             ),
//           )
//       ],
//     );
//   }
// }
