import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import '../presentation.dart';

class WidgetCarousel<T> extends StatefulWidget {
  WidgetCarousel.build({
    Key key,
    this.data,
    this.loadingColor,
    this.widgetError,
    this.viewportFraction = 1,
    this.scrollDirection = Axis.horizontal,
    this.autoPlay = false,
    this.playAnimationDuration,
    this.itemBuilder,
    this.pointAlignment,
    this.isPoint = false,
  }) : super(key: key);

  final List<T> data;
  final Color loadingColor;
  final Widget widgetError;
  final double viewportFraction;
  final Axis scrollDirection;
  final bool autoPlay;
  final bool isPoint;
  final Duration playAnimationDuration;
  final ItemBuilder itemBuilder;
  final Alignment pointAlignment;

  @override
  _WidgetCarouselState createState() => _WidgetCarouselState<T>();
}

class _WidgetCarouselState<T> extends State<WidgetCarousel> {
  int indexPoint;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (mounted)
      this.setState(() {
        indexPoint = 0;
      });
  }

  @override
  Widget build(BuildContext context) {
    return _buildCarouselSlider();
  }

  Widget _buildCarouselSlider() {
    return Container(
      child: Stack(
        alignment: widget.pointAlignment ?? Alignment(1, 1),
        children: [
          CarouselSlider.builder(
            itemCount: widget.data.length,
            options: CarouselOptions(
              enlargeStrategy: CenterPageEnlargeStrategy.height,
              aspectRatio: 2,
              viewportFraction: widget.viewportFraction ?? 1,
              enlargeCenterPage: true,
              scrollDirection: widget.scrollDirection ?? Axis.horizontal,
              autoPlay: widget.autoPlay ?? false,
              autoPlayAnimationDuration: widget.playAnimationDuration ??
                  const Duration(milliseconds: 1500),
              onPageChanged: (index, reason) {
                setState(() {
                  indexPoint = index;
                });
              },
              scrollPhysics: BouncingScrollPhysics(),
            ),
            itemBuilder: (context, index, realIndex) =>
                widget.itemBuilder(widget.data, context, index),
          ),
          _buildSliderPoint()
        ],
      ),
    );
  }

  Widget _buildSliderPoint() {
    return widget.isPoint
        ? Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: List.generate(
          widget.data.length,
              (index) => Container(
            width: 10.0,
            height: 10.0,
            margin:
            EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white, width: 1),
              shape: BoxShape.circle,
              color: indexPoint == index
                  ? Colors.white
                  : Colors.transparent,
            ),
          )),
    )
        : SizedBox();
  }

  Widget loadingProgress(loadingColor) {
    return Center(
      child: CircularProgressIndicator(
        strokeWidth: 2.0,
        valueColor: AlwaysStoppedAnimation<Color>(loadingColor),
      ),
    );
  }
}
