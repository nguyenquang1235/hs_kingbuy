import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';

class DialogGoLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: GestureDetector(
        onTap: () => Navigator.pop(context, false),
        child: Container(
          color: Colors.transparent,
          width: double.maxFinite,
          height: double.maxFinite,
          alignment: Alignment.center,
          child: GestureDetector(
            onTap: () {},
            child: Container(
              width: MediaQuery.of(context).size.width - 60,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 35),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(25),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(AppUtils.translate("need_login", context) + " ?",
                      style: AppStyles.DEFAULT_LARGE_BOLD),
                  const SizedBox(height: 20),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Expanded(
                        child: InkWell(
                          onTap: () => Navigator.pop(context, false),
                          child: PhysicalModel(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            elevation: 4,
                            color: Colors.grey,
                            child: Padding(
                              padding: EdgeInsets.all(16),
                              child: Text(
                                AppUtils.translate("cancel", context),
                                textAlign: TextAlign.center,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: AppStyles.DEFAULT_MEDIUM_BOLD
                                    .copyWith(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 50),
                      Expanded(
                        child: InkWell(
                          onTap: () => Navigator.pop(context, true),
                          child: PhysicalModel(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            elevation: 4,
                            color: Colors.blue,
                            child: Padding(
                              padding: EdgeInsets.all(16),
                              child: Text(
                                AppUtils.translate("continue", context),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.center,
                                style: AppStyles.DEFAULT_MEDIUM_BOLD
                                    .copyWith(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
