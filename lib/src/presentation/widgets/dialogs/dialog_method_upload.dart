import 'dart:io';

import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

enum DialogMethodUploadType { avatar, normal }

class DialogMethodUpload extends StatelessWidget {
  final DialogMethodUploadType type;
  final VoidCallback callback;

  const DialogMethodUpload({this.type, this.callback});

  changeImage(bool fromGallery, BuildContext context) async {
    final image = await ImagePicker().getImage(
      source: fromGallery ? ImageSource.gallery : ImageSource.camera,
      imageQuality: 70,
      maxWidth: 720,
    );
    if (image != null) {
      Navigator.pop(context, File(image.path));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: GestureDetector(
        onTap: () => Navigator.pop(context),
        child: Container(
          color: Colors.transparent,
          width: double.maxFinite,
          height: double.maxFinite,
          alignment: Alignment.center,
          child: GestureDetector(
            onTap: () {},
            child: Container(
              width: MediaQuery.of(context).size.width - 60,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 35),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(25),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                      AppUtils.translate(
                          type == DialogMethodUploadType.avatar
                              ? "update_avatar"
                              : "choose_method_upload",
                          context),
                      style: AppStyles.DEFAULT_LARGE),
                  const SizedBox(height: 20),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          onTap: () => changeImage(false, context),
                          child: Container(
                            color: Colors.transparent,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                CircleAvatar(
                                  radius: 40,
                                  backgroundColor:
                                      AppColors.grey.withOpacity(0.25),
                                  child: Icon(
                                    Icons.camera_alt_outlined,
                                    size: 32,
                                    color: Colors.grey,
                                  ),
                                ),
                                const SizedBox(height: 8),
                                Text(
                                  AppUtils.translate("pick_image_camera", context),
                                  textAlign: TextAlign.center,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: AppStyles.DEFAULT_MEDIUM,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: GestureDetector(
                          onTap: () => changeImage(true, context),
                          child: Container(
                            color: Colors.transparent,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                CircleAvatar(
                                  radius: 40,
                                  backgroundColor:
                                      AppColors.grey.withOpacity(0.25),
                                  child: Icon(
                                    Icons.camera,
                                    size: 32,
                                    color: Colors.grey,
                                  ),
                                ),
                                const SizedBox(height: 8),
                                Text(
                                  AppUtils.translate("pick_image_gallery", context),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center,
                                  style: AppStyles.DEFAULT_MEDIUM,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
