import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../configs/configs.dart';
import '../presentation.dart';

class WidgetButtonGradient extends StatelessWidget {
  final bool loading;
  final String title;
  final Function action;
  final Color colorStart;
  final Color colorTitle;
  final Color colorEnd;
  final Color colorLoading;
  final Alignment alignmentStart;
  final Alignment alignmentEnd;
  final EdgeInsets padding;
  final Widget prefix;

  const WidgetButtonGradient(
      {this.alignmentStart,
      this.alignmentEnd,
      this.colorLoading,
      this.colorEnd,
      this.colorTitle,
      this.colorStart,
      this.loading = false,
      this.padding,
      this.prefix,
      @required this.title,
      @required this.action});

  static const double HEIGHT = 45.0;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: HEIGHT,
      child: FlatButton(
        onPressed: loading ? null : action ?? () {},
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        padding: const EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  colorStart ?? AppColors.primary,
                  colorEnd ?? AppColors.primaryLight
                ],
                begin: alignmentStart ?? Alignment.bottomCenter,
                end: alignmentEnd ?? Alignment.topCenter,
              ),
              borderRadius: BorderRadius.circular(30.0)),
          child: Container(
            padding: padding ?? const EdgeInsets.all(8),
            alignment: Alignment.center,
            child: !loading
                ? Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      prefix != null
                          ? Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                prefix,
                                SizedBox(
                                  width: 5,
                                ),
                              ],
                            )
                          : SizedBox(),
                      FittedBox(
                        fit: BoxFit.cover,
                        child: Text(
                          AppUtils.translate(title, context),
                          textAlign: TextAlign.center,
                          style: AppStyles.DEFAULT_MEDIUM
                              .copyWith(color: colorTitle ?? Colors.white),
                        ),
                      ),
                    ],
                  )
                : Center(
                    child: WidgetLoading(
                      dotOneColor: colorLoading ?? Colors.white,
                      dotTwoColor: colorLoading ?? Colors.white,
                      dotThreeColor: colorLoading ?? Colors.white,
                      dotType: DotType.circle,
                      duration: Duration(milliseconds: 1000),
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
