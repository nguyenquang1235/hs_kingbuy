import 'package:flutter/material.dart';
import '../../configs/configs.dart';
import 'package:rxdart/rxdart.dart';

class WidgetInput extends StatefulWidget {
  final TextEditingController inputController;
  final ValueChanged<String> onChanged;
  final VoidCallback onComplete;
  final FormFieldValidator<String> validator;
  final String hint;
  final bool obscureText;
  final bool required;
  final bool autovalidate;
  final TextInputType inputType;
  final Widget endIcon;
  final double height;
  final double width;
  final double paddingHorizontal;
  final double paddingVertical;
  final TextStyle style;
  final TextStyle hintStyle;
  final double radiusBorder;
  final int maxLines;
  final double elevation;
  final double paddingLeftMore;
  final TextAlign textAlign;
  final Widget preIcon;
  final bool enabled;
  final bool autoFocus;
  final FocusNode focusNode;

  WidgetInput(
      {this.inputController,
      this.required,
      this.onChanged,
      this.textAlign,
      this.validator,
      this.paddingLeftMore,
      this.hint,
      this.maxLines,
      this.inputType = TextInputType.text,
      this.obscureText = false,
      this.autovalidate = false,
      this.endIcon,
      this.elevation,
      this.style,
      this.hintStyle,
      this.radiusBorder,
      this.width,
      this.paddingHorizontal,
      this.paddingVertical,
      this.height,
      this.preIcon,
      this.enabled,
      this.onComplete,
      this.autoFocus = false,
      this.focusNode});

  @override
  _WidgetInputState createState() => _WidgetInputState();
}

class _WidgetInputState extends State<WidgetInput> {
  // final BehaviorSubject<bool> hasText = BehaviorSubject<bool>();

  bool hasText = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if ((widget.inputController ?? null) != null) {
      hasText = widget.inputController.text.isNotEmpty;
      widget.inputController.addListener(() {
        if (mounted) {
          setState(() {
            hasText = widget.inputController.text.isNotEmpty;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Container(
      height: widget.height ?? AppValues.INPUT_FORM_HEIGHT,
      width: widget.width,
      child: Card(
        elevation: widget.elevation ?? 4,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(widget.radiusBorder ?? 20)),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(width: 0.5, color: AppColors.grey),
            borderRadius: BorderRadius.circular(widget.radiusBorder ?? 20),
          ),
          padding: EdgeInsets.symmetric(
              horizontal: widget.paddingHorizontal ?? 15,
              vertical: widget.paddingVertical ?? 0),
          child: Row(
            children: [
              SizedBox(
                width: widget.paddingLeftMore ?? 2,
              ),
              widget.preIcon ?? const SizedBox(),
              SizedBox(
                width:
                    widget.preIcon != null ? widget.paddingLeftMore ?? 16 : 0,
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Stack(
                    alignment: Alignment.centerLeft,
                    children: [
                      TextFormField(
                        autofocus: widget.autoFocus,
                        // onTap: () {
                        //   setState(() {
                        //     hasText = true;
                        //   });
                        // },
                        focusNode: widget.focusNode,
                        onFieldSubmitted: (value) {
                          if (widget.onComplete != null)
                            widget.onComplete();
                        },
                        enabled: widget.enabled,
                        controller: widget.inputController,
                        onChanged: (change) {
                          widget.onChanged(change);
                        },
                        autovalidate: widget.autovalidate ?? false,
                        validator: widget.validator,
                        style: widget.style ?? AppStyles.DEFAULT_MEDIUM,
                        maxLines: widget.maxLines ?? 1,
                        keyboardType: widget.inputType ?? TextInputType.text,
                        textAlign: widget.textAlign ?? TextAlign.left,
                        obscureText: widget.obscureText,
                        decoration: InputDecoration.collapsed(),
                      ),
                      hasText
                          ? Container()
                          : IgnorePointer(
                              ignoring: true,
                              child: Container(
                                color: Colors.transparent,
                                child: RichText(
                                  textAlign: TextAlign.center,
                                  textDirection: TextDirection.ltr,
                                  text: TextSpan(
                                      text: '${widget.hint ?? ""} ',
                                      style: widget.hintStyle ??
                                          AppStyles.DEFAULT_MEDIUM
                                              .copyWith(color: Colors.grey),
                                      children: <TextSpan>[
                                        (widget.required ?? false)
                                            ? TextSpan(
                                                text: "*",
                                                style: (widget.hintStyle ??
                                                        AppStyles
                                                            .DEFAULT_MEDIUM)
                                                    .copyWith(
                                                        color: Colors.red))
                                            : TextSpan()
                                      ]),
                                ),
                              ))
                    ],
                  ),
                ),
              ),
              widget.endIcon ?? const SizedBox()
            ],
          ),
        ),
      ),
    ));
  }
}
