import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/widgets/widget_avatar.dart';
import 'package:King_Buy/src/utils/app_valid.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen>
    with ResponsiveWidget {
  UserProfileViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<UserProfileViewModel>(
      viewModel: UserProfileViewModel(),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel..init();
      },
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: buildUi(context: context),
          ),
        );
      },
    );
  }

  _buildScreen() {
    return StreamBuilder<Object>(
        stream: _viewModel.loadingSubject,
        builder: (context, snapshot) {
          bool isLoading = snapshot.data ?? true;
          return WidgetLoadingPage(isLoading, page: _buildBody(),);
        });
  }

  _buildBody(){
    return Column(
      children: [
        WidgetAppBar(
          bgColor: Colors.blue,
          colorButton: Colors.white,
          keyTitle: "user_profile",
          supWidget: _buildButtonSave(),
        ),
        Expanded(
            child: SingleChildScrollView(
              padding: const EdgeInsets.only(bottom: 75, top: 24),
              physics: BouncingScrollPhysics(),
              child: _buildBodyScreen(),
            ))
      ],
    );
  }

  _buildBodyScreen() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        children: [
          _buildAvatar(),
          SizedBox(
            height: 55,
          ),
          _buildFormProfile(),
          SizedBox(
            height: 30,
          ),
          _buildGender(),
        ],
      ),
    );
  }

  _buildFormProfile() {
    TextStyle style = AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.blue);
    return Form(
      key: _viewModel.formKeySignIn,
      child: Container(
        width: Get.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildInputField(
              title: "full_name",
              controller: _viewModel.nameController,
              validator: AppValid.validateFullName(context),
            ),
            _buildInputField(
              title: "number_phone",
              controller: _viewModel.phoneController,
              validator: _viewModel.validatePhoneNumber,
              onComplete: () {
                String value = _viewModel.phoneController.value.text;
                if (value.length > 8 && value.length < 11) {
                  _viewModel.phoneController.text =
                      AppUtils.convertPhoneNumber(value);
                }
              },
            ),
            GestureDetector(
              onTap: _viewModel.showDayTimePicker,
              child: Stack(
                children: [
                  _buildInputField(
                    title: "birth_day",
                    controller: _viewModel.birthDayController,
                    validator: (value) {
                      if (value == null || value.length == 0) {
                        return AppUtils.translate(
                            "valid_enter_birth_day", context);
                      }
                      return null;
                    },
                  ),
                  Opacity(
                    opacity: 0,
                    child: _buildInputField(
                      enabled: false,
                      title: "birth_day",
                      controller: _viewModel.birthDayController,
                    ),
                  ),
                ],
              ),
            ),
            _buildInputField(
              title: "email",
              controller: _viewModel.emailController,
              validator: AppValid.validateEmail(context),
            ),
          ],
        ),
      ),
    );
  }

  _buildGender() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(AppUtils.translate("gender", context),
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.black)),
        SizedBox(
          height: 10,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            _buildTitleGender(true, "male"),
            SizedBox(
              width: 15,
            ),
            _buildTitleGender(false, "female"),
          ],
        ),
      ],
    );
  }

  _buildTitleGender(bool value, String title) {
    return StreamBuilder<bool>(
        stream: _viewModel.genderController,
        builder: (context, snapshot) {
          return Container(
            child: Row(
              children: [
                Radio<bool>(
                  activeColor: Colors.redAccent,
                  value: value,
                  groupValue: snapshot?.data ?? true,
                  onChanged: (value) {
                    _viewModel.changeGender(value);
                    _viewModel.checkChangeUser();
                  },
                ),
                Text(
                  AppUtils.translate(title, context),
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.grey),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildAvatar() {
    return Center(
      child: InkWell(
        onTap: () => _viewModel.changeAvatarImage(),
        child: Stack(
          overflow: Overflow.visible,
          children: [
            WidgetAvatar(
              radius: 55,
              url: _viewModel.userModel?.avatarSource ?? "",
              imageStorage: _viewModel.imageStorage,
            ),
            Positioned(
              bottom: 4,
              right: 9,
              child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.5,
                    color: Colors.white,
                  ),
                  shape: BoxShape.circle,
                  color: Colors.redAccent,
                ),
                child: Center(
                  child: Icon(
                    Icons.edit,
                    size: 20,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildButtonSave() {
    return StreamBuilder<bool>(
        stream: _viewModel.isConfirm,
        builder: (context, snapshot) {
          bool isConfirm = snapshot.data ?? false;
          return PhysicalModel(
            borderRadius: BorderRadius.all(Radius.circular(100)),
            elevation: 2,
            color: (isConfirm) ? Colors.red : Colors.grey,
            child: Container(
              height: 40,
              child: WidgetButtonGradient(
                // padding: EdgeInsets.fromLTRB(30, 12, 30, 12),
                colorStart: (isConfirm) ? Colors.redAccent : Colors.grey,
                colorEnd: (isConfirm) ? Colors.red : Colors.grey,
                title: "save",
                action: () {
                  if (isConfirm) _viewModel.confirmUser();
                },
              ),
            ),
          );
        });
  }

  _buildInputField({
    String title,
    TextEditingController controller,
    FormFieldValidator<String> validator,
    bool enabled,
    onComplete(),
  }) {
    UnderlineInputBorder border =
        UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey[300]));

    return Container(
      margin: EdgeInsets.only(top: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate(title, context),
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
          TextFormField(
            style: AppStyles.DEFAULT_LARGE,
            controller: controller,
            decoration: InputDecoration(
                border: border,
                errorBorder: border,
                focusedBorder: border,
                disabledBorder: border,
                focusedErrorBorder: border,
                enabledBorder: border,
                hintText: AppUtils.translate(title, context),
                hintStyle:
                    AppStyles.DEFAULT_LARGE.copyWith(color: Colors.grey)),
            enabled: enabled ?? true,
            onEditingComplete: () {
              if (onComplete != null) onComplete();
              _viewModel.checkChangeUser();
              _viewModel.unFocus();
            },
            validator: validator,
          ),
        ],
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
