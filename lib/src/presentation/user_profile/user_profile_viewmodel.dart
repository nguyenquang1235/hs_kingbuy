import 'dart:io';

import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/base/base.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart';
import 'package:toast/toast.dart';

import '../presentation.dart';

class UserProfileViewModel extends BaseViewModel {
  final formKeySignIn = GlobalKey<FormState>();
  final genderController = BehaviorSubject<bool>();
  final isConfirm = BehaviorSubject<bool>();

  UserModel userModel;
  Image imageStorage;
  File fileImage;
  TextEditingController nameController;
  TextEditingController phoneController;
  TextEditingController birthDayController;
  TextEditingController emailController;

  init() async {
    setLoading(true);
    nameController = TextEditingController();
    phoneController = TextEditingController();
    birthDayController = TextEditingController();
    emailController = TextEditingController();
    userModel = await AppShared.getUser();
    _preSetTextController();
    await Future.delayed(const Duration(milliseconds: 1500));
    setLoading(false);
  }

  changeAvatarImage() async {
    fileImage = await showDialog(
      context: context,
      builder: (context) => DialogMethodUpload(
        type: DialogMethodUploadType.avatar,
      ),
    );

    if (fileImage != null)
      imageStorage = Image.memory(await fileImage.readAsBytes());
    notifyListeners();
  }

  showDayTimePicker() {
    LocaleType localeType;
    for (var item in LocaleType.values) {
      if (item.toString().split('.').last == Get.deviceLocale.languageCode)
        localeType = item;
    }

    return DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(1900, 1, 1),
      maxTime: DateTime.now(),
      onConfirm: (date) {
        birthDayController.text =
            AppUtils.convertDateTime2String(date, format: "yyyy-MM-dd");
        checkChangeUser();
      },
      currentTime: DateTime.now(),
      locale: localeType,
    );
  }

  changeGender(bool value) {
    genderController.add(value);
  }

  confirmUser() async {
    unFocus();
    if (formKeySignIn.currentState.validate()) {
      try {
        setLoading(true);

        NetworkState<UserModel> networkState = await authRepository.updateUser(
          name: nameController.value.text.trim(),
          email: emailController.value.text.trim(),
          phoneNumber: "0${phoneController.text.substring(3).trim()}",
          dateOfBirth: birthDayController.value.text.trim(),
          gender: (genderController.value ?? true) ? 0 : 1,
        );

        _checkUser(networkState: networkState);

        setLoading(false);
      } catch (e) {
        print(e);
        Toast.show(AppUtils.translate("update_errors", context), context);
      }
    }
  }

  checkChangeUser() {
    UserModel newUser = userModel.copyWith(
      name: nameController.value.text == "" ? null : nameController.value.text,
      phoneNumber: phoneController.value.text == ""
          ? null
          : "0${phoneController.text.substring(3)}",
      dateOfBirth: birthDayController.value.text == ""
          ? null
          : birthDayController.value.text,
      email:
          emailController.value.text == "" ? null : emailController.value.text,
      gender: (genderController.value) ? 0 : 1,
    );

    if (newUser == userModel) {
      isConfirm.add(false);
    } else {
      isConfirm.add(true);
    }
  }

  String validatePhoneNumber(String value) {
    Pattern pattern = r'^(?:[+0]9)?[0-9]{10}$';
    String val = value != "" ? "0${value?.substring(3)}" : "";
    RegExp regex = new RegExp(pattern);
    if (value == null || value.length == 0) {
      return AppLocalizations.of(context).translate("valid_enter_phone");
    } else if (val.length != 10) {
      return AppLocalizations.of(context).translate("valid_phone");
    } else if (!regex.hasMatch(val)) {
      return AppLocalizations.of(context).translate("valid_phone");
    } else {
      return null;
    }
  }

  _preSetTextController() {
    String phoneNumber =
        (userModel?.phoneNumber == null || userModel?.phoneNumber == "")
            ? ""
            : AppUtils.convertPhoneNumber(userModel?.phoneNumber);

    nameController.text = userModel?.name ?? "";
    phoneController.text = phoneNumber;
    birthDayController.text = userModel?.dateOfBirth ?? "";
    emailController.text = userModel?.email ?? "";
    genderController.add((userModel?.gender ?? 0) == 0);

    checkChangeUser();
  }

  _checkUser({NetworkState<UserModel> networkState}) async {
    if (networkState == null) return;
    if (networkState.isSuccess) {
      if (networkState.response.isSuccess) {
        Toast.show("${AppUtils.translate("update_success", context)}", context);
        await AppShared.setUser(networkState.data);
        Navigator.pop(context);
      } else {
        Toast.show(networkState.response.message, context,
            duration: Toast.LENGTH_LONG);
      }
    } else {
      Toast.show(AppUtils.translate("update_errors", context), context);
    }
  }
}
