import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CommitmentsScreen extends StatefulWidget {
  @override
  _CommitmentsScreenState createState() => _CommitmentsScreenState();
}

class _CommitmentsScreenState extends State<CommitmentsScreen>
    with ResponsiveWidget {
  CommitmentsViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CommitmentsViewModel>(
      viewModel: CommitmentsViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, appbar) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: Colors.grey[200],
            body: buildUi(context: context),
          ),
        );
      },
    );
  }

  _buildScreen() {
    return StreamBuilder<bool>(
      stream: _viewModel.loadingSubject,
      builder: (context, snapshot) {
        bool isLoading = snapshot.data ?? false;
        return WidgetLoadingPage(
          isLoading,
          page: Container(
            child: Column(
              children: [
                _buildAppbar(),
                Expanded(
                    child: _viewModel.listCommitments != null
                        ? _buildBody()
                        : Center(
                            child: Text(
                              AppUtils.translate("no_data", context),
                            ),
                          )),
              ],
            ),
          ),
        );
      },
    );
  }

  _buildBody() {
    List<Commitments> data = _viewModel.listCommitments;
    return ListView(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.only(bottom: 50),
      children: List.generate(
          data.length, (index) => _buildCommitmentItem(data[index])),
    );
  }

  _buildCommitmentItem(Commitments item) {
    return Container(
      width: Get.width,
      margin: const EdgeInsets.only(top: 24),
      child: PhysicalModel(
        color: Colors.white,
        elevation: 4,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
          child: Column(
            children: [
              Container(
                width: 75,
                height: 75,
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                    border: Border.all(color: Colors.black)),
                child: Center(
                  child: Text(
                    item.shortTitle,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style:
                        AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.grey),
                    maxLines: 2,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(item.title,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: AppStyles.DEFAULT_MEDIUM_BOLD
                      .copyWith(color: Colors.redAccent),
                  maxLines: 1),
              Text(item.description,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: AppStyles.DEFAULT_MEDIUM,
                  maxLines: 4)
            ],
          ),
        ),
      ),
    );
  }

  _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "kingbuy_commitment",
      colorButton: Colors.white,
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
