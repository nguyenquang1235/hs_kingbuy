import 'package:King_Buy/src/presentation/base/base.dart';
import 'package:King_Buy/src/resource/resource.dart';

class CommitmentsViewModel extends BaseViewModel {

  List<Commitments> listCommitments;

  init() async {
    setLoading(true);
    listCommitments = await getCommitments();
    setLoading(false);
  }

  Future<List<Commitments>> getCommitments() async {
    NetworkState<List<Commitments>> networkState;
    try {
      networkState = await otherRepository.getCommitments();
      return networkState.data;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
