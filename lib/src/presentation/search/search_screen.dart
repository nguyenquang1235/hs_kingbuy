import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/base/base.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../presentation.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> with ResponsiveWidget {
  SearchViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<SearchViewModel>(
      viewModel: SearchViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
            child: Scaffold(
          backgroundColor: Colors.grey[200],
          body: Column(
            children: [
              _buildAppbar(),
              Expanded(
                child: buildUi(
                  context: context,
                ),
              ),
            ],
          ),
        ));
      },
    );
  }

  Widget _buildAppbar() {
    return PhysicalModel(
      elevation: 2,
      color: Colors.lightBlue,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8),
        color: Colors.lightBlue,
        height: 75,
        child: Row(
          children: [
            Expanded(
              child: WidgetInput(
                focusNode: _viewModel.nodeOne,
                autoFocus: true,
                style: AppStyles.DEFAULT_LARGE,
                onChanged: (value) => null,
                // onChanged: (value) => _viewModel.onChangeSearchBox(value),
                inputController: _viewModel.searchController,
                radiusBorder: 10,
                height: 55,
                hint: AppUtils.translate("search", context),
                preIcon: Icon(
                  Icons.search,
                  color: Colors.grey,
                ),
                onComplete: () => _viewModel.onCompleteSearch(),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
              onTap: (){
                Navigator.pop(context);
              },
              child: Container(
                color: Colors.transparent,
                child: Center(
                  child: FittedBox(
                    fit: BoxFit.cover,
                    child: Text(
                      AppUtils.translate("cancel", context),
                      style: AppStyles.DEFAULT_MEDIUM
                          .copyWith(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildScreen() {
    if (_viewModel.isChange) {
      return StreamBuilder<List<ProductModel>>(
          stream: _viewModel.listResult,
          builder: (context, snapshot) {
            if (!(snapshot.connectionState == ConnectionState.waiting)) {
              if (snapshot.data.length > 0) {
                return ListView(
                    padding: EdgeInsets.only(bottom: 75),
                    physics: BouncingScrollPhysics(),
                    children: List.generate(
                      snapshot.data.length,
                      (index) => _buildResultItem(snapshot.data[index]),
                    ));
              }
              return Container(
                  constraints: BoxConstraints(minHeight: 60),
                  // margin: const EdgeInsets.only(bottom: 10),
                  alignment: Alignment.center,
                  color: Colors.white,
                  child: Text(
                    AppUtils.translate("no_data", context),
                    style: AppStyles.DEFAULT_MEDIUM,
                  ));
            }
            return loadingProgress(Colors.redAccent);
          });
    } else {
      return ListView(
        padding: EdgeInsets.only(bottom: 75),
        physics: BouncingScrollPhysics(),
        children: _viewModel.listSearchKey.length > 0
            ? (List.generate(
                _viewModel.listSearchKey.length,
                (index) => _buildSearchItem(_viewModel.listSearchKey[index]),
              )..add(_buildCleanSearchKey()))
            : [
                Container(
                    constraints: BoxConstraints(minHeight: 60),
                    // margin: const EdgeInsets.only(bottom: 10),
                    alignment: Alignment.center,
                    color: Colors.white,
                    child: Text(
                      AppUtils.translate("no_search_key", context),
                      style: AppStyles.DEFAULT_MEDIUM,
                    ))
              ],
      );
    }
  }

  Widget _buildSearchItem(String value) {
    return GestureDetector(
      onTap: () {
        _viewModel.searchController.text = value;
        FocusScope.of(context).requestFocus(_viewModel.nodeOne);
      },
      child: Container(
        padding: const EdgeInsets.only(left: 32),
        alignment: Alignment.centerLeft,
        height: 60,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
                bottom: BorderSide(
              color: Colors.grey[200],
              width: 1.0,
            ))),
        child: Text(
          value,
          style: AppStyles.DEFAULT_MEDIUM,
        ),
      ),
    );
  }

  Widget _buildResultItem(ProductModel model) {
    return Container(
      padding: const EdgeInsets.only(left: 10),
      alignment: Alignment.center,
      height: 60,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
              bottom: BorderSide(
            color: Colors.grey[200],
            width: 1.0,
          ))),
      child: Row(
        children: [
          WidgetImageNetwork(
            url: model.imageSource,
            height: 45,
            width: 45,
            isAvatar: false,
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
              child: Text(
            model.name,
            maxLines: 1,
            style: AppStyles.DEFAULT_MEDIUM,
          )),
        ],
      ),
    );
  }

  Widget _buildCleanSearchKey() {
    return Container(
        constraints: BoxConstraints(minHeight: 60),
        // margin: const EdgeInsets.only(bottom: 10),
        alignment: Alignment.center,
        color: Colors.white,
        child: GestureDetector(
          onTap: () => _viewModel.cleanSearchHistory(),
          child: Container(
            color: Colors.transparent,
            child: Text(
              AppUtils.translate("delete_search_history", context),
              style: AppStyles.DEFAULT_MEDIUM,
            ),
          ),
        ));
  }

  Widget loadingProgress(loadingColor) {
    return Center(
      child: CircularProgressIndicator(
        strokeWidth: 2.0,
        valueColor: AlwaysStoppedAnimation<Color>(loadingColor),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
