import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class SearchViewModel extends BaseViewModel {
  FocusNode nodeOne = FocusNode();
  final TextEditingController searchController = TextEditingController();

  List<String> listSearchKey = [];
  final BehaviorSubject<List<ProductModel>> listResult = BehaviorSubject();

  bool isChange = false;

  init() async {
    listSearchKey = await AppShared.getListSearch() ?? [];
    notifyListeners();
    searchController.addListener(() {
      onChangeSearchBox(searchController.text);
    });
  }

  void onCompleteSearch() async {
    unFocus();
    if (searchController.text.length > 0) {
      addAndReverse(searchController.text);
      await AppShared.setListSearch(listSearchKey);
      Navigator.pushReplacementNamed(context, Routers.search_filter,
          arguments: SearchArgument(searchKey: searchController.text));
    }
  }

  void onChangeSearchBox(String value) async {
    if (value.length > 0) {
      isChange = true;
      _searchProduct(value);
      notifyListeners();
    } else {
      isChange = false;
      notifyListeners();
    }
  }

  _searchProduct(String value) async {
    try {
      NetworkState<List<ProductModel>> networkState = await searchRepository
          .searchProduct(keyWord: value, offset: 0, limit: 5);
      listResult.add(networkState.data ?? []);
    } catch (e) {
      print(e);
      listResult.add([]);
    }
  }

  void cleanSearchHistory() async {
    unFocus();
    listSearchKey = [];
    await AppShared.setListSearch(listSearchKey);
    notifyListeners();
    searchController.clear();
  }

  void addAndReverse(String text) {
    listSearchKey.add(text);
    listSearchKey = listSearchKey.reversed.toList();
  }
}
