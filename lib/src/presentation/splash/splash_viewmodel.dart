import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';

import '../presentation.dart';

class SplashViewModel extends BaseViewModel {
  AppData _appData;
  CartDataBase _cartDataBase;

  init(BuildContext context) async {
    _appData = await AppData.instance();

    _cartDataBase = CartDataBase.instance();

    if (await _cartDataBase.getCart() == null) {
      await _cartDataBase.insertCart(CartModel());
    }

    await getProfile();
    await preGetPopups(context);
    await preGetBanner(context);
    await preGetMyPromotion(context);
    await preGetImageCategory(context);
    await preGetSellingProduct(context);
    await getNewProducts(context);

    Navigator.pushReplacementNamed(
      context,
      Routers.navigation,
    );
  }

  preGetPopups(BuildContext context) async {
    try {
      NetworkState<PopupModel> networkState = await otherRepository.getPopups();
      if (networkState.isSuccess && networkState.response.isSuccess) {
        await AppCached.preLoadImage(networkState.data.popupImage, context);
      }
    } catch (e) {
      print(e);
    }
  }

  preGetBanner(BuildContext context) async {
    var data = await _appData.listPromotions;
    if (data.length <= 0) return;
    data.forEach((element) async {
      await AppCached.preLoadImage(element.imageSource, context);
    });
  }

  preGetMyPromotion(BuildContext context) async {
    var data = await _appData.listMyPromotions;
    if (data.length <= 0) return;
    data.forEach((element) async {
      await AppCached.preLoadImage(element.imageSource, context);
    });
  }

  preGetImageCategory(BuildContext context) async {
    var data = await _appData.listCategories;
    if (data.length <= 0) return;
    data.forEach((element) async {
      await AppCached.preLoadImage(element.imageSource, context);
    });
  }

  preGetSellingProduct(BuildContext context) async {
    try {
      NetworkState<List<ProductModel>> networkState =
          await productRepository.getSellingProducts(offset: 0, limit: 20);
      if (networkState.isSuccess && networkState.response.isSuccess) {
        await networkState.data.forEach((element) async {
          await AppCached.preLoadImage(element.imageSource, context);
        });
      }
    } catch (e) {
      print(e);
    }
  }

  getNewProducts(BuildContext context) async {
    try {
      NetworkState<List<ProductModel>> networkState =
          await productRepository.getNewProducts(offset: 0, limit: 20);
      if (networkState.isSuccess && networkState.response.isSuccess) {
        await networkState.data.forEach((element) async {
          await AppCached.preLoadImage(element.imageSource, context);
        });
      }
    } catch (e) {
      print(e);
    }
  }
}
