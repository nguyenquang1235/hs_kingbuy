import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../configs/configs.dart';

import '../presentation.dart';

class SplashScreen extends StatelessWidget with ResponsiveWidget {

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SplashViewModel>(
        viewModel: SplashViewModel(),
        onViewModelReady: (viewModel) {
          viewModel.init(context);
        },
        child: Image.asset(AppImages.bgSplashScreen,width: Get.width, height: Get.height, fit: BoxFit.fill,),
        builder: (context, viewModel, child) {
          return SafeArea(
            child: Scaffold(
              body: Stack(
                children: [
                  child,
                  buildUi(context: context)
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return Container(
      child: Center(
        child: Image.asset(AppImages.icLogo),
      ),
    );
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return Container(
      child: Center(
        child: Image.asset(AppImages.icLogo),
      ),
    );
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return Container(
      child: Center(
        child: Image.asset(AppImages.icLogo),
      ),
    );
  }

}
