import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/app_shared.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:toast/toast.dart';

class ChangeDeliveryAddressViewModel extends BaseViewModel {
  String provinceCode;
  String districtCode;
  String wardCode;
  bool isAddNew;

  List<ProvinceModel> listProvince;
  List<ProvinceModel> listDistrict;
  List<ProvinceModel> listWard;

  final isAvailableSave = BehaviorSubject<bool>();
  final isDefault = BehaviorSubject<bool>();
  bool get getDefault => isDefault.value;

  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  final phone2Controller = TextEditingController();
  final addressController = TextEditingController();

  final formKeySignIn = GlobalKey<FormState>();

  final DeliveryAddressModel model;

  ChangeDeliveryAddressViewModel({this.model});

  init() async {
    setLoading(true);
    isAddNew = model == null;
    await _preGetDeliveryAddress();
    checkChangeUser();
    setLoading(false);
  }

  Future<List<ProvinceModel>> getAllProvince() async {
    try {
      NetworkState<List<ProvinceModel>> networkState =
          await provinceRepository.getAllProvince();
      return networkState.data;
    } catch (e) {
      print(e);
      return null;
    }
  }

  getDistrict(String provinceCode) async {
    try {
      NetworkState<List<ProvinceModel>> networkState =
          await provinceRepository.getDistrict(provinceCode);
      listDistrict = networkState.data;
      notifyListeners();
    } catch (e) {
      print(e);
      listDistrict = [];
      notifyListeners();
    }
  }

  getWard(String districtCode) async {
    try {
      NetworkState<List<ProvinceModel>> networkState =
          await provinceRepository.getWard(districtCode);
      listWard = networkState.data;
      notifyListeners();
    } catch (e) {
      print(e);
      listWard = [];
      notifyListeners();
    }
  }

  onChangeProvince(String value) async {
    if (provinceCode == value) return;
    getDistrict(value);
    provinceCode = value;
    notifyListeners();
    _reset(-1);
  }

  onChangeDistrict(String value) async {
    if (districtCode == value) return;
    getWard(value);
    districtCode = value;
    notifyListeners();
    _reset(0);
  }

  onChangeWard(String value) async {
    if (wardCode == value) return;
    wardCode = value;
    notifyListeners();
  }

  checkChangeUser() {
    isAvailableSave.add(false);
    if (!isAddNew) {
      DeliveryAddressModel newModel = model.copyWith(
        fullName: nameController.text,
        address: addressController.text,
        firstPhone: phoneController.text,
        secondPhone:
            (phone2Controller.text == "") ? null : phone2Controller.text,
        districtCode: districtCode,
        provinceCode: provinceCode,
        wardCode: wardCode,
        isDefault: (getDefault ?? false) ? 1 : 0,
      );

      if (newModel != model) {
        isAvailableSave.add(true);
      }
    } else {
      if (nameController.text.isNotEmpty &&
          addressController.text.isNotEmpty &&
          phoneController.text.isNotEmpty &&
          districtCode != null &&
          provinceCode != null &&
          wardCode != null) isAvailableSave.add(true);
    }
  }

  Future onSave() async {
    if (formKeySignIn.currentState.validate()) {
      try {
        setLoading(true);
        if (await AppShared.getLogged()) {
          await onSaveWithLogin();
        } else {
          await onSaveWithoutLogin();
        }
        setLoading(false);
      } catch (e) {
        print(e);
        setLoading(false);
        Toast.show(AppUtils.translate("save_delivery_address_failed", context),
            context);
      }
    }
  }

  Future onSaveWithLogin() async {
    NetworkState<List<DeliveryAddressModel>> networkState;
    if (isAddNew) {
      networkState = await deliveryRepository.createDeliveryAddress(
        fullName: nameController.text,
        firstPhone: phoneController.text,
        secondPhone: phone2Controller.text ?? "",
        provinceCode: provinceCode,
        districtCode: districtCode,
        wardCode: wardCode,
        address: addressController.text,
        isDefault: (getDefault ?? false) ? 1 : 0,
      );
    } else {
      networkState = await deliveryRepository.changeDeliveryAddress(
        deliveryAddressId: model.id,
        fullName: nameController.text,
        firstPhone: phoneController.text,
        secondPhone: phone2Controller.text ?? "",
        provinceCode: provinceCode,
        districtCode: districtCode,
        wardCode: wardCode,
        address: addressController.text,
        isDefault: (getDefault ?? false) ? 1 : 0,
      );
    }
    _checkData(networkState);
  }

  Future onSaveWithoutLogin() async {
    DeliveryAddressModel deliveryAddressModel = DeliveryAddressModel(
      id: -1,
      fullName: nameController.text,
      firstPhone: phoneController.text,
      secondPhone: phone2Controller.text ?? "",
      provinceCode: provinceCode,
      districtCode: districtCode,
      wardCode: wardCode,
      address: addressController.text,
      deliveryStatus: listDistrict
          .firstWhere((element) => element.code == districtCode)
          .deliveryStatus,
      shipFeeBulky: listDistrict
          .firstWhere((element) => element.code == districtCode)
          .shipFeeBulky,
      shipFeeNotBulky: listDistrict
          .firstWhere((element) => element.code == districtCode)
          .shipFeeNotBulky,
      isDefault: (getDefault ?? false) ? 1 : 0,
    );
    String fullAddress =
        "${addressController.text}, ${listWard.firstWhere((element) => element.code == wardCode).nameWithType}, ${listDistrict.firstWhere((element) => element.code == districtCode).nameWithType}, ${listProvince.firstWhere((element) => element.code == provinceCode).name}";
    deliveryAddressModel.fullAddress = fullAddress;
    if (await AppShared.setDeliveryAddress(deliveryAddressModel)) {
      Toast.show(AppUtils.translate("save_delivery_address_success", context),
          context);
      Navigator.pop(context, true);
    } else {
      Toast.show(
          AppUtils.translate("save_delivery_address_failed", context), context);
    }
  }

  changeDefault(bool value) {
    isDefault.add(value);
    checkChangeUser();
  }

  _checkData(NetworkState<List<DeliveryAddressModel>> networkState) async {
    if (networkState == null) return;
    if (networkState.isSuccess) {
      if (networkState.response.isSuccess) {
        Toast.show(AppUtils.translate("save_delivery_address_success", context),
            context);
        await AppShared.setDeliveryAddress(
            networkState.data.firstWhere((e) => e.isDefault == 1));
        Navigator.pop(context, true);
      } else
        Toast.show(networkState.response.message, context);
    } else {
      Toast.show(
          AppUtils.translate("save_delivery_address_failed", context), context);
    }
  }

  _preGetDeliveryAddress() async {
    listProvince = await getAllProvince();

    if (!isAddNew) {
      //Load trước danh sách quận và xã
      getDistrict(model.provinceCode);
      getWard(model.districtCode);

      nameController.text = model.fullName;
      phoneController.text = model.firstPhone;
      phone2Controller.text = model.secondPhone;
      addressController.text = model.address;

      provinceCode = model.provinceCode;
      districtCode = model.districtCode;
      wardCode = model.wardCode;

      isDefault.add((model.isDefault == 1));
    }
  }

  _reset(int typeReset) {
    switch (typeReset) {

      //reset all
      case -1:
        districtCode = null;
        wardCode = null;
        listDistrict = [];
        listWard = [];
        break;

      //reset ward
      case 0:
        wardCode = null;
        listWard = [];
        break;
    }

    notifyListeners();
  }
}
