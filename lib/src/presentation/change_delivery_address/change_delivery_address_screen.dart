import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:King_Buy/src/utils/app_valid.dart';
import 'package:flutter/material.dart';

import '../presentation.dart';

class ChangeDeliveryAddressScreen extends StatefulWidget {
  final DeliveryAddressModel params;

  const ChangeDeliveryAddressScreen({Key key, this.params}) : super(key: key);

  @override
  _ChangeDeliveryAddressScreenState createState() =>
      _ChangeDeliveryAddressScreenState();
}

class _ChangeDeliveryAddressScreenState
    extends State<ChangeDeliveryAddressScreen> with ResponsiveWidget {
  ChangeDeliveryAddressViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ChangeDeliveryAddressViewModel>(
      viewModel: ChangeDeliveryAddressViewModel(model: widget.params),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, appbar) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? true;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildScreen() {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(8),
      physics: BouncingScrollPhysics(),
      child: Form(
        key: _viewModel.formKeySignIn,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            _buildWidgetInput("full_name",
                required: true,
                controller: _viewModel.nameController,
                validator: AppValid.validateFullName(context)),
            SizedBox(
              height: 5,
            ),
            _buildWidgetInput("number_phone",
                required: true,
                controller: _viewModel.phoneController,
                validator: AppValid.validatePhoneNumber(context)),
            SizedBox(
              height: 5,
            ),
            _buildWidgetInput(
              "number_phone_2",
              controller: _viewModel.phone2Controller,
              validator:
                  AppValid.validatePhoneNumber(context, notRequired: true),
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Expanded(
                    child: _buildDropDownButton("province",
                        value: _viewModel.provinceCode,
                        onChanged: (value) async =>
                            _viewModel.onChangeProvince(value),
                        items: (_viewModel.listProvince ?? [])
                            .map<DropdownMenuItem<String>>((e) =>
                                DropdownMenuItem<String>(
                                  value: e.code,
                                  child: Text(
                                    e?.name ?? "",
                                    maxLines: 1,
                                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                        color: e.code == _viewModel.provinceCode
                                            ? Colors.black
                                            : Colors.grey),
                                  ),
                                ))
                            .toList())),
                Expanded(
                    child: _buildDropDownButton("district",
                        value: _viewModel.districtCode,
                        onChanged: (value) async =>
                            _viewModel.onChangeDistrict(value),
                        items: (_viewModel.listDistrict ?? [])
                            .map<DropdownMenuItem<String>>((e) =>
                                DropdownMenuItem<String>(
                                  value: e.code,
                                  child: Text(
                                    e?.nameWithType ?? "",
                                    maxLines: 1,
                                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                        color: e.code == _viewModel.districtCode
                                            ? Colors.black
                                            : Colors.grey),
                                  ),
                                ))
                            .toList())),
                // Expanded(child: _buildDropDownButton("district")),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            _buildDropDownButton("ward",
                value: _viewModel.wardCode,
                onChanged: (value) async => _viewModel.onChangeWard(value),
                items: (_viewModel.listWard ?? [])
                    .map<DropdownMenuItem<String>>(
                        (e) => DropdownMenuItem<String>(
                              value: e.code,
                              child: Text(
                                e?.nameWithType ?? "",
                                maxLines: 1,
                                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                    color: e.code == _viewModel.wardCode
                                        ? Colors.black
                                        : Colors.grey),
                              ),
                            ))
                    .toList()),
            SizedBox(
              height: 5,
            ),
            _buildWidgetInput(
              "full_address",
              required: true,
              controller: _viewModel.addressController,
              validator: (value) {
                if (value == null || value.length == 0) {
                  return AppLocalizations.of(context).translate("need_valid");
                }
                return null;
              },
            ),
            _buildCheckBoxDefault(),
            SizedBox(
              height: 15,
            ),
            Row(
              children: [Expanded(child: SizedBox()), _buildSaveButton()],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildCheckBoxDefault() {
    return StreamBuilder<bool>(
        stream: _viewModel.isDefault,
        builder: (context, snapshot) {
          bool value = snapshot.data ?? false;
          return Row(
            children: [
              Checkbox(
                value: value,
                onChanged: _viewModel.changeDefault,
                activeColor: Colors.redAccent,
              ),
              Text(
                AppUtils.translate("set_default", context).toUpperCase(),
                style: AppStyles.DEFAULT_MEDIUM_BOLD
                    .copyWith(color: (value) ? Colors.black : Colors.grey),
              ),
            ],
          );
        });
  }

  Widget _buildSaveButton() {
    return StreamBuilder<bool>(
        stream: _viewModel.isAvailableSave,
        builder: (context, snapshot) {
          bool isConfirm = snapshot.data ?? false;
          return PhysicalModel(
            borderRadius: BorderRadius.all(Radius.circular(100)),
            elevation: 2,
            color: (isConfirm) ? Colors.red : Colors.grey,
            child: Container(
              height: 40,
              child: WidgetButtonGradient(
                padding: const EdgeInsets.symmetric(horizontal: 50),
                colorStart: (isConfirm) ? Colors.redAccent : Colors.grey,
                colorEnd: (isConfirm) ? Colors.red : Colors.grey,
                title: "save",
                prefix: Icon(
                  Icons.save_alt,
                  size: AppStyles.DEFAULT_MEDIUM.fontSize,
                  color: Colors.white,
                ),
                action: () {
                  if (isConfirm) _viewModel.onSave();
                },
              ),
            ),
          );
        });
  }

  Widget _buildWidgetInput(String text,
      {TextEditingController controller,
      bool required = false,
      FormFieldValidator<String> validator}) {
    return WidgetInput(
      required: required,
      height: 65,
      radiusBorder: 10,
      elevation: 2,
      validator: validator,
      onChanged: (value) => _viewModel.checkChangeUser(),
      inputController: controller,
      hint: AppUtils.translate(text, context),
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "input_address",
      colorButton: Colors.white,
      actionBack: () {
        Navigator.pop(context, false);
      },
    );
  }

  Widget _buildDropDownButton(String name,
      {List<DropdownMenuItem<String>> items,
      ValueChanged<Object> onChanged,
      String value}) {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(left: 0, right: 0),
        decoration: BoxDecoration(
            // border:
            //     Border.all(color: Colors.black12.withOpacity(0.2), width: 1.2),
            borderRadius: BorderRadius.circular(10)),
        child: DropdownButtonHideUnderline(
          child: ButtonTheme(
            alignedDropdown: true,
            child: DropdownButtonFormField<String>(
              decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent))),
              hint: Row(
                children: [
                  Text(AppUtils.translate(name, context),
                      style: AppStyles.DEFAULT_MEDIUM
                          .copyWith(color: Colors.grey)),
                  Text(
                    "*",
                    style: AppStyles.DEFAULT_MEDIUM
                        .copyWith(color: Colors.redAccent),
                  )
                ],
              ),
              dropdownColor: Colors.white,
              icon: Icon(Icons.keyboard_arrow_down_sharp),
              iconSize: 20,
              isExpanded: true,
              // underline: SizedBox(),
              validator: (value) {
                if (value == null || value.length == 0) {
                  return AppLocalizations.of(context).translate("need_valid");
                }
                return null;
              },
              style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.black87),
              value: value ?? null,
              onChanged: (value) {
                onChanged(value);
                _viewModel.checkChangeUser();
              },
              items: items ?? [],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
