import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class RegisterViewModel extends BaseViewModel {
  final formKeySignIn = GlobalKey<FormState>();
  final String router;

  TextEditingController accountController;
  TextEditingController passController;
  TextEditingController confirmPassController;

  RegisterViewModel({this.router});

  init() async {
    accountController = TextEditingController();
    passController = TextEditingController();
    confirmPassController = TextEditingController();
  }

  login(Login loginType) async {
    setLoading(true);
    try {
      NetworkState _networkState;
      switch (loginType) {
        case Login.facebook:
          _networkState = await authRepository.loginFacebook();
          break;
        case Login.google:
          _networkState = await authRepository.loginGoogle();
          break;
        case Login.identity:
          unFocus();
          if (formKeySignIn.currentState.validate()) {
            _networkState = await authRepository.loginIdentity(
                identity: accountController.value.text,
                password: passController.value.text);
          }
          break;
        case Login.apple:
          break;
      }

      //Check login và trả về exception hay lỗi
      _checkLogin(networkState: _networkState);
    } catch (e) {
      print(e);
      Toast.show(AppUtils.translate("login_failure", context), context);
    }
    setLoading(false);
  }

  register() async {
    unFocus();

    if (formKeySignIn.currentState.validate()) {
      setLoading(true);

      try {
        await _checkRegister(
            networkState: await authRepository.register(
          identity: accountController.value.text,
          password: passController.value.text,
          passwordConfirm: confirmPassController.value.text,
        ));
      } catch (e) {
        print(e);
        Toast.show(AppUtils.translate("register_failure", context), context);
      }
      setLoading(false);
    }
  }

  _checkLogin({NetworkState<UserModel> networkState}) async {
    if (networkState == null) return;
    if (networkState.isSuccess && networkState.data != null) {
      Toast.show("${AppUtils.translate("login_success", context)}", context);
      await AppShared.setUser(networkState.data);
      await AppShared.setLogged(true);
      Navigator.pushReplacementNamed(context, router ?? Routers.navigation);
    } else {
      Toast.show(AppUtils.translate("login_failure", context), context);
    }
  }

  _checkRegister({NetworkState<int> networkState}) async {
    if (networkState == null) return;

    if (networkState.isSuccess) {
      if (!networkState.response.isSuccess) {
        Toast.show(networkState.response.message, context);
      } else {
        Toast.show(
            "${AppUtils.translate("register_success", context)}", context,
            duration: Toast.LENGTH_LONG);
        await Future.delayed(const Duration(milliseconds: 500));
        Navigator.pushNamed(context, Routers.otp,
            arguments: OtpScreenArguments(
              otpType: OtpType.register,
              identity: accountController.value.text,
            ));
      }
    } else {
      Toast.show(AppUtils.translate("register_failure", context), context);
    }
  }
}
