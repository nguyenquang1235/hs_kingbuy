import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../presentation.dart';

class HomeScreen extends StatefulWidget {
  final Function showPopup;
  final Function goStorePage;

  const HomeScreen({Key key, this.showPopup, this.goStorePage}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with ResponsiveWidget {
  HomeViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<HomeViewModel>(
      viewModel: HomeViewModel(),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel..init(widget.showPopup);
      },
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? true;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildScreen() {
    return WidgetLoadMoreWrapHeader<CategoryModel>.build(
      emptyBellowHeader: true,
      itemBuilder: (data, context, index) => CategoryAndItemsScreen(
        model: data[index],
      ),
      dataRequester: (offset) => _viewModel.dataRequesterCategories(offset),
      initRequester: () => _viewModel.initRequesterCategories(),
      loadingColor: Colors.redAccent,
      rangeHeader: 0,
      widgetHeader: (data, context) => _buildHeader(),
      padding: const EdgeInsets.only(bottom: 50),
    );
  }

  Widget _buildHeader() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildPromotionCarousel(),
          SizedBox(
            height: 10,
          ),
          _buildFirstCategories(),
          SizedBox(
            height: 10,
          ),
          _buildMyPromotion(),
          _buildCategoryWithItem(
            title: "new_products",
            initRequester: _viewModel.initRequesterNewProduct,
            dataRequester: _viewModel.dataRequesterNewProduct,
          ),
          _buildCategoryWithItem(
            title: "selling_products",
            initRequester: _viewModel.initRequesterSellingProducts,
            dataRequester: _viewModel.dataRequesterSellingProducts,
          ),
        ],
      ),
    );
  }

  Widget _buildAppbar() {
    return PhysicalModel(
      elevation: 2,
      color: Colors.lightBlue,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8),
        color: Colors.lightBlue,
        height: 75,
        child: Row(
          children: [
            Image.asset(
              AppImages.icLogo,
              width: 50,
              height: 50,
            ),
            Expanded(
              child: GestureDetector(
                onTap: () => _viewModel.search(),
                child: WidgetInput(
                  enabled: false,
                  height: 50,
                  hint: AppUtils.translate("search", context),
                  endIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
            Image.asset(
              AppImages.icPinGrey,
              width: 40,
              height: 30,
            ),
            GestureDetector(
              onTap: () => Navigator.pushNamed(context, Routers.cart),
              child: Image.asset(
                AppImages.icCart,
                width: 40,
                height: 30,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildPromotionCarousel() {
    return Container(
      child: _viewModel.listPromotions.length > 0
          ? WidgetCarousel<PromotionModel>.build(
              loadingColor: Colors.redAccent,
              data: _viewModel.listPromotions,
              itemBuilder: (data, context, index) =>
                  _buildPromotionItem(data[index]),
              isPoint: true,
            )
          : SizedBox(),
    );
  }

  Widget _buildPromotionItem(PromotionModel model) {
    return RepaintBoundary(
      child: WidgetImageNetwork(
        url: model.imageSource,
        width: Get.width,
        fit: BoxFit.fill,
        isAvatar: false,
      ),
    );
  }

  Widget _buildFirstCategories() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 8),
          child: Text(
            AppUtils.translate("hot_categories", context),
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Wrap(
          alignment: WrapAlignment.start,
          spacing: 8,
          runSpacing: 8,
          children: List.generate(_viewModel.listCategories.length,
              (index) => _buildCategoryItem(_viewModel.listCategories[index])),
        ),
      ],
    );
  }

  Widget _buildCategoryItem(CategoryModel model) {
    return RepaintBoundary(
      child: Container(
        width: Get.width * 0.23,
        child: Column(
          children: [
            Card(
              elevation: 2,
              margin: EdgeInsets.all(0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: model.id != -1
                    ? GestureDetector(
                        onTap: () => Navigator.pushNamed(context, Routers.category_details,
                            arguments: CategoryDetailsArguments(
                              parent: model,
                            )),
                        child: WidgetImageNetwork(
                          url: model.imageSource,
                          isAvatar: false,
                          fit: BoxFit.fill,
                          width: Get.width * 0.20,
                          height: Get.width * 0.20,
                        ),
                      )
                    : GestureDetector(
                        onTap: () => widget.goStorePage(),
                        child: Image.asset(
                          AppImages.icAllCategories,
                          width: Get.width * 0.20,
                          height: Get.width * 0.20,
                          fit: BoxFit.fill,
                        ),
                      ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              model.name.replaceAll("-", "\n").toUpperCase(),
              style: AppStyles.DEFAULT_SMALL_BOLD,
              maxLines: 2,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildMyPromotion() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 8),
          child: Text(
            AppUtils.translate("your_promotions", context),
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        SingleChildScrollView(
          physics: const PageScrollPhysics(parent: ClampingScrollPhysics()),
          scrollDirection: Axis.horizontal,
          child: Row(
            children: List.generate(
                _viewModel.listMyPromotions.length,
                (index) =>
                    _buildMyPromotionItem(_viewModel.listMyPromotions[index])),
          ),
        )
      ],
    );
  }

  Widget _buildMyPromotionItem(PromotionModel model) {
    return RepaintBoundary(
      child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 8),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: WidgetImageNetwork(
              url: model.imageSource,
              width: Get.width * 0.7,
              fit: BoxFit.fill,
              isAvatar: false,
            ),
          )),
    );
  }

  Widget _buildCategoryWithItem(
      {String title,
      DataRequesterWrap dataRequester,
      InitRequesterWrap initRequester}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8),
          child: Text(
            AppUtils.translate(title, context),
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
        ),
        WidgetLoadMoreWrapVertical<ProductModel>.build(
          direction: Axis.horizontal,
          itemBuilder: (data, context, index) => WidgetProductItem(
            model: data[index],
          ),
          dataRequester: dataRequester,
          initRequester: initRequester,
          loadingColor: Colors.redAccent,
        )
      ],
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
