import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';

import '../presentation.dart';

class HomeViewModel extends BaseViewModel {

  ScrollController scrollController = ScrollController();

  List<CategoryModel> listCategories = [];
  List<PromotionModel> listMyPromotions = [];
  List<PromotionModel> listPromotions = [];

  AppData appData;

  init(Function showPopup) async {
    setLoading(true);
    appData = await AppData.instance();
    await getFirstCategory();
    await getFirstMyPromotion();
    listPromotions = await appData.listPromotions;
    setLoading(false);
    await showPopup();
  }

  getFirstCategory() async {
    listCategories = [
      CategoryModel(
        name: "Tất cả",
        iconSource: null,
        backgroundImage: null,
        children: null,
        description: null,
        id: -1,
        imageSource: null,
        parent: null,
        parentId: null,
        slug: null,
        videoLink: null,
      )
    ];

    listCategories.addAll(await appData.listCategories);
  }

  getFirstMyPromotion() async {
    listMyPromotions.addAll(await appData.listMyPromotions);
  }

  Future<List<ProductModel>> getNewProducts(int offset) async {
    try {
      List<ProductModel> model = [];
      NetworkState<List<ProductModel>> networkState =
          await productRepository.getNewProducts(offset: offset, limit: 4);
      if (networkState.isSuccess && networkState.data != null)
        model = networkState.data;
      return model;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<ProductModel>> initRequesterNewProduct() async {
    return await getNewProducts(0);
  }

  Future<List<ProductModel>> dataRequesterNewProduct(int offset) async {
    return await getNewProducts(offset);
  }

  Future<List<ProductModel>> getSellingProducts(int offset) async {
    try {
      List<ProductModel> model = [];
      NetworkState<List<ProductModel>> networkState =
          await productRepository.getSellingProducts(offset: offset, limit: 4);
      if (networkState.isSuccess && networkState.data != null)
        model = networkState.data;
      return model;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<ProductModel>> initRequesterSellingProducts() async {
    return await getSellingProducts(0);
  }

  Future<List<ProductModel>> dataRequesterSellingProducts(int offset) async {
    return await getSellingProducts(offset);
  }

  Future<List<CategoryModel>> getCategories(int offset) async {
    try {
      NetworkState<List<CategoryModel>> networkState =
          await categoryRepository.getAllCategory(limit: 1, offset: offset);
      return networkState.data ?? [];
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<CategoryModel>> initRequesterCategories() async {
    return await getCategories(0);
  }

  Future<List<CategoryModel>> dataRequesterCategories(int offset) async {
    return await getCategories(offset);
  }

  void search() {
    Navigator.pushNamed(context, Routers.search);
  }
}
