import 'package:King_Buy/src/configs/constanst/constants.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class StoreScreen extends StatefulWidget {
  const StoreScreen({Key key}) : super(key: key);

  @override
  _StoreScreenState createState() => _StoreScreenState();
}

class _StoreScreenState extends State<StoreScreen> with ResponsiveWidget {
  StoreViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<StoreViewModel>(
      viewModel: StoreViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? false;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      isLeading: false,
      supWidget: Icon(
        FlutterIcons.dots_three_horizontal_ent,
        color: Colors.white,
      ),
      keyTitle: "categories",
      colorButton: Colors.white,
    );
  }

  Widget _buildScreen() {
    return Container(
      width: Get.width,
      child: Row(
        children: [
          _buildColumnOriginalCategory(),
          Expanded(
            child: _ChildCategoryView(
              model: _viewModel.categoryChoose,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildColumnOriginalCategory() {
    return Container(
      child: Stack(
        children: [
          Positioned.fill(
              child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[200],
                  offset: Offset(1, 1),
                )
              ],
              border: Border(
                right: BorderSide(
                  color: Colors.grey[200],
                  width: 1,
                ),
              ),
            ),
          )),
          SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: List.generate(
                _viewModel.listCategories.length,
                (index) => Stack(
                  children: [
                    Positioned.fill(
                        child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          left: BorderSide(
                            color: _viewModel.categoryChoose ==
                                    _viewModel.listCategories[index]
                                ? Colors.blue
                                : Colors.white,
                            width: 4,
                          ),
                        ),
                      ),
                    )),
                    Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: _viewModel.listCategories.last !=
                                  _viewModel.listCategories[index]
                              ? Border(
                                  bottom: BorderSide(
                                    color: Colors.grey[200],
                                    width: 1,
                                  ),
                                )
                              : null),
                      child:
                          _buildCategoryItem(_viewModel.listCategories[index]),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCategoryItem(CategoryModel model) {
    return RepaintBoundary(
      child: Container(
        width: Get.width * 0.20,
        child: model.id != -1
            ? GestureDetector(
                onTap: () => _viewModel.changeCategoriesChoose(model),
                child: Container(
                  color: Colors.transparent,
                  child: Column(
                    children: [
                      Card(
                        elevation: 2,
                        margin: EdgeInsets.all(0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          child: WidgetImageNetwork(
                            url: model.imageSource,
                            isAvatar: false,
                            fit: BoxFit.fill,
                            width: Get.width * 0.20,
                            height: Get.width * 0.20,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        (model.name.replaceAll("-", "\n") + "\n").toUpperCase(),
                        style: AppStyles.DEFAULT_SMALL_BOLD,
                        maxLines: 2,
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              )
            : Image.asset(
                AppImages.icStars,
                width: Get.width * 0.20,
                height: Get.width * 0.20,
                fit: BoxFit.fill,
              ),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}

class _ChildCategoryView extends StatefulWidget {
  final CategoryModel model;
  _ChildCategoryView({Key key, this.model}) : super(key: key);

  @override
  _ChildCategoryViewState createState() => _ChildCategoryViewState();
}

class _ChildCategoryViewState extends State<_ChildCategoryView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          _buildAppBar(),
          Expanded(
            child: _buildChildCategories(),
          ),
        ],
      ),
    );
  }

  Widget _buildAppBar() {
    return WidgetAppBar(
      height: AppStyles.DEFAULT_MEDIUM.fontSize * 2.5,
      isLeading: false,
      supWidget: InkWell(
        onTap: () => Navigator.pushNamed(context, Routers.category_details,
            arguments: CategoryDetailsArguments(parent: widget.model)),
        child: Icon(
          Icons.arrow_forward_ios,
          color: Colors.grey,
          size: AppStyles.DEFAULT_MEDIUM.fontSize,
        ),
      ),
      keyTitle: widget.model?.name ?? "",
      bgColor: Colors.white,
      colorTitle: Colors.black,
      titleSize: AppStyles.DEFAULT_MEDIUM.fontSize,
    );
  }

  Widget _buildChildCategories() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.only(bottom: 24),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: Card(
              elevation: 4,
              shadowColor: Colors.grey[200],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                child: WidgetImageNetwork(
                  isAvatar: false,
                  url: widget.model?.backgroundImage ?? "",
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: Get.width * 0.05),
            child: _buildCategoriesWrap(),
          ),
        ],
      ),
    );
  }

  Widget _buildCategoriesWrap() {
    return Container(
      width: Get.width,
      child: Wrap(
        spacing: Get.width * 0.1,
        crossAxisAlignment: WrapCrossAlignment.start,
        alignment: WrapAlignment.start,
        children: List.generate(widget.model?.children?.length ?? 0,
            (index) => _buildCategoryItem(widget.model.children[index])),
      ),
    );
  }

  Widget _buildCategoryItem(CategoryModel model) {
    return RepaintBoundary(
      child: GestureDetector(
        onTap: () => Navigator.pushNamed(context, Routers.category_details,
            arguments: CategoryDetailsArguments(
              parent: widget.model,
              child: model,
            )),
        child: Container(
          width: Get.width * 0.145,
          color: Colors.transparent,
          child: Container(
            color: Colors.transparent,
            margin: const EdgeInsets.symmetric(vertical: 8),
            child: Column(
              children: [
                Card(
                  elevation: 2,
                  margin: EdgeInsets.all(0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: WidgetImageNetwork(
                      url: model.imageSource,
                      isAvatar: false,
                      fit: BoxFit.fill,
                      width: Get.width * 0.15,
                      height: Get.width * 0.15,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  (model.name.replaceAll("-", "\n") + "\n"),
                  style: AppStyles.DEFAULT_SMALL_BOLD,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
