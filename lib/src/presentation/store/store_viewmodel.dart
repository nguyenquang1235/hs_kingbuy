import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/utils.dart';

class StoreViewModel extends BaseViewModel {
  AppData appData;
  CategoryModel categoryChoose;
  List<CategoryModel> listCategories = [
    CategoryModel(
      name: "Tất cả",
      iconSource: null,
      backgroundImage: null,
      children: null,
      description: null,
      id: -1,
      imageSource: null,
      parent: null,
      parentId: null,
      slug: null,
      videoLink: null,
    )
  ];

  init() async {
    setLoading(true);
    appData = await AppData.instance();
    listCategories.addAll(await appData.listCategories);
    categoryChoose = listCategories[1];
    setLoading(false);
  }

  void changeCategoriesChoose(CategoryModel choose){
    categoryChoose = choose;
    notifyListeners();
  }
}
