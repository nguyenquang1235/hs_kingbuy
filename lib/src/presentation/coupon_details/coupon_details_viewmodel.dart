import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/coupon_application.dart';
import 'package:King_Buy/src/resource/model/model.dart';

class CouponDetailsViewModel extends BaseViewModel {
  final int couponId;
  CouponDetailsViewModel({this.couponId});

  CouponModel model;

  init() async {
    setLoading(true);
    model = await getDetails();
    setLoading(false);
  }

  Future<CouponModel> getDetails() async {
    try {
      NetworkState networkState =
          await couponRepository.getDetails(couponId: couponId);
      return networkState.data;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
