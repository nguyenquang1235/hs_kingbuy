import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class CouponDetails extends StatefulWidget {
  final int couponId;

  const CouponDetails({Key key, this.couponId}) : super(key: key);
  @override
  _CouponDetailsState createState() => _CouponDetailsState();
}

class _CouponDetailsState extends State<CouponDetails> with ResponsiveWidget {
  CouponDetailsViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CouponDetailsViewModel>(
      viewModel: CouponDetailsViewModel(couponId: widget.couponId),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
                stream: _viewModel.loadingSubject,
                builder: (context, snapshot) {
                  bool isLoading = snapshot.data ?? true;
                  return WidgetLoadingPage(
                    isLoading,
                    page: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context))
                      ],
                    ),
                  );
                }),
          ),
        );
      },
    );
  }

  Widget _buildScreen() {
    return _viewModel.model != null
        ? SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            padding:
                const EdgeInsets.only(top: 24, bottom: 75, left: 12, right: 4),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  _viewModel.model.name,
                  style: AppStyles.DEFAULT_MEDIUM_BOLD
                      .copyWith(color: Colors.redAccent),
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 5,
                ),
                Text(_viewModel.model?.description ?? "",
                    textAlign: TextAlign.start, style: AppStyles.DEFAULT_MEDIUM,),
                WidgetImageNetwork(
                  url: _viewModel.model?.imageSource ?? "",
                  isAvatar: false,
                )
              ],
            ),
          )
        : _buildNoneData();
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: _viewModel.model?.name ?? "coupon",
      colorButton: Colors.white,
    );
  }

  Widget _buildNoneData() {
    return Container(
      width: Get.width,
      height: Get.height,
      child: Center(
        child: Text(AppUtils.translate("no_data", context)),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
