import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:King_Buy/src/utils/app_valid.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../presentation.dart';

class ResetPassArgument {
  final String smsCode;
  final String identity;

  ResetPassArgument({this.smsCode, this.identity});
}

class ResetPasswordScreen extends StatefulWidget {
  final ResetPassArgument resetPassArgument;

  const ResetPasswordScreen({Key key, this.resetPassArgument})
      : super(key: key);

  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen>
    with ResponsiveWidget {
  ResetPasswordViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ResetPasswordViewModel>(
      viewModel:
          ResetPasswordViewModel(resetPassArgument: widget.resetPassArgument),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: buildUi(context: context),
          ),
        );
      },
    );
  }

  _buildScreen() {
    return StreamBuilder(
      stream: _viewModel.loadingSubject,
      builder: (context, snapshot) {
        bool isLoading = snapshot.data ?? false;
        return WidgetLoadingPage(
          isLoading,
          page: _buildBody(),
        );
      },
    );
  }

  _buildBody() {
    return Column(
      children: [
        WidgetAppBar(
          bgColor: Colors.blue,
          colorButton: Colors.white,
          keyTitle: widget.resetPassArgument != null
              ? "reset_password"
              : "change_pass",
        ),
        Expanded(
            child: SingleChildScrollView(
          child: _buildBodyScreen(),
        ))
      ],
    );
  }

  _buildBodyScreen() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25, horizontal: 16),
      child: Column(
        children: [
          SizedBox(
            height: 100,
          ),
          Text(
            AppUtils.translate(
              widget.resetPassArgument != null ? "new_pass" : "change_pass",
              context,
            ).toUpperCase(),
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(fontSize: 25),
          ),
          SizedBox(
            height: 45,
          ),
          Form(
            key: _viewModel.formKeySignIn,
            child: Column(
              children: [
                if (widget.resetPassArgument == null)
                  WidgetInput(
                    hint: AppUtils.translate(
                      "old_pass",
                      context,
                    ),
                    obscureText: true,
                    validator: AppValid.validatePassword(context),
                    preIcon: Icon(
                      FlutterIcons.lock_ant,
                      color: AppColors.grey,
                      size: 40,
                    ),
                    inputController: _viewModel.passController,
                    onChanged: (value) => null,
                  ),
                SizedBox(
                  height: 12,
                ),
                WidgetInput(
                  hint: AppUtils.translate(
                    "new_pass",
                    context,
                  ),
                  obscureText: true,
                  validator: AppValid.validatePassword(context),
                  preIcon: Icon(
                    FlutterIcons.lock_ant,
                    color: AppColors.grey,
                    size: 40,
                  ),
                  inputController: _viewModel.newPassController,
                  onChanged: (value) => null,
                ),
                SizedBox(
                  height: 12,
                ),
                WidgetInput(
                  hint: AppUtils.translate(
                    "enter_the_password",
                    context,
                  ),
                  obscureText: true,
                  validator: AppValid.validatePassword(context),
                  preIcon: Icon(
                    FlutterIcons.lock_ant,
                    color: AppColors.grey,
                    size: 40,
                  ),
                  inputController: _viewModel.confirmPassController,
                  onChanged: (value) => null,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 25,
          ),
          _buildRowButtonContinue(),
          SizedBox(
            height: 25,
          ),
        ],
      ),
    );
  }

  _buildRowButtonContinue() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Opacity(
          opacity: 0,
          child: Padding(
            padding: const EdgeInsets.only(left: 16),
            child: Text(
              AppUtils.translate("forgot_pass", context) + " ?",
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
          ),
        ),
        PhysicalModel(
          borderRadius: BorderRadius.all(Radius.circular(100)),
          elevation: 2,
          color: Colors.grey,
          child: WidgetButtonGradient(
            padding: EdgeInsets.fromLTRB(30, 12, 30, 12),
            colorStart: Colors.redAccent,
            colorEnd: Colors.red,
            title: "update",
            action: () => _viewModel.resetPass(),
          ),
        ),
      ],
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
