import 'package:King_Buy/src/presentation/base/base.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../presentation.dart';

class ResetPasswordViewModel extends BaseViewModel {
  final ResetPassArgument resetPassArgument;
  final formKeySignIn = GlobalKey<FormState>();

  TextEditingController passController;
  TextEditingController newPassController;
  TextEditingController confirmPassController;

  ResetPasswordViewModel({this.resetPassArgument});

  init() async {
    setLoading(true);
    newPassController = TextEditingController();
    confirmPassController = TextEditingController();
    if (resetPassArgument == null) passController = TextEditingController();
    await Future.delayed(const Duration(milliseconds: 700));
    setLoading(false);
  }

  resetPass() async {
    unFocus();
    if (formKeySignIn.currentState.validate()) {
      setLoading(true);
      try {
        await checkData(
          networkState: await upDateOrResetPassword(),
        );
      } catch (e) {
        print(e);
        Toast.show(AppUtils.translate("system_errors", context), context);
      }
      setLoading(false);
    }
  }

  Future<NetworkState> upDateOrResetPassword() async {
    if (resetPassArgument != null) {

      return await authRepository.resetPassWhenForgot(
          identity: resetPassArgument.identity,
          smsCode: resetPassArgument.smsCode,
          passwordConfirm: confirmPassController.value.text,
          password: newPassController.value.text);
    }
    
    return await authRepository.changePassword(
      oldPassword: passController.value.text,
      newPassword: newPassController.value.text,
      newConfirmPassword: confirmPassController.value.text,
    );
  }

  checkData({NetworkState networkState}) async {
    if (networkState == null) return;
    if (networkState.isSuccess) {
      if (!networkState.response.isSuccess) {
        Toast.show(networkState.response.message, context);
      } else {
        Toast.show(
            AppUtils.translate("reset_password_success", context), context);
      }
    } else {
      //Khi đăng ký bị lỗi
      Toast.show(AppUtils.translate("system_errors", context), context);
    }
  }
}
