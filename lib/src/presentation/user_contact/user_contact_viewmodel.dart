import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:toast/toast.dart';

typedef ContactForm({
  String fullName,
  String email,
  String phoneNumber,
  String subject,
  String body,
});

class UserContactViewModel extends BaseViewModel {
  bool isSendFeedback;

  UserContactViewModel({this.isSendFeedback});

  init() async {
    // setLoading(true);
    // await Future.delayed(const Duration(milliseconds: 1000));
    // setLoading(false);
  }

  sendContractForm({
    String fullName,
    String email,
    String phoneNumber,
    String subject,
    String body,
  }) async {
    try {
      setLoading(true);
      NetworkState networkState = await contactRepository.sendContractForm(
          fullName, email, phoneNumber, subject, body);
      if (networkState.isSuccess) {
        Toast.show(networkState.response.message, context);
        if (networkState.response.isSuccess) {
          isSendFeedback = true;
          notifyListeners();
        }
      } else {
        Toast.show(networkState.message, context);
      }
      setLoading(false);
    } catch (e) {
      print(e);
      setLoading(false);
      Toast.show(AppUtils.translate("system_errors", context), context);
    }
  }
}
