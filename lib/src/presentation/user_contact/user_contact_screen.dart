import 'package:flutter/material.dart';

import '../presentation.dart';

class UserContactScreen extends StatefulWidget {
  final bool isSendFeedback;

  const UserContactScreen({Key key, this.isSendFeedback = false})
      : super(key: key);

  @override
  _UserContactScreenState createState() => _UserContactScreenState();
}

class _UserContactScreenState extends State<UserContactScreen> {
  UserContactViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<UserContactViewModel>(
      viewModel: UserContactViewModel(isSendFeedback: widget.isSendFeedback),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, appbar) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: Colors.grey[200],
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? false;
                return WidgetLoadingPage(
                  isLoading,
                  page: _buildScreen(),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildScreen() {
    if (_viewModel.isSendFeedback) {
      return ContactDetailsScreen(
        viewModel: _viewModel,
      );
    }
    return CreateContactScreen(
      viewModel: _viewModel,
    );
  }
}
