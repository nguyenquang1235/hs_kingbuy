import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:flutter/cupertino.dart';

class CreateContactVieModel extends BaseViewModel {
  final GlobalKey<FormState> formKeySignIn = GlobalKey();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController titleController = TextEditingController();
  final TextEditingController contentController = TextEditingController();

  init() async {}

  submitForm(ContactForm contactForm) {
    if (formKeySignIn.currentState.validate()) {
      contactForm(
        fullName: nameController.text,
        email: emailController.text,
        phoneNumber: phoneNumberController.text,
        subject: titleController.text,
        body: contentController.text,
      );
    }
  }
}
