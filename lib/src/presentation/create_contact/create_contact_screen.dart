import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/utils/app_valid.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class CreateContactScreen extends StatefulWidget {
  final UserContactViewModel viewModel;

  const CreateContactScreen({Key key, this.viewModel}) : super(key: key);

  @override
  _CreateContactScreenState createState() => _CreateContactScreenState();
}

class _CreateContactScreenState extends State<CreateContactScreen>
    with ResponsiveWidget {
  CreateContactVieModel _vieModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreateContactVieModel>(
      viewModel: CreateContactVieModel(),
      onViewModelReady: (viewModel) => _vieModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Column(
          children: [
            _buildAppbar(),
            Expanded(child: buildUi(context: context))
          ],
        );
      },
    );
  }

  Widget _buildScreen() {
    return Container(
      color: Colors.grey[200],
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        padding: const EdgeInsets.only(top: 16, left: 10, right: 10),
        child: Form(
          key: _vieModel.formKeySignIn,
          child: Column(
            children: [
              _buildWidgetInput("full_name",
                  controller: _vieModel.nameController,
                  validator: AppValid.validateFullName(context)),
              SizedBox(
                height: 5,
              ),
              _buildWidgetInput("email",
                  controller: _vieModel.emailController,
                  validator: AppValid.validateEmail(context)),
              SizedBox(
                height: 5,
              ),
              _buildWidgetInput("number_phone",
                  controller: _vieModel.phoneNumberController,
                  validator: AppValid.validatePhoneNumber(context)),
              SizedBox(
                height: 5,
              ),
              _buildWidgetInput(
                "name_title",
                controller: _vieModel.titleController,
                validator: (value) {
                  if (value == null || value.length == 0) {
                    return AppLocalizations.of(context).translate("need_valid");
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 5,
              ),
              _buildContentField()
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildWidgetInput(String text,
      {TextEditingController controller,
      FormFieldValidator<String> validator}) {
    return WidgetInput(
      height: 65,
      radiusBorder: 10,
      elevation: 2,
      validator: validator,
      onChanged: (value) => null,
      inputController: controller,
      hint: AppUtils.translate(text, context),
    );
  }

  Widget _buildContentField() {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Container(
        width: Get.width,
        decoration: BoxDecoration(
          border: Border.all(width: 0.5, color: AppColors.grey),
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppUtils.translate("content", context),
              style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.grey),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              controller: _vieModel.contentController,
              style: AppStyles.DEFAULT_MEDIUM,
              maxLines: 7,
              textAlign: TextAlign.left,
              decoration: InputDecoration.collapsed(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "contact",
      colorButton: Colors.white,
      supWidget: GestureDetector(
          onTap: () => _vieModel.submitForm(widget.viewModel.sendContractForm),
          child: Icon(
            FlutterIcons.paper_plane_faw5,
            color: Colors.white,
          )),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
