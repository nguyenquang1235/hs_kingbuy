import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:flutter/material.dart';
class InvoiceDetailsViewModel extends BaseViewModel {

  ScrollController controller = new ScrollController();

  init({Function callback}) async {
    setLoading(true);
    controller.addListener(() {
      if (controller.position.pixels ==
          controller.position.maxScrollExtent) {
        callback();
      }
    });
    await Future.delayed(const Duration(milliseconds: 700));
    setLoading(false);

  }
}
