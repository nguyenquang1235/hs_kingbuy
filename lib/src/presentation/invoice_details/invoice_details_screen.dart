import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/invoice_model.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class InvoiceDetailsScreen extends StatefulWidget {
  final InvoiceModel param;

  const InvoiceDetailsScreen({Key key, this.param}) : super(key: key);
  @override
  _InvoiceDetailsScreenState createState() => _InvoiceDetailsScreenState();
}

class _InvoiceDetailsScreenState extends State<InvoiceDetailsScreen>
    with ResponsiveWidget {
  InvoiceDetailsViewModel _viewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<InvoiceDetailsViewModel>(
      viewModel: InvoiceDetailsViewModel(),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel..init(callback: _loadMore);
      },
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: Colors.grey[200],
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? true;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "invoice",
      colorButton: Colors.white,
    );
  }

  _buildScreen() {
    return ListView(
      padding: const EdgeInsets.only(bottom: 50),
      controller: _viewModel.controller,
      physics: BouncingScrollPhysics(),
      children: [
        _buildFirstDetails(),
        if (widget.param?.isExportInvoice != null &&
            widget.param?.isExportInvoice == 1)
          _buildTaxDetails(),
        _buildPaymentDetails(),
        _buildPaymentMethods(),
        _buildRewardsPoints(),
      ],
    );
  }

  _buildFirstDetails() {
    return Container(
      color: Colors.white,
      margin: const EdgeInsets.symmetric(vertical: 5),
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildBasicDetails(),
          _buildStatusDetails(),
          _buildInvoicesProduct(),
          _buildDeliveryDetails(),
          _buildNoteDetails(),
        ],
      ),
    );
  }

  _buildBasicDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppUtils.translate("invoice_success", context).toUpperCase(),
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
            AppUtils.convertString2String(widget.param.createdAt,
                outputFormat: "hh:mm - EEEE dd/MM/yyyy",
                inputFormat: "hh:mm:ss yyyy-MM-dd"),
            style: AppStyles.DEFAULT_MEDIUM),
        SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Text("${AppUtils.translate("invoice_code", context)}: ",
                style: AppStyles.DEFAULT_MEDIUM),
            Text("${widget.param.invoiceCode}",
                style: AppStyles.DEFAULT_MEDIUM_BOLD)
          ],
        ),
        Divider()
      ],
    );
  }

  _buildStatusDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppUtils.translate("invoice_status", context).toUpperCase(),
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
            AppUtils.translate(
                "invoice_status_${widget.param.status}", context),
            style: AppStyles.DEFAULT_MEDIUM),
        SizedBox(
          height: 10,
        ),
        if (widget.param?.deliveryDate != null &&
            widget.param?.deliveryDate != "")
          Text(
              AppUtils.convertString2String(widget.param.deliveryDate,
                  outputFormat: "hh:mm - EEEE dd/MM/yyyy",
                  inputFormat: "hh:mm:ss yyyy-MM-dd"),
              style: AppStyles.DEFAULT_MEDIUM),
        Divider()
      ],
    );
  }

  _buildInvoicesProduct() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "${AppUtils.translate("list_product", context).toUpperCase()} (${widget.param.items.length})",
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        SizedBox(
          height: 10,
        ),
        Column(
          children: List.generate(widget.param.items.length,
              (index) => _buildProductInInvoice(widget.param.items[index])),
        ),
        SizedBox(
          height: 5,
        ),
        Divider()
      ],
    );
  }

  _buildDeliveryDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppUtils.translate("invoice_info", context).toUpperCase(),
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        SizedBox(
          height: 10,
        ),
        Text(widget.param.orderName, style: AppStyles.DEFAULT_MEDIUM_BOLD),
        SizedBox(
          height: 5,
        ),
        SizedBox(
          width: Get.width * 0.75,
          child: Text(
            widget.param?.orderAddress ?? "",
            style: AppStyles.DEFAULT_MEDIUM,
            maxLines: 2,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
            "${widget.param.orderPhone} ${widget.param?.orderPhone2 != null ? "-" : ""} ${widget.param?.orderPhone2 ?? ""}"),
        Divider()
      ],
    );
  }

  _buildNoteDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppUtils.translate("note", context).toUpperCase(),
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        SizedBox(
          height: 10,
        ),
        Text(widget.param?.note ?? "", style: AppStyles.DEFAULT_MEDIUM),
      ],
    );
  }

  _buildProductInInvoice(InvoiceItem item) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Card(
          elevation: 2,
          child: WidgetImageNetwork(
            url: item.imageSource,
            width: Get.width * 0.25,
            height: Get.width * 0.25,
            isAvatar: false,
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.productName,
                style: AppStyles.DEFAULT_MEDIUM,
                maxLines: 1,
              ),
              _buildMoneyPrice(item.price,
                  styleInput: AppStyles.DEFAULT_MEDIUM_BOLD
                      .copyWith(color: Colors.redAccent)),
            ],
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Container(
          height: Get.width * 0.25,
          child: Center(
            child: Text(
              "x${item.qty.toString()}",
              style: AppStyles.DEFAULT_MEDIUM,
            ),
          ),
        )
      ],
    );
  }

  _buildTaxDetails() {
    return Container(
      color: Colors.white,
      margin: const EdgeInsets.symmetric(vertical: 5),
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("tax_required", context).toUpperCase(),
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          SizedBox(
            height: 15,
          ),
          Text(
              "${AppUtils.translate("tax_code", context)} ${widget.param?.taxCode ?? ""}",
              style: AppStyles.DEFAULT_MEDIUM_BOLD),
          Text(widget.param?.companyName ?? "",
              style: AppStyles.DEFAULT_MEDIUM),
          SizedBox(
            width: Get.width * 0.75,
            child: Text(widget.param?.companyAddress ?? "",
                maxLines: 2, style: AppStyles.DEFAULT_MEDIUM),
          ),
          Text(widget.param?.companyEmail ?? "",
              style: AppStyles.DEFAULT_MEDIUM),
        ],
      ),
    );
  }

  _buildPaymentDetails() {
    TextStyle style = AppStyles.DEFAULT_MEDIUM;
    int provisional = (widget.param?.total ?? 0) -
        (widget.param?.deliveryCharge ?? 0) +
        (widget.param?.discount ?? 0);
    return Container(
      color: Colors.white,
      margin: const EdgeInsets.symmetric(vertical: 5),
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("payment_info", context).toUpperCase(),
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppUtils.translate("provisional", context),
                style: style,
              ),
              _buildMoneyPrice(provisional,
                  styleInput: AppStyles.DEFAULT_MEDIUM)
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppUtils.translate("transport_fee", context),
                style: style,
              ),
              _buildMoneyPrice(widget.param?.deliveryCharge ?? 0,
                  styleInput: AppStyles.DEFAULT_MEDIUM)
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppUtils.translate("discount", context),
                style: style,
              ),
              _buildMoneyPrice(widget.param?.discount ?? 0,
                  styleInput: AppStyles.DEFAULT_MEDIUM, isNegative: true)
            ],
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppUtils.translate("total", context).toUpperCase(),
                style:
                    style.copyWith(fontWeight: FontWeight.bold),
              ),
              _buildMoneyPrice(widget.param?.total ?? 0,
                  styleInput: AppStyles.DEFAULT_LARGE_BOLD
                      .copyWith(color: Colors.redAccent))
            ],
          ),
        ],
      ),
    );
  }

  _buildMoneyPrice(int price, {TextStyle styleInput, bool isNegative = false}) {
    TextStyle style = styleInput ?? AppStyles.DEFAULT_MEDIUM_BOLD;
    return Row(
      children: [
        Text(
          '${isNegative ? "-" : ""}${NumberFormat(",###", "vi").format(price)}',
          style: style,
        ),
        Text("đ", style: style.copyWith(fontSize: style.fontSize - 3)),
      ],
    );
  }

  _buildPaymentMethods() {
    return Container(
      color: Colors.white,
      margin: const EdgeInsets.symmetric(vertical: 5),
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("payment_type", context).toUpperCase(),
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
              AppUtils.translate(
                  "payment_type_${widget.param.paymentType}", context),
              style: AppStyles.DEFAULT_MEDIUM)
        ],
      ),
    );
  }

  _buildRewardsPoints() {
    return Container(
      color: Colors.white,
      margin: const EdgeInsets.symmetric(vertical: 5),
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("reward_point", context).toUpperCase(),
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
              "${widget.param?.rewardPoints??0}",
              style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.redAccent))
        ],
      ),
    );
  }

  _loadMore() {}

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
