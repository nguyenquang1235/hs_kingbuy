import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../presentation.dart';

class ProductList extends StatefulWidget {
  final CartViewModel viewModel;
  const ProductList({Key key, this.viewModel}) : super(key: key);

  @override
  _ProductListState createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("list_product", context).toUpperCase(),
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
          SizedBox(
            height: 15,
          ),
          _buildListProduct()
        ],
      ),
    );
  }

  Widget _buildListProduct() {
    var data = widget.viewModel.productItems;
    if (data.length > 0)
      return Column(
        children: List.generate(
            data.length,
            (index) => ProductItem(
                  action: () => widget.viewModel.updateCartItem(data[index]),
                  delete: () => widget.viewModel.deleteCartItem(data[index]),
                  model: data[index],
                )),
      );
    return _buildNoneData();
  }

  Widget _buildNoneData() {
    return Container(
      width: Get.width,
      child: Text(
        AppUtils.translate("cart_none_products", context),
        style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.red),
        maxLines: 2,
        textAlign: TextAlign.start,
      ),
    );
  }
}

class ProductItem extends StatefulWidget {
  final Function action;
  final ProductModel model;
  final Function delete;
  const ProductItem({Key key, this.model, this.delete, this.action})
      : super(key: key);

  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Card(
            elevation: 4,
            child: WidgetImageNetwork(
              url: widget.model.imageSource,
              width: Get.width * 0.20,
              height: Get.width * 0.20,
              isAvatar: false,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 5,
                ),
                Text(
                  widget.model.name,
                  style: AppStyles.DEFAULT_MEDIUM,
                  maxLines: 1,
                ),
                SizedBox(
                  height: 5,
                ),
                _buildMoneyPrice(widget.model.salePrice,
                    styleInput: AppStyles.DEFAULT_MEDIUM_BOLD
                        .copyWith(color: Colors.redAccent)),
                SizedBox(
                  height: 5,
                ),
                _buildChangeQltBox()
              ],
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            height: Get.width * 0.25,
            child: GestureDetector(
              onTap: () {
                widget.delete();
              },
              child: Center(
                child: Text(
                  AppUtils.translate("delete", context),
                  style: AppStyles.DEFAULT_SMALL.copyWith(color: Colors.blue),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildMoneyPrice(int price,
      {TextStyle styleInput, bool isNegative = false}) {
    TextStyle style = styleInput ?? AppStyles.DEFAULT_MEDIUM_BOLD;
    return Row(
      children: [
        Text(
          '${isNegative ? "-" : ""}${NumberFormat(",###", "vi").format(price)}',
          style: style,
        ),
        Text("đ", style: style.copyWith(fontSize: style.fontSize - 3)),
      ],
    );
  }

  Widget _buildChangeQltBox() {
    return Container(
      width: Get.width * 0.2,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
      child: Row(
        children: [
          _buildButton("-"),
          Expanded(
              child: Center(
            child: Text(widget.model.qty.toString()),
          )),
          _buildButton("+", isLeft: true),
        ],
      ),
    );
  }

  Widget _buildButton(String text, {bool isLeft = false}) {
    return GestureDetector(
      onTap: () => changeQlt(isLeft),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 4),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border(
            left: isLeft
                ? BorderSide(
                    color: Colors.grey,
                  )
                : BorderSide(
                    color: Colors.white,
                  ),
            right: isLeft
                ? BorderSide(
                    color: Colors.white,
                  )
                : BorderSide(
                    color: Colors.grey,
                  ),
          ),
        ),
        child: Center(
          child: Text(text),
        ),
      ),
    );
  }

  void changeQlt(isIncrease) {
    int change = widget.model.qty;
    if (isIncrease) {
      change++;
    } else {
      if (change > 1) {
        change--;
      }
    }
    if (change != widget.model.qty) {
      widget.model.qty = change;
    }

    widget.action();
  }
}
