import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class DeliveryInfo extends StatefulWidget {
  final CartViewModel viewModel;
  const DeliveryInfo({Key key, this.viewModel}) : super(key: key);

  @override
  _DeliveryInfoState createState() => _DeliveryInfoState();
}

class _DeliveryInfoState extends State<DeliveryInfo> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => widget.viewModel.onChangeAddress(),
      child: Container(
        width: Get.width,
        padding: EdgeInsets.symmetric(vertical: 8),
        color: Colors.transparent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppUtils.translate("invoice_info", context).toUpperCase(),
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
            if (widget.viewModel.deliveryAddressModel != null)
              _buildDeliveryAddress()
          ],
        ),
      ),
    );
  }

  Widget _buildDeliveryAddress() {
    TextStyle defaultStyle = AppStyles.DEFAULT_MEDIUM;
    return Container(
      width: Get.width,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 15,
          ),
          Text(
            widget.viewModel.deliveryAddressModel.fullName,
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            widget.viewModel.deliveryAddressModel.fullAddress,
            style: defaultStyle,
            maxLines: 2,
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            widget.viewModel.deliveryAddressModel.firstPhone,
            style: defaultStyle,
            maxLines: 2,
          ),
          SizedBox(
            height: 2,
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
