import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/cart/cart.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeliveryTime extends StatefulWidget {
  final CartViewModel viewModel;
  const DeliveryTime({Key key, this.viewModel}) : super(key: key);

  @override
  _DeliveryTimeState createState() => _DeliveryTimeState();
}

class _DeliveryTimeState extends State<DeliveryTime> {
  int status;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      padding: EdgeInsets.symmetric(vertical: 8),
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("delivery_time", context).toUpperCase(),
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
          SizedBox(
            height: 15,
          ),
          if (widget.viewModel.deliveryAddressModel != null)
            if(widget.viewModel.deliveryAddressModel.deliveryStatus.length > 0)
            _buildListDeliveryTimes(),
        ],
      ),
    );
  }

  Widget _buildListDeliveryTimes() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: List.generate(
          widget.viewModel.deliveryAddressModel.deliveryStatus.length,
          (index) => _buildDeliveryTime(
              widget.viewModel.deliveryAddressModel.deliveryStatus[index])),
    );
  }

  Widget _buildDeliveryTime(int index) {
    return Row(
      children: [
        Container(
            height: Get.width * 0.08,
            width: Get.width * 0.07,
            child: Radio(
              activeColor: Colors.red,
              value: (index + 1),
              groupValue: status,
              onChanged: (value) {
                if (status != value) {
                  setState(() {
                    status = value;
                  });
                }
              },
            )),
        SizedBox(
          width: 10,
        ),
        Text(
          AppUtils.translate("delivery_status_${index}", context),
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: (index + 1 == status) ? Colors.black : Colors.grey),
        )
      ],
    );
  }
}
