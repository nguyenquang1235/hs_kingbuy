import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_shared.dart';
import 'package:flutter/material.dart';

enum TaxField {
  taxCode,
  companyName,
  companyAddress,
  companyEmail,
}

class CartMoney {
  double total;
  double totalUnread;
  double reduce;
  double fee;

  CartMoney({this.totalUnread, this.reduce, this.fee}) {
    this.total = (totalUnread + fee) - reduce;
  }

  change({double totalUnread, double reduce, double fee}) {
    this.totalUnread = totalUnread ?? this.totalUnread;
    this.reduce = reduce ?? this.reduce;
    this.fee = fee ?? this.fee;
    total = (this.totalUnread + this.fee) - this.reduce;
  }
}

class CartViewModel extends BaseViewModel {
  CartDataBase _cartDataBase;
  CartMoney cartMoney = CartMoney(reduce: 0, totalUnread: 0, fee: 0);
  CartModel cartModel;
  CouponModel couponModel;
  DeliveryAddressModel deliveryAddressModel;
  List<ProductModel> productItems = [];
  List<CartItem> cartItems = [];
  List<int> deliveriesStatus = [];

  TextEditingController taxCodeController = TextEditingController();
  TextEditingController companyNameController = TextEditingController();
  TextEditingController companyAddressController = TextEditingController();
  TextEditingController companyEmailController = TextEditingController();

  init() async {
    setLoading(true);
    _cartDataBase = CartDataBase.instance();
    await getCartModel();
    await getCartItemDataBase();
    await getDeliveryAddress();
    _checkBulky();
    calculateMoney();
    calculateReduceCoupon();
    getTaxDetail();
    setLoading(false);
  }

  getDeliveryAddress() async {
    try {
      if (await AppShared.getLogged()) {
        deliveryAddressModel = (await deliveryRepository.getDeliveryAddress())
            .data
            .firstWhere((element) => element.isDefault == 1);
      } else {
        deliveryAddressModel = await AppShared.getDeliveryAddress();
      }
    } catch (e) {
      print(e);
    }
  }

  Future getCartItemDataBase() async {
    try {
      double totalUnread = 0;
      cartItems = await _cartDataBase.getCartItems();
      if (cartItems.length <= 0) return;
      List<int> listId = cartItems.map((e) => e.productId).toList();
      List<ProductModel> listProduct =
          ((await productRepository.getProductByIds(listId: listId)).data)
            ..forEach((product) {
              product.qty =
                  cartItems.where((e) => e.productId == product.id).first.qty;
              totalUnread += product.salePrice * product.qty;
            });

      cartMoney.change(totalUnread: totalUnread);
      productItems = listProduct;
    } catch (e) {
      print(e);
    }
  }

  Future getCartModel() async {
    try {
      cartModel = await _cartDataBase.getCart();
    } catch (e) {
      print(e);
    }
  }

  Future updateCartItem(ProductModel model) async {
    CartItem itemUpdate =
        cartItems.where((element) => element.productId == model.id).first;
    itemUpdate.qty = model.qty;
    await _cartDataBase.updateCartItem(itemUpdate);
    calculateMoney();
    calculateReduceCoupon();
    notifyListeners();
  }

  _checkBulky() {
    for (var item in productItems) {
      if (item.isBulky == 1) {
        cartMoney.change(
            fee: double.parse(
                (deliveryAddressModel?.shipFeeBulky ?? 0).toString()));
        return;
      }
    }
    cartMoney.change(
        fee: double.parse(
            (deliveryAddressModel?.shipFeeNotBulky ?? 0).toString()));
  }

  Future deleteCartItem(ProductModel model) async {
    CartItem itemDelete =
        cartItems.where((element) => element.productId == model.id).first;
    await _cartDataBase.deleteCartItem(itemDelete.id);
    productItems.remove(model);
    cartItems.remove(itemDelete);
    _checkBulky();
    calculateMoney();
    calculateReduceCoupon();
    notifyListeners();
  }

  void calculateMoney() {
    double totalUnread = 0;
    if (productItems.length > 0) {
      productItems.forEach((element) {
        totalUnread += element.qty * element.salePrice;
      });
    }
    cartMoney.change(totalUnread: totalUnread);
  }

  void onChangeAddress() {
    Navigator.pushNamed(context, Routers.choose_delivery_address).then((value) {
      if (value != null) {
        deliveryAddressModel = value;
        _checkBulky();
        calculateReduceCoupon();
        notifyListeners();
      }
    });
  }

  void onChangeCoupon() {
    Navigator.pushNamed(context, Routers.cart_coupons,
            arguments: cartMoney.totalUnread)
        .then((value) {
      if (value != null) {
        couponModel = value;
        calculateReduceCoupon();
        notifyListeners();
      }
    });
  }

  void calculateReduceCoupon() {
    if (couponModel != null && deliveryAddressModel != null) {
      switch (couponModel.type) {
        case 1:
          double reduce = cartMoney.total * (couponModel.percentOff / 100);
          if (reduce > couponModel.maxSale) {
            cartMoney.change(
                reduce: double.parse(couponModel.maxSale.toString()));
          } else {
            cartMoney.change(reduce: reduce);
          }
          break;
        case 2:
          cartMoney.change(
              reduce: double.parse(couponModel.salePrice.toString()));
          break;
        case 3:
          cartMoney.change(reduce: cartMoney.fee);
          break;
        default:
          break;
      }
    }
  }

  void onChangePaymentType() {
    Navigator.pushNamed(context, Routers.cart_payment).then((value) async {
      if (value != null) {
        cartModel.paymentType = value;
        notifyListeners();
        await _cartDataBase.updateCart(cartModel);
      }
    });
  }

  void onChangeRequestVAT(bool value) async {
    cartModel.isExportInvoice = value ? 1 : 0;
    notifyListeners();
    await _cartDataBase.updateCart(cartModel);
  }

  void onChangeVATField(TaxField taxField) async {
    switch (taxField) {
      case TaxField.taxCode:
        cartModel.taxCode = taxCodeController.text;
        break;
      case TaxField.companyName:
        cartModel.companyName = companyNameController.text;
        break;
      case TaxField.companyAddress:
        cartModel.companyAddress = companyAddressController.text;
        break;
      case TaxField.companyEmail:
        cartModel.companyEmail = companyEmailController.text;
        break;
      default:
        break;
    }
    await _cartDataBase.updateCart(cartModel);
  }

  void getTaxDetail() {
    taxCodeController.text = cartModel.taxCode;
    companyNameController.text = cartModel.companyName;
    companyAddressController.text = cartModel.companyAddress;
    companyEmailController.text = cartModel.companyEmail;
  }
}
