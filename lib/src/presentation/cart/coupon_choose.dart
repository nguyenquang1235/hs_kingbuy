import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CouponChoose extends StatefulWidget {
  final CartViewModel viewModel;
  const CouponChoose({Key key, this.viewModel}) : super(key: key);
  @override
  _CouponChooseState createState() => _CouponChooseState();
}

class _CouponChooseState extends State<CouponChoose> {
  @override
  Widget build(BuildContext context) {

    String couponName = widget.viewModel.couponModel?.name ?? "";
    bool availableCoupon = widget.viewModel.couponModel != null;
    String paymentName = AppUtils.translate("payment_type_${widget.viewModel.cartModel?.paymentType??0}", context);
    bool availablePayment = widget.viewModel.cartModel?.paymentType != null;

    var style = AppStyles.DEFAULT_MEDIUM;
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      child: Column(
        children: [
          buildRow(
            alignment: MainAxisAlignment.spaceBetween,
            leading: buildRow(
              leading: buildImage(AppImages.icCoupons),
              space: 5,
              sub: Text(
                AppUtils.translate("coupon", context),
                style: style,
              ),
            ),
            sub: GestureDetector(
              onTap: () => widget.viewModel.onChangeCoupon(),
              child: buildRow(
                alignment: MainAxisAlignment.end,
                space: 5,
                leading: SizedBox(
                  width: Get.width * 0.4,
                  child: Text(
                    availableCoupon
                        ? couponName
                        : AppUtils.translate("choose_coupon", context),
                    style: style.copyWith(
                        color: availableCoupon ? Colors.black : Colors.grey),
                    maxLines: 2,
                    textAlign: TextAlign.right,
                  ),
                ),
                sub: Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                ),
              ),
            ),
          ),
          Divider(),
          buildRow(
            alignment: MainAxisAlignment.spaceBetween,
            leading: buildRow(
              leading: buildImage(AppImages.icCoin),
              space: 5,
              sub: Text(
                AppUtils.translate("reward_point", context),
                style: style,
              ),
            ),
            sub: buildRow(
              alignment: MainAxisAlignment.end,
              space: 5,
              leading: Text(
                AppUtils.translate("(0 điểm)", context),
                style: style.copyWith(color: Colors.red),
              ),
              sub: SizedBox(
                height: 15,
                child: Switch(
                  value: true,
                  activeColor: Colors.blue,
                  onChanged: (value) => null,
                ),
              ),
            ),
          ),
          Divider(),
          buildRow(
            space: 0,
            alignment: MainAxisAlignment.spaceBetween,
            leading: buildRow(
              leading: buildImage(AppImages.icSurface),
              space: 5,
              sub: SizedBox(
                width: Get.width * 0.3,
                child: Text(
                  AppUtils.translate("payment_type", context),
                  style: style,
                  maxLines: 2,
                ),
              ),
            ),
            sub: GestureDetector(
              onTap: () => widget.viewModel.onChangePaymentType(),
              child: buildRow(
                alignment: MainAxisAlignment.end,
                space: 5,
                leading: SizedBox(
                  width: Get.width * 0.45,
                  child: Text(
                    availablePayment ? paymentName : AppUtils.translate("choose_payment_type", context),
                    maxLines: 2,
                    textAlign: TextAlign.right,
                    style: style.copyWith(
                        color: availablePayment ? Colors.black : Colors.grey),
                  ),
                ),
                sub: Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildRow({
    Widget leading,
    Widget sub,
    MainAxisAlignment alignment = MainAxisAlignment.start,
    double space = 0,
  }) {
    return Container(
      child: Row(
        mainAxisAlignment: alignment,
        children: [
          leading ?? SizedBox(),
          SizedBox(
            width: space,
          ),
          sub ?? SizedBox(),
        ],
      ),
    );
  }

  Widget buildImage(String url) {
    return Image.asset(
      url,
      width: 20,
    );
  }
}
