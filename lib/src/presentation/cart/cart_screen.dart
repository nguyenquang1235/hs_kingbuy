import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key key}) : super(key: key);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> with ResponsiveWidget {
  CartViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CartViewModel>(
      viewModel: CartViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: Colors.grey[200],
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? false;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "cart",
      colorButton: Colors.white,
    );
  }

  Widget _buildScreen() {
    return Container(
      child: SingleChildScrollView(
        padding: const EdgeInsets.only(bottom: 16),
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildDeliveryInfo(),
            _buildField(
              child: VAT(
                viewModel: _viewModel,
              ),
            ),
            _buildField(
              child: CouponChoose(
                viewModel: _viewModel,
              ),
            ),
            _buildField(
              child: _buildPaymentDetails(),
            ),
            SizedBox(
              height: 5,
            ),
            _buildConfirmOrderButton()
          ],
        ),
      ),
    );
  }

  Widget _buildConfirmOrderButton() {
    var style = AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.white);
    bool isConfirm = _viewModel.productItems.length > 0;
    return Center(
      child: Card(
        elevation: 4,
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        child: Container(
          height: 45,
          width: Get.width * 0.8,
          decoration: BoxDecoration(
              color: isConfirm ? Colors.red : Colors.grey,
              borderRadius: BorderRadius.all(Radius.circular(30))),
          child: FlatButton(
            onPressed: () => null,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              children: [
                _buildMoneyPrice(
                  _viewModel.cartMoney.total,
                  styleInput: style,
                ),
                Expanded(child: SizedBox()),
                Text(
                  AppUtils.translate("order", context),
                  style: style,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDeliveryInfo() {
    return _buildField(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        width: Get.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ProductList(
              viewModel: _viewModel,
            ),
            Divider(),
            DeliveryInfo(
              viewModel: _viewModel,
            ),
            Divider(),
            _buildNote(),
            Divider(),
            DeliveryTime(
              viewModel: _viewModel,
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildNote() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("note", context).toUpperCase(),
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
          SizedBox(
            height: 15,
          ),
          TextField(
            onSubmitted: (value) {
              _viewModel.cartModel.note = value;
              print(_viewModel.cartModel.note);
            },
            decoration: InputDecoration.collapsed(
                hintText: AppUtils.translate("want_note", context)),
          )
        ],
      ),
    );
  }

  _buildPaymentDetails() {
    TextStyle style = AppStyles.DEFAULT_MEDIUM;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppUtils.translate("provisional", context),
                style: style,
              ),
              _buildMoneyPrice(_viewModel.cartMoney.totalUnread,
                  styleInput: AppStyles.DEFAULT_MEDIUM)
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppUtils.translate("transport_fee", context),
                style: style,
              ),
              _buildMoneyPrice(_viewModel.cartMoney.fee,
                  styleInput: AppStyles.DEFAULT_MEDIUM)
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppUtils.translate("discount", context),
                style: style,
              ),
              _buildMoneyPrice(_viewModel.cartMoney.reduce,
                  styleInput: AppStyles.DEFAULT_MEDIUM, isNegative: true)
            ],
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppUtils.translate("total", context).toUpperCase(),
                style: style.copyWith(fontWeight: FontWeight.bold),
              ),
              _buildMoneyPrice(_viewModel.cartMoney.total,
                  styleInput: AppStyles.DEFAULT_LARGE_BOLD
                      .copyWith(color: Colors.redAccent))
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildMoneyPrice(double price,
      {TextStyle styleInput, bool isNegative = false}) {
    TextStyle style = styleInput ?? AppStyles.DEFAULT_MEDIUM_BOLD;
    return Row(
      children: [
        Text(
          '${isNegative ? "-" : ""}${NumberFormat(",###", "vi").format(price)}',
          style: style,
        ),
        Text("đ", style: style.copyWith(fontSize: style.fontSize - 3)),
      ],
    );
  }

  Widget _buildField({Widget child}) {
    return Container(
      margin: const EdgeInsets.only(bottom: 8),
      child: PhysicalModel(
        elevation: 2,
        color: Colors.white,
        child: Container(
          color: Colors.white,
          child: child,
        ),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}

class VAT extends StatefulWidget {
  final CartViewModel viewModel;
  const VAT({Key key, this.viewModel}) : super(key: key);

  @override
  _VATState createState() => _VATState();
}

class _VATState extends State<VAT> {
  FocusNode nodeCompanyName = FocusNode();
  FocusNode nodeCompanyAddress = FocusNode();
  FocusNode nodeCompanyEmail = FocusNode();

  @override
  Widget build(BuildContext context) {
    bool isVAT = widget.viewModel.cartModel?.isExportInvoice == 1;
    return Container(
      width: Get.width,
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: () => widget.viewModel.onChangeRequestVAT(!isVAT),
            child: Container(
              color: Colors.transparent,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Checkbox(
                    value: isVAT,
                    activeColor: Colors.red,
                    onChanged: (value) =>
                        widget.viewModel.onChangeRequestVAT(value),
                  ),
                  Text(
                    AppUtils.translate("tax_required", context).toUpperCase(),
                    style: AppStyles.DEFAULT_MEDIUM_BOLD
                        .copyWith(color: (isVAT) ? Colors.black : Colors.grey),
                  )
                ],
              ),
            ),
          ),
          if (isVAT) _buildIsVAT()
        ],
      ),
    );
  }

  Widget _buildIsVAT() {
    return Column(
      children: [
        SizedBox(
          height: 15,
        ),
        _buildInput(
          title: "tax_code",
          inputType: TextInputType.number,
          controller: widget.viewModel.taxCodeController,
          onComplete: () {
            widget.viewModel.onChangeVATField(TaxField.taxCode);
            FocusScope.of(context).requestFocus(nodeCompanyName);
          },
        ),
        _buildInput(
          title: "company_name",
          node: nodeCompanyName,
          controller: widget.viewModel.companyNameController,
          onComplete: () {
            widget.viewModel.onChangeVATField(TaxField.companyName);
            FocusScope.of(context).requestFocus(nodeCompanyAddress);
          },
        ),
        _buildInput(
          title: "company_address",
          isRequired: false,
          node: nodeCompanyAddress,
          controller: widget.viewModel.companyAddressController,
          onComplete: () {
            widget.viewModel.onChangeVATField(TaxField.companyAddress);
            FocusScope.of(context).requestFocus(nodeCompanyEmail);
          },
        ),
        _buildInput(
            title: "email",
            node: nodeCompanyEmail,
            inputType: TextInputType.emailAddress,
            controller: widget.viewModel.companyEmailController,
            onComplete: () {
              widget.viewModel.onChangeVATField(TaxField.companyEmail);
            }),
        SizedBox(
          height: 15,
        ),
      ],
    );
  }

  Widget _buildInput(
      {String title,
      bool isRequired = true,
      FocusNode node,
      FormFieldValidator<String> validator,
      TextInputType inputType = TextInputType.text,
      Function onComplete,
      TextEditingController controller}) {
    return WidgetInput(
      required: isRequired,
      inputType: inputType,
      focusNode: node,
      validator: validator,
      onComplete: onComplete,
      inputController: controller,
      onChanged: (value) => null,
      hint: AppUtils.translate(title, context),
      elevation: 1,
      radiusBorder: 10,
    );
  }
}
