import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/resource/model/promotion_model.dart';

enum PromotionState{
  listItems,
  itemDetails,
}

class PromotionViewModel extends BaseViewModel {

  PromotionState state;
  PromotionModel promotionModel;

  init() async {
    // getPromotion(0);
    setLoading(true);
    changeState(PromotionState.listItems);
    await Future.delayed(const Duration(milliseconds: 1500));
    setLoading(false);
  }

  Future<List<PromotionModel>> initRequester() async {
    return await getPromotion(0);
  }

  Future<List<PromotionModel>> dataRequester(int offset) async {
    return await getPromotion(offset);
  }

  getPromotion(int offset) async {
    List<PromotionModel> model = [];
    NetworkState<List<PromotionModel>> networkState =
        await promotionRepository.getPromotions(offset: offset);
    if (networkState.isSuccess && networkState.data != null)
      model = networkState.data;
    await Future.delayed(Duration(milliseconds: 500));
    return model;
  }

  changeState(PromotionState state, {PromotionModel promotionModel}){
    this.state = state;
    if(state == PromotionState.itemDetails){
      this.promotionModel = promotionModel;
    }
    notifyListeners();
  }

}
