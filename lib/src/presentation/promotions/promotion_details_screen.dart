import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:flutter/material.dart';


class PromotionDetails extends StatelessWidget with ResponsiveWidget {
  final PromotionModel promotion;

  PromotionDetails(this.promotion);

  @override
  Widget build(BuildContext context) {
    return buildUi(context: context);
  }

  _buildScreen() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          WidgetImageNetwork(url: promotion.imageSource, fit: BoxFit.fill,),
          SizedBox(
            height: 15,
          ),
          Text(
            promotion.title,
            style:
                AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.redAccent),
          ),
          // Html(
          //   data: promotion.content ?? "",
          // ),
          SizedBox(
            height: 15,
          ),
          Text(
            promotion.description
          )
        ],
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
