import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/promotion_model.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PromotionsScreen extends StatefulWidget {
  @override
  _PromotionsScreenState createState() => _PromotionsScreenState();
}

class _PromotionsScreenState extends State<PromotionsScreen>
    with ResponsiveWidget {
  PromotionViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<PromotionViewModel>(
      viewModel: PromotionViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, appbar) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? true;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  _buildScreen() {
    switch (_viewModel.state) {
      case PromotionState.listItems:
        return WidgetLoadMoreVertical<PromotionModel>.build(
          loadingColor: Colors.red,
          itemBuilder: _buildPromotionItems,
          dataRequester: _viewModel.dataRequester,
          initRequester: _viewModel.initRequester,
        );

      case PromotionState.itemDetails:
        return PromotionDetails(_viewModel.promotionModel);

      default:
        return WidgetLoadMoreVertical<PromotionModel>.build(
          itemBuilder: _buildPromotionItems,
          dataRequester: _viewModel.dataRequester,
          initRequester: _viewModel.initRequester,
        );
    }
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "promotion",
      colorButton: Colors.white,
      actionBack: _viewModel.state == PromotionState.itemDetails
          ? () => _viewModel.changeState(PromotionState.listItems)
          : () => Navigator.pop(context),
    );
  }

  Widget _buildPromotionItems(
      List<dynamic> data, BuildContext context, int index) {
    List<PromotionModel> model = data;
    return Container(
      margin: EdgeInsets.all(15),
      child: PhysicalModel(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        elevation: 1,
        child: InkWell(
          borderRadius: BorderRadius.all(Radius.circular(30)),
          onTap: () => _viewModel.changeState(PromotionState.itemDetails,
              promotionModel: model[index]),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              WidgetImageNetwork(
                url: data[index].imageSource,
                height: Get.width * 0.3,
                width: Get.width * 0.4,
                fit: BoxFit.fill,
              ),
              SizedBox(
                width: 15,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      constraints: BoxConstraints(
                        minHeight: Get.width * 0.2,
                      ),
                      child: Text(
                        model[index].title,
                        style: AppStyles.DEFAULT_MEDIUM_BOLD,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Text(AppUtils.convertString2String(model[index].createdAt,
                        outputFormat: "dd/MM/yyyy",
                        inputFormat: "yyyy-MM-dd hh:mm:ss"))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
