import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';

class TermsOfUseViewModel extends BaseViewModel {
  String terms;

  init() async {
    setLoading(true);
    terms = await getTermsOfUse();
    setLoading(false);
  }

  Future<String> getTermsOfUse() async {
    NetworkState<ConfigModel> networkState;
    try {
      networkState = await otherRepository.getConfigs();
      return (networkState.data.useOfTerms == "")
          ? null
          : networkState.data.privacyAndTerms;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
