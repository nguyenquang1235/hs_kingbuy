import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_html/flutter_html.dart';

import '../presentation.dart';

class TermsOfUseScreen extends StatefulWidget {
  @override
  _TermsOfUseScreenState createState() => _TermsOfUseScreenState();
}

class _TermsOfUseScreenState extends State<TermsOfUseScreen>
    with ResponsiveWidget {
  TermsOfUseViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<TermsOfUseViewModel>(
      viewModel: TermsOfUseViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, appbar) {
        return SafeArea(
          child: Scaffold(
            body: buildUi(context: context),
          ),
        );
      },
    );
  }

  _buildScreen() {
    return StreamBuilder<bool>(
      stream: _viewModel.loadingSubject,
      builder: (context, snapshot) {
        bool isLoading = snapshot.data ?? false;
        return WidgetLoadingPage(
          isLoading,
          page: Container(
            child: Column(
              children: [
                _buildAppbar(),
                Expanded(
                    child: _viewModel.terms != null
                        ? _buildBody(_viewModel.terms)
                        : Center(
                            child: Text(
                              AppUtils.translate("no_data", context),
                            ),
                          )),
              ],
            ),
          ),
        );
      },
    );
  }

  _buildBody(String data) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.only(top: 12, bottom: 75, left: 12),
      child: Html(
        data: data,
      ),
    );
  }

  _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "rules",
      colorButton: Colors.white,
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
