import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:flutter_icons/flutter_icons.dart';

class CartPaymentScreen extends StatefulWidget {
  const CartPaymentScreen({Key key}) : super(key: key);

  @override
  _CartPaymentScreenState createState() => _CartPaymentScreenState();
}

class _CartPaymentScreenState extends State<CartPaymentScreen>
    with ResponsiveWidget {
  CartPaymentViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CartPaymentViewModel>(
      viewModel: CartPaymentViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
            child: Scaffold(
          body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? false;
                return WidgetLoadingPage(
                  isLoading,
                  page: Column(
                    children: [
                      _buildAppbar(),
                      Expanded(child: buildUi(context: context))
                    ],
                  ),
                );
              }),
        ));
      },
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "payment_type",
      colorButton: Colors.white,
      supWidget: _buildButtonSave(),
    );
  }

  Widget _buildButtonSave() {
    bool isConfirm = _viewModel.typeChoose != null ?? false;
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(100)),
      elevation: 2,
      color: (isConfirm) ? Colors.red : Colors.grey,
      child: Container(
        height: 30,
        child: WidgetButtonGradient(
          colorStart: (isConfirm) ? Colors.red : Colors.grey,
          colorEnd: (isConfirm) ? Colors.red : Colors.grey,
          title: "done",
          action: () {
            if (isConfirm) {
              _viewModel.confirmPayment();
            }
          },
        ),
      ),
    );
  }

  Widget _buildScreen() {
    return Container(
      child: Column(
        children: [
          _buildPaymentTypeItem(1, icon: FlutterIcons.package_oct),
          _buildPaymentTypeItem(2,
              icon: FlutterIcons.bank_outline_mco, color: Colors.blue),
          _buildPaymentTypeItem(3,
              icon: FlutterIcons.credit_card_ent, color: Colors.green),
        ],
      ),
    );
  }

  Widget _buildPaymentTypeItem(int paymentType, {IconData icon, Color color}) {
    return GestureDetector(
      onTap: () => _viewModel.changeTypeChoose(paymentType),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 4),
        padding: const EdgeInsets.symmetric(horizontal: 8),
        color: Colors.transparent,
        child: Row(
          children: [
            Icon(icon, color: color ?? Colors.red),
            SizedBox(
              width: 5,
            ),
            Expanded(
                child: Text(
              AppUtils.translate("payment_type_$paymentType", context),
              style: AppStyles.DEFAULT_MEDIUM,
            )),
            Radio(
              value: paymentType,
              activeColor: Colors.red,
              groupValue: _viewModel.typeChoose,
              onChanged: (value) => _viewModel.changeTypeChoose(value),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
