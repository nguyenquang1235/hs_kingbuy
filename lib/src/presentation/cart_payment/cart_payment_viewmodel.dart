import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

class CartPaymentViewModel extends BaseViewModel {
  int typeChoose;
  init() async {}

  void changeTypeChoose(int value) {
    if (typeChoose != value) {
      typeChoose = value;
      notifyListeners();
    }
  }

  void confirmPayment() async {
    Toast.show(
        "Xác nhận " +
            AppUtils.translate("payment_type_$typeChoose", context)
                .toLowerCase(),
        context,
        duration: Toast.LENGTH_LONG);
    await Future.delayed(const Duration(milliseconds: 500));
    Navigator.pop(context, typeChoose);
  }
}
