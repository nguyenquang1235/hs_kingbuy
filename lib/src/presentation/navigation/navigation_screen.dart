import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import '../../configs/configs.dart';
import '../presentation.dart';

class NavigationScreen extends StatefulWidget {
  const NavigationScreen({Key key}) : super(key: key);

  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen>
    with ResponsiveWidget {
  DeviceScreenType _deviceScreenType;
  NavigationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NavigationViewModel>(
        viewModel: NavigationViewModel(),
        onViewModelReady: (viewModel) async {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return SafeArea(
            child: Scaffold(
              body: Stack(
                children: [
                  Column(
                    children: [
                      Expanded(child: Center(child: buildUi(context: context))),
                      _buildAppbar(context),
                    ],
                  ),
                  Align(
                    alignment: Alignment(0, 1),
                    child: _buildWalletIcon(),
                  ),
                ],
              ),
            ),
          );
        });
  }

  _buildScreen() {
    switch (_viewModel.state) {
      case NavigationState.home:
        return HomeScreen(
          showPopup: _viewModel.getPopups,
          goStorePage: () => _viewModel.changeState(NavigationState.store),
        );
      case NavigationState.store:
        return StoreScreen();
      case NavigationState.notify:
        return NotificationsScreen();
      case NavigationState.personal:
        return PersonalScreen();
      case NavigationState.memberCard:
        return MemberCardScreen();
      default:
        return HomeScreen();
    }
  }

  @override
  Widget buildDesktop(BuildContext context) {
    _deviceScreenType = DeviceScreenType.desktop;
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    _deviceScreenType = DeviceScreenType.mobile;
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    _deviceScreenType = DeviceScreenType.tablet;
    return _buildScreen();
  }

  Widget _buildMobileAppbar() {
    return Stack(
      children: [
        PhysicalModel(
          elevation: 10,
          color: Colors.white,
          shadowColor: Colors.black,
          child: Container(
            height: Get.width * 0.22,
            child: Row(
              children: [
                _buildIcon(Icons.home, "home", state: NavigationState.home),
                _buildIcon(FlutterIcons.store_mall_directory_mdi, "store",
                    state: NavigationState.store),
                Expanded(child: SizedBox()),
                _buildCountNotification(),
                _buildIcon(Icons.person, "account",
                    state: NavigationState.personal),
              ],
            ),
          ),
        ),
      ],
    );
  }

  _buildCountNotification() {
    return Container(
      child: Stack(
        children: [
          _buildIcon(FlutterIcons.notifications_mdi, "notification",
              state: NavigationState.notify),
          // Positioned(
          //   top: 5,
          //   right: 15,
          //   child: FutureBuilder(
          //     future: _viewModel.getCountNotification(),
          //     builder: (context, snapshot) {
          //       if(snapshot.hasData && snapshot.data != 0)
          //         return Text(
          //           snapshot.data.toString(),
          //           style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.red),
          //         );
          //       return SizedBox();
          //     },
          //   ),
          // ),
        ],
      ),
    );
  }

  _buildWalletIcon() {
    return GestureDetector(
      onTap: () => _viewModel.changeState(NavigationState.memberCard),
      child: CircleAvatar(
        radius: 41,
        backgroundColor: Colors.redAccent,
        child: CircleAvatar(
          backgroundColor: Colors.white,
          radius: 35,
          child: CircleAvatar(
            backgroundColor: Colors.redAccent,
            radius: 30,
            child: Center(
              child: Center(
                child: Icon(
                  FlutterIcons.credit_card_mco,
                  color: Colors.white,
                  size: 45,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _buildIcon(IconData iconData, String text, {NavigationState state}) {
    Color color = _viewModel.state == state ? Colors.redAccent : Colors.grey;
    return InkWell(
      onTap: () => _viewModel.changeState(state),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8),
        constraints: BoxConstraints(
          minWidth: Get.width * 0.19,
        ),
        child: Column(
          children: [
            Icon(
              iconData,
              color: color,
              size: 35,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              AppUtils.translate(text, context),
              style: AppStyles.DEFAULT_SMALL.copyWith(color: color),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAppbar(BuildContext context) {
    switch (_deviceScreenType) {
      case DeviceScreenType.desktop:
        return _buildMobileAppbar();
      case DeviceScreenType.mobile:
        return _buildMobileAppbar();
      case DeviceScreenType.tablet:
        return _buildMobileAppbar();
      default:
        return _buildMobileAppbar();
    }
  }
}
