import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../resource/resource.dart';
import '../presentation.dart';

enum NavigationState { home, store, notify, personal, memberCard }

class NavigationViewModel extends BaseViewModel {
  NavigationState state;
  bool hidePopups = false;
  init() async {
    changeState(NavigationState.home);
  }

  changeState(NavigationState newState) async {
    if (newState == NavigationState.memberCard) {
      requestLoginNoneNavigator(callback: () {
        state = newState;
        notifyListeners();
      });
      return;
    }
    state = newState;
    notifyListeners();
  }

  Future<int> getCountNotification() async {
    NetworkState<int> networkState;
    try {
      networkState = await notificationRepository.getCountNotification();
      if (networkState.isSuccess && networkState.response.isSuccess) {
        print(networkState.data);
        return networkState.data;
      }
      return 0;
    } catch (e) {
      print(e);
      return 0;
    }
  }

  getPopups() async {
    if (!hidePopups) {
      NetworkState<PopupModel> state = await otherRepository.getPopups();
      if (state.isSuccess && state.data != null) {
        await showDialog(
          context: context,
          builder: (context) => Scaffold(
            backgroundColor: Colors.transparent,
            body: GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Container(
                // color: Colors.redAccent,
                color: Colors.transparent,
                width: double.maxFinite,
                height: double.maxFinite,
                alignment: Alignment.center,
                child: GestureDetector(
                  onTap: () async {},
                  child: Container(
                    color: Colors.transparent,
                    width: Get.width * 3 / 4,
                    constraints: BoxConstraints(
                        minHeight: Get.width * 3 / 4, maxHeight: Get.width),
                    child: Center(
                      child: WidgetImageNetwork(
                        isAvatar: false,
                        url: state.data.popupImage,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
        hidePopups = true;
      }
    }
  }
}
