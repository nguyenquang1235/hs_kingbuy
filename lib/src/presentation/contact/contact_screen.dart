import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class ContactScreen extends StatefulWidget {
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> with ResponsiveWidget {
  ContactViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ContactViewModel>(
      viewModel: ContactViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, appbar) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? true;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget buildScreen() {
    if (_viewModel.model != null) {
      return Container(
        padding: EdgeInsets.all(24),
        child: Column(
          children: [
            _buildItemContactUs(
              _viewModel.model.message,
              Icons.chat,
              sufIcon: Icon(
                FlutterIcons.facebook_messenger_faw5d,
                color: Colors.blue,
              ),
              action: () => _viewModel.goMessenger(),
            ),
            SizedBox(
              height: 15,
            ),
            _buildItemContactUs(
              _viewModel.model.hotLine,
              Icons.phone,
              action: () => _viewModel.call(),
            ),
            SizedBox(
              height: 15,
            ),
            _buildItemContactUs(
              _viewModel.model.email,
              Icons.email,
              action: () => _viewModel.checkFeedbackOfUser(),
            ),
          ],
        ),
      );
    }
    return Container(
      child: Center(
        child: Text(AppUtils.translate("no_data", context)),
      ),
    );
  }

  Widget _buildItemContactUs(String title, IconData preIcon,
      {Widget sufIcon, action()}) {
    return GestureDetector(
      onTap: () => action() ?? null,
      child: Container(
        color: Colors.transparent,
        child: Row(
          children: [
            Icon(
              preIcon,
              color: Colors.red,
              size: 30,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Text(
                title,
                style: AppStyles.DEFAULT_MEDIUM,
              ),
            ),
            sufIcon ??
                Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                  color: Colors.grey,
                )
          ],
        ),
      ),
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "contact",
      colorButton: Colors.white,
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return buildScreen();
  }
}
