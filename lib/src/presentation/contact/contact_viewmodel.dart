import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/contact_us_model.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactViewModel extends BaseViewModel {
  ContactUsModel model;

  init() async {
    setLoading(true);
    model = await getContactUs();
    setLoading(false);
  }

  Future<ContactUsModel> getContactUs() async {
    try {
      NetworkState networkState = await contactRepository.getContactUs();
      return networkState.data.last;
    } catch (e) {
      print(e);
      return null;
    }
  }

  void checkFeedbackOfUser() async {
    try {
      setLoading(true);
      NetworkState<bool> networkState =
          await contactRepository.checkUserFeedback();
      if (networkState.isSuccess) {
        if (networkState.response.isSuccess) {
          Navigator.pushNamed(context, Routers.user_contact,
              arguments: networkState.data);
        } else {
          Toast.show(networkState.response.message, context);
        }
      } else {
        Toast.show(networkState.message, context);
      }
      setLoading(false);
    } catch (e) {
      print(e);
      setLoading(false);
      Toast.show(AppUtils.translate("system_errors", context), context);
    }
  }

  void call() async {
    print(model.message);
    if (await canLaunch('tel:${model.hotLine}')) {
      await launch('tel:${model.hotLine}');
    } else
      Toast.show(AppUtils.translate("can_launch_phone", context), context);
  }

  void goMessenger() async {
    if (await canLaunch('http://m.me/${model.message}')) {
      await launch('https://m.me/${model.message}');
    } else
      Toast.show(AppUtils.translate("can_launch_messenger", context), context);
  }
}
