import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class NotificationsScreen extends StatefulWidget {
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen>
    with ResponsiveWidget {
  NotificationViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<NotificationViewModel>(
      viewModel: NotificationViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return buildUi(context: context);
      },
    );
  }

  Widget _buildScreen() {
    return StreamBuilder<bool>(
      stream: _viewModel.loadingSubject,
      builder: (context, snapshot) {
        bool isLoading = snapshot.data ?? true;
        return WidgetLoadingPage(
          isLoading,
          page: Container(
            child: Column(
              children: [
                _buildAppbar(),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: WidgetLoadMoreVertical<NotificationModel>.build(
        loadingColor: Colors.red,
        itemBuilder: _buildNotificationItem,
        dataRequester: _viewModel.dataRequester,
        initRequester: _viewModel.initRequester,
      ),
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      isLeading: false,
      keyTitle: "notification",
      colorButton: Colors.white,
    );
  }

  Widget _buildNotificationItem(
      List<dynamic> data, BuildContext context, int index) {
    List<NotificationModel> model = data;
    return Container(
      margin: const EdgeInsets.all(4),
      child: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                AppImages.icLogo,
                width: 50,
                height: 50,
              ),
              SizedBox(
                width: 15,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      constraints: BoxConstraints(
                        maxHeight: Get.width * 0.2,
                      ),
                      child: Text(
                        model[index].title,
                        style: AppStyles.DEFAULT_MEDIUM_BOLD,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(AppUtils.convertString2String(model[index].createdAt,
                        outputFormat: "EEEE dd/MM/yyyy",
                        inputFormat: "yyyy-MM-dd hh:mm:ss"))
                  ],
                ),
              )
            ],
          ),
          if (model.last != model[index])
            Divider(
              thickness: 1,
            ),
        ],
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
