import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/model.dart';

class NotificationViewModel extends BaseViewModel {
  init() async {
    setLoading(true);
    await Future.delayed(const Duration(milliseconds: 1500));
    setLoading(false);
  }

  Future<List<NotificationModel>> initRequester() async {
    return await getNotification(0);
  }

  Future<List<NotificationModel>> dataRequester(int offset) async {
    return await getNotification(offset);
  }

  getNotification(int offset) async {
    List<NotificationModel> model = [];
    NetworkState<List<NotificationModel>> networkState =
        await notificationRepository.getNotification(offset: offset);
    if (networkState.isSuccess && networkState.data != null)
      model = networkState.data;
    await Future.delayed(Duration(milliseconds: 500));
    return model;
  }
}
