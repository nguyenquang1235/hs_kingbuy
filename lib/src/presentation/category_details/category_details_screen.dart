import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/category_model.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class CategoryDetailsArguments {
  final CategoryModel parent;
  final CategoryModel child;

  CategoryDetailsArguments({this.parent, this.child});
}

class CategoryDetailsScreen extends StatefulWidget {
  final CategoryDetailsArguments arguments;

  const CategoryDetailsScreen({Key key, this.arguments}) : super(key: key);
  @override
  _CategoryDetailsScreenState createState() => _CategoryDetailsScreenState();
}

class _CategoryDetailsScreenState extends State<CategoryDetailsScreen>
    with ResponsiveWidget {
  CategoryDetailsViewModel _viewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CategoryDetailsViewModel>(
      viewModel: CategoryDetailsViewModel(model: widget.arguments.parent),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel..init(child: widget.arguments.child);
      },
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            key: _viewModel.scaffoldKey,
            endDrawer: SearchDrawer(
              viewModel: _viewModel,
              displayCategories: 0,
            ),
            backgroundColor: Colors.grey[200],
            body: Column(
              children: [
                _buildAppbar(),
                Expanded(
                  child: buildUi(
                    context: context,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppbar() {
    return PhysicalModel(
      elevation: 2,
      color: Colors.lightBlue,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8),
        color: Colors.lightBlue,
        height: 75,
        child: Row(
          children: [
            InkWell(
              child: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
                size: 30,
              ),
              onTap: () => Navigator.pop(context),
            ),
            Expanded(
              child: WidgetInput(
                style: AppStyles.DEFAULT_LARGE,
                onChanged: (value) => null,
                onComplete: () {
                  _viewModel.unFocus();
                  _viewModel.onCompleteInput();
                },
                inputController: _viewModel.searchController,
                radiusBorder: 10,
                height: 55,
                hint: AppUtils.translate("search", context),
                preIcon: Icon(
                  Icons.search,
                  color: Colors.grey,
                ),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
              onTap: () {
                _viewModel.scaffoldKey.currentState.openEndDrawer();
              },
              child: Container(
                width: 40,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      FlutterIcons.filter_outline_mco,
                      color: Colors.white,
                    ),
                    Text(
                      AppUtils.translate("filter", context),
                      style:
                          AppStyles.DEFAULT_SMALL.copyWith(color: Colors.white),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildScreen() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        children: [
          Container(
            color: Colors.white,
            child: Column(
              children: [
                _buildHeader(),
                _buildChildrenCategories(),
                _buildListItem(),
              ],
            ),
          ),
          widget.arguments.parent.description != null
              ? Html(
                  data: widget.arguments.parent.description,
                )
              : SizedBox(),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      width: Get.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              Icon(
                Icons.home,
                size: 35,
              ),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.black54,
                size: 15,
              ),
              Text(
                AppUtils.translate("categories", context),
                style: AppStyles.DEFAULT_MEDIUM,
              ),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.black54,
                size: 15,
              ),
              Text(widget.arguments.parent.name, style: AppStyles.DEFAULT_MEDIUM)
            ],
          ),
          RepaintBoundary(
            child: WidgetImageNetwork(
              fit: BoxFit.cover,
              url: widget.arguments.parent.backgroundImage,
              isAvatar: false,
              width: Get.width,
              height: Get.width * 0.5,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildChildrenCategories() {
    return SingleChildScrollView(
      padding: EdgeInsets.only(left: 8),
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: Row(
        children: List.generate(
          _viewModel.model.children.length,
          (index) =>
              _buildCategoryStatusItem((_viewModel.model.children[index])),
        ),
      ),
    );
  }

  Widget _buildCategoryStatusItem(CategoryModel model) {
    return StreamBuilder<DrawerFilter>(
      stream: _viewModel.drawerFilterController,
      builder: (context, snapshot) {
        bool isSelect = (snapshot.data?.categoryModel ?? widget.arguments.parent) == model;
        return GestureDetector(
          onTap: () {
            if (!isSelect)
              _viewModel.onChangeDrawer(model, type: ChangeDrawerType.category);
          },
          child: RepaintBoundary(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              margin: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
              child: PhysicalModel(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: isSelect ? Colors.blue : Colors.white,
                elevation: 2,
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Center(
                    child: Text(
                      model.name,
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      style: AppStyles.DEFAULT_MEDIUM.copyWith(
                          color: isSelect ? Colors.white : Colors.black),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildListItem() {
    return WidgetLoadMoreWrapVertical<ProductModel>.build(
      padding: EdgeInsets.only(bottom: 20),
      key: _viewModel.keyLoadMore,
      itemBuilder: (data, context, index) => WidgetProductItem(
        model: data[index],
      ),
      dataRequester: _viewModel.dataRequest,
      initRequester: _viewModel.initRequest,
      loadingColor: Colors.redAccent,
      buttonSeeMore: true,
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
