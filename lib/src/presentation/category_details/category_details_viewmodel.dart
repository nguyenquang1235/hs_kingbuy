import 'dart:convert';

import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class CategoryDetailsViewModel extends BaseViewModel {
  final TextEditingController searchController = TextEditingController();
  final GlobalKey<WidgetLoadMoreWrapVerticalState> keyLoadMore = GlobalKey();
  final BehaviorSubject<DrawerFilter> drawerFilterController =
      BehaviorSubject();
  final ScrollController scrollController = ScrollController();

  BehaviorSubject<bool> availableResetFilter = BehaviorSubject();

  DrawerFilter get getDrawerFilter => drawerFilterController.value;
  CategoryModel model;

  CategoryDetailsViewModel({CategoryModel model}) {
    this.model = CategoryModel.fromJson(jsonDecode(jsonEncode(model)));
  }

  init({CategoryModel child}) async {
    model.children.add(CategoryModel(
      name: "Tất cả",
      iconSource: null,
      backgroundImage: null,
      children: null,
      description: null,
      id: model.id,
      imageSource: null,
      parent: null,
      parentId: null,
      slug: null,
      videoLink: null,
    ));
    model.children.sort((a, b) => a.id.compareTo(b.id));
    drawerFilterController.add(
      DrawerFilter(
          brandModel: null,
          priceFilter: null,
          categoryModel: child ?? model.children.first),
    );
    availableResetFilter.add(false);
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        keyLoadMore.currentState.loadMore();
      }
    });
  }

  Future<List<ProductModel>> getItems(int offset) async {
    try {
      Map<String, dynamic> price;
      if (getDrawerFilter.priceFilter != null) {
        price = {
          "from": getDrawerFilter.priceFilter.priceFrom ?? 0,
          "to": getDrawerFilter.priceFilter.priceTo ?? 999999999999999999,
        };
      }
      NetworkState<List<ProductModel>> networkState =
          await productRepository.getProductByCategory(
              offset: offset,
              limit: 6,
              brandId: getDrawerFilter.brandModel?.id ?? null,
              categoryId: getDrawerFilter.categoryModel?.id ?? model.id,
              price: price ?? null);

      return networkState.data ?? [];
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<ProductModel>> initRequest() async {
    return getItems(0);
  }

  Future<List<ProductModel>> dataRequest(int offset) async {
    return getItems(offset);
  }

  Future<List<BrandModel>> getBrands() async {
    try {
      List<BrandModel> listResult = await AppShared.getListBrands();
      return listResult;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<PriceFilter>> getPriceFilter() async {
    try {
      List<PriceFilter> listResult = PriceFilter.raw;
      return listResult;
    } catch (e) {
      print(e);
      return [];
    }
  }

  onChangeDrawer(dynamic value, {ChangeDrawerType type}) {
    switch (type) {
      case ChangeDrawerType.category:
        drawerFilterController.add(DrawerFilter(
          categoryModel: value,
          brandModel: getDrawerFilter.brandModel,
          priceFilter: getDrawerFilter.priceFilter,
        ));
        keyLoadMore.currentState.onRefresh();
        break;
      case ChangeDrawerType.brand:
        drawerFilterController.add(DrawerFilter(
          categoryModel: getDrawerFilter.categoryModel,
          brandModel: value,
          priceFilter: getDrawerFilter.priceFilter,
        ));
        break;
      case ChangeDrawerType.price:
        drawerFilterController.add(DrawerFilter(
          categoryModel: getDrawerFilter.categoryModel,
          brandModel: getDrawerFilter.brandModel,
          priceFilter: value,
        ));
        break;
      default:
        drawerFilterController.add(DrawerFilter(
          categoryModel: model.children.first,
          brandModel: null,
          priceFilter: null,
        ));
    }
    bool available = (getDrawerFilter.categoryModel != model.children.first) ||
        (getDrawerFilter.brandModel != null) ||
        (getDrawerFilter.priceFilter != null);
    availableResetFilter.add(available);
  }

  onSubmitFilter() {
    keyLoadMore.currentState.onRefresh();
  }

  onCompleteInput() {
    Navigator.pushNamed(context, Routers.search_filter,
        arguments: SearchArgument(
          searchKey: searchController.text,
          drawerFilter: getDrawerFilter,
        ));
  }
}
