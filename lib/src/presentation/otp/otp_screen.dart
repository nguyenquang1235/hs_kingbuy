import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pinput/pin_put/pin_put.dart';
import '../presentation.dart';

enum OtpType {
  register,
  forgotPass,
}

class OtpScreenArguments {
  final String identity;
  final OtpType otpType;

  OtpScreenArguments({this.identity, this.otpType = OtpType.register});
}

class OtpScreen extends StatefulWidget {
  final otpScreenArguments;

  const OtpScreen({Key key, this.otpScreenArguments}) : super(key: key);

  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> with ResponsiveWidget {
  OtpViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<OtpViewModel>(
      viewModel: OtpViewModel(
        identity: widget.otpScreenArguments.identity,
        otpType: widget.otpScreenArguments.otpType,
      ),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: buildUi(context: context),
          ),
        );
      },
    );
  }

  _buildScreen() {
    return StreamBuilder(
      stream: _viewModel.loadingSubject,
      builder: (context, snapshot) {
        bool isLoading = snapshot.data ?? false;
        return WidgetLoadingPage(
          isLoading,
          page: _buildBody(),
        );
      },
    );
  }

  _buildBody() {
    return Column(
      children: [
        WidgetAppBar(
          bgColor: Colors.blue,
          colorButton: Colors.white,
          keyTitle: "enter_otp",
        ),
        Expanded(
            child: SingleChildScrollView(
          child: _buildBodyScreen(),
        ))
      ],
    );
  }

  _buildBodyScreen() {
    BoxDecoration pinPutDecoration = BoxDecoration(
      color: AppColors.grey,
    );

    return Container(
      padding: EdgeInsets.symmetric(vertical: 25, horizontal: 16),
      child: Column(
        children: [
          Text(
            AppUtils.translate(
              "enter_otp_to",
              context,
            ),
            style: AppStyles.DEFAULT_LARGE
                .copyWith(color: Colors.redAccent, fontSize: 25),
          ),
          Text(
            _viewModel.identity.replaceRange(5, 7, "***"),
            style: AppStyles.DEFAULT_LARGE
                .copyWith(color: AppColors.grey, fontSize: 25),
          ),
          SizedBox(
            height: 25,
          ),
          Form(
            key: _viewModel.formKeySignIn,
            child: PinPut(
              controller: _viewModel.otpController,
              validator: (value) {
                if (value == null || value.length == 0)
                  return AppLocalizations.of(context)
                      .translate("valid_enter_otp");
                return null;
              },
              useNativeKeyboard: true,
              withCursor: true,
              fieldsCount: 6,
              fieldsAlignment: MainAxisAlignment.spaceAround,
              textStyle: const TextStyle(fontSize: 25.0, color: Colors.blue),
              eachFieldMargin: EdgeInsets.all(0),
              eachFieldWidth: Get.width * 0.13,
              eachFieldHeight: 45.0,
              submittedFieldDecoration: pinPutDecoration,
              selectedFieldDecoration: pinPutDecoration,
              followingFieldDecoration: pinPutDecoration,
              pinAnimationType: PinAnimationType.scale,
            ),
          ),
          SizedBox(
            height: 25,
          ),
          FlatButton(
            onPressed: () => _viewModel.confirmOtp(),
            padding: const EdgeInsets.all(0.0),
            child: Container(
              height: 45,
              color: AppColors.grey,
              child: Center(
                child: Text(
                  AppUtils.translate(
                    "continue",
                    context,
                  ),
                  style: AppStyles.DEFAULT_MEDIUM
                      .copyWith(color: Colors.redAccent),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Text(
            AppUtils.translate(
              "not_get_otp",
              context,
            ),
            style: AppStyles.DEFAULT_LARGE
                .copyWith(color: AppColors.grey, fontSize: 25),
          )
        ],
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
