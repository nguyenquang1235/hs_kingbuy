import 'package:King_Buy/src/presentation/base/base.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../presentation.dart';

class OtpViewModel extends BaseViewModel {
  final String identity;
  final OtpType otpType;

  TextEditingController otpController;
  final formKeySignIn = GlobalKey<FormState>();

  OtpViewModel({this.identity, this.otpType});

  init() async {
    otpController = TextEditingController();
  }

  confirmOtp() async {
    unFocus();

    if (formKeySignIn.currentState.validate()) {
      setLoading(true);

      try {
        if (this.otpType == OtpType.register)
          checkAccount(
            networkState: await authRepository.activeAccount(
                identity: identity, smsCode: otpController.value.text),
          );
        else
          checkAccount(
            networkState: await authRepository.checkOtpForgotPass(
                identity: identity, smsCode: otpController.value.text),
          );
      } catch (e) {
        print(e);
        Toast.show(AppUtils.translate("login_failure", context), context);
      }
      setLoading(false);
    }
  }

  checkAccount({NetworkState networkState}) async {
    if (networkState == null) return;
    if (networkState.isSuccess) {
      if (networkState.response.isSuccess) {
        nextStep();
      } else {
        Toast.show(networkState.response.message, context);
        // Khi nào có api thì vứt cái ni
        await Future.delayed(const Duration(milliseconds: 500));
        nextStep();
      }
    } else {
      Toast.show(AppUtils.translate("otp_failure", context), context);
    }
  }

  ///Điều hướng sang các màn hình đăng ký thông tin user hoặc đổi mật khẩu
  nextStep() {
    switch (this.otpType) {
      case OtpType.register:
        Navigator.popAndPushNamed(context, Routers.register_user_profile);
        break;
      case OtpType.forgotPass:
        Navigator.popAndPushNamed(context, Routers.reset_pass,
            arguments: ResetPassArgument(
              smsCode: otpController.value.text,
              identity: identity,
            ));
        break;
    }
  }
}
