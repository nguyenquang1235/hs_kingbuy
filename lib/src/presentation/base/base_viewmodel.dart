import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/repo/auth_repository.dart';
import 'package:King_Buy/src/resource/repo/coupon_repository.dart';
import 'package:King_Buy/src/resource/repo/notification_repository.dart';
import 'package:King_Buy/src/resource/repo/other_repository.dart';
import 'package:King_Buy/src/resource/repo/product_repository.dart';
import 'package:King_Buy/src/resource/repo/promotion_repository.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_shared.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';

import '../routers.dart';

abstract class BaseViewModel extends ChangeNotifier {
  BuildContext _context;

  BuildContext get context => _context;

  setContext(BuildContext value) {
    _context = value;
  }

  final loadingSubject = BehaviorSubject<bool>();
  final errorSubject = BehaviorSubject<String>();

  void setLoading(bool loading) {
    if (loading != isLoading) loadingSubject.add(loading);
  }

  bool get isLoading => loadingSubject.value;

  void setError(String message) {
    errorSubject.add(message);
  }

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  final AuthRepository authRepository = AuthRepository();
  final FirebaseRepository firebaseRepository = FirebaseRepository();
  final ExampleRepository exampleRepository = ExampleRepository();
  final OtherRepository otherRepository = OtherRepository();
  final PromotionRepository promotionRepository = PromotionRepository();
  final NotificationRepository notificationRepository =
      NotificationRepository();
  final InvoiceRepository invoiceRepository = InvoiceRepository();
  final DeliveryRepository deliveryRepository = DeliveryRepository();
  final ProvinceRepository provinceRepository = ProvinceRepository();
  final ContactRepository contactRepository = ContactRepository();
  final CouponRepository couponRepository = CouponRepository();
  final WarrantyRepository warrantyRepository = WarrantyRepository();
  final ProductRepository productRepository = ProductRepository();
  final CategoryRepository categoryRepository = CategoryRepository();
  final SearchRepository searchRepository = SearchRepository();
  final AddressRepository addressRepository = AddressRepository();
  final RatingRepository ratingRepository = RatingRepository();

  @override
  void dispose() async {
    await loadingSubject.drain();
    loadingSubject.close();
    await errorSubject.drain();
    errorSubject.close();
    super.dispose();
  }

  void unFocus() {
    FocusScope.of(context).unfocus();
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  Future<UserModel> getProfile() async {
    bool isLogger = false;
    try {
      NetworkState<UserModel> networkState = await authRepository.getProfile();
      isLogger = networkState.isSuccess && networkState.data != null;
      if (isLogger) await AppShared.setUser(networkState.data);
      await AppShared.setLogged(isLogger);
      notifyListeners();
      return networkState.data;
    } catch (e) {
      print(e);
      return null;
    }
  }

  requestLogin({String router, Function callback}) async {
    bool isLogged = await AppShared.getLogged();
    if (isLogged) {
      Navigator.pushNamed(context, router).then((value) {
        if (callback != null) callback();
      });
    } else {
      bool isGoLogin = await showDialog(
        context: context,
        builder: (context) => DialogGoLogin(),
      );
      if (isGoLogin)
        Navigator.pushNamed(context, Routers.login_navigation,
                arguments: router)
            .then((value) {
          if (callback != null) callback();
        });
    }
  }

  requestLoginNoneNavigator({Function callback}) async {
    bool isLogged = await AppShared.getLogged();
    if (isLogged) {
      callback();
    } else {
      bool isGoLogin = await showDialog(
        context: context,
        builder: (context) => DialogGoLogin(),
      );
      if (isGoLogin)
        Navigator.pushNamed(
          context,
          Routers.login_navigation,
        ).then((value) {
          if (callback != null) callback();
        });
    }
  }
}
