import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';

class DeliveryAddressScreen extends StatefulWidget {
  @override
  _DeliveryAddressScreenState createState() => _DeliveryAddressScreenState();
}

class _DeliveryAddressScreenState extends State<DeliveryAddressScreen>
    with ResponsiveWidget {
  DeliveryAddressViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<DeliveryAddressViewModel>(
      viewModel: DeliveryAddressViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, appbar) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: Colors.grey[200],
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? true;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildScreen() {
    return WidgetLoadMoreVertical<DeliveryAddressModel>.build(
      key: _viewModel.listViewKey,
      padding: EdgeInsets.only(bottom: 50),
      itemBuilder: _buildAddressItems,
      dataRequester: (offset) => null,
      initRequester: _viewModel.getListDelivery,
      loadingColor: Colors.redAccent,
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "delivery_address",
      colorButton: Colors.white,
      supWidget: InkWell(
        // onTap: () => Navigator.pushNamed(context, Routers.change_delivery_address),
        onTap: () => _viewModel.goChangeDelivery(
          arguments: null,
        ),
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _buildAddressItems(
      List<dynamic> data, BuildContext context, int index) {
    List<DeliveryAddressModel> model = data;
    TextStyle defaultStyle = AppStyles.DEFAULT_MEDIUM;
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.only(top: 8, left: 8, right: 16),
      child: InkWell(
        onTap: () => _viewModel.goChangeDelivery(
          arguments: model[index]
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              model[index].fullName,
              style: AppStyles.DEFAULT_LARGE_BOLD,
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              model[index].fullAddress,
              style: defaultStyle,
              maxLines: 2,
            ),
            SizedBox(
              height: 2,
            ),
            Text(
              model[index].firstPhone,
              style: defaultStyle,
              maxLines: 2,
            ),
            SizedBox(
              height: 2,
            ),
            if ((model[index]?.isDefault ?? 0) == 1)
              Text(
                AppUtils.translate("default_address", context),
                style: defaultStyle.copyWith(color: Colors.redAccent),
              ),
            SizedBox(
              height: 10,
            ),
            if (data.last != data[index]) Divider()
          ],
        ),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
