import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/delivery_address_model.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_shared.dart';
import 'package:flutter/material.dart';

class DeliveryAddressViewModel extends BaseViewModel {
  final GlobalKey<WidgetLoadMoreVerticalState> listViewKey = GlobalKey();
  DeliveryAddressModel chooseModel;
  bool isLogin;
  init() async {
    setLoading(true);
    isLogin = await AppShared.getLogged();
    await Future.delayed(const Duration(milliseconds: 1000));
    setLoading(false);
  }

  Future<List<DeliveryAddressModel>> getListDelivery() async {
    List<DeliveryAddressModel> model = [];
    if (await AppShared.getLogged()) {
      NetworkState<List<DeliveryAddressModel>> networkState =
          await deliveryRepository.getDeliveryAddress();
      if (networkState.isSuccess && networkState.data != null)
        model = networkState.data;
      await Future.delayed(Duration(milliseconds: 500));
    } else {
      var data = await AppShared.getDeliveryAddress();
      if (data != null) model.add(data);
    }
    return model;
  }

  goChangeDelivery({arguments}) {
    Navigator.pushNamed(context, Routers.change_delivery_address,
            arguments: arguments)
        .then((value) {
      if (value) listViewKey.currentState.onRefresh();
    });
  }

  void chooseDeliveryAddress(DeliveryAddressModel value) {
    chooseModel = value;
    notifyListeners();
  }

  void submitChooseDeliveryAddress() {
    Navigator.pop(context, chooseModel);
  }
}
