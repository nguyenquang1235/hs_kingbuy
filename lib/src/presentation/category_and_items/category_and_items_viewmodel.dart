import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';

class CategoryAndItemsViewModel extends BaseViewModel {
  final GlobalKey<WidgetLoadMoreWrapVerticalState> keyLoadMore = GlobalKey();

  CategoryModel model;

  int childId;

  CategoryAndItemsViewModel({CategoryModel model}) {
    this.model = CategoryModel.fromJson(jsonDecode(jsonEncode(model)));
  }

  init() async {
    model.children.add(CategoryModel(
      name: "Tất cả",
      iconSource: null,
      backgroundImage: null,
      children: null,
      description: null,
      id: model.id,
      imageSource: null,
      parent: null,
      parentId: null,
      slug: null,
      videoLink: null,
    ));
    model.children.sort((a, b) => a.id.compareTo(b.id));
    childId = model.children.first.id;
  }

  changeStatus(int id) {
    childId = id;
    notifyListeners();
    keyLoadMore.currentState.onRefresh();
  }

  Future<List<ProductModel>> getProductByCategory({int offset}) async {
    try {
      List<ProductModel> model = [];
      NetworkState<List<ProductModel>> networkState = await productRepository
          .getProductByCategory(offset: offset, limit: 4, categoryId: childId);
      if (networkState.isSuccess && networkState.data != null)
        model = networkState.data;
      return model;
    } catch (e) {
      print(e);
      return [];
    }
  }
}
