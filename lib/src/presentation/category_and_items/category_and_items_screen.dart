import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_shared.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class CategoryAndItemsScreen extends StatefulWidget {
  final CategoryModel model;

  const CategoryAndItemsScreen({Key key, this.model}) : super(key: key);

  @override
  _CategoryAndItemsScreenState createState() => _CategoryAndItemsScreenState();
}

class _CategoryAndItemsScreenState extends State<CategoryAndItemsScreen>
    with ResponsiveWidget {
  CategoryAndItemsViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CategoryAndItemsViewModel>(
      viewModel: CategoryAndItemsViewModel(model: widget.model),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel..init();
      },
      builder: (context, viewModel, child) {
        return StreamBuilder<bool>(
          stream: _viewModel.loadingSubject,
          builder: (context, snapshot) {
            bool isLoading = snapshot.data ?? false;
            return WidgetLoadingPage(
              isLoading,
              page: buildUi(context: context),
              opacity: 1,
            );
          },
        );
      },
    );
  }

  Widget _buildScreen() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8),
          child: Text(
            AppUtils.translate(widget.model.name, context),
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: PhysicalModel(
            elevation: 2,
            color: Colors.white,
            child: RepaintBoundary(
              child: WidgetImageNetwork(
                fit: BoxFit.cover,
                url: widget.model.backgroundImage,
                isAvatar: false,
                width: Get.width,
                height: Get.width * 0.5,
              ),
            ),
          ),
        ),
        _buildChildrenCategories(),
        _buildListItem(),
        SizedBox(
          height: 30,
        ),
        _buildButtonShowAll(),
      ],
    );
  }

  Widget _buildChildrenCategories() {
    return SingleChildScrollView(
      padding: EdgeInsets.only(left: 8),
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: Row(
        children: List.generate(
          _viewModel.model.children.length,
          (index) =>
              _buildCategoryStatusItem((_viewModel.model.children[index])),
        ),
      ),
    );
  }

  Widget _buildCategoryStatusItem(CategoryModel model) {
    bool isSelect = _viewModel.childId == model.id;
    return GestureDetector(
      onTap: () {
        _viewModel.changeStatus(model.id);
      },
      child: RepaintBoundary(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          margin: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
          child: PhysicalModel(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: isSelect ? Colors.blue : Colors.white,
            elevation: 2,
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Center(
                child: Text(
                  model.name,
                  textAlign: TextAlign.center,
                  maxLines: 2,
                  style: AppStyles.DEFAULT_MEDIUM
                      .copyWith(color: isSelect ? Colors.white : Colors.black),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildListItem() {
    return WidgetLoadMoreWrapVertical<ProductModel>.build(
      key: _viewModel.keyLoadMore,
      direction: Axis.horizontal,
      itemBuilder: (data, context, index) => WidgetProductItem(
        model: data[index],
      ),
      dataRequester: (offset) =>
          _viewModel.getProductByCategory(offset: offset),
      initRequester: () => _viewModel.getProductByCategory(offset: 0),
      loadingColor: Colors.redAccent,
    );
  }

  Widget _buildButtonShowAll() {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, Routers.category_details,
          arguments: CategoryDetailsArguments(
            parent: widget.model
          )),
      // onTap: () => print(widget.model.children.first.name),
      child: RepaintBoundary(
        child: Container(
          color: Colors.transparent,
          child: Center(
            child: Text(
              AppUtils.translate("see_more", context),
              style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                decoration: TextDecoration.underline,
                color: Colors.lightBlue,
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
