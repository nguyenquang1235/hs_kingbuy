import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/search_filter/widget_filter_arguments.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class SearchDrawer extends StatefulWidget {
  final dynamic viewModel;
  final int displayCategories;
  final int displayBrands;
  final int displayPrice;

  const SearchDrawer(
      {Key key,
      this.viewModel,
      this.displayCategories = 8,
      this.displayBrands = 4,
      this.displayPrice = 4})
      : super(key: key);

  @override
  _SearchDrawerState createState() => _SearchDrawerState();
}

class _SearchDrawerState extends State<SearchDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Scaffold(
        // key: GlobalKey<ScaffoldState>(),
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 8, top: 16, bottom: 16),
              color: Colors.lightBlue,
              child: Row(
                children: [
                  Icon(
                    FlutterIcons.filter_outline_mco,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  FittedBox(
                    fit: BoxFit.cover,
                    child: Text(
                      AppUtils.translate("search_filters", context),
                      style: AppStyles.DEFAULT_LARGE_BOLD
                          .copyWith(color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                padding: const EdgeInsets.only(top: 10, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    StreamBuilder<DrawerFilter>(
                        stream: widget.viewModel.drawerFilterController,
                        builder: (context, snapshot) {
                          return _buildDrawerField(snapshot.data);
                        }),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: _buildDrawerButton(
                            "reset",
                            widget.viewModel.availableResetFilter,
                            callback: () {
                              widget.viewModel.onChangeDrawer(null);
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: _buildDrawerButton(
                            "apply",
                            null,
                            callback: () => widget.viewModel.onSubmitFilter(),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDrawerField(DrawerFilter model) {
    return Column(
      children: [
        if (widget.displayCategories > 0)
          FutureBuilder<List<CategoryModel>>(
            future: widget.viewModel.getCategories(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return WidgetFilterArguments<CategoryModel>(
                  title: "categories",
                  displayNumber: widget.displayCategories,
                  chooseItem: model.categoryModel,
                  listData: snapshot.data,
                  action: (model) => widget.viewModel
                      .onChangeDrawer(model, type: ChangeDrawerType.category),
                );
              }
              return SizedBox();
            },
          ),
        if (widget.displayBrands > 0)
          FutureBuilder<List<BrandModel>>(
            future: widget.viewModel.getBrands(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return WidgetFilterArguments<BrandModel>(
                  displayNumber: widget.displayBrands,
                  title: "brands",
                  chooseItem: model.brandModel,
                  listData: snapshot.data,
                  action: (model) => widget.viewModel
                      .onChangeDrawer(model, type: ChangeDrawerType.brand),
                );
              }
              return SizedBox();
            },
          ),
        if (widget.displayPrice > 0)
          FutureBuilder<List<PriceFilter>>(
          future: widget.viewModel.getPriceFilter(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return WidgetFilterPriceArguments<PriceFilter>(
                title: "price",
                displayNumber: widget.displayPrice,
                chooseItem: model.priceFilter,
                listData: snapshot.data,
                action: (model) => widget.viewModel
                    .onChangeDrawer(model, type: ChangeDrawerType.price),
                onChangeText: (model) {
                  widget.viewModel.onChangeDrawer(PriceFilter.fromJson(model),
                      type: ChangeDrawerType.price);
                },
              );
            }
            return SizedBox();
          },
        ),
      ],
    );
  }

  Widget _buildDrawerButton(String title, Stream stream, {callback()}) {
    return StreamBuilder<bool>(
        stream: stream,
        builder: (context, snapshot) {
          bool isConfirm = snapshot.data ?? true;
          return PhysicalModel(
            borderRadius: BorderRadius.all(Radius.circular(100)),
            elevation: 2,
            color: (isConfirm) ? Colors.red : Colors.grey,
            child: Container(
              width: Get.width * 0.3,
              height: 40,
              child: WidgetButtonGradient(
                colorStart: (isConfirm) ? Colors.redAccent : Colors.grey,
                colorEnd: (isConfirm) ? Colors.red : Colors.grey,
                title: title,
                action: () {
                  if (isConfirm) callback();
                },
              ),
            ),
          );
        });
  }
}
