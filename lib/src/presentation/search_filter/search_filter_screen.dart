import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class SearchArgument{
  final String searchKey;
  final DrawerFilter drawerFilter;

  SearchArgument({this.searchKey, this.drawerFilter});
}

class SearchFilterScreen extends StatefulWidget {
  final SearchArgument searchArgument;

  const SearchFilterScreen({Key key, this.searchArgument}) : super(key: key);

  @override
  _SearchFilterScreenState createState() => _SearchFilterScreenState();
}

class _SearchFilterScreenState extends State<SearchFilterScreen>
    with ResponsiveWidget {
  SearchFilterViewModel _viewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SearchFilterViewModel>(
      viewModel: SearchFilterViewModel(),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel..init(widget.searchArgument);
      },
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            key: _viewModel.scaffoldKey,
            endDrawer: SearchDrawer(
              viewModel: _viewModel,
            ),
            backgroundColor: Colors.grey[200],
            body: Column(
              children: [
                _buildAppbar(),
                Expanded(
                  child: buildUi(
                    context: context,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppbar() {
    return PhysicalModel(
      elevation: 2,
      color: Colors.lightBlue,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 8),
        color: Colors.lightBlue,
        height: 75,
        child: Row(
          children: [
            InkWell(
              child: Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
                size: 30,
              ),
              onTap: () => Navigator.pop(context),
            ),
            Expanded(
              child: WidgetInput(
                style: AppStyles.DEFAULT_LARGE,
                onChanged: (value) => null,
                onComplete: () {
                  _viewModel.unFocus();
                  _viewModel.keyLoadMore.currentState.onRefresh();
                },
                inputController: _viewModel.searchController,
                radiusBorder: 10,
                height: 55,
                hint: AppUtils.translate("search", context),
                preIcon: Icon(
                  Icons.search,
                  color: Colors.grey,
                ),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
              onTap: () {
                _viewModel.scaffoldKey.currentState.openEndDrawer();
              },
              child: Container(
                width: 40,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      FlutterIcons.filter_outline_mco,
                      color: Colors.white,
                    ),
                    Text(
                      AppUtils.translate("filter", context),
                      style:
                          AppStyles.DEFAULT_SMALL.copyWith(color: Colors.white),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildScreen() {
    return WidgetLoadMoreWrapHeader<ProductModel>.build(
      emptyBellowHeader: true,
      rangeHeader: 0,
      widgetHeader: (data, context) =>  _buildRowFilterItems(),
      key: _viewModel.keyLoadMore,
      loadingColor: Colors.redAccent,
      padding: EdgeInsets.symmetric(vertical: 4),
      itemBuilder: (data, context, index) {
        return WidgetProductItem(
          model: data[index],
        );
      },
      dataRequester: _viewModel.dataRequest,
      initRequester: _viewModel.initRequest,
    );
  }

  Widget _buildRowFilterItems() {
    var data = _viewModel.drawerFilter;
    return Container(
      width: Get.width,
      child: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 8),
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            if (data.categoryModel != null)
              _buildFilterItem(
                data.categoryModel.name,
                action: () =>
                    _viewModel.onRemoveFilterItem(ChangeDrawerType.category),
              ),
            if (data.brandModel != null)
              _buildFilterItem(
                data.brandModel.name,
                action: () =>
                    _viewModel.onRemoveFilterItem(ChangeDrawerType.brand),
              ),
            if (data.priceFilter != null)
              _buildFilterItem(
                data.priceFilter.name ?? data.priceFilter.toString(),
                action: () =>
                    _viewModel.onRemoveFilterItem(ChangeDrawerType.price),
              ),
          ],
        ),
      ),
    );
  }

  Widget _buildFilterItem(String title, {action()}) {
    return Container(
      child: Stack(
        alignment: Alignment(1, -1),
        children: [
          Container(
            margin: const EdgeInsets.all(8),
            constraints: BoxConstraints(
              minHeight: Get.width * 0.15,
              minWidth: Get.width * 0.3,
            ),
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: PhysicalModel(
              elevation: 2,
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                padding: const EdgeInsets.all(8),
                child: Center(
                  child: Text(
                    title,
                    maxLines: 1,
                  ),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              action();
            },
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(100))),
                child: Center(
                    child: Icon(
                  Icons.close,
                  color: Colors.white,
                ))),
          )
        ],
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
