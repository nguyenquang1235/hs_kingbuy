import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import '../presentation.dart';

class DrawerFilter {
  CategoryModel categoryModel;
  BrandModel brandModel;
  PriceFilter priceFilter;

  DrawerFilter({this.categoryModel, this.brandModel, this.priceFilter});
}

enum ChangeDrawerType {
  category,
  brand,
  price,
}

class SearchFilterViewModel extends BaseViewModel {
  final TextEditingController searchController = TextEditingController();
  final GlobalKey<WidgetLoadMoreWrapVerticalState2> keyLoadMore = GlobalKey();
  final BehaviorSubject<DrawerFilter> drawerFilterController =
      BehaviorSubject();
  DrawerFilter drawerFilter;
  BehaviorSubject<bool> availableResetFilter = BehaviorSubject();
  DrawerFilter get getDrawerFilter => drawerFilterController.value;

  init(SearchArgument searchArgument) async {
    drawerFilter = searchArgument.drawerFilter ??
        DrawerFilter(
          categoryModel: null,
          priceFilter: null,
          brandModel: null,
        );
    searchController.text = searchArgument.searchKey;
    drawerFilterController.add(
      drawerFilter,
    );
    availableResetFilter.add(false);
  }

  Future<List<ProductModel>> searchItem(int offset) async {
    try {
      Map<String, dynamic> price;
      if (drawerFilter.priceFilter != null) {
        price = {
          "from": drawerFilter.priceFilter.priceFrom ?? 0,
          "to": drawerFilter.priceFilter.priceTo ?? 999999999999999999,
        };
      }
      NetworkState<List<ProductModel>> networkState =
          await searchRepository.searchProduct(
              keyWord: searchController.text,
              offset: offset,
              limit: 6,
              brandId: drawerFilter.brandModel?.id ?? null,
              productCategoryId: drawerFilter.categoryModel?.id ?? null,
              price: price ?? null);
      return networkState.data ?? [];
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<ProductModel>> initRequest() async {
    return searchItem(0);
  }

  Future<List<ProductModel>> dataRequest(int offset) async {
    return searchItem(offset);
  }

  Future<List<CategoryModel>> getCategories() async {
    try {
      List<CategoryModel> listResult = [];
      for (var item in await AppShared.getListCategories()) {
        if (item.children.length > 0) listResult.addAll(item.children);
      }
      return listResult;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<BrandModel>> getBrands() async {
    try {
      List<BrandModel> listResult = await AppShared.getListBrands();
      return listResult;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<PriceFilter>> getPriceFilter() async {
    try {
      List<PriceFilter> listResult = PriceFilter.raw;
      return listResult;
    } catch (e) {
      print(e);
      return [];
    }
  }

  onChangeDrawer(dynamic value, {ChangeDrawerType type}) {
    switch (type) {
      case ChangeDrawerType.category:
        drawerFilterController.add(DrawerFilter(
          categoryModel: value,
          brandModel: getDrawerFilter.brandModel,
          priceFilter: getDrawerFilter.priceFilter,
        ));
        break;
      case ChangeDrawerType.brand:
        drawerFilterController.add(DrawerFilter(
          categoryModel: getDrawerFilter.categoryModel,
          brandModel: value,
          priceFilter: getDrawerFilter.priceFilter,
        ));
        break;
      case ChangeDrawerType.price:
        drawerFilterController.add(DrawerFilter(
          categoryModel: getDrawerFilter.categoryModel,
          brandModel: getDrawerFilter.brandModel,
          priceFilter: value,
        ));
        break;
      default:
        drawerFilterController.add(DrawerFilter(
          categoryModel: null,
          brandModel: null,
          priceFilter: null,
        ));
    }
    bool available = (getDrawerFilter.categoryModel != null) ||
        (getDrawerFilter.brandModel != null) ||
        (getDrawerFilter.priceFilter != null);
    availableResetFilter.add(available);
  }

  onSubmitFilter() {
    drawerFilter = DrawerFilter(
      categoryModel: getDrawerFilter.categoryModel,
      priceFilter: getDrawerFilter.priceFilter,
      brandModel: getDrawerFilter.brandModel,
    );
    notifyListeners();
    keyLoadMore.currentState.onRefresh();
  }

  onRemoveFilterItem(ChangeDrawerType type) {
    switch (type) {
      case ChangeDrawerType.category:
        drawerFilter.categoryModel = null;
        break;
      case ChangeDrawerType.brand:
        drawerFilter.brandModel = null;
        break;
      case ChangeDrawerType.price:
        drawerFilter.priceFilter = null;
        break;
    }
    keyLoadMore.currentState.onRefresh();
    notifyListeners();
  }
}
