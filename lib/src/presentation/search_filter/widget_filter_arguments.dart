import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

typedef ChangeDrawer(dynamic model);

class WidgetFilterArguments<T> extends StatefulWidget {
  final String title;
  final List<T> listData;
  final int displayNumber;
  final T chooseItem;
  final ChangeDrawer action;
  WidgetFilterArguments({
    Key key,
    this.listData,
    this.title,
    this.displayNumber = 4,
    this.action,
    this.chooseItem,
  }) : super(key: key);

  @override
  WidgetFilterArgumentsState<T> createState() =>
      WidgetFilterArgumentsState<T>();
}

class WidgetFilterArgumentsState<T> extends State<WidgetFilterArguments> {
  bool isMore = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 12),
            child: Text(
              AppUtils.translate(widget.title, context),
              style: AppStyles.DEFAULT_LARGE_BOLD,
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Wrap(
                  alignment: WrapAlignment.center,
                  runAlignment: WrapAlignment.center,
                  children: _listWidget()),
              widget.displayNumber < widget.listData.length
                  ? _buildButtonSeeMore()
                  : SizedBox(
                      width: Get.width,
                    ),
              Divider(
                color: Colors.grey[200],
                thickness: 1,
              ),
            ],
          )
        ],
      ),
    );
  }

  List<Widget> _listWidget() {
    if (isMore) {
      return List.generate(widget.listData.length,
          (index) => _buildItem(widget.listData[index]));
    } else {
      int num = widget.listData.length <= widget.displayNumber
          ? widget.listData.length
          : widget.displayNumber;
      return List.generate(num, (index) => _buildItem(widget.listData[index]));
    }
  }

  Widget _buildItem(dynamic model) {
    return GestureDetector(
      onTap: () async {
        widget.action(widget.chooseItem == model ? null : model);
      },
      child: RepaintBoundary(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
          width: Get.width * 0.3,
          padding: EdgeInsets.only(top: 8, left: 6, right: 6, bottom: 8),
          decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(widget.chooseItem == model
                    ? AppImages.bgDrawerItemSelected
                    : AppImages.bgDrawerItem)),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Center(
            child: Text(
              model.name + "\n",
              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildButtonSeeMore() {
    return GestureDetector(
      // onTap: () async => await _loadMore(),
      onTap: () {
        setState(() {
          isMore = !isMore;
        });
      },
      child: Container(
        child: Center(
          child: Text(
            AppUtils.translate(isMore ? "zoom_out" : "see_more", context),
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
              decoration: TextDecoration.underline,
              color: Colors.lightBlue,
            ),
          ),
        ),
      ),
    );
  }
}

class WidgetFilterPriceArguments<T> extends StatefulWidget with ChangeNotifier {
  final String title;
  final List<T> listData;
  final int displayNumber;
  final T chooseItem;
  final ChangeDrawer action;
  final ChangeDrawer onChangeText;
  WidgetFilterPriceArguments(
      {Key key,
      this.listData,
      this.title,
      this.displayNumber = 4,
      this.action,
      this.chooseItem,
      this.onChangeText})
      : super(key: key);

  @override
  WidgetFilterPriceArgumentsState<T> createState() =>
      WidgetFilterPriceArgumentsState<T>();
}

class WidgetFilterPriceArgumentsState<T>
    extends State<WidgetFilterPriceArguments> {
  final TextEditingController minPriceController = TextEditingController();
  final TextEditingController maxPriceController = TextEditingController();
  final FocusNode nodePriceTo = FocusNode();
  bool isMore = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 12),
            child: Text(
              AppUtils.translate(widget.title, context),
              style: AppStyles.DEFAULT_LARGE_BOLD,
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Column(
            children: [
              Wrap(
                  alignment: WrapAlignment.center,
                  runAlignment: WrapAlignment.center,
                  children: _listWidget()),
              widget.displayNumber != widget.listData.length
                  ? _buildButtonSeeMore()
                  : SizedBox(
                      width: Get.width,
                    ),
            ],
          ),
          _buildInputPrice(),
          Divider(
            color: Colors.grey[200],
            thickness: 1,
          ),
        ],
      ),
    );
  }

  List<Widget> _listWidget() {
    if (isMore) {
      return List.generate(widget.listData.length,
          (index) => _buildItem(widget.listData[index]));
    } else {
      return List.generate(
          widget.displayNumber, (index) => _buildItem(widget.listData[index]));
    }
  }

  Widget _buildItem(dynamic model) {
    return GestureDetector(
      onTap: () async {
        minPriceController.text = "";
        maxPriceController.text = "";
        widget.action(widget.chooseItem == model ? null : model);
      },
      child: RepaintBoundary(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
          width: Get.width * 0.3,
          padding: EdgeInsets.only(top: 8, left: 6, right: 6, bottom: 8),
          decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(widget.chooseItem == model
                    ? AppImages.bgDrawerItemSelected
                    : AppImages.bgDrawerItem)),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Center(
            child: Text(
              model.name + "\n",
              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildButtonSeeMore() {
    return GestureDetector(
      // onTap: () async => await _loadMore(),
      onTap: () {
        setState(() {
          isMore = !isMore;
        });
      },
      child: Container(
        child: Center(
          child: Text(
            AppUtils.translate(isMore ? "zoom_out" : "see_more", context),
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
              decoration: TextDecoration.underline,
              color: Colors.lightBlue,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildInputPrice() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 26),
            child: Text("Hoặc nhập giá ở ô dưới"),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                child: WidgetInput(
                  onChanged: (value) => null,
                  inputController: minPriceController,
                  onComplete: () {
                    onCompleteText();
                    FocusScope.of(context).requestFocus(nodePriceTo);
                  },
                  inputType: TextInputType.number,
                  elevation: 2,
                  radiusBorder: 10,
                  width: Get.width * 0.3,
                  hint: "Từ 0 đ",
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                child: WidgetInput(
                  focusNode: nodePriceTo,
                  onChanged: (value) => null,
                  inputController: maxPriceController,
                  onComplete: () {
                    onCompleteText();
                    FocusScope.of(context).unfocus();
                  },
                  inputType: TextInputType.number,
                  elevation: 2,
                  radiusBorder: 10,
                  width: Get.width * 0.3,
                  hint: "Đến 0 đ",
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  void onCompleteText() {
    Map<String, dynamic> data = {
      "id": -1,
      "minPrice": minPriceController.text != ""
          ? int.parse(minPriceController.text)
          : null,
      "maxPrice": maxPriceController.text != ""
          ? int.parse(maxPriceController.text)
          : null,
      // "name": "${minPriceController.text}-${maxPriceController.text} đ",
    };
    widget.onChangeText(data);
  }
}
