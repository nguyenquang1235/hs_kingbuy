import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/presentation/widgets/widget_avatar.dart';
import 'package:King_Buy/src/utils/app_valid.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterUserProfileScreen extends StatefulWidget {
  @override
  _RegisterUserProfileScreenState createState() =>
      _RegisterUserProfileScreenState();
}

class _RegisterUserProfileScreenState extends State<RegisterUserProfileScreen>
    with ResponsiveWidget {
  RegisterUserProfileViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<RegisterUserProfileViewModel>(
      viewModel: RegisterUserProfileViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: buildUi(context: context),
          ),
        );
      },
    );
  }

  _buildScreen() {
    return StreamBuilder(
      stream: _viewModel.loadingSubject,
      builder: (context, snapshot) {
        bool isLoading = snapshot.data ?? false;
        return WidgetLoadingPage(
          isLoading,
          page: _buildBody(),
        );
      },
    );
  }

  _buildBody() {
    return Column(
      children: [
        WidgetAppBar(
          bgColor: Colors.blue,
          colorButton: Colors.white,
          keyTitle: "user_profile",
          supWidget: _buildButtonSave(),
          actionBack: () {
            Navigator.pushReplacementNamed(context, Routers.login_navigation);
          },
        ),
        Expanded(
            child: SingleChildScrollView(
          child: _buildBodyScreen(),
        ))
      ],
    );
  }

  _buildBodyScreen() {
    return Container(
      padding: EdgeInsets.all(24),
      child: Column(
        children: [
          _buildAvatar(),
          SizedBox(
            height: 55,
          ),
          _buildFormProfile(),
          SizedBox(
            height: 30,
          ),
          _buildGender(),
        ],
      ),
    );
  }

  _buildFormProfile() {
    TextStyle style = AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.blue);
    return Form(
      key: _viewModel.formKeySignIn,
      child: Column(
        children: [
          WidgetInput(
            hint: AppUtils.translate(
              "full_name",
              context,
            ),
            inputController: _viewModel.nameController,
            validator: AppValid.validateFullName(context),
            preIcon: Icon(
              Icons.person_outline,
              color: AppColors.grey,
              size: 40,
            ),
            onChanged: (value) => null,
            style: style,
          ),
          WidgetInput(
            hint: AppUtils.translate(
              "number_phone",
              context,
            ),
            inputController: _viewModel.phoneController,
            validator: AppValid.validatePhoneNumber(context),
            preIcon: Icon(
              Icons.phone_outlined,
              color: AppColors.grey,
              size: 40,
            ),
            inputType: TextInputType.phone,
            onChanged: (value) => null,
            style: style,
          ),
          GestureDetector(
            onTap: () {
              _viewModel.showDayTimePicker();
            },
            child: Stack(
              children: [
                WidgetInput(
                  hint: AppUtils.translate(
                    "birth_day",
                    context,
                  ),
                  validator: (value) {
                    if (value == null || value.length == 0) {
                      return AppUtils.translate(
                          "valid_enter_birth_day", context);
                    }
                    return null;
                  },
                  style: style,
                  inputController: _viewModel.birthDayController,
                  preIcon: Icon(
                    Icons.calendar_today_outlined,
                    color: AppColors.grey,
                    size: 40,
                  ),
                  onChanged: (value) => null,
                ),
                Opacity(
                  opacity: 0,
                  child: WidgetInput(
                    hint: AppUtils.translate(
                      "birth_day",
                      context,
                    ),
                    enabled: false,
                    preIcon: Icon(
                      Icons.calendar_today_outlined,
                      color: AppColors.grey,
                      size: 40,
                    ),
                    onChanged: (value) => null,
                  ),
                ),
              ],
            ),
          ),
          WidgetInput(
            hint: AppUtils.translate(
              "email",
              context,
            ),
            inputController: _viewModel.emailController,
            validator: AppValid.validateEmail(context),
            inputType: TextInputType.emailAddress,
            preIcon: Icon(
              Icons.email_outlined,
              color: AppColors.grey,
              size: 40,
            ),
            onChanged: (value) => null,
            style: style,
          ),
        ],
      ),
    );
  }

  _buildGender() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(AppUtils.translate("gender", context),
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.black)),
        SizedBox(
          height: 10,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            _buildTitleGender(true, "male"),
            SizedBox(
              width: 15,
            ),
            _buildTitleGender(false, "female"),
          ],
        ),
      ],
    );
  }

  _buildTitleGender(bool value, String title) {
    return StreamBuilder<bool>(
        stream: _viewModel.genderController,
        builder: (context, snapshot) {
          return Container(
            child: Row(
              children: [
                Radio<bool>(
                  activeColor: Colors.redAccent,
                  value: value,
                  groupValue: snapshot?.data ?? true,
                  onChanged: (value) {
                    _viewModel.changeGender(value);
                  },
                ),
                Text(
                  AppUtils.translate(title, context),
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.grey),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildAvatar() {
    return Center(
      child: InkWell(
        onTap: () => _viewModel.changeAvatarImage(),
        child: Stack(
          overflow: Overflow.visible,
          children: [
            WidgetAvatar(
              radius: 55,
              url: "",
              imageStorage: _viewModel.imageStorage,
            ),
            Positioned(
              bottom: 4,
              right: 9,
              child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.5,
                    color: Colors.white,
                  ),
                  shape: BoxShape.circle,
                  color: Colors.redAccent,
                ),
                child: Center(
                  child: Icon(
                    Icons.edit,
                    size: 20,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildButtonSave() {
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(100)),
      elevation: 2,
      color: Colors.red,
      child: Container(
        height: 40,
        child: WidgetButtonGradient(
          // padding: EdgeInsets.fromLTRB(30, 12, 30, 12),
          colorStart: Colors.redAccent,
          colorEnd: Colors.red,
          title: "save",
          action: _viewModel.confirmUser,
        ),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
