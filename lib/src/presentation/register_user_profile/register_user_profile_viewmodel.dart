import 'dart:io';

import 'package:King_Buy/src/presentation/base/base.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart';
import 'package:toast/toast.dart';

import '../presentation.dart';

class RegisterUserProfileViewModel extends BaseViewModel {

  final formKeySignIn = GlobalKey<FormState>();
  final genderController = BehaviorSubject<bool>();

  Image imageStorage;
  TextEditingController nameController;
  TextEditingController phoneController;
  TextEditingController birthDayController;
  TextEditingController emailController;


  init() async {
    nameController = TextEditingController();
    phoneController = TextEditingController();
    birthDayController = TextEditingController();
    emailController = TextEditingController();
  }

  changeAvatarImage() async {
    File file = await showDialog(
      context: context,
      builder: (context) => DialogMethodUpload(
        type: DialogMethodUploadType.avatar,
      ),
    );
    if (file != null) imageStorage = Image.memory(await file.readAsBytes());
    notifyListeners();
  }

  showDayTimePicker() {
    LocaleType localeType;
    for (var item in LocaleType.values) {
      if (item.toString().split('.').last == Get.deviceLocale.languageCode)
        localeType = item;
    }

    return DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(1900, 1, 1),
      maxTime: DateTime.now(),
      onConfirm: (date) {
        birthDayController.text =
            AppUtils.convertDateTime2String(date, format: "dd/MM/yyyy");
      },
      currentTime: DateTime.now(),
      locale: localeType,
    );
  }

  changeGender(bool value){
    genderController.add(value);
  }
  
  confirmUser() async {
    if(formKeySignIn.currentState.validate()){
      try{
        setLoading(true);

        await Future.delayed(const Duration(seconds: 1));
        setLoading(false);
        Toast.show(AppUtils.translate("update_success", context), context);
        Navigator.pushReplacementNamed(context, Routers.navigation);
      }catch(e){
        print(e);
        Toast.show(AppUtils.translate("update_errors", context), context);
      }
    }

  }
}
