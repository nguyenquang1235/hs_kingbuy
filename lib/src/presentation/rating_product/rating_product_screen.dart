import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class RatingProduct extends StatefulWidget {
  final int productId;
  const RatingProduct({Key key, this.productId}) : super(key: key);

  @override
  RatingProductState createState() => RatingProductState();
}

class RatingProductState extends State<RatingProduct> with ResponsiveWidget {
  RatingProductViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<RatingProductViewModel>(
      viewModel:
          RatingProductViewModel(productId: widget.productId, state: this),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) => _buildScreen(),
    );
  }

  Widget _buildScreen() {
    return Container(
      width: Get.width,
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.only(bottom: 8),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RepaintBoundary(
            child: Text(
              AppUtils.translate("reviews_comments", context),
              style: AppStyles.DEFAULT_LARGE_BOLD,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildRatingStatistical(),
              SizedBox(
                height: 10,
              ),
              _buttonWriteComment(),
              SizedBox(
                height: 10,
              ),
              Divider(),
              _buildListComment(),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildListComment() {
    return WidgetLoadMoreWrapVertical<CommentModel>.build(
      padding: EdgeInsets.symmetric(vertical: 10),
      buttonSeeMore: true,
      loadingColor: Colors.red,
      dataRequester: _viewModel.dataRequest,
      initRequester: _viewModel.initDataRequest,
      itemBuilder: (data, context, index) {
        if (index + 2 > data.length) {
          return _buildCommentItem(data[index]);
        } else {
          bool isDivider = _viewModel.convertDatetime(data[index].createdAt) !=
              _viewModel.convertDatetime(data[index + 1].createdAt);
          return _buildCommentItem(data[index], isDivider: isDivider);
        }
      },
    );
  }

  Widget _buildCommentItem(CommentModel model, {bool isDivider = false}) {
    const styleMedium = AppStyles.DEFAULT_MEDIUM;
    const styleSmall = AppStyles.DEFAULT_SMALL;
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 8),
          padding: EdgeInsets.only(left: 16, right: 4),
          width: Get.width,
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    model.name,
                    style: styleSmall,
                  ),
                  Text(
                    model.phoneNumber.length > 0
                        ? model.phoneNumber.replaceRange(5, 7, "***")
                        : "",
                    style: styleSmall.copyWith(color: Colors.grey),
                  ),
                ],
              ),
              SmoothStarRating(
                  allowHalfRating: false,
                  starCount: 5,
                  rating: double.parse(model.star.toString()),
                  size: 13,
                  isReadOnly: true,
                  filledIconData: Icons.star,
                  halfFilledIconData: Icons.star_half,
                  color: Colors.orange,
                  borderColor: Colors.orange,
                  spacing: 0.0),
              Text(
                model.comment,
                style: styleMedium,
              ),
              Text(_viewModel.convertDatetime(model.createdAt),
                  style: styleSmall.copyWith(color: Colors.grey))
            ],
          ),
        ),
        if (isDivider)
          Divider(
            thickness: 1,
          )
      ],
    );
  }

  Widget _buttonWriteComment() {
    return GestureDetector(
      onTap: () => _viewModel.goUserRating(),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8),
        decoration: BoxDecoration(
          color: Colors.transparent,
            border: Border.all(width: 1, color: Colors.red),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Center(
            child: Text(
          AppUtils.translate("write_comment", context),
          style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.red),
        )),
      ),
    );
  }

  Widget _buildRatingStatistical() {
    return FutureBuilder<RatingInfoModel>(
      future: _viewModel.getRatingInfo(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: WidgetCircleProgress(
              color: Colors.red,
            ),
          );
        } else {
          if (snapshot.hasData)
            return Row(
              children: [
                Expanded(
                  child: _buildAvgRating(snapshot.data),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _buildRatingItem(
                          title: 5,
                          rating: snapshot.data.fiveStarCount,
                          count: snapshot.data.ratingCount),
                      _buildRatingItem(
                          title: 4,
                          rating: snapshot.data.fourStarCount,
                          count: snapshot.data.ratingCount),
                      _buildRatingItem(
                          title: 3,
                          rating: snapshot.data.threeStarCount,
                          count: snapshot.data.ratingCount),
                      _buildRatingItem(
                          title: 2,
                          rating: snapshot.data.twoStarCount,
                          count: snapshot.data.ratingCount),
                      _buildRatingItem(
                          title: 1,
                          rating: snapshot.data.oneStarCount,
                          count: snapshot.data.ratingCount),
                    ],
                  ),
                )
              ],
            );
          return SizedBox();
        }
      },
    );
  }

  Widget _buildAvgRating(RatingInfoModel model) {
    return Column(
      children: [
        RepaintBoundary(
          child: FittedBox(
            fit: BoxFit.cover,
            child: Text(
              AppUtils.translate("avg_rating", context),
              style: AppStyles.DEFAULT_LARGE,
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        RepaintBoundary(
          child: FittedBox(
            fit: BoxFit.cover,
            child: Text(
              "(${model.ratingCount} ${AppUtils.translate("rating", context)})",
              style: AppStyles.DEFAULT_LARGE.copyWith(color: Colors.grey),
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        RepaintBoundary(
          child: FittedBox(
            fit: BoxFit.cover,
            child: Text(
              "${model.avgRating} / 5",
              style: AppStyles.DEFAULT_LARGE_BOLD
                  .copyWith(color: Colors.grey, fontSize: 25),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildRatingItem({int title, int rating, int count}) {
    var style = AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.grey);
    return Row(
      children: [
        RepaintBoundary(
          child: Text(
            title.toString(),
            style: style,
          ),
        ),
        Icon(
          Icons.star,
          color: Colors.grey,
          size: 15,
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: LinearPercentIndicator(
            animation: false,
            lineHeight: 8,
            backgroundColor: Colors.grey,
            padding: const EdgeInsets.all(0),
            percent: count <= 0 ? 0 : (rating / count),
            progressColor: Colors.amber,
            fillColor: Colors.white,
          ),
        ),
        SizedBox(
          width: 10,
        ),
        RepaintBoundary(
          child: Text(
            "$rating ${AppUtils.translate("rating", context)}",
            style: style,
          ),
        )
      ],
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
