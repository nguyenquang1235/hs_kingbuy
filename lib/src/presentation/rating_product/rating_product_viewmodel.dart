import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';

class RatingProductViewModel extends BaseViewModel {
  final int productId;
  final RatingProductState state;
  RatingProductViewModel({this.productId, this.state});

  init() async {}

  Future<RatingInfoModel> getRatingInfo() async {
    NetworkState<RatingInfoModel> netWorkState =
        await ratingRepository.getRatingInfo(productId);
    if (netWorkState.isSuccess && netWorkState.response.isSuccess) {
      return netWorkState.data;
    }
    return null;
  }

  Future<List<CommentModel>> getComment() async {
    NetworkState<List<CommentModel>> netWorkState =
        await ratingRepository.getComment(productId);
    if (netWorkState.isSuccess && netWorkState.response.isSuccess) {
      return netWorkState.data;
    }
    return [];
  }

  Future<List<CommentModel>> initDataRequest() async {
    List<CommentModel> listData = await getComment();
    bool isHasData = (listData.length > 0);
    if (isHasData)
      return (listData.length < 5)
          ? listData
          : listData.getRange(0, 5).toList();
    return [];
  }

  Future<List<CommentModel>> dataRequest(int offset) async {
    try {
      List<CommentModel> listData = await getComment();
      bool isHasData = (listData.length > 0);
      if (isHasData) return listData.getRange(offset, 5).toList();
      return [];
    } catch (e) {
      print(e);
      return [];
    }
  }

  String convertDatetime(String data) {
    return AppUtils.convertString2String(data,
        outputFormat: "dd/MM/yyyy", inputFormat: "yyyy-MM-dd hh:mm:ss");
  }

  void goUserRating() {
    Navigator.pushNamed(context, Routers.create_rating, arguments: productId)
        .then((value) {
      if (value) {
        state.setState(() {});
      }
    });
  }
}
