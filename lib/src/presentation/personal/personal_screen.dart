import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/presentation/widgets/widget_avatar.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PersonalScreen extends StatefulWidget {
  // final NavigationViewModel navigationViewModel;
  // const PersonalScreen({Key key, this.navigationViewModel}) : super(key: key);

  @override
  _PersonalScreenState createState() => _PersonalScreenState();
}

class _PersonalScreenState extends State<PersonalScreen> with ResponsiveWidget {
  PersonalViewModel _viewModel;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<PersonalViewModel>(
      viewModel: PersonalViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) => buildUi(context: context),
    );
  }

  _buildScreen() {
    return StreamBuilder<Object>(
        stream: _viewModel.loadingSubject,
        builder: (context, snapshot) {
          bool isLoading = snapshot.data ?? true;
          return WidgetLoadingPage(
            isLoading,
            page: _buildBody(),
          );
        });
  }

  _buildBody() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 16),
        child: Column(
          children: [
            _buildUser(),
            SizedBox(
              height: 30,
            ),
            _buildPersonalMenu(),
            SizedBox(
              height: 30,
            ),
            _buildFooter(),
          ],
        ),
      ),
    );
  }

  _buildUser() {
    bool isLogger = _viewModel.userModel != null;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildAvatar(_viewModel.userModel?.avatarSource ?? ""),
        SizedBox(
          height: 10,
        ),
        if (isLogger)
          Text(
              _viewModel.userModel?.name ??
                  AppUtils.translate("account", context),
              style:
                  AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.black)),
        SizedBox(
          height: 5,
        ),
        if (isLogger)
          _viewModel.userModel?.phoneNumber != null
              ? Text(
                  AppUtils.convertPhoneNumber(
                      _viewModel.userModel?.phoneNumber),
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.grey),
                )
              : SizedBox(),
        SizedBox(
          height: 5,
        ),
        if (isLogger)
          Text(
              "${_viewModel.userModel?.rewardPoints?.toString() ?? ""} ${AppUtils.translate("point", context).toLowerCase()}",
              style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.red)),
      ],
    );
  }

  _buildPersonalMenu() {
    return Column(
      children: [
        _buildMenuItem(
          image: AppImages.icUser,
          title: "personal_info",
          callback: () => _viewModel.requestLogin(
            router: Routers.user_profile,
            callback: () => _viewModel.init(),
          ),
        ),
        _buildMenuItem(
            image: AppImages.icGift,
            title: "promotion",
            callback: () => Navigator.pushNamed(context, Routers.promotion)),
        _buildMenuItem(
            image: AppImages.icDelivery,
            title: "delivery_address",
            callback: () => _viewModel.requestLogin(
                router: Routers.delivery_address, callback: () => _viewModel.init())),
        _buildMenuItem(
            image: AppImages.icTag,
            title: "coupon",
            callback: () => _viewModel.requestLogin(
                router: Routers.coupons, callback: () => _viewModel.init())),
        _buildMenuItem(
            image: AppImages.icReload,
            title: "invoice_history",
            callback: () => _viewModel.requestLogin(
                router: Routers.invoice_history, callback: () => _viewModel.init())),
        _buildMenuItem(
            image: AppImages.icRefreshDisable,
            title: "check_warranty",
            callback: () => _viewModel.requestLogin(
                router: Routers.warranty, callback: () => _viewModel.init())),
        _buildMenuItem(
          image: AppImages.icEye,
          title: "viewed_products",
          callback: () => Navigator.pushNamed(context, Routers.viewed_products),
        ),
        _buildMenuItem(
            image: AppImages.icLock,
            title: "change_pass",
            // callback: () => Navigator.pushNamed(context, Routers.reset_pass)
            callback: () => _viewModel.requestLogin(
                router: Routers.reset_pass, callback: () => _viewModel.init())),
        _buildMenuItem(
            image: AppImages.icAboutUs,
            title: "kingbuy_commitment",
            callback: () => Navigator.pushNamed(context, Routers.commitments)),
        _buildMenuItem(
            image: AppImages.icPhoneGrey,
            title: "contact",
            callback: () => _viewModel.requestLogin(
                router: Routers.contact, callback: () => _viewModel.init())),
        _buildMenuItem(
            image: AppImages.icPage,
            title: "rules",
            callback: () => Navigator.pushNamed(context, Routers.terms_of_use)),
      ],
    );
  }

  _buildMenuItem({String image, String title, Function callback}) {
    return InkWell(
      borderRadius: BorderRadius.all(Radius.circular(30)),
      onTap: callback,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 2),
        padding: EdgeInsets.symmetric(vertical: 6, horizontal: 2),
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.all(6),
              child: Image.asset(
                image,
                width: 30,
                height: 30,
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              AppUtils.translate(title, context),
              style: AppStyles.DEFAULT_MEDIUM,
            ),
            Expanded(child: SizedBox()),
            Icon(
              Icons.arrow_forward_ios,
              color: Colors.grey,
              size: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildAvatar(String avatarSource) {
    return Center(
      child: Stack(
        overflow: Overflow.visible,
        children: [
          WidgetAvatar(
            radius: 55,
            url: avatarSource ?? "",
          ),
          Positioned(
            bottom: 4,
            right: 9,
            child: Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1.5,
                  color: Colors.white,
                ),
                shape: BoxShape.circle,
                color: Colors.redAccent,
              ),
              child: Center(
                child: Icon(
                  Icons.edit,
                  size: 20,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  _buildFooter() {
    return Column(
      children: [
        Text(
          "${AppUtils.translate("hotline", context)}: ${_viewModel.configModel?.hotLine ?? ""}",
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          _viewModel.configModel?.verApp ?? "",
          style: AppStyles.DEFAULT_MEDIUM,
        ),
        SizedBox(
          height: 30,
        ),
        GestureDetector(
          onTap: () => _viewModel.logout(),
          child: Text(
            AppUtils.translate("logout", context),
            style:
                AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.redAccent),
          ),
        )
      ],
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
