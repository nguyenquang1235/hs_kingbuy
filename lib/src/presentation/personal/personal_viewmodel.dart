import 'dart:io';

import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/app_shared.dart';
import 'package:flutter/material.dart';

class PersonalViewModel extends BaseViewModel {
  UserModel userModel;
  ConfigModel configModel;

  init() async {
    setLoading(true);
    userModel = await AppShared.getUser();
    await getConfigs();
    await Future.delayed(const Duration(milliseconds: 500));
    setLoading(false);
  }

  login() async {
    Navigator.pushReplacementNamed(context, Routers.login_navigation);
  }

  getConfigs() async {
    try {
      NetworkState<ConfigModel> networkState =
          await otherRepository.getConfigs();
      if (networkState.isSuccess && networkState.response.isSuccess) {
        configModel = networkState.data;
      }
    } catch (e) {
      print(e);
    }
  }

  logout() async {
    try {
      setLoading(true);
      await AppShared.setAccessToken("");
      await AppShared.setUser(null);
      await AppShared.setLogged(false);
      userModel = await getProfile();
      notifyListeners();
      setLoading(false);
    } catch (e) {
      print(e);
    }
  }

}
