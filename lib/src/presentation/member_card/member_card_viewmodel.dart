import 'dart:io';

import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/app_shared.dart';
import 'package:barcode/barcode.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class MemberCardViewModel extends BaseViewModel {
  UserModel userModel;
  File fileBarcode;
  CouponApplication couponApplication;
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  init() async {
    setLoading(true);
    userModel = await AppShared.getUser();
    couponApplication = await getCouponApplication();
    await getBarcode();
    setLoading(false);
  }

  getBarcode() async {
    final svg = Barcode.code93().toSvg(
      userModel.memberCardNumber,
      width: 300,
      color: 2,
      drawText: false,
    );
    String path = await _localPath;
    fileBarcode = File("$path/kingbuy_barcode.svg")..writeAsString(svg);
  }

  Future<CouponApplication> getCouponApplication() async {
    try {
      NetworkState<CouponApplication> networkState =
          await couponRepository.getMyCoupon();
      return networkState.data;
    } catch (e) {
      print(e);
      return null;
    }
  }

  getCouponDetails(int couponId) {
    Navigator.pushNamed(context, Routers.coupon_details, arguments: couponId);
  }
}
