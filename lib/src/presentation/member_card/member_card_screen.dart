import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class MemberCardScreen extends StatefulWidget {
  @override
  _MemberCardScreenState createState() => _MemberCardScreenState();
}

class _MemberCardScreenState extends State<MemberCardScreen>
    with ResponsiveWidget {
  MemberCardViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<MemberCardViewModel>(
      viewModel: MemberCardViewModel(),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel..init();
      },
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? true;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      isLeading: false,
      keyTitle: "member_card",
      colorButton: Colors.white,
    );
  }

  Widget _buildScreen() {
    return Container(
      padding: const EdgeInsets.only(top: 12),
      child: Column(
        children: [
          _buildImage(),
          Expanded(child: _buildCouponNotUse()),
        ],
      ),
    );
  }

  Widget _buildImage() {
    return _viewModel.fileBarcode != null
        ? Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white,
                border: Border.all(width: 10, color: Colors.red)),
            child: Column(
              children: [
                SvgPicture.file(_viewModel.fileBarcode),
                SizedBox(
                  height: 10,
                ),
                Text(
                  _viewModel.userModel.memberCardNumber,
                  style: AppStyles.DEFAULT_LARGE_BOLD,
                )
              ],
            ),
          )
        : SizedBox();
  }

  Widget _buildCouponNotUse() {
    return Container(
      margin: const EdgeInsets.only(top: 12),
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "${AppUtils.translate("non_use", context)}: ${_viewModel.couponApplication?.nonUse?.length ?? 0}",
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          Expanded(
            child: ListView(
              padding: EdgeInsets.only(top: 8),
              physics: BouncingScrollPhysics(),
              children: List.generate(
                      _viewModel.couponApplication?.nonUse?.length ?? 0, (index) {
                    var data = _viewModel.couponApplication?.nonUse ?? [];
                    return _buildCouponItem(data[index],
                        isLast: data.last == data[index]);
                  }) +
                  [
                    Center(
                      child: InkWell(
                        onTap: () =>
                            Navigator.pushNamed(context, Routers.coupons),
                        child: Text(
                          AppUtils.translate("see_all_coupons", context),
                          style: AppStyles.DEFAULT_MEDIUM
                              .copyWith(color: Colors.red),
                        ),
                      ),
                    )
                  ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCouponItem(CouponModel model, {bool isLast = false}) {
    return GestureDetector(
      onTap: () => _viewModel.getCouponDetails(model.id),
      child: Container(
        color: Colors.transparent,
        margin: const EdgeInsets.all(4),
        child: Column(
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                WidgetImageNetwork(
                  url: model.imageSource,
                  height: Get.width * 0.25,
                  width: Get.width * 0.3,
                  fit: BoxFit.fill,
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          constraints: BoxConstraints(
                            maxHeight: Get.width * 0.2,
                          ),
                          child: Text(
                            model.name,
                            style: AppStyles.DEFAULT_MEDIUM_BOLD,
                            textAlign: TextAlign.start,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "${AppUtils.translate("expiry_date", context)}: ${AppUtils.convertString2String(model.expiresAt, outputFormat: "dd/MM/yyyy", inputFormat: "yyyy-MM-dd")}",
                          style: AppStyles.DEFAULT_MEDIUM,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            if (!isLast)
              Divider(
                thickness: 1,
              ),
          ],
        ),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
