import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:King_Buy/src/utils/app_valid.dart';
import 'package:flutter/material.dart';

class ForgotPassScreen extends StatefulWidget {
  final Function back;

  const ForgotPassScreen({Key key, this.back}) : super(key: key);

  @override
  _ForgotPassScreenState createState() => _ForgotPassScreenState();
}

class _ForgotPassScreenState extends State<ForgotPassScreen>
    with ResponsiveWidget {
  ForgotPasswordViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ForgotPasswordViewModel>(
      viewModel: ForgotPasswordViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return buildUi(context: context);
      },
    );
  }

  _buildScreen() {
    return StreamBuilder(
      stream: _viewModel.loadingSubject,
      builder: (context, snapshot) {
        bool isLoading = snapshot.data ?? false;
        return WidgetLoadingPage(
          isLoading,
          page: _buildBody(),
        );
      },
    );
  }

  _buildBody() {
    return Column(
      children: [
        WidgetAppBar(
          bgColor: Colors.blue,
          colorButton: Colors.white,
          keyTitle: "password_retrieval",
          actionBack: widget.back,
        ),
        Expanded(
            child: SingleChildScrollView(
          child: _buildBodyScreen(),
        ))
      ],
    );
  }

  _buildBodyScreen() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25, horizontal: 16),
      child: Column(
        children: [
          SizedBox(
            height: 100,
          ),
          Text(
            AppUtils.translate(
              "input_account_information",
              context,
            ).toUpperCase(),
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(fontSize: 25),
          ),
          SizedBox(
            height: 25,
          ),
          Form(
            key: _viewModel.formKeySignIn,
            child: WidgetInput(
              hint: AppUtils.translate(
                "email_and_phone_number",
                context,
              ),
              validator: AppValid.validatePhoneAndEmail(context),
              preIcon: Icon(
                Icons.person_outline,
                color: AppColors.grey,
                size: 40,
              ),
              inputController: _viewModel.accountController,
              onChanged: (value) => null,
            ),
          ),
          SizedBox(
            height: 25,
          ),
          _buildRowButtonContinue(),
          SizedBox(
            height: 25,
          ),
        ],
      ),
    );
  }

  _buildRowButtonContinue() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Opacity(
          opacity: 0,
          child: Padding(
            padding: const EdgeInsets.only(left: 16),
            child: Text(
              AppUtils.translate("forgot_pass", context) + " ?",
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
          ),
        ),
        PhysicalModel(
          borderRadius: BorderRadius.all(Radius.circular(100)),
          elevation: 2,
          color: Colors.grey,
          child: WidgetButtonGradient(
            padding: EdgeInsets.fromLTRB(30, 12, 30, 12),
            colorStart: Colors.redAccent,
            colorEnd: Colors.red,
            title: "continue",
            action: () => _viewModel.onRetrievalPassword(),
          ),
        ),
      ],
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
