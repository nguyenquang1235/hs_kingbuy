import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class ForgotPasswordViewModel extends BaseViewModel {
  final formKeySignIn = GlobalKey<FormState>();
  TextEditingController accountController;

  init() {
    accountController = TextEditingController();
  }

  onRetrievalPassword() async {
    unFocus();
    if (formKeySignIn.currentState.validate()) {
      setLoading(true);
      try {
        NetworkState networkState = await authRepository.sendForgotPassOTP(
            identity: accountController.value.text);
        await checkData(networkState);
      } catch (e) {
        print(e);
        Toast.show(AppUtils.translate("system_errors", context), context);
      }
      setLoading(false);
    }
  }

  checkData(NetworkState networkState) async {
    if (networkState == null) return;
    if (networkState.isSuccess) {
      if (networkState.response.isSuccess) {
        Toast.show(networkState.response.message, context,
            duration: Toast.LENGTH_LONG);
        await Future.delayed(const Duration(milliseconds: 500));
        Navigator.pushNamed(context, Routers.otp,
            arguments: OtpScreenArguments(
              otpType: OtpType.forgotPass,
              identity: accountController.value.text,
            ));
      } else {
        Toast.show(networkState.response.message, context);
      }
    } else {
      Toast.show(AppUtils.translate("system_errors", context), context);
    }
  }
}
