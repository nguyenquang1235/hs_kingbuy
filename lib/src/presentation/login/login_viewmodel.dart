import 'package:King_Buy/src/presentation/base/base.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../routers.dart';

enum Login {
  facebook,
  google,
  identity,
  apple,
}

class LoginViewModel extends BaseViewModel {
  final formKeySignIn = GlobalKey<FormState>();
  final String router;

  TextEditingController accountController;
  TextEditingController passController;

  LoginViewModel({this.router});

  init() async {
    accountController = TextEditingController();
    passController = TextEditingController();
  }

  login(Login loginType) async {
    setLoading(true);
    try {
      NetworkState _networkState;
      switch (loginType) {
        case Login.facebook:
          _networkState = await authRepository.loginFacebook();
          break;
        case Login.google:
          _networkState = await authRepository.loginGoogle();
          break;
        case Login.identity:
          unFocus();
          if (formKeySignIn.currentState.validate()) {
            _networkState = await authRepository.loginIdentity(
                identity: accountController.value.text,
                password: passController.value.text);
          }
          break;
        case Login.apple:
          break;
      }
      //Check login và trả về exception hay lỗi
      await _checkLogin(networkState: _networkState);
    } catch (e) {
      print(e);
      Toast.show(AppUtils.translate("login_failure", context), context);
    }
    setLoading(false);
  }

  _checkLogin({NetworkState<UserModel> networkState}) async {
    if (networkState == null) return;
    if (networkState.isSuccess && networkState.data != null) {
      Toast.show("${AppUtils.translate("login_success", context)}", context);
      await AppShared.setUser(networkState.data);
      await AppShared.setLogged(true);
      if(router != null){
        Navigator.pushReplacementNamed(context, router);
      }else{
        Navigator.pop(context);
      }
    } else {
      Toast.show(AppUtils.translate("login_failure", context), context);
    }
  }
}
