import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:King_Buy/src/utils/app_valid.dart';
import "package:flutter/material.dart";
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

import 'login.dart';

class LoginScreen extends StatefulWidget {
  final LoginNavigationViewModel navigationViewModel;

  LoginScreen({this.navigationViewModel});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with ResponsiveWidget {
  LoginViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<LoginViewModel>(
      viewModel: LoginViewModel(router: widget.navigationViewModel.router),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return buildUi(context: context);
      },
    );
  }

  _buildScreen() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: StreamBuilder<bool>(
          stream: _viewModel.loadingSubject,
          builder: (context, snapshot) {
            bool isLoading = snapshot.data ?? false;
            return WidgetLoadingPage(
              isLoading,
              page: _buildBody(),
            );
          }),
    );
  }

  _buildBody() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          SizedBox(
            height: Get.width * 0.15,
          ),
          Center(
            child: Image.asset(
              AppImages.icLogo,
              width: Get.width * 0.4,
              height: Get.width * 0.4,
              fit: BoxFit.fill,
            ),
          ),
          Text(
            AppUtils.translate("login", context).toUpperCase(),
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(fontSize: 25),
          ),
          SizedBox(
            height: Get.width * 0.12,
          ),
          Form(
            key: _viewModel.formKeySignIn,
            child: Column(
              children: [
                WidgetInput(
                  hint: AppUtils.translate(
                    "email_and_phone_number",
                    context,
                  ),
                  validator: AppValid.validatePhoneAndEmail(context),
                  preIcon: Icon(
                    Icons.person_outline,
                    color: AppColors.grey,
                    size: 40,
                  ),
                  inputController: _viewModel.accountController,
                  onChanged: (value) => null,
                ),
                WidgetInput(
                  hint: AppUtils.translate(
                    "password",
                    context,
                  ),
                  validator: AppValid.validatePassword(context),
                  obscureText: true,
                  preIcon: Icon(
                    FlutterIcons.lock_ant,
                    color: AppColors.grey,
                    size: 40,
                  ),
                  inputController: _viewModel.passController,
                  onChanged: (value) => null,
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          _buildRowButtonLogin(),
          SizedBox(
            height: Get.width * 0.35,
          ),
          _buildBottom(),
        ],
      ),
    );
  }

  _buildRowButtonLogin() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 16),
          child: GestureDetector(
            onTap: () => widget.navigationViewModel
                .changeState(LoginState.forgotPassword),
            child: Text(
              AppUtils.translate("forgot_pass", context) + " ?",
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
          ),
        ),
        PhysicalModel(
          borderRadius: BorderRadius.all(Radius.circular(100)),
          elevation: 2,
          color: Colors.grey,
          child: WidgetButtonGradient(
            padding: EdgeInsets.fromLTRB(30, 12, 30, 12),
            colorStart: Colors.redAccent,
            colorEnd: Colors.red,
            title: "login",
            action: () => _viewModel.login(Login.identity),
          ),
        ),
      ],
    );
  }

  Widget _buildIconButton({IconData icon, Function callback, Color color}) {
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(100)),
      elevation: 2,
      color: Colors.grey,
      child: Container(
        width: 45,
        height: 45,
        decoration: BoxDecoration(
            color: color ?? Colors.blue,
            borderRadius: BorderRadius.all(Radius.circular(100))),
        child: IconButton(
          color: Colors.white,
          icon: Icon(
            icon,
            color: Colors.white,
          ),
          onPressed: callback,
        ),
      ),
    );
  }

  Widget _buildBottom() {
    return Column(
      children: [
        Text(
          AppUtils.translate("login_by", context),
          style: AppStyles.DEFAULT_MEDIUM_BOLD,
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            _buildIconButton(
                color: Colors.indigo,
                icon: FlutterIcons.facebook_f_faw,
                callback: () {
                  _viewModel.login(Login.facebook);
                }),
            SizedBox(
              width: 20,
            ),
            _buildIconButton(
                color: Colors.redAccent,
                icon: FlutterIcons.google_ant,
                callback: () {
                  _viewModel.login(Login.google);
                })
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(AppUtils.translate("you_don't_have_account", context) + " ?",
                style: AppStyles.DEFAULT_MEDIUM_BOLD),
            SizedBox(
              width: 10,
            ),
            InkWell(
              onTap: () =>
                  widget.navigationViewModel.changeState(LoginState.register),
              child: Text(
                AppUtils.translate("register", context),
                style: AppStyles.DEFAULT_MEDIUM_BOLD
                    .copyWith(decoration: TextDecoration.underline),
              ),
            )
          ],
        )
      ],
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
