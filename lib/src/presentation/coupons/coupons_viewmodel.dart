import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:flutter/material.dart';

class CouponsVieModel extends BaseViewModel {
  CouponApplication couponApplication;

  init() async {
    setLoading(true);
    couponApplication = await getCouponApplication();
    setLoading(false);
  }

  Future<CouponApplication> getCouponApplication() async {
    try {
      NetworkState<CouponApplication> networkState =
          await couponRepository.getMyCoupon();
      return networkState.data;
    } catch (e) {
      print(e);
      return null;
    }
  }

  getCouponDetails(int couponId){
    Navigator.pushNamed(context, Routers.coupon_details, arguments: couponId);
  }
}
