import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../presentation.dart';

class CouponsScreen extends StatefulWidget {
  const CouponsScreen({Key key}) : super(key: key);
  @override
  CouponsScreenState createState() => CouponsScreenState();
}

class CouponsScreenState extends State<CouponsScreen>
    with ResponsiveWidget, SingleTickerProviderStateMixin {
  TabController _tabController;
  CouponsVieModel _viewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CouponsVieModel>(
      viewModel: CouponsVieModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
            child: Scaffold(
          body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? true;
                return WidgetLoadingPage(
                  isLoading,
                  page: Column(
                    children: [
                      _buildAppbar(),
                      Expanded(child: buildUi(context: context))
                    ],
                  ),
                );
              }),
        ));
      },
    );
  }

  Widget _buildScreen() {
    return Container(
      child: Column(
        children: [
          _buildTabBar(),
          Expanded(
            child: _buildListData(),
          ),
        ],
      ),
    );
  }

  Widget _buildTabBar() {
    return Stack(
      children: [
        Positioned.fill(
            child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.grey,
                width: 2.0,
              ),
            ),
          ),
        )),
        Container(
          width: Get.width,
          child: TabBar(
            controller: _tabController,
            physics: BouncingScrollPhysics(),
            isScrollable: true,
            tabs: [
              _buildTab("non_use"),
              _buildTab("used"),
              _buildTab("expired"),
            ],
            indicatorColor: Colors.red,
          ),
        ),
      ],
    );
  }

  Widget _buildTab(String text) {
    return Tab(
      child: Container(
        constraints: BoxConstraints(minWidth: Get.width * 0.25),
        child: Center(
          child: Text(
            AppUtils.translate(text, context),
            style: AppStyles.DEFAULT_MEDIUM_BOLD,
          ),
        ),
      ),
    );
  }

  Widget _buildListData() {
    return TabBarView(
      controller: _tabController,
      physics: BouncingScrollPhysics(),
      children: [
        _buildPage(_viewModel.couponApplication?.nonUse ?? []),
        _buildPage(_viewModel.couponApplication?.used ?? []),
        _buildPage(_viewModel.couponApplication?.expired ?? []),
      ],
    );
  }

  Widget _buildPage(List<CouponModel> data) {
    return data.length > 0
        ? ListView(
            physics: BouncingScrollPhysics(),
            padding: const EdgeInsets.all(16),
            children: List.generate(
                data.length,
                (index) => _buildCouponItem(data[index],
                    isLast: data[index] != data.last)),
          )
        : _buildNoneData();
  }

  Widget _buildCouponItem(CouponModel model, {bool isLast = false}) {
    return GestureDetector(
      onTap: () => _viewModel.getCouponDetails(model.id),
      child: Container(
        color: Colors.transparent,
        margin: const EdgeInsets.all(4),
        child: Column(
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                WidgetImageNetwork(
                  url: model.imageSource,
                  height: Get.width * 0.25,
                  width: Get.width * 0.3,
                  fit: BoxFit.fill,
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          constraints: BoxConstraints(
                            maxHeight: Get.width * 0.2,
                          ),
                          child: Text(
                            model.name,
                            style: AppStyles.DEFAULT_MEDIUM_BOLD,
                            textAlign: TextAlign.start,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "${AppUtils.translate("expiry_date", context)}: ${AppUtils.convertString2String(model.expiresAt, outputFormat: "dd/MM/yyyy", inputFormat: "yyyy-MM-dd")}",
                          style: AppStyles.DEFAULT_MEDIUM,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            if (isLast)
              Divider(
                thickness: 1,
              ),
          ],
        ),
      ),
    );
  }

  Widget _buildNoneData() {
    return Container(
      width: Get.width,
      height: Get.height,
      child: Center(
        child: Text(AppUtils.translate("no_data", context)),
      ),
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "coupon",
      colorButton: Colors.white,
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
