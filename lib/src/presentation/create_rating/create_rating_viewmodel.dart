import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:toast/toast.dart';

class CreateRatingViewModel extends BaseViewModel {
  FocusNode nodeName = FocusNode();
  FocusNode nodePhoneNumber = FocusNode();
  TextEditingController commentController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  BehaviorSubject<bool> confirmController = BehaviorSubject();
  int rate = 0;
  GlobalKey<FormState> keyForm = GlobalKey();

  final int productId;

  CreateRatingViewModel(this.productId);

  init() async {}

  void onRate(double rate) {
    this.rate = rate.toInt();
    if (this.rate > 0) {
      confirmController.add(true);
    } else {
      confirmController.add(false);
    }
  }

  void confirmRating() async {
    if (keyForm.currentState.validate()) {
      NetworkState netWorkState;
      try {
        netWorkState = await ratingRepository.userRating(
          productId: productId,
          rating: rate,
          comment: commentController.text,
          name: nameController.text,
          phoneNumber: phoneController.text,
        );

        if (netWorkState.isSuccess) {
          Toast.show(netWorkState.response.message, context);
          if (netWorkState.response.isSuccess) Navigator.pop(context, true);
        } else {
          Toast.show(AppUtils.translate("system_errors", context), context);
        }
      } catch (e) {
        Toast.show(AppUtils.translate("system_errors", context), context);
      }
    }
  }
}
