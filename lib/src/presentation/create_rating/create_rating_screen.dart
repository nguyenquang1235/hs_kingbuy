import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:King_Buy/src/utils/app_valid.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class CreateRatingScreen extends StatefulWidget {
  final int productId;
  const CreateRatingScreen({Key key, this.productId}) : super(key: key);

  @override
  _CreateRatingScreenState createState() => _CreateRatingScreenState();
}

class _CreateRatingScreenState extends State<CreateRatingScreen>
    with ResponsiveWidget {
  CreateRatingViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreateRatingViewModel>(
      viewModel: CreateRatingViewModel(widget.productId),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? false;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "write_comment",
      colorButton: Colors.white,
      supWidget: _buildButtonSave(),
    );
  }

  Widget _buildButtonSave() {
    return StreamBuilder<bool>(
        stream: _viewModel.confirmController,
        builder: (context, snapshot) {
          bool isConfirm = snapshot.data ?? false;
          return PhysicalModel(
            borderRadius: BorderRadius.all(Radius.circular(100)),
            elevation: 2,
            color: (isConfirm) ? Colors.red : Colors.grey,
            child: Container(
              height: 40,
              child: WidgetButtonGradient(
                colorStart: (isConfirm) ? Colors.redAccent : Colors.grey,
                colorEnd: (isConfirm) ? Colors.red : Colors.grey,
                title: "send",
                action: () {
                  if (isConfirm) _viewModel.confirmRating();
                },
              ),
            ),
          );
        });
  }

  Widget _buildScreen() {
    return Form(
      key: _viewModel.keyForm,
      child: Container(
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            _buildRatingStars(),
            SizedBox(
              height: 10,
            ),
            _buildTextField(
              hint: "input_comment",
              maxLines: 4,
              onSubmit: (value) {
                _viewModel.nodeName.requestFocus();
              },
              validator: AppValid.validateFullName(context),
              controller: _viewModel.commentController,
            ),
            SizedBox(
              height: 10,
            ),
            _buildTextField(
              hint: "enter_full_name",
              focusNode: _viewModel.nodeName,
              validator: AppValid.validateFullName(context),
              onSubmit: (value) {
                _viewModel.nodePhoneNumber.requestFocus();
              },
              controller: _viewModel.nameController,
            ),
            _buildTextField(
              hint: "number_phone",
              focusNode: _viewModel.nodePhoneNumber,
              validator:
                  AppValid.validatePhoneNumber(context, notRequired: true),
              controller: _viewModel.phoneController,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRatingStars() {
    return Container(
      child: Column(
        children: [
          Text(
            AppUtils.translate("your_rating", context),
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          SizedBox(
            height: 10,
          ),
          SmoothStarRating(
            allowHalfRating: false,
            onRated: _viewModel.onRate,
            starCount: 5,
            rating: _viewModel.rate.toDouble(),
            size: Get.width * 0.12,
            filledIconData: Icons.star,
            halfFilledIconData: Icons.star_half,
            color: Colors.orange,
            borderColor: Colors.orange,
            spacing: 0.0,
          ),
        ],
      ),
    );
  }

  Widget _buildTextField({
    String hint,
    int maxLines = 1,
    FocusNode focusNode = null,
    ValueChanged<String> onSubmit,
    FormFieldValidator<String> validator,
    TextEditingController controller,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      width: Get.width * 0.85,
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey, width: 0.5))),
      child: TextFormField(
        controller: controller,
        validator: validator,
        onFieldSubmitted: onSubmit,
        focusNode: focusNode,
        maxLines: maxLines,
        decoration: InputDecoration.collapsed(
            hintText: AppUtils.translate(hint, context)),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
