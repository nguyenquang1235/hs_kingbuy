import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_shared.dart';

class ViewedProductsViewModel extends BaseViewModel {
  bool logged;
  List<int> raw = [
    121,
    120,
    119,
    118,
    109,
    108,
    105,
    103,
    102,
    101,
    100,
    99,
    97,
    95,
    94,
    93,
    91,
    88,
    85,
    82,
    81,
    70,
    69,
    55,
    53,
    51,
    50,
    48,
    47,
    43,
    25,
    24,
    23,
    22,
    21,
    20,
    19,
    28,
    17,
    16,
    25,
    121,
    120,
    119,
    118,
    109,
    108,
    105,
  ];

  init() async {}

  Future<List<ProductModel>> initRequester() async {
    return await getViewedProducts(0);
  }

  Future<List<ProductModel>> dataRequester(int offset) async {
    return await getViewedProducts(offset);
  }

  Future<List<ProductModel>> getViewedProducts(int offset) async {
    logged = await AppShared.getLogged();
    if (logged) {
      //Lấy sản phẩm từ dữ liệu trên server
      return getViewedProductIsLogged(offset);
    } else {
      //Láy sản phẩm theo AppShared

      return getViewedProductNotLogged(offset);
    }
  }

  Future<List<ProductModel>> getViewedProductIsLogged(int offset) async {
    try {
      NetworkState networkState =
          await productRepository.getViewedProducts(offset: offset, limit: 10);
      return networkState.data;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<ProductModel>> getViewedProductNotLogged(int offset) async {
    try {
      List<int> listId = raw.getRange(offset, offset + 10).toList();
      NetworkState networkState =
          await productRepository.getProductByIds(listId: listId);
      return networkState.data;
    } catch (e) {
      print(e);
      return [];
    }
  }
}
