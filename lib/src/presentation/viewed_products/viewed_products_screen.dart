import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../presentation.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class ViewedProductsScreen extends StatefulWidget {
  @override
  _ViewedProductsScreenState createState() => _ViewedProductsScreenState();
}

class _ViewedProductsScreenState extends State<ViewedProductsScreen>
    with ResponsiveWidget {
  ViewedProductsViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ViewedProductsViewModel>(
      viewModel: ViewedProductsViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, appbar) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? false;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "viewed_products",
      colorButton: Colors.white,
    );
  }

  Widget _buildScreen() {
    return WidgetLoadMoreWrapVertical<ProductModel>.build(
      loadingColor: Colors.redAccent,
      padding: EdgeInsets.symmetric(vertical: 24),
      itemBuilder: (data, context, index) {
        return WidgetProductItem(
          model: data[index],
        );
      },
      dataRequester: _viewModel.dataRequester,
      initRequester: _viewModel.initRequester,
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
