import 'package:King_Buy/src/presentation/login/login.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';
import 'login_navigation.dart';

class LoginNavigationScreen extends StatefulWidget {
  final String router;

  const LoginNavigationScreen({Key key, this.router}) : super(key: key);

  @override
  _LoginNavigationScreenState createState() => _LoginNavigationScreenState();
}

class _LoginNavigationScreenState extends State<LoginNavigationScreen> {
  LoginNavigationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<LoginNavigationViewModel>(
      viewModel: LoginNavigationViewModel(router: widget.router),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? false;
                return WidgetLoadingPage(
                  isLoading,
                  page: _buildScreen(),
                );
              },
            ),
          ),
        );
      },
    );
  }

  _buildScreen() {
    switch (_viewModel.state) {
      case LoginState.login:
        return LoginScreen(
          navigationViewModel: _viewModel,
        );
      case LoginState.register:
        return RegisterScreen(
          navigationViewModel: _viewModel,
        );
      case LoginState.forgotPassword:
        return ForgotPassScreen(
          back: () => _viewModel.changeState(LoginState.login),
        );
      default:
        return Container();
    }
  }
}
