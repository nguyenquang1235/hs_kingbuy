import 'package:King_Buy/src/presentation/base/base.dart';

enum LoginState{
  login,
  register,
  forgotPassword
}
class LoginNavigationViewModel extends BaseViewModel {
  LoginState state;
  final String router;

  LoginNavigationViewModel({this.router});

  init() async {
    changeState(LoginState.login);
  }

  changeState(LoginState state) async {
    setLoading(true);
    await Future.delayed(const Duration(milliseconds: 500));
    this.state = state;
    notifyListeners();
    setLoading(false);
  }
}
