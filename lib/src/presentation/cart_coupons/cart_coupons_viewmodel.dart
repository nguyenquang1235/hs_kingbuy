import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class CartCouponViewModel extends BaseViewModel {
  final double minPrice;
  CouponModel choose;
  CouponApplication couponApplication;

  CartCouponViewModel(this.minPrice);

  init() async {
    setLoading(true);
    couponApplication = await getCouponApplication();
    setLoading(false);
  }

  Future<CouponApplication> getCouponApplication() async {
    try {
      NetworkState<CouponApplication> networkState =
          await couponRepository.getMyCoupon();
      return networkState.data;
    } catch (e) {
      print(e);
      return null;
    }
  }

  void chooseCoupon(CouponModel couponModel) {
    choose = couponModel;
    print(choose.invoiceTotal);
    notifyListeners();
  }

  void confirmCoupon() async {
    if (minPrice >= choose.invoiceTotal) {
      Toast.show(AppUtils.translate("apply_coupon", context), context);
      await Future.delayed(const Duration(milliseconds: 500));
      Navigator.pop(context, choose);
    } else {
      Toast.show(AppUtils.translate("not_minimum_total", context), context,
          duration: Toast.LENGTH_LONG);
    }
  }
}
