import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../presentation.dart';

class CartCouponsScreen extends StatefulWidget {
  final double minPrice;
  const CartCouponsScreen({Key key, this.minPrice}) : super(key: key);

  @override
  _CartCouponsScreenState createState() => _CartCouponsScreenState();
}

class _CartCouponsScreenState extends State<CartCouponsScreen>
    with ResponsiveWidget {
  CartCouponViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CartCouponViewModel>(
      viewModel: CartCouponViewModel(widget.minPrice),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
            child: Scaffold(
          body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? true;
                return WidgetLoadingPage(
                  isLoading,
                  page: Column(
                    children: [
                      _buildAppbar(),
                      Expanded(child: buildUi(context: context))
                    ],
                  ),
                );
              }),
        ));
      },
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "coupon",
      colorButton: Colors.white,
      supWidget: _buildButtonSave(),
    );
  }

  Widget _buildButtonSave() {
    bool isConfirm = _viewModel.choose != null ?? false;
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(100)),
      elevation: 2,
      color: (isConfirm) ? Colors.red : Colors.grey,
      child: Container(
        height: 30,
        child: WidgetButtonGradient(
          // padding: EdgeInsets.fromLTRB(30, 12, 30, 12),
          colorStart: (isConfirm) ? Colors.red : Colors.grey,
          colorEnd: (isConfirm) ? Colors.red : Colors.grey,
          title: "done",
          action: () {
            if(isConfirm){
              _viewModel.confirmCoupon();
            }
          },
        ),
      ),
    );
  }

  Widget _buildScreen() {
    return Container(
      child: _buildListData(),
    );
  }

  Widget _buildListData() {
    var data = _viewModel.couponApplication?.nonUse ?? [];
    return data.length > 0
        ? ListView(
            physics: BouncingScrollPhysics(),
            padding: const EdgeInsets.all(16),
            children: List.generate(
                data.length,
                (index) => _buildCouponItem(data[index],
                    isLast: data[index] != data.last)),
          )
        : _buildNoneData();
  }

  Widget _buildCouponItem(CouponModel model, {bool isLast = false}) {
    return GestureDetector(
      onTap: () => _viewModel.chooseCoupon(model),
      child: Container(
        color: Colors.transparent,
        margin: const EdgeInsets.all(4),
        child: Column(
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                WidgetImageNetwork(
                  url: model.imageSource,
                  height: Get.width * 0.25,
                  width: Get.width * 0.3,
                  fit: BoxFit.fill,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          constraints: BoxConstraints(
                            maxHeight: Get.width * 0.2,
                          ),
                          child: Text(
                            model.name,
                            style: AppStyles.DEFAULT_MEDIUM_BOLD,
                            textAlign: TextAlign.start,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "${AppUtils.translate("expiry_date", context)}: ${AppUtils.convertString2String(model.expiresAt, outputFormat: "dd/MM/yyyy", inputFormat: "yyyy-MM-dd")}",
                          style: AppStyles.DEFAULT_MEDIUM,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: 30,
                  height: 30,
                  child: Radio<CouponModel>(
                    value: model,
                    groupValue: _viewModel.choose,
                    onChanged: (model) => _viewModel.chooseCoupon(model),
                    activeColor: Colors.red,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            if (isLast)
              Divider(
                thickness: 1,
              ),
          ],
        ),
      ),
    );
  }

  Widget _buildNoneData() {
    return Container(
      width: Get.width,
      height: Get.height,
      child: Center(
        child: Text(AppUtils.translate("no_data", context)),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
