import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:toast/toast.dart';

class ProductDetailsViewModel extends BaseViewModel {
  final int productId;
  CartDataBase _cartDataBase;
  ProductDetailsViewModel(this.productId);
  init() async {
    getDetails(productId);
    _cartDataBase = CartDataBase.instance();
  }

  Future<List<AddressModel>> getAddress() async {
    try {
      return (await addressRepository.getAllAddress()).data ?? null;
    } catch (e) {
      print(e);
      return null;
    }
  }

  void getDetails(int productId) async {
    try {
      await productRepository.getSingleProduct(productId: productId);
    } catch (e) {
      print(e);
    }
  }

  Future<List<ProductModel>> getRelatedData(int offset) async {
    NetworkState<List<ProductModel>> netWorkState =
        await productRepository.getRelatedProduct(
      productId: productId,
      offset: offset,
    );
    if (netWorkState.isSuccess && netWorkState.response.isSuccess) {
      return netWorkState.data;
    }
    return [];
  }

  Future<List<ProductModel>> initData() async {
    return await getRelatedData(0);
  }

  Future<List<ProductModel>> requestData(int offset) async {
    return await getRelatedData(offset);
  }

  Future addProductInCart(ProductModel model) async {
    CartItem cartItem = await _cartDataBase.getCartItem("product_id", model.id);
    if (cartItem == null) {
      //Thêm mới
      CartItem newCartItem = CartItem(productId: model.id, qty: 1);
      int id = await _cartDataBase.insertCartItem(newCartItem);
      checkId(id);
    } else {
      CartItem updateCartItem = cartItem.copyWith(qty: cartItem.qty + 1);
      print(updateCartItem.qty);
      int id = await _cartDataBase.updateCartItem(
        updateCartItem,
      );
      checkId(id);
    }
  }

  void checkId(int id) {
    if (id > 0) {
      Toast.show(AppUtils.translate("add_cart_success", context), context);
    } else {
      Toast.show(AppUtils.translate("add_cart_fail", context), context);
    }
  }
}
