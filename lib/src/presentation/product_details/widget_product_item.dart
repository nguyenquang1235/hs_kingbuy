import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/model/model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import '../presentation.dart';

class WidgetProductItem extends StatelessWidget with ResponsiveWidget {
  final ProductModel model;

  const WidgetProductItem({Key key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return buildUi(context: context);
  }

  Widget _buildProductItem(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, Routers.product_details,
          arguments: model),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.transparent,
        ),
        margin: EdgeInsets.all(8),
        width: Get.width * 0.45,
        child: PhysicalModel(
          elevation: 2,
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildItemImage(model.imageSource),
                _buildItemDetails(model),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildItemImage(String url) {
    return RepaintBoundary(
      child: ClipRRect(
        borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
        child: WidgetImageNetwork(
          width: Get.width * 0.45,
          height: Get.width * 0.45,
          fit: BoxFit.fill,
          url: url,
          isAvatar: false,
        ),
      ),
    );
  }

  Widget _buildItemDetails(ProductModel model) {
    return RepaintBoundary(
      child: Container(
        padding: const EdgeInsets.all(4),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              model.name + "\n",
              style: AppStyles.DEFAULT_SMALL_BOLD,
              maxLines: 2,
              textAlign: TextAlign.left,
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              model.brandName,
              style: AppStyles.DEFAULT_SMALL.copyWith(color: Colors.blue),
              maxLines: 2,
              textAlign: TextAlign.left,
            ),
            SizedBox(
              height: 5,
            ),
            SmoothStarRating(
                allowHalfRating: false,
                starCount: 5,
                rating: double.parse(model.star.toString()),
                size: 13,
                isReadOnly: true,
                filledIconData: Icons.star,
                halfFilledIconData: Icons.star_half,
                color: Colors.orange,
                borderColor: Colors.orange,
                spacing: 0.0),
            SizedBox(
              height: 5,
            ),
            _buildRowPrice(model.salePrice, model.price),
            SizedBox(
              height: 5,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRowPrice(int cost, int reduce) {
    return Container(
      width: Get.width * 0.45,
      height: AppStyles.DEFAULT_SMALL_BOLD.fontSize,
      child: FittedBox(
        fit: BoxFit.cover,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildMoneyPrice(cost,
                styleInput: AppStyles.DEFAULT_MEDIUM_BOLD
                    .copyWith(color: Colors.redAccent)),
            SizedBox(
              width: 5,
            ),
            _buildMoneyPrice(reduce,
                styleInput: AppStyles.DEFAULT_SMALL_BOLD.copyWith(
                    decoration: TextDecoration.lineThrough,
                    color: Colors.grey)),
          ],
        ),
      ),
    );
  }

  Widget _buildMoneyPrice(int price, {TextStyle styleInput}) {
    TextStyle style = styleInput ?? AppStyles.DEFAULT_MEDIUM_BOLD;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '${NumberFormat(",###", "vi").format(price)}',
          style: style,
        ),
        Text("đ", style: style.copyWith(fontSize: style.fontSize - 3)),
      ],
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildProductItem(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildProductItem(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildProductItem(context);
  }
}
