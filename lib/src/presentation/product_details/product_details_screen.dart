import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../presentation.dart';

class ProductDetailsScreen extends StatefulWidget {
  final ProductModel model;

  const ProductDetailsScreen({Key key, this.model}) : super(key: key);

  @override
  _ProductDetailsScreenState createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen>
    with ResponsiveWidget {
  ProductDetailsViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProductDetailsViewModel>(
      viewModel: ProductDetailsViewModel(widget.model.id),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? false;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                        _buildFooterBar(),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "product_details",
      colorButton: Colors.white,
    );
  }

  Widget _buildFooterBar() {
    return Container(
      width: Get.width,
      height: Get.width * 0.15,
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            color: Colors.grey[200],
            spreadRadius: 0.5,
            blurRadius: 0.5,
            offset: Offset(1, -1))
      ]),
      child: Row(
        children: [
          Expanded(child: SizedBox()),
          GestureDetector(
            onTap: () => _viewModel.addProductInCart(widget.model),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              color: Colors.red,
              child: Center(
                child: FittedBox(
                  fit: BoxFit.cover,
                  child: Text(
                    AppUtils.translate("buy_now", context),
                    style: AppStyles.DEFAULT_LARGE_BOLD
                        .copyWith(color: Colors.white),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildScreen() {
    return Container(
      color: Colors.grey[200],
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            _buildImageAndPrice(),
            GiftIncluded(
              gifts: widget.model.gifts,
            ),
            KingBuySystem(
              data: _viewModel.getAddress(),
            ),
            ProductInformation(
              data: widget.model.content ?? "",
            ),
            Specifications(
              data: widget.model.specifications ?? "",
            ),
            BrandInfo(
              data: widget.model.brandInfo ?? "",
            ),
            RatingProduct(
              productId: widget.model.id,
            ),
            _buildRelatedProducts(),
          ],
        ),
      ),
    );
  }

  Widget _buildRelatedProducts() {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.only(bottom: 8),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("related_product", context),
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          SizedBox(
            height: 10,
          ),
          WidgetLoadMoreWrapVertical<ProductModel>.build(
            direction: Axis.horizontal,
            dataRequester: _viewModel.requestData,
            initRequester: _viewModel.initData,
            itemBuilder: (data, context, index) => WidgetProductItem(
              model: data[index],
            ),
            loadingColor: Colors.red,
          ),
        ],
      ),
    );
  }

  Widget _buildImageAndPrice() {
    return RepaintBoundary(
      child: Container(
        padding: const EdgeInsets.all(8),
        margin: EdgeInsets.only(bottom: 10),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: WidgetImageNetwork(
                url: widget.model.imageSource,
                isAvatar: false,
                width: Get.width,
              ),
            ),
            Text(
              widget.model.name + "\n",
              maxLines: 2,
              style: AppStyles.DEFAULT_LARGE_BOLD,
            ),
            SizedBox(
              height: 10,
            ),
            _buildRowPrice(widget.model.salePrice, widget.model.price)
          ],
        ),
      ),
    );
  }

  Widget _buildRowPrice(int cost, int reduce) {
    return RepaintBoundary(
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildMoneyPrice(cost,
                styleInput: AppStyles.DEFAULT_MEDIUM_BOLD
                    .copyWith(color: Colors.redAccent)),
            SizedBox(
              width: 15,
            ),
            _buildMoneyPrice(reduce,
                styleInput: AppStyles.DEFAULT_SMALL_BOLD.copyWith(
                    decoration: TextDecoration.lineThrough,
                    color: Colors.grey)),
          ],
        ),
      ),
    );
  }

  Widget _buildMoneyPrice(int price, {TextStyle styleInput}) {
    TextStyle style = styleInput ?? AppStyles.DEFAULT_MEDIUM_BOLD;
    return RepaintBoundary(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${NumberFormat(",###", "vi").format(price)}',
            style: style,
          ),
          Text("đ", style: style.copyWith(fontSize: style.fontSize - 3)),
        ],
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}

class GiftIncluded extends StatelessWidget {
  final List<Gifts> gifts;

  const GiftIncluded({Key key, this.gifts}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.only(bottom: 8),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                AppImages.icGiftColor,
                width: Get.width * 0.12,
                fit: BoxFit.fill,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                AppUtils.translate("gift_include", context) + ":",
                style:
                    AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.blue),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          _buildListItem()
        ],
      ),
    );
  }

  Widget _buildListItem() {
    return Column(
      children: List.generate(
            gifts?.length ?? 0,
            (index) => _buildGiftItem(gifts[index] ?? null),
          ) +
          [if (gifts?.length <= 0) Text("Không có")],
    );
  }

  Widget _buildGiftItem(Gifts gift) {
    return Container(
      width: Get.width,
      margin: EdgeInsets.all(4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PhysicalModel(
            elevation: 2,
            color: Colors.white,
            child: Container(
              margin: const EdgeInsets.all(2),
              padding: const EdgeInsets.all(2),
              width: Get.width * 0.12,
              height: Get.width * 0.12,
              child: WidgetImageNetwork(
                url: gift.imageSource ?? "",
                isAvatar: false,
                fit: BoxFit.fill,
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  gift.name,
                  maxLines: 2,
                  style: AppStyles.DEFAULT_MEDIUM,
                ),
                _buildMoneyPrice(gift.price,
                    styleInput: AppStyles.DEFAULT_MEDIUM_BOLD
                        .copyWith(color: Colors.red))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildMoneyPrice(int price, {TextStyle styleInput}) {
    TextStyle style = styleInput ?? AppStyles.DEFAULT_MEDIUM_BOLD;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '${NumberFormat(",###", "vi").format(price)}',
          style: style,
        ),
        Text("đ", style: style.copyWith(fontSize: style.fontSize - 3)),
      ],
    );
  }
}

class KingBuySystem extends StatelessWidget {
  final Future<List<AddressModel>> data;

  const KingBuySystem({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<AddressModel>>(
      future: data,
      builder: (context, snapshot) {
        if (snapshot.hasData)
          return Container(
            color: Colors.white,
            margin: const EdgeInsets.only(bottom: 8),
            padding: const EdgeInsets.all(8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppUtils.translate("kingbuy_system", context),
                  style: AppStyles.DEFAULT_LARGE_BOLD,
                ),
                SizedBox(
                  height: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(snapshot.data.length,
                      (index) => _buildAddressItem(snapshot.data[index])),
                ),
              ],
            ),
          );
        return SizedBox();
      },
    );
  }

  Widget _buildAddressItem(AddressModel addressModel) {
    return Container(
      margin: const EdgeInsets.all(4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                AppImages.icPinRed,
                width: Get.width * 0.064,
                height: Get.width * 0.064,
                fit: BoxFit.fill,
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                  child: Text(
                addressModel.address,
                style:
                    AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.red),
                maxLines: 2,
              ))
            ],
          ),
        ],
      ),
    );
  }
}

class ProductInformation extends StatefulWidget {
  final String data;

  const ProductInformation({Key key, this.data}) : super(key: key);

  @override
  _ProductInformationState createState() => _ProductInformationState();
}

class _ProductInformationState extends State<ProductInformation> {
  double height = Get.width;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: const EdgeInsets.only(bottom: 8),
      padding: const EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("product_information", context),
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          SizedBox(
            height: 10,
          ),
          SizedBox(
            height: height,
            child: Stack(
              children: [
                ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    Html(
                      data: widget.data,
                    ),
                  ],
                ),
                if (height != null)
                  Align(
                      alignment: Alignment(0, 1),
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Colors.white.withOpacity(0.05),
                            Colors.white.withOpacity(0.99),
                          ],
                        )),
                        width: Get.width,
                        height: 75,
                      ))
              ],
            ),
          ),
          _buildLoadMoreButton(),
        ],
      ),
    );
  }

  _buildLoadMoreButton() {
    return SizedBox(
      height: 50,
      child: InkWell(
        onTap: () {
          setState(() {
            height != null ? height = null : height = Get.width;
          });
        },
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              AppUtils.translate(
                  height != null ? "see_all" : "zoom_out", context),
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
            Icon(Icons.arrow_forward_ios)
          ],
        ),
      ),
    );
  }
}

class Specifications extends StatelessWidget {
  final String data;

  const Specifications({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.only(bottom: 8),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("specifications", context),
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          SizedBox(
            height: 10,
          ),
          Html(
            data: data,
          )
        ],
      ),
    );
  }
}

class BrandInfo extends StatelessWidget {
  final String data;
  const BrandInfo({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.only(bottom: 8),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppUtils.translate("brands_information", context),
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          SizedBox(
            height: 10,
          ),
          Html(
            data: data,
          )
        ],
      ),
    );
  }
}
