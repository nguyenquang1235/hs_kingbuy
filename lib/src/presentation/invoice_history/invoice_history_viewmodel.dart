import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/invoice_model.dart';
import 'package:King_Buy/src/resource/resource.dart';
import 'package:flutter/material.dart';

class InvoiceHistoryViewModel extends BaseViewModel {
  int invoiceStatus;
  ScrollController scrollController = ScrollController();
  double widthItem = 120;
  final GlobalKey<WidgetLoadMoreVerticalState> keyListView = GlobalKey();

  init() async {
    setLoading(true);
    invoiceStatus = -1;
    await Future.delayed(const Duration(milliseconds: 2000));
    setLoading(false);
  }

  changeInvoiceStatus(int status) {
    invoiceStatus = status;
    scrollTo(0);
    notifyListeners();
  }

  changeInvoiceStatusRight() {
    if (invoiceStatus < 5) {
      invoiceStatus++;
      scrollTo(0);
    } else {
      invoiceStatus = -1;
      scrollTo(-1);
    }
    notifyListeners();
  }

  changeInvoiceStatusLeft() {
    if (invoiceStatus > -1) {
      invoiceStatus--;
      scrollTo(0);
    } else {
      invoiceStatus = 5;
      scrollTo(1);
    }
    notifyListeners();
  }

  scrollTo(int type) {
    try {
      switch (type) {
        case -1:
          scrollController.animateTo(scrollController.position.minScrollExtent,
              duration: const Duration(milliseconds: 1000),
              curve: Curves.fastLinearToSlowEaseIn);
          break;
        case 0:
          scrollController.animateTo((invoiceStatus + 1) * widthItem,
              duration: const Duration(milliseconds: 1000),
              curve: Curves.fastLinearToSlowEaseIn);
          break;

        case 1:
          scrollController.animateTo(scrollController.position.maxScrollExtent,
              duration: const Duration(milliseconds: 1000),
              curve: Curves.fastLinearToSlowEaseIn);
          break;
      }
      keyListView.currentState.onRefresh();
    } catch (e) {
      throw Exception("Error on server");
    }
  }

  onDragBody(DragEndDetails details) {
    if (details.primaryVelocity > 0) {
      changeInvoiceStatusLeft();
    } else if (details.primaryVelocity < 0) {
      changeInvoiceStatusRight();
    }
  }

  //Get data

  Future<List<InvoiceModel>> initRequester() async {
    return await getInvoiceHistory(0);
  }

  Future<List<InvoiceModel>> dataRequester(int offset) async {
    return await getInvoiceHistory(offset);
  }

  getInvoiceHistory(int offset) async {
    List<InvoiceModel> model = [];
    NetworkState<List<InvoiceModel>> networkState = await invoiceRepository
        .getInvoiceHistory(offset: offset, filter: invoiceStatus);
    if (networkState.isSuccess && networkState.data != null)
      model = networkState.data;
    await Future.delayed(Duration(milliseconds: 500));
    return model;
  }
}
