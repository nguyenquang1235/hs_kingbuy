import 'package:King_Buy/src/configs/configs.dart';
import 'package:King_Buy/src/presentation/presentation.dart';
import 'package:King_Buy/src/resource/model/invoice_model.dart';
import 'package:King_Buy/src/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class InvoiceHistoryScreen extends StatefulWidget {
  @override
  _InvoiceHistoryScreenState createState() => _InvoiceHistoryScreenState();
}

class _InvoiceHistoryScreenState extends State<InvoiceHistoryScreen>
    with ResponsiveWidget, SingleTickerProviderStateMixin {
  InvoiceHistoryViewModel _viewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<InvoiceHistoryViewModel>(
      viewModel: InvoiceHistoryViewModel(),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel..init();
      },
      builder: (context, viewModel, child) {
        return SafeArea(
          child: Scaffold(
            body: StreamBuilder<bool>(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                bool isLoading = snapshot.data ?? true;
                return WidgetLoadingPage(
                  isLoading,
                  page: Container(
                    child: Column(
                      children: [
                        _buildAppbar(),
                        Expanded(child: buildUi(context: context)),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  _buildAppbar() {
    return WidgetAppBar(
      keyTitle: "invoice_history",
      colorButton: Colors.white,
    );
  }

  _buildScreen() {
    return Column(
      children: [
        _buildHeaderScreen(),
        Expanded(child: _buildBodyScreen()),
      ],
    );
  }

  _buildHeaderScreen() {
    List<Widget> listInvoiceStatusItem = [];
    for (int i = -1; i < 6; i++) {
      listInvoiceStatusItem.add(_buildInvoiceStatusItem(i));
    }

    return Column(
      children: [
        SingleChildScrollView(
          controller: _viewModel.scrollController,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.only(left: 24, right: 50),
          child: Row(
            children: listInvoiceStatusItem,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Divider(
            color: Colors.grey[200],
            thickness: 2,
          ),
        )
      ],
    );
  }

  _buildInvoiceStatusItem(int invoiceStatus) {
    bool isSelect = invoiceStatus == _viewModel.invoiceStatus;
    return GestureDetector(
      onTap: () {
        _viewModel.changeInvoiceStatus(invoiceStatus);
      },
      child: Container(
        constraints: BoxConstraints(
            maxWidth: _viewModel.widthItem,
            minWidth: _viewModel.widthItem,
            maxHeight: 70),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        margin: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: PhysicalModel(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: isSelect ? Colors.blue : Colors.white,
          elevation: 4,
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Center(
              child: Text(
                AppUtils.translate("invoice_status_$invoiceStatus", context),
                textAlign: TextAlign.center,
                maxLines: 2,
                style: AppStyles.DEFAULT_MEDIUM
                    .copyWith(color: isSelect ? Colors.white : Colors.black),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _buildBodyScreen() {
    return GestureDetector(
      onHorizontalDragEnd: _viewModel.onDragBody,
      child: Container(
        padding: EdgeInsets.fromLTRB(24, 0, 16, 0),
        width: Get.width,
        height: Get.height,
        color: Colors.transparent,
        child: WidgetLoadMoreVertical<InvoiceModel>.build(
          itemBuilder: _buildInvoiceHistoryItem,
          dataRequester: _viewModel.dataRequester,
          initRequester: _viewModel.initRequester,
          key: _viewModel.keyListView,
          loadingColor: Colors.redAccent,
        ),
      ),
    );
  }


  Widget _buildInvoiceHistoryItem(
      List<dynamic> data, BuildContext context, int index) {
    List<InvoiceModel> model = data;
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, Routers.invoice_details,
            arguments: data[index]);
      },
      child: Container(
        margin: const EdgeInsets.only(top: 5, bottom: 5),
        color: Colors.transparent,
        child: Row(
          children: [
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    AppUtils.convertString2String(model[index].createdAt,
                        outputFormat: "hh:mm - EEEE dd/MM/yyyy",
                        inputFormat: "hh:mm:ss yyyy-MM-dd"),
                    style: AppStyles.DEFAULT_MEDIUM_BOLD),
                SizedBox(
                  height: 5,
                ),
                Text(
                    AppUtils.translate(
                        "invoice_status_${model[index].status}", context),
                    style:
                        AppStyles.DEFAULT_MEDIUM.copyWith(color: Colors.grey)),
                SizedBox(
                  height: 5,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${NumberFormat(",###", "vi").format(model[index].total)}',
                      style: AppStyles.DEFAULT_LARGE_BOLD
                          .copyWith(color: Colors.redAccent),
                    ),
                    Text("đ",
                        style: AppStyles.DEFAULT_SMALL_BOLD
                            .copyWith(color: Colors.redAccent)),
                  ],
                ),
                if (index != data.indexOf(data.last)) Divider()
              ],
            )),
            Icon(
              Icons.arrow_forward_ios,
              color: Colors.grey,
              size: 20,
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildScreen();
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildScreen();
  }

  @override
  Widget buildTablet(BuildContext context) {
    // TODO: implement buildTablet
    return _buildScreen();
  }
}
